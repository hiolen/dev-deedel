@extends('layouts.app')

@section('content')

<div class="container">
    <h2>Booklulu</h2>
    <p></p>
    <p>Domain: <a href="">booklulu.com</a></p>
    <p>Social: <a href="">Facebook</a>, <a href="">Instagram</a>, <a href="">Pinterest</a>, </p>

    <h2>Pricing</h2>
    <ul>
    	<li>Adding a book (10 silver coins - free members, 5 silver coins - pro member, 3 silver coin - publisher)</li>
    	<li>Adding a book preview (250 gold coins - pro member, 150 gold coins - publisher)</li>
    	<li>aprroving a review (3 gold coins - pro member, 1 gold coin - publisher)</li>
    </ul>

    <h3>Membership</h3>
    <h3>Booklulu Pro</h3>
    <ul class="nav">
        <li>
        	<p>$8.88/year</p>
           	<p>if your an author with lots of books</p>
            <p>Get FREE 10 Points</p>
            <p>Get FREE 1 Pink Coin</p>
            <p>Get FREE 10 goldcoins icon</p>
            <p>Get FREE 20 silvercoins icon</p>
            <a href="#">Become a Pro</a>
        </li>
    </ul>
    <h3>Booklulu Publisher</h3>
    <ul class="nav">
        <li>
        	<p>$18.88/year</p>
            <p>if you are a publishing company and will add books from outher authors</p>
            <p>Unlimitted authors (adding books for authors, manage reviews for authors, creating book preview for authors,)</p>
            <p>Get FREE 20 Points</p>
            <p>Get FREE 2 Pink Coins</p>
            <a href="">Sign up for 20 goldcoins icon</a>
            <a href="">Sign up for 50 silvercoins icon</a>
            <a href="#">Become a Publisher</a>
        </li>
    </ul>

    <h2>Earn Points</h2>
    <ul>
    	<li>sharing a book (1 point/share)</li>
    	<li>adding a book (5 points/book)</li>
    	<li>adding a book preview (25 points/book)</li>
    	<li>refer a friend (10 points/new deedel user) (5 point/existing deedel user)</li>
    	<li>membership upgrade (upto 20 points/upgrade)</li>
    	<li>adding book review (10 points/approved review)</li>
    	<li>receiving gift (5 points/gift)</li>
    	<li>buying in market (5 points/item)</li>
    </ul>
    <h2>Redeem Points & Rewards</h2>
    <ul>
    	<li>how to redeem points</li>
    	<li>Rewards</li>
    </ul>
</div>
@endsection