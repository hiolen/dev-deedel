@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Booklulu Pricing</h2>
    <ul>
    	<li><a href="{{ url('/pricing/booklulu/indie') }}">Indie</a></li>
    	<li><a href="{{ url('/pricing/booklulu/professional') }}">Professional</a></li>
    	<li><a href="{{ url('/pricing/booklulu/publisher') }}">Publisher</a></li>
    </ul>
</div>
@endsection
