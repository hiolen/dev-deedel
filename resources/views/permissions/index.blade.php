@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Permission Management</h2>
	        </div>
	        <div class="pull-right">
	            <button id="btn-add" name="btn-add" class="btn btn-success btn-xs">Create New Permission <i class="fa fa-plus"></i></button>
	        </div>
	    </div>
	</div>
	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th width="280px">Action</th>
			</tr>
		</thead>
		<tbody id="permission-list" name="permission-list">
			@foreach ($permissions as $key => $permission)
			<tr class="item{{ $permission->id }}">
				<td>{{ $permission->display_name }}</td>
				<td>{{ $permission->description }}</td>
				<td>
					<button class="btn btn-info btn-xs btn-detail open-modal" value="{{ $permission->id }}"><i class="fa fa-files-o"></i> Edit</button>
					<a href="#modalDelPermission" data-toggle="modal" data-id="{{ $permission->id }}" data-name="{{ $permission->name }}" class="btn-del-permission btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{!! $permissions->render() !!}
</div>
@endsection

@section('modal')
	<!-- Add/Edit Permission Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="modalPermissionLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalPermissionLabel">Permission <span class="permission-title">Editor</span></h4>
                </div>
                <form action="#" method="POST" id="formPermissions" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" name="name" placeholder="Name" class="form-control" id="name" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Display Name:</strong>
                            <input type="text" name="display_name" placeholder="Display Name" class="form-control" id="display_name" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            <textarea name="description" placelholder="Description" class="form-control" id="description" value=""></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="permission_id" name="permission_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Permission Modal -->
    <div class="modal fade modalDelPermission" tabindex="-1" role="dialog" id="modalDelPermission" aria-labelledby="modalDelPermissionLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelPermissionLabel">Delete <span class="permission-del-title">Permission</span></h4>
                </div>
                <form action="#" method="POST" id="delPermissionForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer')
	<!-- Jquery Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

    <!-- script for permission -->
    <script type="text/javascript">
    	$(function() {
    		//display modal form for creating new permission
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#formPermissions').trigger("reset");
                $('#myModal').modal('show');
            });

            // Edit User
            $('.open-modal').click(function() {
                var permission_id = $(this).val();

                $.get('permission' + '/' + permission_id, function (data) {
                    //success data
                    console.log(data);
                    $('#permission_id').val(data.id);
                    $('#name').val(data.name);
                    $('#display_name').val(data.display_name);
                    $('#description').val(data.display_name);
                    $('#btn-save').val("update");

                    $('#myModal').modal('show');
                }); 
            });

            //create new permission / update existing permission
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var permission_id = $('#permission_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'permission';

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + permission_id;
                }

           		if ($('#formPermissions').valid()) {
	                $.ajax({
	                    type: type,
	                    url: my_url,
	                    data: $('#formPermissions').serialize(),
	                    dataType: 'json',
	                    success: function (data) {
	                        console.log(data);

	                        var permission = '<tr class="item' + data.permission.id + '"><td>' + data.permission.display_name + '</td><td>' + data.permission.description + '</td>';
	                        permission += '<td><button class="btn btn-info btn-xs btn-detail open-modal" value="' + data.permission.id + '"><i class="fa fa-files-o"></i> Edit</button>&nbsp;';
	                        permission += '<a href="#modalDelpermission" data-toggle="modal" data-id="' + data.permission.id + '" data-name="' + data.permission.name + '" class="btn-del-permission btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a></td></tr>';

	                        if (state == "add"){ //if permission added a new record
	                            $('#permission-list').prepend(permission);
	                        }else{ //if permission updated an existing record
	                            $(".item" + permission_id).replaceWith( permission );
	                        }

	                        $('#formPermissions').trigger("reset");

	                        $('#myModal').modal('hide')
	                    },
	                    error: function (data) {
	                        console.log('Error:', data);
	                    }
	                });
	            }else
                    return false;               

            });

            // DELETE Permission Ajax
            $('.btn-del-permission').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);
                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: 'permission/delete',
                    data: data,
                    success: function(data) {
                        console.log(data);
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelPermission').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

			// Validate Add Permission Form
            $('#formPermissions').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    display_name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
    	});
    </script>
@endsection