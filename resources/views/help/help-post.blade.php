@extends('layouts.app')

@section('content')
<div class="container">
    <h1>{{$category->name}}</h1>
    <form>    	
		<div class="input-group input-group-lg">
		  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
		  <input type="text" class="form-control" placeholder="Search Topics" aria-describedby="sizing-addon1">
		</div>
    </form>

    <div class="row">
    	<div class="col-xs-12 col-sm-4">
    		<h3>Related post from this category</h3>
		    <ul class="nav">
		    	@foreach($category->help as $cat)
                    <li><a href="/help/booklulu/{{$cat->helpCategoryID}}/{{$cat->id}}">{{ $cat->title }}</a></li>
                @endforeach 
		    </ul>

    		<h3><em>Booklulu</em> Help Topics</h3>    
    		<p><a href="/websites/booklulu">Visit Booklulu</a></p>		
		    <ul class="nav">
		    	<li><a href="/help/booklulu/category">Category Title</a></li>
		    	<li><a href="/help/booklulu/category">Category Title</a></li>
		    	<li><a href="/help/booklulu/category">Category Title</a></li>
		    	<li><a href="/help/booklulu/category">Category Title</a></li>
		    </ul>
    	</div>
    	<div class="col-xs-12 col-sm-6 col-md-8">
    		<p></p>
    		<ul class="breadcrumb">
    			<li><a href="/help/booklulu"><em>Booklulu</em> Help Topics</a></li>
    			<li><a href="/help/booklulu/{{$category->id}}">{{ $category->name }}</a></li>
		    	<li>{{ $post->title }}</li>
    		</ul>
    		<div>
    			<p>{{ $post->content }}</p>
    		</div>
    		<p>Is this post helpful to you? <a href="#form">
    			<span><i class="fa fa-thumbs-up"></i></span>
    			<span><i class="fa fa-thumbs-down"></i></span>
    		</a></p>
    		<!-- Note: To submit feedback form login is required -->
    		<form id="form">
    			<h4>Please send us your feedback.</h4>
    			<textarea class="form-control"></textarea>
    		</form>
    	</div>
    </div>
</div>
@endsection
