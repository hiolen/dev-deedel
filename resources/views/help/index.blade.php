@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Help Center</h1>
    <form>    	
		<div class="input-group input-group-lg">
		  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
		  <input type="text" class="form-control" placeholder="Search Topics" aria-describedby="sizing-addon1">
		</div>
    </form>

    <div class="row">
    	<div class="col-xs-12 col-sm-6 col-md-4">
    		<h3><em>Booklulu</em> Help Topics</h3>    
    		<p><a href="/websites/booklulu">Visit Booklulu</a></p>		
		    <ul class="nav">
		    	@foreach($helpCategory as $category)
		    		<li><a href="/help/booklulu/{{$category->id}}">{{ $category->name}}</a></li>
		    	@endforeach
		    </ul>
    	</div>
    	<div class="col-xs-12 col-sm-6 col-md-4">
    		<h3><em>Renta</em> Help Topics</h3>
    		<p><a href="/websites/renta">Visit Renta</a></p>	
		    <ul class="nav">
		    	@foreach($helpCategory as $category)
		    		<li><a href="/help/booklulu/category">{{ $category->name}}</a></li>
		    	@endforeach
		    </ul>
    	</div>
    </div>
</div>
@endsection
