@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Help Category Title</h1>
    <form>    	
		<div class="input-group input-group-lg">
		  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
		  <input type="text" class="form-control" placeholder="Search Topics" aria-describedby="sizing-addon1">
		</div>
    </form>

    <div class="row">
    	<div class="col-xs-12 col-sm-4">
    		<h3><em>Booklulu</em> Help Topics</h3>    
    		<p><a href="/websites/booklulu">Visit Booklulu</a></p>		
		    <ul class="nav">
		    	@foreach($helpCategory as $categories)
		    		<li><a href="/help/booklulu/{{$categories->id}}">{{$categories->name}}</a></li>
		    	@endforeach
		    </ul>
    	</div>
    	<div class="col-xs-12 col-sm-6 col-md-4">
    		<p></p>
    		<ul class="breadcrumb">
    			<li><a href="#"><em>Booklulu</em> Help Topics</a></li>
    			<li>{{$category->name}}</li>
    		</ul>
		    <ul class="nav">
		    	{{$category->name}}
		    	
				@foreach($category->help as $cat)
					<li><a href="/help/booklulu/{{$cat->helpCategoryID}}/{{$cat->id}}">{{ $cat->title }}</a></li>
				@endforeach
		    	
		    </ul>
    	</div>
    </div>
</div>
@endsection
