@extends('layouts.app')

@section('content')
<div class="container">
    <h3><em>Website Name</em> Help Topics</h3> 
    <p><a href="/websites/booklulu">Visit Booklulu</a></p>	
    <form>    	
		<div class="input-group input-group-lg">
		  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
		  <input type="text" class="form-control" placeholder="Search (Website name) Topics" aria-describedby="sizing-addon1">
		</div>
    </form>

    <div class="row">
    	<div class="col-xs-12 col-sm-4">   
    		<h4>Help Categories</h4>
		    <ul class="nav">
		    	{{$category->title}}
		    	@foreach($category->help as $categories)
		    		<li><a href="/help/booklulu/{{$categories->id}}">{{$categories->name}}</a></li>
		    	@endforeach
		    </ul>
    	</div>
    	<div class="col-xs-12 col-sm-6 col-md-8">
    		<ul class="breadcrumb">
    			<li><a href="#">Help Center</a></li>
    			<li><em>Booklulu</em> Help Topics</li>
    		</ul>
    		<h3>Popular Topics</h3>
		    <ul class="nav">
		    	@foreach($category->help as $categories)
		    		<li><a href="/help/booklulu/{{$categories->id}}">{{$categories->name}}</a></li>
		    	@endforeach
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    	<li><a href="/help/booklulu/category/slug">Help Post Title</a></li>
		    </ul>
    	</div>
    </div>
</div>
@endsection
