@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        {!! Form::open(['method'=>'GET','url'=>'offices','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
		<a href="{{ url('offices/create') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Add</a>

		<div class="input-group custom-search-form">
		    <input type="text" class="form-control" name="search" placeholder="Search...">
		    <span class="input-group-btn">
		        <button class="btn btn-default-sm" type="submit">
		            <i class="fa fa-search"><!--<span class="hiddenGrammarError" pre="" data-mce-bogus="1"-->i>
		        </button>
		    </span>
		</div>
		{!! Form::close() !!}

        <table class="table table-bordered table-hover" >
            <thead>
                <th>Name</th>
            </thead>
            <tbody>
                @foreach($offices as $office)
                <tr>
                    <td>{{ $office->name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('script')
<script type="text/javascript">
	$(function()
	{
		 $( "#q" ).autocomplete({
		  source: "search/autocomplete",
		  minLength: 3,
		  select: function(event, ui) {
		  	$('#q').val(ui.item.value);
		  }
		});
	});
</script>
@endsection