@extends('network.deedel.inc.app')

@section('content_deedel')
<style type="text/css">
	.line-through{
		text-decoration: line-through;
	}
</style>
<?php

	// $a = 'How are sample here you tex here [video(id=1 type=video heading=title)][/video] sample? [image(id=3 type=image)][/image] text tetx text text ttex [post(id=1 type=post)][/post]';
	// $types = 'post,image,video';
 //    foreach (explode(',',$types) as $ty){
	// 	$start = strpos($a, '['.$ty.'(');
	// 	$end = strpos($a, ')][/'.$ty.']');
	// 	$c_l = strlen($ty)+2;
	// 	if ($start !== false && $end !== false) {
	// 		$start = $start+$c_l;
	// 		$end = $end-$start;
	//         $value = substr($a, $start, $end);
	//         if ($value !== false){
	// 	        foreach (explode(' ',$value) as $pair){
	// 	        	if (strpos($pair, '=') !== false) {
	// 		            list ($k,$v) = explode ('=',$pair);
	// 		            $pairs[$k] = $v;
	// 					echo '<h3> - '.$k." = ".$pairs[$k].'</h3>';
	// 	        	}
	// 			}
	// 		}
	// 	}
	// 	echo "<p>&nbsp;</p>";
 //    }

?>
<div class="container">
    @if (session()->has('warning_message'))
        <div class="alert alert-warning">
            {{ session()->get('warning_message') }}
        </div>
    @endif

    @if (session()->has('error_message'))
        <div class="alert alert-danger">
            {{ session()->get('error_message') }}
        </div>
    @endif
    
    <h2>Coins</h2>
    <p>What are coins used for?</p>
    <p>All purchase for products and services are paid using coins. You need to purchase coins in order to gte the products and services you desire.</p>
    <section class="section" id="">
	    <h3>Gold Coins</h3>
	    <p>When to use gold coins?</p>
	    <p>Gold Coins can only be used to purchase products</p>
		<div class="row">
			<strong class="col-xs-3">Coins</strong>
			<strong class="col-xs-5">Price</strong>
			<strong class="col-xs-4">Buy Now</strong>
		</div>
	    <div class="row">
	        <div class="col-xs-3">
	        	100 Gold
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(100,0.05) }}</span>
	        	<strong>$4.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="100" data-name="Gold Coin" data-type="gold" data-category="coins" data-price="4.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	200 Gold
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(200,0.05) }}</span>
	        	<strong>$8.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="200" data-name="Gold Coin" data-type="gold" data-category="coins" data-price="8.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	500 Gold
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(500,0.05) }}</span>
	        	<strong>$19.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" id="addToCartGold10" data-link="{{ url('/cart') }}" 
	            data-token="{{ csrf_token() }}" data-value="500" data-name="Gold Coin" data-type="gold" data-category="coins" data-price="19.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	1000 Gold
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(1000,0.05) }}</span>
	        	<strong>$36.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="1000" data-name="Gold Coin" data-type="gold" data-category="coins" data-price="36.88">Add To Cart</button>
	       	</div>
	    </div>
	</section>
    <section class="section" id="">
	    <h3>Silver Coins</h3>
	    <p>When to use silver coins?</p>
	    <p>Silver Coins can only be used to purchase products</p>
		<div class="row">
			<strong class="col-xs-3">Coins</strong>
			<strong class="col-xs-5">Price</strong>
			<strong class="col-xs-4">Buy Now</strong>
		</div>
	    <div class="row">
	        <div class="col-xs-3">
	        	500 Silver
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(500,0.01) }}</span>
	        	<strong>$4.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="500" data-name="Silver Coin" data-type="silver" data-category="coins" data-price="4.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	1000 Silver
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(1000,0.01) }}</span>
	        	<strong>$8.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="1000" data-name="Silver Coin" data-type="silver" data-category="coins" data-price="8.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	2500 Silver
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(2500,0.01) }}</span>
	        	<strong>$19.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="2500" data-name="Silver Coin" data-type="silver" data-category="coins" data-price="19.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	5000 Silver
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(5000,0.01) }}</span>
	        	<strong>$36.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="5000" data-name="Silver Coin" data-type="silver" data-category="coins" data-price="36.88">Add To Cart</button>
	       	</div>
	    </div>
	</section>
    <section class="section" id="">
	    <h3>Green Coins</h3>
	    <p>When to use green coins?</p>
	    <p>Green Coins can only be used to pay for a service. This is the only coins convertable to cash in Philippine Peso only 1 Green Coin is equivalent to P{{ compute_green_coin_value() }}.</p>
	    <p>Use your green coins now! Hire someone for your project! Visit these <a href="#">websites</a> now and select the best contractor for you amazing projects.</p>
	    <p>Withdraw your green coins now!</p>
	    <div class="row">
	        <div class="col-xs-3">
	        	500 Green
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(500,0.10) }}</span>
	        	<strong>$49.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="1000" data-name="Green Coin" data-type="green" data-category="coins" data-price="98.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	1000 Green
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(1000,0.10) }}</span>
	        	<strong>$98.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="1000" data-name="Green Coin" data-type="green" data-category="coins" data-price="98.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	2000 Green
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(2000,0.10) }}</span>
	        	<strong>$196.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="2000" data-name="Green Coin" data-type="green" data-category="coins" data-price="196.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	5000 Green
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(5000,0.10) }}</span>
	        	<strong>$488.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="5000" data-name="Green Coin" data-type="green" data-category="coins" data-price="488.88">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	10000 Green
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(10000,0.10) }}</span>
	        	<strong>$968.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="10000" data-name="Green Coin" data-type="green" data-category="coins" data-price="968.88">Add To Cart</button>
	       	</div>
	    </div>
	</section>
    <section class="section" id="">
	    <h3>Pink Coins</h3>
	    <p>When to use pink coins?</p>
	    <p>Pink Coins can only be used to send donations. A single pink coin is quivalent to P{{ compute_pink_coin_value() }} Philippine Peso</p>
	    <p>Visit these <a href="#">websites</a> to help!</p>
	    <div class="row">
	        <div class="col-xs-3">
	        	5 Pink
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(5,1.25) }}</span>
	        	<strong>$6.18</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="5" data-name="Pink Coin" data-type="pink" data-category="coins" data-price="6.18">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	10 Pink
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(10,1.25) }}</span>
	        	<strong>$11.98</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="10" data-name="Pink Coin" data-type="pink" data-category="coins" data-price="11.98">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	25 Pink
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(25,1.25) }}</span>
	        	<strong>$29.98</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="25" data-name="Pink Coin" data-type="pink" data-category="coins" data-price="29.98">Add To Cart</button>
	       	</div>
	    </div>
	    <div class="row">
	        <div class="col-xs-3">
	        	50 Pink
	       	</div>
	        <div class="col-xs-5">
	        	<span class="line-through">${{ compute_coins(50,1.25) }}</span>
	        	<strong>$58.88</strong>
	       	</div>
	        <div class="col-xs-4">
	            <button class="btn btn-sm btn-success btn-add-coins" data-link="{{ url('/cart') }}" data-token="{{ csrf_token() }}" 
	            data-value="50" data-name="Pink Coin" data-type="pink" data-category="coins" data-price="58.88">Add To Cart</button>
	       	</div>
	    </div>
	</section>
</div>
@endsection

@section('modal')
<div class="modal fade" id="login" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="col-md-4 control-label">E-Mail Address</label>

              <div class="col-md-6">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="col-md-4 control-label">Password</label>

              <div class="col-md-6">
                  <input id="password" type="password" class="form-control" name="password">

                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="remember"> Remember Me
                      </label>
                  </div>
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                      <i class="fa fa-btn fa-sign-in"></i> Login
                  </button>

                  <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
              </div>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('script_footer_deedel')
	<script type="text/javascript">
		$(function() {
			// disable button when already added to cart
			@if (sizeof(Cart::content()) > 0)
				@foreach (Cart::content() as $item)
					$('*[data-price="{{$item->price}}"][data-name="{{$item->name}}"]').addClass('hide');
				@endforeach
			@endif

			// Add to cart
			$('.btn-add-coins').click(function () {
				$(this).addClass('disabled');
				var url = $(this).attr("data-link");
				var data = {
		            _token:$(this).data('token'),
		            _name:$(this).data('name'),
		            _value: $(this).data('value'),
		            _type: $(this).data('type'),
		            _category:$(this).data('category'),
		            _price: $(this).data('price')
		        }

				@if (Auth::check());
					$.ajax({
						type: "POST",
						url: "cart",
						datatype: 'JSON',
						data: data,
						success: function( data ) {
							console.log(data);
							setTimeout(function() {window.location = data.redirect_url;}, 0);
						}, error:function() {
							console.log("Error!");
						}
					});
				@else
					// login before adding to cart
					// $('#login').modal('show');
					window.location.href = '/login';
				@endif
			});
		});
	</script>
	@if (session()->has('success_message'))
        <script>        	
      		$(document).ready(function() {
	          $.toast({
	            heading: "{{ session()->get('success_message') }}",
	            text: '',
	            position: 'top-right',
	            loaderBg:'#ff6849',
	            icon: 'success',
	            hideAfter: 3500, 
	            
	            stack: 6
	          });
			});
        </script>

    @endif

@endsection