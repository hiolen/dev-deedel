
  <footer class="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <p class="pt0"><img src="{{ deedel_url() }}/assets/network/deedel/img/logo-deedel-white.png" alt="DeeDel"></p>
            <p><span class="white-text">DeeDel</span> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <div class="col-md-3 col-sm-6">
            <h5 class="white-text">Pages</h5>
            <ul class="footer-link list-icons">
              <li><a href="{{ route('static.deedel.faq') }}">FAQ</a></li>
              <li><a href="{{ route('static.deedel.termsDefinition') }}">Terms</a></li>
              <li><a href="{{ privacy_url() }}">Privacy Policy</a></li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6">
            <h5 class="white-text">Highlight</h5>
            <ul class="footer-link list-icons">
              <li><a href="">How to earn points</a></li>
              <!-- earn points from website activities, affiliates and more -->
              <li><a href="">how to get rewards</a></li>
              <!-- get rewards from contest and selected website activities like sharing posts, affiliate and more -->
              <li><a href="">how to claim rewards</a></li>
              <!-- claiming rewards is depending on how you gathered points -->
              <li><a href="">how to join to contest</a></li>
              <li><a href="">do you have affiliate program</a></li>
            </ul> 
          </div>
          <div class="col-md-3 col-sm-6">          
            <h5 class="white-text">Get in touch</h5>
            <form>
                <p><input type="text" name="" placeholder="Name" class="form-control"></p>
                <div class="input-group m-t-10">
                  <input type="email" name="" placeholder="Email" class="form-control">
                  <span class="input-group-btn">
                    <button type="submit" class="btn waves-effect waves-light btn-info">Submit</button>
                  </span>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-btm">
      <div class="container">
          &copy;Copyright 2016. All Rights Reserved by <a class="white-text" href="{{ route('static.deedel.index') }}">DeeDel</a>
      </div>
    </div>
  </footer>

</div>
@yield('script_footer')
