<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 back">
      <p class="p-10 m-0 text-center text-white"><strong>Join the most advance tool for promoting your online products and services <a href="{{ route('auth.deedel.register') }}" class="btn btn-inverse btn-rounded custom-btn">Join Now</a></strong></p>
    </div>
  </div>
</div>



  <div class="container">
      <nav class="navbar navbar-default">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ route('static.deedel.index') }}"><img src="{{ deedel_url() }}/assets/network/deedel/img/logo-deedel.png" alt="DeeDel" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav custom-nav navbar-right">
            <li><a href="{{ route('static.deedel.network') }}">Network</a></li>
            <li><a href="{{ route('pricing.deedel.home') }}">Buy Coins</a></li>
            <li><a href="{{ route('support.deedel.home') }}">Get Help</a></li>
            <li><a href="{{ route('auth.deedel.login') }}" class="btn btn-danger btn-rounded custom-btn">Login NOW</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
  </div>
  <!-- /Row -->