
    @include('network.deedel.inc.htmlhead')
    @include('network.deedel.inc.header')

    @yield('content_deedel')
    @yield('modal_deedel')

    @if (Auth::guest())
        @include('auth.modalLogin')
        @include('auth.modalRegister')
    @endif
    @include('network.deedel.inc.footer')
    {{ jquery_js() }}
    {{ bootstrap_front_js() }}
    {{ slimscroll_js() }}
    {{ waves_js() }}
    {{ waypoints_js() }}
    {{ counterup_js() }}
    {{ raphael_js() }}
    {{ custom_js() }}
    {{ dashboard1_js() }}
    {{ sparkline_js() }}
    {{ charts_sparkline_js() }}
    {{ toast_js() }}
    @yield('script_footer_deedel')
</body>
</html>
