<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Elite Admin is Premium Admin Dashboard Template with Flat design, Bootstrap Framework, Less, HTML5, CSS3 & Media Query. Collection of Lots of UI Components and more than thousand Pages">
<meta name="author" content="">
<meta name="keyword" content="metronic, admin templates, free dashboard templates, admin panel template, html dashboard templates, bootstrap themes, bootstrap template, bootstrap admin template, elite admin, metronic admin, free dashboard themes, premium dashboard template">
<link rel="icon" type="image/png" sizes="16x16" href="{{ deedel_url() }}/assets/network/deedel/img/favicon.png">
<title>DeeDel pay with coins with the most advance way of marketing and promoting your products</title>

    {{ bootstrap_front_css() }}
    {{ front_bower_components_toast_css() }}
    {{ front_bower_components_morrisjs_css() }}
    {{ front_animate_css() }}
    {{ deedel_style_css() }}
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('script_header_front')
</head>
<body id="app-layout" class="fix-header">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->