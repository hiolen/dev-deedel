@extends('network.deedel.inc.app')

@section('content_deedel')
    <div class="row">
        <div class="col-md-12">
            <div class="fix-width text-center banner-part">
                <div class="min-h"><h1 class="banner-title">Your online products and services promotion and marketing best friend</h1></div>
                <span class="banner-small-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec lacus nec diam interdum porta. Praesent cursus ligula id eros convallis, eu scelerisque enim varius. Integer sagittis, ligula eget lobortis dictum, sapien mauris luctus leo, vitae dictum purus est in magna. Aenean convallis placerat nisl non sodales. . .
                </span>
                <div class="btn-box"><a href="{{ route('auth.deedel.register') }}" class="left-btn">Get Started</a><a href="{{ route('static.deedel.howto') }}" class="right-btn">Learn More</a></div>
                <a href="#choose-demo"><img src="{{ deedel_url() }}/assets/elite/lp/images/banner-img-2.jpg" class="img-responsive" /></a>
            </div>
        </div>
    </div>


	<div class="popular light-blue-bg">
	    <div class="container">
			<div class="row" id="choose-demo">
		      	<div class="text-center">
					<small class="text-danger">THE MOST POPULAR NETWORK IN MOST POPULAR FEATURES</small>
					<h2 class="font-500">Popular in our Network</h2>
					<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		      	</div>
	          	<div class="row">
	          		@foreach ($features as $feature)
	            	<div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
	    						<div class="white-box">
					          <img src="{{ $feature->image }}" class="img-responsive" />
					          <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="{{ $feature->domain }}" target="_blank">Visit Network</a></div>
					        </div>
					        <h5 class="m-t-20 font-500">{{ $feature->name }}</h5>
	              </div>
	              @endforeach
	            </div>
	              
				<p class="p-t-10 text-center demo-text">
					<a href="{{ route('static.deedel.network') }}" class="btn btn-outline btn-rounded btn-default">View more</a>
				</p>
			</div>
		</div>    
	</div>

	<div class="container">
    	<div class="m-t-60" id="myfeatures">
        	<div class="row">
			<div class="col-md-7 pull-right auto-img"><img src="{{ deedel_url() }}/assets/elite/lp/images/responsive.jpg" /></div>
			<div class="col-md-5 demo-text">
				<small class="text-danger">All in one place, email templates, forms, landing pages</small>
				<h2 class="font-500">Marketing Tools</h2>
				<p class="m-t-30 m-b-30">You will get almost everything you need. We have added all possible widgets to make your life easier with easy editing. Check them out.</p>
				<a class="btn btn-outline btn-rounded btn-default" href="#" target="_blank">Check all widgets</a>
			</div>
         	</div>
    	</div>
	<div class="row m-t-60">
		<div class="col-md-7 auto-img "><img src="{{ deedel_url() }}/assets/elite/lp/images/ecommerce.jpg" class="pull-right" /></div>
		<div class="col-md-5 demo-text">
			<small class="text-danger">Create Content Pages and Blog, add assets</small>
			<h2 class="font-500">Content Management</h2>
			<p class="m-t-30 m-b-30">Yes, We have also added eCommerce Dashboard option. It also has relevant sections, like List of Products, Total Earnings and & Status.</p>
			<a class="btn btn-outline btn-rounded btn-default" href="#">Check  eCommerce Demo</a>
		</div>
	</div>
    	<div class="m-t-60">
        	<div class="row">
			<div class="col-md-7 pull-right auto-img"><img src="{{ deedel_url() }}/assets/elite/lp/images/responsive.jpg" /></div>
			<div class="col-md-5 demo-text">
				<small class="text-danger">In a form Points, Coins, Gifts & Tokens</small>
				<h2 class="font-500">Contests and Rewards</h2>
				<p class="m-t-30 m-b-30">You will get almost everything you need. We have added all possible widgets to make your life easier with easy editing. Check them out.</p>
				<a class="btn btn-outline btn-rounded btn-default" href="{{ route('static.deedel.howto') }}">How-To's</a>
			</div>
         	</div>
    	</div>
  </div>

	<div class="container">
		<div class="row m-t-60">
			<center class="col-md-6 col-md-offset-3 m-t-40">
				<small class="text-danger">ALMOST COVERED EVERYTHING</small>
				<h2 class="font-500">We are proud on Features provided with DeeDel</h2>
			</center>
		</div>

		<div class="row inner-margin">
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/tables.png"/>
	            <h4 class="font-500">Create Pages</h4>
	            <p>premium accounts can create custom pages</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/gallery.png"/>
	            <h4 class="font-500">Blog</h4>
	            <p>you can create your own blog site. add add sample test text here so you can add more here, please add it here</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/gallery.png"/>
	            <h4 class="font-500">Assets</h4>
	            <p>can add wide-range type of assets like links, embeded video, audio and images, can upload images, .pdf files, and other acceptable file types</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/multi-level.png"/>
	            <h4 class="font-500">Create Landing Pages</h4>
	            <p>premium accounts can create landing pages to capture leads and more more more</p>
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/forms.png"/>
	            <h4 class="font-500">Forms</h4>
	            <p>premium accounts can create forms that can be embeded to any custom pages and blog posts</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/e-com.png"/>
	            <h4 class="font-500">Email Templates</h4>
	            <p>can create email templates to be sent as autorespond to forms and can send using the conact lists for subscribers</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/docs.png"/>
	            <h4 class="font-500">Detailed Documentation</h4>
	            <p>We have made detailed documentation, <br/>so it will easy to use.</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/dedicated.png"/>
	            <h4 class="font-500">Dedicated Support</h4>
	            <p>We believe in supreme support is <br/>key and we offer that.</p>  
	        </div>
	        <div class="col-md-4 col-sm-4 text-center">
	            <img src="{{ deedel_url() }}/assets/elite/lp/images/ragular.png"/>
	            <h4 class="font-500">Regular Updates</h4>
	            <p>We are constantly updating our <br/>pack with new features.</p>  
	        </div>
		</div>

		<div class="row m-t-60">
			<div class="col-md-12">
				<center class="col-md-6 col-md-offset-3">
				<small class="text-danger">ALMOST COVERED EVERYTHING</small>
				<h2 class="font-500">We are accepting money from the following</h2>
				</center>
			</div>
		</div>

		<div class="row inner-margin">
			<div class="col-md-3 col-sm-6 text-center">
				<img src="{{ deedel_url() }}/assets/elite/lp/images/color-skim.png"/>
				<h4 class="font-500">Mobile Bank Transfer</h4>
				<p>You can send money using mobile app by BPI, BDO, and more</p>  
			</div>
			<div class="col-md-3 col-sm-6 text-center">
				<img src="{{ deedel_url() }}/assets/elite/lp/images/sidebars.png"/>
				<h4 class="font-500">Money Remittances</h4>
				<p>Included Dark and Light Sidebar for getting desire look and feel.</p>  
			</div>
			<div class="col-md-3 col-sm-6 text-center">
				<img src="{{ deedel_url() }}/assets/elite/lp/images/pages.png"/>
				<h4 class="font-500">Paypal</h4>
				<p>Yes, we have added 1100+ Pages template to make it easier.</p>  
			</div>
			<div class="col-md-3 col-sm-6 text-center">
				<img src="{{ deedel_url() }}/assets/elite/lp/images/ui-component.png"/>
				<h4 class="font-500">Credit Card</h4>
				<p>Almost 500+ UI Components being given with Elite Admin Pack.</p>  
			</div>
		</div>
		<p class="text-center">
			<a href="#" class="btn btn-primary btn-lg">Deposit NOW!</a>
		</p>

	</div>

	<div class="call-to-action bg-danger">
		<div class="container">
			<div class="row">
				<div class="col-md-6 m-t-20 m-b-20"><span>Sounds good?</span></div>
				<div class="col-md-6 "><a href="{{ coins_url() }}" class="btn btn-default btn-rounded suggest-btn m-t-10">Buy Coins Now</a><a href="#" class="btn btn-outline btn-rounded btn-default buy-btn m-t-10">Upgrade Membership</a></div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script_footer_deedel')
    {{ typed_js() }}
<script>
  $(function(){
      $(".banner-title").typed({
        strings: ["Advance Tool for", "Build Your backend in No-Time for any plateform", "Powerfull webapp kit with countless features"],
        typeSpeed: 100,
          loop:true
      });
  });
</script>
@endsection