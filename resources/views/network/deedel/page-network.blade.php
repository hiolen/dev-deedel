@extends('network.deedel.inc.app')

@section('content_deedel')
<div class="container">
    <div class="min-h banner-part text-center">
        <h1 class="banner-title">Network</h1>
    </div>    
    <div class="row">
        @foreach($networks as $network)               
        <div class="col-md-4 col-sm-6 col-xs-12 m-t-40 text-center">
            <div class="white-box">
              <img src="{{ $network->image }}" class="img-responsive" />
              <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="{{ $network->domain }}">Visit Network</a></div>
            </div>
            <h5 class="m-t-20 font-500">{{ $network->name }}</h5>
        </div>
        @endforeach
    </div>
</div>
@endsection