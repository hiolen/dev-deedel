@extends('layouts.app')

@section('script')
    <script type="text/javascript">
        $(function() {
            // update quantity
            $('.quantity').on('change', function() {
                var id = $(this).attr('data-id');
                var token = $(this).data('token');
                var data = {
                    'quantity': this.value,
                    _token:$(this).data('token'),
                }

                $.ajax({
                  type: "PATCH",
                  url: '/cart/' + id,
                  data: data,
                  success: function(data) {
                    console.log('change!');
                    window.location.href = '/cart';
                  }, error:function() {
                    console.log("Error!");
                  }
                });
            });
        });
    </script>
@endsection

@section('content')
    <div class="container">
        <p><a href="{{ url('/') }}">Home</a> / Cart</p>
        <h1>Your Cart</h1>

        <hr>


        @if (sizeof(Cart::content()) > 0)
        <table class="table">
            <thead>
                <tr>
                    <th class="table-image"></th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th class="column-spacer"></th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach (Cart::content() as $item)
                <tr>
                    <td class="table-image"><a href="{{ url('buy-coins') }}"><img src="one.png" alt="product" class="img-responsive cart-image"></a></td>
                    <td><a href="{{ url('buy-coins') }}">{{ $item->options[0] * $item->qty }}  {{ $item->name }}s</a></td>
                    <td>
                        <select class="quantity" data-id="{{ $item->rowid }}" data-token="{{ csrf_token() }}">
                            <option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
                            <option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
                            <option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
                            <option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
                            <option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
                            <option {{ $item->qty == 6 ? 'selected' : '' }}>6</option>
                            <option {{ $item->qty == 7 ? 'selected' : '' }}>7</option>
                            <option {{ $item->qty == 8 ? 'selected' : '' }}>8</option>
                            <option {{ $item->qty == 9 ? 'selected' : '' }}>9</option>
                            <option {{ $item->qty == 10 ? 'selected' : '' }}>10</option>
                        </select>
                    </td>
                    <td>${{ $item->subtotal }}</td>
                    <td class=""></td>
                    <td>
                        <form action="{{ url('cart', [$item->rowid]) }}" method="POST" class="side-by-side">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                        </form>
                        <!-- <form action="{{ url('switchToWishlist', [$item->rowId]) }}" method="POST" class="side-by-side">
                            {!! csrf_field() !!}
                            <input type="submit" class="btn btn-success btn-sm" value="To Wishlist">
                        </form> -->
                    </td>
                </tr>
                @endforeach

                <tr class="border-bottom">
                    <td class="table-image"></td>
                    <td style="padding: 40px;"></td>
                    <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                    <td class="table-bg">${{ Cart::total() }}</td>
                    <td class="column-spacer"></td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <a href="{{ coins_url() }}" class="btn btn-primary btn-lg">Buy More Coins</a> &nbsp;
        <!-- checkout -->
        <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
          Checkout
        </button>

        <div style="float:right">
            <form action="/emptyCart" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" class="btn btn-danger btn-lg" value="Empty Cart">
            </form>
        </div>
        @else
            <h3>You have no items in your shopping cart</h3>
            <a href="/" class="btn btn-primary btn-lg">Continue Shopping</a>
        @endif

        <div class="spacer"></div>

    </div> <!-- end container -->

@endsection

@section('modal')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Payments List</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/pay_via_paypal') }}">
          {{ csrf_field() }}
          <input type="hidden" name="cart" value="{{ Cart::content() }}" />
          <input type="submit" class="btn btn-success btn-lg" value="Paypal" />
        </form>

        <!-- https://stripe.com/docs/recipes/subscription-signup -->
        <form action="pay_via_stripe" method="post">
            {{ csrf_field() }}
            <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="{{ env('STRIPE_KEY', 'pk_test_4O2nUot8OB8FP32S2XorPsfo') }}"
                data-amount="{{ Cart::total() * 100 }}"
                data-name="Stripe.com"
                data-description="Widget"
                data-locale="auto"
                data-currency="usd"
                data-label="stripe">
            </script>
        </form>
        <p><a href="" class="btn btn-primary">Account Balance</a></p>
        <p><a href="" class="btn btn-primary">Add Money to My Account</a></p>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

        @if (session()->has('success_message'))
        <div class="alert-msg">
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        </div>
        @endif

        @if (session()->has('error_message'))
        <div class="alert-msg">
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        </div>
        @endif

@endsection