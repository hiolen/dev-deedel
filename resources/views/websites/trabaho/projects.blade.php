@extends('layouts.website.trabaho.app')
<?php
    $website_trabaho_url = Config::get('deedel_variables.website_trabaho_url');
?>
@section('website_trabaho_content')
<div class="container">
    <h2>Projects</h2>
    <form>    	
		<div class="input-group input-group-lg">
		  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search"></i></span>
		  <input type="text" class="form-control" placeholder="Search Projects" aria-describedby="sizing-addon1">
		</div>
    </form>
	@foreach($projects as $proj)
		<li><a href="{{ $website_trabaho_url }}/project/{{ $proj->id }}">{{ $proj->title }}</a></li>
	@endforeach
</div>
@endsection
