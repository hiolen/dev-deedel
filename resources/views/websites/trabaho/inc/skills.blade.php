<?php
    $website_trabaho_url = Config::get('deedel_variables.website_trabaho_url');
?>
@if ($type === "projects")
	@foreach($skill as $sk)
		<li><a href="{{ $website_trabaho_url }}/freelancers/skill/{{ $sk->slug }}">{{ $sk->name }}</a></li>
	@endforeach
@elseif ($type === "freelancers")
	@foreach($skill as $sk)
		<li><a href="{{ $website_trabaho_url }}/freelancers/skill/{{ $sk->slug }}">{{ $sk->name }}</a></li>
	@endforeach
@endif