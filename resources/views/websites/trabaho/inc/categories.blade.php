<?php
    $website_trabaho_url = Config::get('deedel_variables.website_trabaho_url');
?>
@if ($type === "projects")
	@foreach($category as $cat)
		<li><a href="{{ $website_trabaho_url }}/projects/category/{{ $cat->slug }}">{{ $cat->name }}</a></li>
	@endforeach
@elseif ($type === "freelancers")
	@foreach($category as $cat)
		<li><a href="{{ $website_trabaho_url }}/freelancers/category/{{ $cat->slug }}">{{ $cat->name }}</a></li>
	@endforeach
@endif