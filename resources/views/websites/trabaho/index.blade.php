@extends('layouts.website.trabaho.app')

@section('website_trabaho_content')
<div class="container">
    <h2>Trabaho</h2>
    <h3>Projects by Categories</h3>
	@include('websites.trabaho.inc.categories',['type' => 'projects'])
    <h3>Projects by Skills</h3>
	@include('websites.trabaho.inc.skills',['type' => 'projects'])
    <h3>Freelancers by Categories</h3>
	@include('websites.trabaho.inc.categories',['type' => 'freelancers'])
    <h3>Freelancers by Skills</h3>
	@include('websites.trabaho.inc.skills',['type' => 'freelancers'])
	
</div>
@endsection
