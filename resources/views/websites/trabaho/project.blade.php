@extends('layouts.website.trabaho.app')
<?php
    $website_trabaho_url = Config::get('deedel_variables.website_trabaho_url');
?>
@section('website_trabaho_content')
<div class="container">
    <h2>{{ $project->title }}</h2>
    <div>
    	{{ $project->details }}
    </div>
</div>
@endsection
