@extends('layouts.app')

@section('content')

<div class="sidebar">
	<ul>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Science Fiction</a></li>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Drama</a></li>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Romance</a></li>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Travel</a></li>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Health</a></li>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Guide</a></li>
		<li><a href="{{ url('/books/genre/sci-fi') }}">Children's</a></li>
	</ul>
</div>

<div class="container">
	<div class="featured-books">
		<ul>			
	    	<li>
	    		<a href=""><img src="" alt="Book Cover"></a>
	    		<h3><a href="">Book title</a></h3>
	    	</li>
		</ul>
	</div>
	<div class="sort">
		Sort by 
		<a href="">Popularity</a>
		<a href="">Alphabetical</a>
		<a href="">Reviews</a>
		<a href="">Preview (HTML Version)</a>
		<a href="">Free Read (HTML Version)</a>
	</div>
    <h2>Books</h2>
    <ul>
    	<li>
    		<a href=""><img src="" alt="Book Cover"></a>
    		<h3><a href="">Book title</a></h3>
    	</li>
    </ul>
</div>
@endsection
