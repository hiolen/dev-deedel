@extends('layouts.app')

@section('content')
<div class="container">

    <div class="membership">
        <h2>Become a member of BookLulu</h2>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h3>Indie</h3>
                <p>$8.88</p>
                <p>If you are starting as a book author this is best for you</p>
                <p>Coins Image Here (25 gold and 50 silver)</p>
                <p><a href="{{ url('/membership/indie') }}">Learn more</a></p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h3>Professional</h3>
                <p>$18.88</p>
                <p>if you have over 10 books written this package is best for you</p>
                <p>Coins Image Here (50 gold and 100 silver)</p>
                <p><a href="{{ url('/membership/professional') }}">Learn more</a></p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h3>Publisher</h3>
                <p>$38.88</p>
                <p>if you are a publishing company with lot of books to promote this package is the best for you</p>
                <p>Coins Image Here (75 gold and 150 silver)</p>
                <p><a href="{{ url('/membership/publisher') }}">Learn more</a></p>
            </div>
        </div>
    </div>
	<h2>Why add your books to BookLulu</h2>
	<ul>
		<li>List all your books</li>
		<li>Select reviews you want to add to your books</li>
	</ul>
</div>
@endsection
