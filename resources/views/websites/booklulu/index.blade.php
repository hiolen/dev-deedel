@extends('layouts.website.booklulu.app')

@section('website_booklulu_content')
<div class="container">

    <div class="row">
        <section class="col-xs-12 col-sm-4">
            <h2>Make your book reach your targeted readers.</h2>
            <p>display the book detail page (demo)</p>
            <img src="">
            <a href="{{ url('/book-marketing') }}">Read More</a>
        </section>
        <section class="col-xs-12 col-sm-4">
            <h2>Display a glance of your book content to attract readers.</h2>
            <p>create html version of your book preview</p>
            <img src="">
            <a href="{{ url('/ebook-html-demo') }}">Read More</a>
        </section>
        <section class="col-xs-12 col-sm-4">
            <h2>Promote you self as a book author</h2>
            <p>display the author page, with the list of your books</p>
            <img src="">
            <a href="{{ url('/deesignerd') }}">Demo</a>
        </section>
    </div>

    <div class="features">        
        <h2 class="text-center">Why add your books in BookLulu </h2>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h3>Search Engine Optimization</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet risus sed volutpat tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce rhoncus augue sed porta pellentesque. Praesent ultrices enim eu lacus fermentum, a mattis nibh hendrerit.</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <img src="http://www.globalsynergydesign.com/wp-content/uploads/2013/02/seo.jpg" alt="" class="img-responsive">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <img src="https://www.customlegalmarketing.com/wp-content/uploads/structured-data.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-6">
                <h3>Structured Data</h3>
                <p>Aliquam tristique cursus est, in venenatis lacus vulputate vitae. Nullam cursus ac velit sed laoreet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc volutpat mi mauris, et blandit mauris aliquam non. Nullam a imperdiet tellus, quis lobortis nunc. Donec id turpis nec mauris euismod bibendum. Nulla eget iaculis lorem.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h3>Accelerate Mobile Pages</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet risus sed volutpat tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce rhoncus augue sed porta pellentesque. Praesent ultrices enim eu lacus fermentum, a mattis nibh hendrerit.</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <img src="https://www.design19.org/blog/wp-content/uploads/2015/10/ampproject_design19blog_big.gif" alt="" class="img-responsive">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <img src="http://mattkeys.me/assets/product-image.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-6">
                <h3>Web Performance</h3>
                <p>Aliquam tristique cursus est, in venenatis lacus vulputate vitae. Nullam cursus ac velit sed laoreet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc volutpat mi mauris, et blandit mauris aliquam non. Nullam a imperdiet tellus, quis lobortis nunc. Donec id turpis nec mauris euismod bibendum. Nulla eget iaculis lorem.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h3>Browser and Device Compatibility</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet risus sed volutpat tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce rhoncus augue sed porta pellentesque. Praesent ultrices enim eu lacus fermentum, a mattis nibh hendrerit.</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <img src="http://previews.123rf.com/images/entity1313/entity13131408/entity1313140800018/30741324-Responsive-web-design-cross-browser-compatibility-development-programming-PC-mobile-phone-device-han-Stock-Vector.jpg" alt="" class="img-responsive">
            </div>
        </div>
    </div>

    <div class="membership">
        <div class="">
            <h3>Free</h3>
            <p>Free 100 Silver Coins</p>
            <p><a href="#">Sign up NOW!</a></p>
        </div>

        <h2>Become a premium member of BookLulu</h2>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h3>Indie</h3>
                <p>25 Blue Coins</p>
                <p>Connect to upto 10 members</p>
                <p><a href="#">Become a member NOW!</a></p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h3>Professional</h3>
                <p>50 Blue Coins</p>
                <p>Connect to upto 10 members</p>
                <p><a href="#">Become a member NOW!</a></p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h3>Publisher</h3>
                <p>100 Blue Coins</p>
                <p>if you are a publishing company with lot of books to promote this package is the best for you</p>
                <p>Connect to unlimitted members</p>
                <p><a href="#">Become a member NOW!</a></p>
            </div>
        </div>
    </div>

    <div class="text-center">
        <h2>Buy coins NOW?</h2>
        <p>Coins are </p>
        <a href="#">Buy now</a>
    </div>

    <div class="">
        <p>Book Preview Demo</p>
    </div>
</div>
		
@endsection
