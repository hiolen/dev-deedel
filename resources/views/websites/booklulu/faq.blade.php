@extends('layouts.app')

@section('content')
<div class="container">
    
        <h2>FAQ</h2>
        <div class="section">
            <a class="btn btn-collapse" role="button" data-toggle="collapse" href="#collapseAddBook" aria-expanded="false" aria-controls="collapseAddBook">
                <strong>How can I add my book?</strong>
            </a>
            <div class="collapse" id="collapseAddBook">
                You can add your books in My books page, you need to purchase which membership package that is best for you. We recommend Professional Package to add more books and get some features that is not on the starter package (Indie Package). Please see our membership page and see the features for each packages.
            </div>
        </div>
        <div class="section">
            <a class="btn btn-collapse" role="button" data-toggle="collapse" href="#collapseNumOfBook" aria-expanded="false" aria-controls="collapseNumOfBook">
                <strong>How many books can I add?</strong>
            </a>
            <div class="collapse" id="collapseNumOfBook">
                Depends on the membership package you purchase. For Indie Package you can add upto 5 books, Professional Package is upto 12 books and 25 books for Publisher Package. You can add more books whenever you want for $2 each book.
            </div>
        </div>
        <div class="section">
            <a class="btn btn-collapse" role="button" data-toggle="collapse" href="#collapseBioPage" aria-expanded="false" aria-controls="collapseBioPage">
                <strong>Can I add bio on my profile page?</strong>
            </a>
            <div class="collapse" id="collapseBioPage">
                Yes. All members can update their profile page which includes you bio and other personal info.
            </div>
        </div>
        <div class="section">
            <a class="btn btn-collapse" role="button" data-toggle="collapse" href="#collapseAddBookProf" aria-expanded="false" aria-controls="collapseAddBookProf">
                <strong>How can I add my book?</strong>
            </a>
            <div class="collapse" id="collapseAddBookProf">
                Can my books be added on my profile page?
            </div>
        </div>
        <div class="section">   
            <a class="btn btn-collapse" role="button" data-toggle="collapse" href="#collapsePdfToHtml" aria-expanded="false" aria-controls="collapsePdfToHtml">
                <strong>How can I add my book?</strong>
            </a>
            <div class="collapse" id="collapsePdfToHtml">
                I have a pdf file for my ebook how can I make it to html?
            </div>
        </div>
</div>
@endsection
