@extends('layouts.app')

@section('content')
<div class="sidebar">
	<ul>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Science Fiction</a></li>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Drama</a></li>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Romance</a></li>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Travel</a></li>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Health</a></li>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Guide</a></li>
		<li><a href="{{ url('/authors/genre/sci-fi') }}">Children's</a></li>
	</ul>
</div>
<div class="container">
	<div class="sort">
		Sort by 
		<a href="">Popularity</a>
		<a href="">Alphabetical</a>
		<a href="">Book Reviews</a>
		<a href=""># of Books</a>
	</div>
    <h2>Authors</h2>
    <ul>
    	<li>
    		<a href="{{ url('/author/author-name') }}">Author Name</a>
    	</li>
    </ul>
</div>
@endsection
