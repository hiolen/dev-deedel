@extends('layouts.website.bahay.app')

@section('website_bahay_content')
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="featured-property">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="tags">
					<h3><strong>Tags</strong></h3>
					<ul>
						@foreach ($tagParents as $tag)
						<li><a href="{{website_bahay_url()}}/tag/{{ $tag->slug }}">{{ $tag->name }}</a></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
		
@endsection
