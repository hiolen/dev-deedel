@extends('layouts.website.bahay.app')

@section('website_bahay_content')
<div class="location-single">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-9"> 
				<div class="location-head">
					<h3><strong>Name: {{ $location->name }}</strong></h3>
					<p><strong>Description:</strong> {!! $location->desc !!}</p>
				</div>
				<div class="featured-property">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/renta.jpg" alt="">
								<div class="caption">
									<h4><a href="#">Compostela Land and Farm</a></h4>
									<p><strong>Price</strong>: ₱ 35,000,000</p>
									<p><strong>Size</strong>: 26,622 Land Size (m²)</p>
									<p><strong>Location</strong>: Compostela Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/rent-own.jpg" alt="">
								<div class="caption">
									<h4><a href="#">House and Lot For Sale</a></h4>
									<p><strong>Price</strong>: ₱ 5,000,000</p>
									<p><strong>Size</strong>: 1,070m<sup>2</sup></p>
									<p><strong>Location</strong>: Lawaan Danao Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6  col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/lot-sale.jpg" alt="">
								<div class="caption">
									<h4><a href="#">Residential Lot</a></h4>
									<p><strong>Price</strong>: ₱ 1,850,000</p>
									<p><strong>Size</strong>: 420m<sup>2</sup></p>
									<p><strong>Location</strong>: Calajoan Minglanilla Cebu</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6  col-lg-4">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/residential.jpg" alt="">
								<div class="caption">
									<h4><a href="#">Luana Homes</a></h4>
									<p><strong>Price</strong>: ₱ 1,741,554</p>
									<p><strong>Size</strong>: 2,000m<sup>2</sup></p>
									<p><strong>Location</strong>: Dumlog Talisay City</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="loc-sidebar">
					<h3><strong>Locations</strong></h3>
					@foreach ($locParents as $location)
					<h4><strong><a href="{{website_bahay_url()}}/location/{{ $location->slug }}">{{ $location->name }}</a></strong></h4>
					<ul>
						@foreach ($locations as $item)
							@if ($location->id == $item->parent)
							<li><a href="{{website_bahay_url()}}/location/{{ $item->slug }}">{{ $item->name }}</a></a></li>
							@endif
						@endforeach
					</ul>
					@endforeach	
					<p>Reference: <a href="https://en.wikipedia.org/wiki/List_of_cities_and_municipalities_in_the_Philippines" target="_blank">https://en.wikipedia.org/wiki/List_of_cities_and_municipalities_in_the_Philippines</a></p>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection