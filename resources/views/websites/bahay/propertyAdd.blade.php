@extends('layouts.website.bahay.app')

@section('website_bahay_content')
	<div class="list-property">
		<div class="container">
			<h1>Add Property</h1>
			<div class="tab-contents">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#createProperty" aria-controls="createProperty" role="tab" data-toggle="tab">Create Property</a></li>
					<li role="presentation"><a href="#addAssets" aria-controls="addAssets" role="tab" data-toggle="tab">Add Assets</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="createProperty">
						<form action="#" method="POST" id="addPropertyForm" role="form">
							{!! csrf_field() !!}
							<!-- this is in tab form -->
							<div data-price="generatedcode">
								<h4>adding property</h4>
								{{ form_input("text","name","","Name","form-control","Name") }}
								{{ form_input("text","slug","","Slug","form-control","Slug") }}
								{{ form_input("text","persqmprice","","Price per sqm","form-control","Price per sqm") }}
								{{ form_input("text","totalprice","","Total Price","form-control","Total Price") }}
								<select class="form-control">
									<option value="">P500,000 and Below</option>
									<option value="">P500,000 up to P1,000,000</option>
									<option value="">P1,000,000 up to P2,000,000</option>
									<option value="">P2,000,000 up to P3,000,000</option>
									<option value="">P3,000,000 up to P4,000,000</option>
									<option value="">P4,000,000 up to P5,000,000</option>
									<option value="">P5,000,000 and Above</option>
								</select>
								{{ form_texteditor("description","","Add Description","form-control text-editor-property","Description") }}
							</div>
						</form>
					</div>
					<div role="tabpanel" class="tab-pane" id="addAssets">
						<h2>Add Resources</h2>
						<p>
							<a href="#modalAddAsset" data-toggle="modal" data-type="link" data-title="Link" class="btn-add-asset"><i class="fa fa-plus"></i> Add Link</a>
							<a href="#modalAddAsset" data-toggle="modal" data-type="file" data-title="File" class="btn-add-asset"><i class="fa fa-plus"></i> Add File</a>
							<a href="#modalAddAsset" data-toggle="modal" data-type="embed" data-title="Embed" class="btn-add-asset"><i class="fa fa-plus"></i> Add Embed</a>
						</p>
						<div class="form-group">
							<label>Add new link</label> <label class="pull-right "><span class="btn btn-sm btn-warning">Add new link</span></label>
							<input type="text" class="form-control" name="link[]" placeholder="Add link #1">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="link[]" placeholder="Add link #2">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="link[]" placeholder="Add link #3">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
@endsection

@section('modal')
	<!-- Add Meta -->
	<div class="modal fade" tabindex="-1" role="dialog" id="modalAddAsset" aria-labelledby="modalAddAssetLabel">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="modalAddAssetLabel">Add <span class="asset-add-title">Asset</span></h4>
	            </div>
	            <form action="{{ website_bahay_admin_add_meta_url() }}" method="POST" id="addMetaForm" role="form">
	            	{!! csrf_field() !!}
		            <div class="modal-body">
						{{ form_input("text","name","","Name","form-control","Name") }}
						{{ form_input("text","slug","","Slug","form-control","Slug") }}
						<div class="form-group">
		        			<label for="parent">Parent</label>
		        			<select name="parent" class="form-control">
		        				<option value="0">Select Parent</option>
		        			</select>
						</div>
						{{ form_texteditor("description","","Add Description","form-control text-editor","Description") }}
						<div class="form-group">
							<input type="hidden" name="type" id="meta-id">
						</div>
		            </div>
		            <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        {{ form_submit("Save") }}
			      	</div>
		      	</form>
	        </div>
	    </div>
	</div>
@endsection

@section('script_footer')
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}


{{ tinymce(2) }}
<script type="text/javascript">
	$(function() {
		$(".btn-add-asset").click(function(){
			var t = $(this);
			var id = t.attr("data-type");
			var title = t.attr("data-title");
			$('#modalAddAsset').on('show.bs.modal', function () {
			  $("#asset-id").val(id);
			  $(".asset-add-title").text(title);
			});
			$('#modalAddAsset').on('hide.bs.modal', function () {
			  $("#asset-id").val("");
			  $(".asset-add-title").text("Asset");
			});
		});

		$('#addPropertyForm').on('submit', function(e) {
			e.preventDefault(e);
			$.ajax({
              type: "POST",
              url: '/website/bahay/add/property',
              data: $(this).serialize(),
              success: function(data) {
                console.log(data);
                // setTimeout(function() {window.location = data.redirect_url;}, 2000);
              }, error:function() {
                console.log("Error!");
              }
            });
		});
	});	
</script>
@endsection