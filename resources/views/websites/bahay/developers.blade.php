@extends('layouts.website.bahay.app')

@section('website_bahay_content')
	<div class="list-property">
		<div class="container">
			<h1>Developers</h1>
			<div class="row">
				@foreach($developers as $developer)
					@if($developer->hasRole('real-estate-developer') && !$developer->hasRole('super-admin') )
						<div class="col-xs-12 col-sm-4 col-lg-3">
							<div class="thumbnail">
								<img src="../../../assets/network/bahay/img/real-estate.jpg">
								<div class="caption">
									<h4><a href="{{ website_bahay_url() }}/developer/{{ str_slug($developer->name) }}">{{ $developer->username }}</a></h4>
									<p><strong>Contact No:</strong> <a href="telto: +639102596748">+639102596748</a></p>
									<p><strong>Email</strong>: <a href="mailto: developer@gmail.com">{{ $developer->email }}</a></p>
									<p><strong>Address</strong>: New York, USA</p>
									<p><strong>Description</strong>: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, </p>
								</div>
							</div>
						</div>
					@endif
				@endforeach
			</div>
		</div>
	</div>
		
@endsection
