@extends('layouts.website.bahay.app')

@section('website_bahay_content')
	<style type="text/css">
		.original-price{
		  text-decoration: line-through;
		}
	</style>
	<div class="list-property">
		<div class="container">
			<h1>Membership</h1>
			<div class="tabs">
			  	<!-- Nav tabs -->
			  	<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#home" aria-controls="monthly" role="tab" data-toggle="tab">Monthly</a></li>
			  	</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="monthly">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								{{ get_pricing_membership(1) }}
							</div>
							<div class="col-xs-12 col-sm-4">
								{{ get_pricing_membership(2) }}
							</div>
							<div class="col-xs-12 col-sm-4">
								<form>
									<h2>Agent</h2>
									<input type="hidden" name="membershipType" value="homesAgent">
									<div>
										Membership Desc
									</div>
									<p>Membership Price</p>
									<p><button type="submit" class="btn btn-primary">Upgrade Now</button></p>
								</form>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane active" id="monthly">

					</div>
				</div>

			</div>
		</div>
	</div>
		
@endsection
