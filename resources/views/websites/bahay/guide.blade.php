

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8">
						<h1>Penndave Hills Estate</h1>
						<p>Type: <a href="#">Lot Only</a></p>
						<p>Location: <a href="#">Compostela</a>, <a href="#">Cebu</a></p>
						<p>Developer: <a href="#">Psalm Property Ventures Corp.</a></p>
						<p>Payment Type: <a href="#">In-house Financing</a></p>
						<p class="label label-primary display-inblock"><span class="h2 text-bold">2800 per sqm</span></p>
						<h4>Available Lot Cutting</h4>
						<ul>
							<li>60 sqm</li>
							<li>100 sqm</li>
							<li>200 sqm</li>
							<li>400 sqm</li>
							<li>600 sqm</li>
						</ul>
						<h4>Computation</h4>
						<ul>
							<li>60 sqm x P2,800 = <strong>P168,000</strong><br>
								(P168,000 - P10,000) / 60 months = <strong>P2,633.33</strong>
							</li>
							<li>100 sqm x P2,800 = <strong>P280,000</strong><br>
								(P280,000 - P10,000) / 60 months = <strong>P4,500.00</strong>
							</li>
						</ul>
						<p><strong>Formula:</strong><br>
							A = Lot Cutting <strong>(60sqm,100sqm,200sqm,400sqm,600sqm,etc)</strong><br>
							B = Price per sqm <strong>(P2,800)</strong><br>
							C = Total Amount Payable<br>
							D = Reservation Fee <strong>(P10,000)</strong><br>
							E = # of months to pay (5 years or 60 months)<br>
							F = Monthly Amount Payable<br>
						 	<strong>A x B = C</strong><br>
							<strong>(C - D) / E = F</strong>
						</p>
						<h2>Amenities</h2>
						<ul class="nav">
							<li>Elegant Entrance</li>
							<li>Gutters and Roads</li>
							<li>Basketball Court</li>
							<li>Club House</li>
							<li>Swimming Pool</li>
							<li>Drainage System</li>
						</ul>
						<h2>Highlight</h2>
						<ul class="nav">
							<li>10 mins away from Gaisano Grand Mall Liloan</li>
							<li>15 mins away from SM Consolacion Cebu</li>
							<li>30 mins away from Cebu City</li>
						</ul>
						<div class="alert alert-info">
							Note: This is a Fast Selling Property, Call us Now!
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<h2>Real Estate Type</h2>
						<ul class="nav">
							<li><a href="#">Lot Only</a></li>
							<li><a href="#">House and Lot</a></li>
							<li><a href="#">For Rent/Lease</a></li>
							<li><a href="#">Condominiums</a></li>
						</ul>
						<h2>Payment Type/Scheme</h2>
						<ul class="nav">
							<li><a href="#">Pag-ibig</a></li>
							<li><a href="#">Bank Financing</a></li>
							<li><a href="#">In-house Financing</a></li>
						</ul>
						<h2>Tags</h2>
						<ul class="nav">
							<li><a href="#">5 years to pay</a></li>
							<li><a href="#">6 years to pay</a></li>
							<li><a href="#">No equity</a></li>
							<li><a href="#">No interest</a></li>
							<li><a href="#">no downpayments</a></li>
						</ul>
						<ul class="nav">
							<li><a href="#">Ready for occupancy (RFO)</a></li>
							<li><a href="#">Pre-selling</a></li>
							<li><a href="#">Rent to own</a></li>
							<li><a href="#">Data-data</a></li>
							<li><a href="#">Cash Only</a></li>
							<li><a href="#">Rush Sale</a></li>
							<li><a href="#">Subdivision</a></li>
							<li><a href="#">Commercial</a></li>
							<li><a href="#">Residential</a></li>
							<li><a href="#">Agricultural</a></li>
						</ul>
						<h2>Location</h2>
						<ul class="nav">
							<li><a href="#">Abra</a>
								<ul class="nav">
									<li><a href="#">Bangued</a></li>
									<li><a href="#">Boliney</a></li>
									<li><a href="#">Bucay</a></li>
									<li><a href="#">Bucloc</a></li>
									<li><a href="#">Daguioman</a></li>
									<li><a href="#">Danglas</a></li>
									<li><a href="#">Dolores</a></li>
									<li><a href="#">La Paz</a></li>
									<li><a href="#">Lacub</a></li>
									<li><a href="#">Lagangilang</a></li>
									<li><a href="#">Lagayan</a></li>
									<li><a href="#">Langiden</a></li>
									<li><a href="#">Licuan-Baay (Licuan)</a></li>
									<li><a href="#">Luba</a></li>
									<li><a href="#">Malibcong</a></li>
									<li><a href="#">Manabo</a></li>
									<li><a href="#">Peñarrubia</a></li>
									<li><a href="#">Pidigan</a></li>
									<li><a href="#">Pilar</a></li>
									<li><a href="#">Sallapadan</a></li>
									<li><a href="#">San Isidro</a></li>
									<li><a href="#">San Juan</a></li>
									<li><a href="#">San Quintin</a></li>
									<li><a href="#">Tayum</a></li>
									<li><a href="#">Tineg</a></li>
									<li><a href="#">Tubo</a></li>
									<li><a href="#">Villaviciosa</a></li>
								</ul>
							</li>
							<li><a href="#">Cebu</a>
								<ul class="nav">
									<li><a href="#">Alcantara</a></li>
									<li><a href="#">Alcoy</a></li>
									<li><a href="#">Alegria</a></li>
									<li><a href="#">Aloguinsan</a></li>
									<li><a href="#">Argao</a></li>
									<li><a href="#">Asturias</a></li>
									<li><a href="#">Badian</a></li>
									<li><a href="#">Balamban</a></li>
									<li><a href="#">Bantayan</a></li>
									<li><a href="#">Barili</a></li>
									<li><a href="#">Bogo</a></li>
									<li><a href="#">Boljoon</a></li>
									<li><a href="#">Borbon</a></li>
									<li><a href="#">Carcar</a></li>
									<li><a href="#">Carmen</a></li>
									<li><a href="#">Catmon</a></li>
									<li><a href="#">Cebu City</a></li>
									<li><a href="#">Compostela</a></li>
									<li><a href="#">Consolacion</a></li>
									<li><a href="#">Cordova</a></li>
									<li><a href="#">Daanbantayan</a></li>
									<li><a href="#">Dalaguete</a></li>
									<li><a href="#">Danao</a></li>
									<li><a href="#">Dumanjug</a></li>
									<li><a href="#">Ginatilan</a></li>
									<li><a href="#">Lapu-Lapu (Opon)</a></li>
									<li><a href="#">Liloan</a></li>
									<li><a href="#">Madridejos</a></li>
									<li><a href="#">Malabuyoc</a></li>
									<li><a href="#">Mandaue</a></li>
									<li><a href="#">Medellin</a></li>
									<li><a href="#">Minglanilla</a></li>
									<li><a href="#">Moalboal</a></li>
									<li><a href="#">Naga</a></li>
									<li><a href="#">Oslob</a></li>
									<li><a href="#">Pilar</a></li>
									<li><a href="#">Pinamungajan</a></li>
									<li><a href="#">Poro</a></li>
									<li><a href="#">Ronda</a></li>
									<li><a href="#">Samboan</a></li>
									<li><a href="#">San Fernando</a></li>
									<li><a href="#">San Francisco</a></li>
									<li><a href="#">San Remigio</a></li>
									<li><a href="#">Santa Fe</a></li>
									<li><a href="#">Santander</a></li>
									<li><a href="#">Sibonga</a></li>
									<li><a href="#">Sogod</a></li>
									<li><a href="#">Tabogon</a></li>
									<li><a href="#">Tabuelan</a></li>
									<li><a href="#">Talisay</a></li>
									<li><a href="#">Toledo</a></li>
									<li><a href="#">Tuburan</a></li>
									<li><a href="#">Tudela</a></li>	
								</ul>
							</li>
						</ul>
						<h2>Amenities</h2>
						<ul class="nav">
							<li><a href="#">Elegant Entrance</a></li>
							<li><a href="#">Gutters and Roads</a></li>
							<li><a href="#">Basketball Court</a></li>
							<li><a href="#">Club House</a></li>
							<li><a href="#">Swimming Pool</a></li>
							<li><a href="#">Drainage System</a></li>
						</ul>
						<h2>Price Range</h2>
						<ul class="nav">
							<li><a href="#">Below P1,000,000</a></li>
							<li><a href="#">P1,000,000 - P2,000,000</a></li>
							<li><a href="#">P2,000,000 - P3,000,000</a></li>
							<li><a href="#">P3,000,000 - P4,000,000</a></li>
							<li><a href="#">P4,000,000 - P5,000,000</a></li>
							<li><a href="#">Above P5,000,000</a></li>
						</ul>
						<h2>Price Range for Lot Only (per sqm)</h2>
						<ul class="nav">
							<li><a href="#">Below P1,000</a></li>
							<li><a href="#">P1,000 - P2,000</a></li>
							<li><a href="#">P2,000 - P3,000</a></li>
							<li><a href="#">P3,000 - P4,000</a></li>
							<li><a href="#">P4,000 - P5,000</a></li>
							<li><a href="#">Above P5,000</a></li>
						</ul>
						<h2>Developers</h2>
						<ul class="nav">
							<li><a href="#">Psalm Property Ventures Corp.</a></li>
							<li><a href="#">Others</a></li>
						</ul>
					</div>
				</div>
			</div>