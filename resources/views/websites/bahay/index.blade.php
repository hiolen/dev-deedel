@extends('layouts.website.bahay.app')

@section('website_bahay_content')
<div class="bahay">
	<div class="container">
		<div>
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#locations" aria-controls="locations" role="tab" data-toggle="tab">Locations</a></li>
				<li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a></li>
				<li role="presentation" class="pull-right"><a href="{{ url('website/bahay/create/property') }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Property</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="locations">							
					<ul class="nav">
						@foreach($locParents as $location)
						<li><a href="{{website_bahay_url()}}/location/{{ $location->slug }}">{{ $location->name }}</a></li>
						@endforeach
					</ul>
				</div>
				<div role="tabpanel" class="tab-pane" id="tags">
					<ul class="nav">
						@foreach($tagParents as $tag)
						<li><a href="{{website_bahay_url()}}/tag/{{ $tag->slug }}">{{ $tag->name }}</a></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		<div><strong></strong>
		</div>
		<div class="input-group col-xs-12 col-md-6"> <input class="form-control" placeholder="Search for..."> <span class="input-group-btn"> <button class="btn btn-default" type="button">Go!</button> </span> </div>
		<p><strong>Search by Tags: <em>tag name</em></p>
		</div>
	</div>
</div>
	
@endsection
