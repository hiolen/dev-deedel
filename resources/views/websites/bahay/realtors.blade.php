@extends('layouts.website.bahay.app')

@section('website_bahay_content')
	<div class="list-property">
		<div class="container">
			<h1>Realtor</h1>
			<div class="row">
				<div class="col-xs-12 col-sm-6">					
					<ul class="media-list">
						@foreach($realtors as $realtor)
							@if($realtor->hasRole('real-estate-realtor') && !$realtor->hasRole('super-admin') )
								<li class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object" src="..." alt="...">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading"><a href="{{ website_bahay_url() }}/realtor/{{ $realtor->name }}">{{ $realtor->username }}</a></h4>
										<p>Tagline</p>
										<p><a href="#">Contact Me</a></p>
									</div>
								</li>
							@endif
						@endforeach
					</ul>
				</div>
				<!-- <div class="col-xs-12 col-sm-6">					
					<ul class="media-list">
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="..." alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href="#">Realtor Name</a></h4>
								<p>Tagline</p>
								<p><a href="#">Contact Me</a></p>
							</div>
						</li>
					</ul>
				</div> -->
			</div>
		</div>
	</div>
		
@endsection
