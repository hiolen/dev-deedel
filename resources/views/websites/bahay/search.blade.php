@extends('layouts.website.bahay.app')
@section('style_css')
<link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
@endsection

@section('website_bahay_content')
	<div class="list-property">
		<div class="container">
			<div class="row">
				<h1>Advance Search</h1>
				<form id="searchForm">
					<div class="form-group col-xs-6">
						<label>Property Type</label>
						<select class="form-control" id="pType">
							@foreach($metas as $meta)
							<option value="{{$meta->name}}">{{$meta->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-xs-6">
						<label>Property Name</label>
						<input type="text" class="form-control" id="pName" placeholder="Property Name">
					</div>
					<div class="form-group col-xs-6">
						<label>Property Location</label>
						<input type="text" class="form-control" id="pLocation" placeholder="Property Location">
					</div>
					<div class="form-group col-xs-6">
						<label>Property Tags</label>
						<input type="text" class="form-control" id="pTag" placeholder="Property Tags">
					</div>
					<section>
						<p>
							<strong>Property Price Range</strong>
						</p>
						<label><input type="radio" name="pPrice" value="100000"> Below P100,000</label>
						<label><input type="radio" name="pPrice" value="200000"> Below P200,000</label>
						<label><input type="radio" name="pPrice" value="300000"> Below P300,000</label>
						<label><input type="radio" name="pPrice" value="400000"> Below P400,000</label>
						<label><input type="radio" name="pPrice" value="500000"> Below P500,000</label>
						<label><input type="radio" name="pPrice" value="600000"> Below P600,000</label>
					</section>
				</form>
				<div class="row" id="item">
					@foreach($properties as $property)
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="thumbnail">
							<h4><a href="property/{{$property->slug}}">{{ $property->name }}</a></h4>
							<p>property image</p>
						</div>
					</div>
					@endforeach
				</div>				

			</div>
		</div>
	</div>
		
@endsection

@section('script_footer')
	<script src="http://demo.expertphp.in/js/jquery.js"></script>
	<script src="http://demo.expertphp.in/js/jquery-ui.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#pType').on('change', function() {
				var optionSelected = $(this).find("option:selected");
                var prop_type = optionSelected.val();

                $.ajax({
                	type: "get",
                	url: "search",
                	datatype: "json",
                	data: {pType: prop_type, _token: '{{csrf_token()}}'},
                	success: function(data) {
                		var text = '';
                		if(data.properties) {
	                		for(var i = 0;i < data.properties.length;i++) {
	                			text += '<div class="col-xs-12 col-sm-6 col-md-4">' +
	                				'<div class="thumbnail">' + 
	                					'<h4><a href="property/' + data.properties[i].slug + '">' + data.properties[i].name + '</a></h4>' +
	                					'<p>property image</p>' +
	                				'</div></div>';
	                		}
	                		$("#item").html(text);
	                	}
                	}
                });
			});

			$('input:radio').change(function() {
				var pPrice = $("input[name='pPrice']:checked").val();
				
				$.ajax({
					type: "get",
					url: "search",
					datatype: "json",
					data: {pPrice: pPrice, _token: '{{csrf_token()}}'},
					success: function(data) {
						var text = '';
                		if(data.properties) {
	                		for(var i = 0;i < data.properties.length;i++) {
	                			text += '<div class="col-xs-12 col-sm-6 col-md-4">' +
	                				'<div class="thumbnail">' + 
	                					'<h4><a href="property/' + data.properties[i].slug + '">' + data.properties[i].name + '</a></h4>' +
	                					'<p>property image</p>' +
	                				'</div></div>';
	                		}
	                		$("#item").html(text);
	                	}
					}
				});
			});

			$('#pName').autocomplete({
				source: function(request, response) {
					var data = {
						_token: '{{csrf_token()}}',
						term: request.term,
						key: 'name'
					}
					autoAjax(response, data);
				},
        		min_length: 3,
			});

			$('#pLocation').autocomplete({
				source: function(request, response) {
					var data = {
						_token: '{{csrf_token()}}',
						term: request.term,
						key: 'name',
						type: 'location'
					}
					autoAjax(response, data);
				},
        		min_length: 3,
			});

			$('#pTag').autocomplete({
				source: function(request, response) {
					var data = {
						_token: '{{csrf_token()}}',
						term: request.term,
						key: 'name',
						type: 'tag'
					}
					autoAjax(response, data);
				},
        		min_length: 3,
			});

			function autoAjax(response, data) {
				$.ajax({
	                url: 'filter',
	                dataType: "json",
	                data: data,
	                success: function(data) {
	                    response(data);

	                   	var text = '';
	                   	if(data) {
	                		for(var i = 0;i < data.length;i++) {
	                			if(data[i].properties) {
	                				for(var x = 0;x < data[i].properties.length;x++) {
			                			text += '<div class="col-xs-12 col-sm-6 col-md-4">' +
			                				'<div class="thumbnail">' + 
			                					'<h4><a href="property/' + data[i].properties[x].slug + '">' + data[i].properties[x].name + '</a></h4>' +
			                					'<p>property image</p>' +
			                				'</div></div>';
			                		}
		                		}
	                		}
	                		$("#item").html(text);
	                	}
	                }
        		});
			}
		});
	</script>
@endsection
