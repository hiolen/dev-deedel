@extends('layouts.website.bahay.app')

@section('website_bahay_content')

<div class="container">
	<p>Name: {{ $category->name }}</p>
	<p>Description:</p>
	{!! $category->desc !!}

	@foreach ($category->properties as $property)
		<p><a href="{{ website_bahay_url() }}/category/{{ $category->slug }}/{{ $property->slug }}">{{ $property->name }}</a></p>
	@endforeach
</div>

@endsection