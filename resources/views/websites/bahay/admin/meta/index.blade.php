@extends('layouts.website.bahay.app')
 
@section('website_bahay_content')
	<div class="container">
		@if (session()->has('success_message'))
	    <div class="alert-msg">
	        <div class="alert alert-success">
	            {{ session()->get('success_message') }}
	        </div>
	    </div>
	    @endif

	    @if (session()->has('error_message'))
	    <div class="alert-msg">
	        <div class="alert alert-danger">
	            {{ session()->get('error_message') }}
	        </div>
	    </div>
	    @endif
	   
	  	<div class="meta">
			<h2>
				Categories 
				<a href="#" id="btn-add" name="btn-add" data-type="category" class="btn-add-meta"><i class="fa fa-plus"></i></a>
			</h2>
			@if($categories)
				<div class="meta-list meta-category">
			    @foreach($catParents as $category)
			    	<ul class="category{{ $category->id }}">
						<li class="row">
							<div class="col-xs-8 col-sm-8">{{ $category->name }}</div>
							<div  class="col-xs-2 col-sm-2">
								<a href="#EditMeta-{{ $category->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="EditMeta-{{ $category->id }}"><i class="fa fa-edit"></i> Edit</a>
							</div>
							<div  class="col-xs-2 col-sm-2">
								<a href="#" data-toggle="modal" data-id="{{ $category->id }}" data-name="{{ $category->name }}" data-type="{{ $category->types }}" class="btn-del-meta"><i class="fa fa-trash"></i> Delete</a>
							</div>
							<div class="collapse" id="EditMeta-{{ $category->id }}">
								<form action="#" id="editMetaForm" class="editCatForm" role="form">
									{!! csrf_field() !!}
									{{ form_input("hidden","id",$category->id,"","form-control","") }}
									{{ form_input("text","name",$category->name,"Name","form-control","Name") }}
									{{ form_input("text","slug",$category->slug,"Slug","form-control","Slug") }}
					            	<div class="form-group">
										<label for="slug">Parent</label>
										<Select class="form-control" name="parent">
											<option value="0">Select Parent</option>
											@foreach($catParents as $catParent)
												@if($category->id != $catParent->id)
							        				<option value="{{ $catParent->id }}" @if($catParent->id == $category->child) selected @endif>{{ $catParent->name }}</option>
							        			@endif
							        		@endforeach
										</Select>
									</div>
									<div class="form-group">
										<input type="hidden" name="type" value="category">
									</div>
									{{ form_texteditor("description",$category->desc,"Add details","form-control text-editor","Description") }}

									{{ form_submit("Save changes") }}
								</form>
							</div>

							<div class="col-xs-12">
								<div class="meta-list meta-parent-{{ $category->id }}">
									@foreach ($categories as $item)
										@if ($category->id == $item->parent)
										<ul class="category{{ $item->id }} list-unstyled">
											<li class="row">
												<div class="col-xs-8 col-sm-8">{{ $item->name }}</div>
												<div class="col-xs-2 col-sm-2">
													<a href="#EditMeta-{{ $item->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="EditMeta-{{ $item->id }}"><i class="fa fa-edit"></i> Edit</a>
												</div>
												<div class="col-xs-2 col-sm-2">
													<a href="#" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-type="{{ $item->types }}" class="btn-del-meta"><i class="fa fa-trash"></i> Delete</a>
												</div>
												<div class="collapse" id="EditMeta-{{ $item->id }}">
													<form action="#" id="editMetaForm" class="editCatForm" role="form">
														{!! csrf_field() !!}
														{{ form_input("hidden","id",$item->id,"","form-control","") }}
														{{ form_input("text","name",$item->name,"Name","form-control","Name") }}
														{{ form_input("text","slug",$item->slug,"Slug","form-control","Slug") }}
										            	<div class="form-group">
															<label for="slug">Parent</label>
															<Select class="form-control" name="parent">
																<option value="0">Select Parent</option>
																@foreach($catParents as $catParent)
																	@if($item->id != $catParent->id)
												        				<option value="{{ $catParent->id }}" @if($catParent->id == $item->parent) selected @endif>{{ $catParent->name }}</option>
												        			@endif
												        		@endforeach
															</Select>
														</div>
														<div class="form-group">
															<input type="hidden" name="type" value="category">
														</div>
														{{ form_texteditor("description",$item->desc,"Add details","form-control text-editor","Description") }}

														{{ form_submit("Save changes") }}
													</form>
												</div>
											</li>
										</ul>
										@endif
									@endforeach
								</div>
							</div>
						</li>
					</ul>
			    @endforeach
			    </div>
			@endif
		</div>
		
		<div class="meta">
			<h2>
				Tags 
				<a href="#" id="btn-add" name="btn-add" data-type="tag" class="btn-add-meta"><i class="fa fa-plus"></i></a>
			</h2>
			@if($tags)
				<div class="meta-list meta-tag">
			    @foreach($tagParents as $tag)
			    	<ul class="tag{{ $tag->id }}">
						<li class="row">
							<div class="col col-sm-8">{{ $tag->name }}</div>
							<div class="col col-sm-2">
								<a href="#EditMeta-{{ $tag->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="EditMeta-{{ $tag->id }}"><i class="fa fa-edit"></i> Edit</a>
							</div>
							<div class="col col-sm-2">
								<a href="#" data-toggle="modal" data-id="{{ $tag->id }}" data-name="{{ $tag->name }}" data-type="{{ $tag->types }}" class="btn-del-meta"><i class="fa fa-trash"></i> Delete</a>
							</div>
							<div class="collapse" id="EditMeta-{{ $tag->id }}">
								<form action="#" id="editMetaForm" class="editTagForm" role="form">
									{!! csrf_field() !!}
									{{ form_input("hidden","id",$tag->id,"","form-control","") }}
									{{ form_input("text","name",$tag->name,"Name","form-control","Name") }}
									{{ form_input("text","slug",$tag->slug,"Slug","form-control","Slug") }}
					            	<div class="form-group">
										<label for="slug">Parent</label>
										<Select class="form-control" name="parent">
											<option value="0">Select Parent</option>
											@foreach($tagParents as $tagParent)
												@if($tag->id != $tagParent->id)
							        				<option value="{{ $tagParent->id }}" @if($tagParent->id == $tag->child) selected @endif>{{ $tagParent->name }}</option>
							        			@endif
							        		@endforeach
										</Select>
									</div>
									<div class="form-group">
										<input type="hidden" name="type" value="tag">
									</div>
									{{ form_texteditor("description",$tag->desc,"Add details","form-control text-editor","Description") }}

									{{ form_submit("Save changes") }}
								</form>
							</div>

							<div class="col-xs-12">
								<div class="meta-list meta-parent-{{ $tag->id }}">
									@foreach ($tags as $item)
										@if ($tag->id == $item->parent)
										<ul class="tag{{ $item->id }}">
											<li class="row">
												<div class="col-xs-8 col-sm-8">
													<a href="{{website_bahay_url()}}/location/{{ $item->slug }}">{{ $item->name }}</a>
												</div>
												<div class="col-xs-2 col-sm-2">
													<a href="#EditMeta-{{ $item->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="EditMeta-{{ $item->id }}"><i class="fa fa-edit"></i> Edit</a>
												</div>
												<div class="col-xs-2 col-sm-2">
													<a href="#" data-toggle="modal" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-type="{{ $item->types }}" class="btn-del-meta"><i class="fa fa-trash"></i> Delete</a>
												</div>
												<div class="collapse" id="EditMeta-{{ $item->id }}">
													<form action="#" id="editMetaForm" class="editCatForm" role="form">
														{!! csrf_field() !!}
														{{ form_input("hidden","id",$item->id,"","form-control","") }}
														{{ form_input("text","name",$item->name,"Name","form-control","Name") }}
														{{ form_input("text","slug",$item->slug,"Slug","form-control","Slug") }}
										            	<div class="form-group">
															<label for="slug">Parent</label>
															<Select class="form-control" name="parent">
																<option value="0">Select Parent</option>
																@foreach($tagParents as $tagParent)
																	@if($item->id != $tagParent->id)
												        				<option value="{{ $tagParent->id }}" @if($tagParent->id == $item->parent) selected @endif>{{ $tagParent->name }}</option>
												        			@endif
												        		@endforeach
															</Select>
														</div>
														<div class="form-group">
															<input type="hidden" name="type" value="category">
														</div>
														{{ form_texteditor("description",$item->desc,"Add details","form-control text-editor","Description") }}

														{{ form_submit("Save changes") }}
													</form>
												</div>
											</li>
										</ul>
										@endif
									@endforeach
								</div>
							</div>
						</li>
					</ul>
			    @endforeach
			    </div>
			@endif
		</div>

		<div class="meta">
			<h2>
				Locations 
				<a href="#" id="btn-add" name="btn-add" data-type="location" class="btn-add-meta"><i class="fa fa-plus"></i></a>
			</h2>
			@if($locations)
				<div class="meta-list meta-location">
			    @foreach($locParents as $location)
			    	<ul class="location{{ $location->id }}">
						<li class="row">
							<div class="col-xs-8 col-sm-8">{{ $location->name }}</div>
							<div class="col-xs-2 col-sm-2">
								<a href="#EditMeta-{{ $location->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="EditMeta-{{ $location->id }}"><i class="fa fa-edit"></i> Edit</a>
							</div>
							<div class="col-xs-2 col-sm-2">
								<a href="#" data-toggle="modal" data-id="{{ $location->id }}" data-name="{{ $location->name }}" data-type="{{ $location->types }}" class="btn-del-meta"><i class="fa fa-trash"></i> Delete</a>
							</div>
							<div class="collapse" id="EditMeta-{{ $location->id }}">
								<form action="#" id="editMetaForm" class="editLocForm" role="form">
									{!! csrf_field() !!}
									{{ form_input("hidden","id",$location->id,"","form-control","") }}
									{{ form_input("text","name",$location->name,"Name","form-control","Name") }}
									{{ form_input("text","slug",$location->slug,"Slug","form-control","Slug") }}
					            	<div class="form-group">
										<label for="slug">Parent</label>
										<Select class="form-control" name="parent">
											<option value="0">Select Parent</option>
											@foreach($locParents as $locParent)
												@if($location->id != $locParent->id)
							        				<option value="{{ $locParent->id }}" @if($locParent->id == $location->child) selected @endif>{{ $locParent->name }}</option>
							        			@endif
							        		@endforeach
										</Select>
									</div>
									<div class="form-group">
										<input type="hidden" name="type" value="location">
									</div>
									{{ form_texteditor("description",$location->desc,"Add details","form-control text-editor","Description") }}

									{{ form_submit("Save changes") }}
								</form>
							</div>
							<div class="col-xs-12">
								<div class="meta-list meta-parent-{{ $location->id }}">
									@foreach ($locations as $item)
										@if ($location->id == $item->parent)
										<ul class="location{{ $item->id }}">
											<li class="row">
												<div class="col-xs-8 col-sm-8">{{ $item->name }}</div>
												<div class="col-xs-2 col-sm-2">
													<a href="#EditMeta-{{ $item->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="EditMeta-{{ $item->id }}"><i class="fa fa-edit"></i> Edit</a>
												</div>
												<div class="col-xs-2 col-sm-2">
													<a href="#" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-type="{{ $item->types }}" class="btn-del-meta"><i class="fa fa-trash"></i> Delete</a>
												</div>
												<div class="collapse" id="EditMeta-{{ $item->id }}">
													<form action="#" id="editMetaForm" class="editCatForm" role="form">
														{!! csrf_field() !!}
														{{ form_input("hidden","id",$item->id,"","form-control","") }}
														{{ form_input("text","name",$item->name,"Name","form-control","Name") }}
														{{ form_input("text","slug",$item->slug,"Slug","form-control","Slug") }}
										            	<div class="form-group">
															<label for="slug">Parent</label>
															<Select class="form-control" name="parent">
																<option value="0">Select Parent</option>
																@foreach($locParents as $locParent)
																	@if($item->id != $locParent->id)
												        				<option value="{{ $locParent->id }}" @if($locParent->id == $item->parent) selected @endif>{{ $locParent->name }}</option>
												        			@endif
												        		@endforeach
															</Select>
														</div>
														<div class="form-group">
															<input type="hidden" name="type" value="category">
														</div>
														{{ form_texteditor("description",$item->desc,"Add details","form-control text-editor","Description") }}

														{{ form_submit("Save changes") }}
													</form>
												</div>
											</li>
										</ul>
										@endif
									@endforeach
								</div>
							</div>
						</li>
					</ul>
			    @endforeach
			    </div>
			@endif
		</div>

	</div>
@endsection

@section('modal')
	<!-- Add/Edit Meta Modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="modalAddEditMetaLabel">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="modalAddMetaLabel">Add <span class="meta-add-title">Meta</span></h4>
	            </div>
	            <form action="#" id="formMeta" role="form">
	            	{!! csrf_field() !!}
		            <div class="modal-body">
						{{ form_input("text","name","","Name","form-control","Name") }}
						{{ form_input("text","slug","","Slug","form-control","Slug") }}
						<div class="form-group">
		        			<label for="parent">Parent</label>
		        			<div class="parent"></div>
						</div>
						{{ form_texteditor("description","","Add Description","form-control text-editor","Description") }}
						<div class="form-group">
							<input type="hidden" name="type" id="meta-id">
						</div>
		            </div>
		            <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="meta_id" name="meta_id" value="0">
			      	</div>
		      	</form>
	        </div>
	    </div>
	</div>

	<!-- Delete Meta Modal -->
	<div class="modal fade modalDelMeta" tabindex="-1" role="dialog" id="modalDelMeta" aria-labelledby="modalDelMetaLabel">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="modalDelMetaLabel">Delete <span class="meta-del-title">Meta</span></h4>
	            </div>
	            <form action="{{ website_bahay_admin_delete_meta_url() }}" method="POST" id="addMetaForm" role="form">
	            	{!! csrf_field() !!}
					<input type="hidden" name="_method" value="DELETE">
		            <div class="modal-body">
						Are you sure you want to delete this <span class="dname"></span>? 
						<span class="hidden did"></span>
						<span class="hidden dtype"></span>
		            </div>
		            <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        {{ form_del_submit("Delete") }}
			      	</div>
		      	</form>
	        </div>
	    </div>
	</div>
@endsection

@section('script_footer')
	{{ tinymce(1) }}

	<!-- Jquery Validate -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

	<!-- Script for meta -->
	<script type="text/javascript">
		$(function() {
			//display modal form for creating new meta
            $('.btn-add-meta').click(function (e){
            	e.preventDefault();
                $('#btn-save').val("add");
                $("#meta-id").val($(this).attr("data-type"));
                $('#formMeta').trigger("reset");
                $('#myModal').modal('show');
            });

            //create new Meta
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var meta_id = $('#meta_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'meta';
                var dataType = $("#meta-id").val();
                var append = ".meta-" + dataType;

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + meta_id;
                }

                if ($('#formMeta').valid()) {
	                $.ajax({
	                    type: type,
	                    url: my_url,
	                    data: $('#formMeta').serialize(),
	                    dataType: 'json',
	                    success: function (data) {
	                        console.log(data);
	                        setTimeout(function() {window.location = data.redirect_url;}, 500);
	                    },
	                    error: function (data) {
	                        console.log('Error:', data);
	                    }
	                });
				}else
                    return false;
            });            

			// Edit Meta
			$('#editMetaForm').on('submit', function(e) {
				e.preventDefault(e);

				if ($(this).valid()) {
					$.ajax({
						type: "POST",
						url: 'meta/edit',
						data: $(this).serialize(),
						success: function(data) {
							console.log(data);
							setTimeout(function() {window.location = data.redirect_url;}, 500);
						}, error: function() {
							console.log("Error!");
						}
					});
				} else {
			        return false;
			    }
			});
	
			// DELETE Meta Ajax
			$('.btn-del-meta').on('click', function() {
				$('.dname').html($(this).data('name'));
				$('.did').text($(this).data('id'));
				$('.dtype').text($(this).data('type'));
				$('#modalDelMeta').modal('show');
			});

			$('.modal-footer').on('click', '.delete', function(e) {
				e.preventDefault(e);

				$('#formMeta').trigger("reset");
				var data = {
		            _token: $('input[name=_token]').val(),
		            id: $('.did').text()
		        }

		        $.ajax({
		            type: 'post',
		            url: 'meta/delete',
		            data: data,
		            success: function(data) {
		                $('.' + $('.dtype').text() + $('.did').text()).remove();
		                $('#modalDelMeta').modal('hide');
		            }, error:function() {
		            	console.log("Error!");
		            }
		        });
		    });

		    // get parent for each meta
			$(".btn-add-meta").click(function(){
				var parents = [];
				var t = $(this);
				var id = t.attr("data-type");
				var title = t.attr("data-title");

				$('#myModal').on('show.bs.modal', function () {
				  $("#meta-id").val(id);
				  $(".meta-add-title").text(title);
				});

				$('#myModal').on('hide.bs.modal', function () {
				  $("#meta-id").val("");
				  $(".meta-add-title").text("Meta");
				});

				if(id == 'category'){
					$(".parent").html(
						'<select name="parent" class="form-control">' +						
			        		'<option value="0">Select Parent</option>' +
			        		@foreach($catParents as $catParent)
			        			'<option value="{{ $catParent->id }}">{{ $catParent->name }}</option>' +
			        		@endforeach
			        	'</select>'
			        );
			    }else if(id == 'tag') {
			    	$(".parent").html(
						'<select name="parent" class="form-control">' +						
			        		'<option value="0">Select Parent</option>' +
			        		@foreach($tagParents as $tagParent)
			        			'<option value="{{ $tagParent->id }}">{{ $tagParent->name }}</option>' +
			        		@endforeach
			        	'</select>'
			        );
				}else if(id == 'location') {
			    	$(".parent").html(
						'<select name="parent" class="form-control">' +						
			        		'<option value="0">Select Parent</option>' +
			        		@foreach($locParents as $locParent)
			        			'<option value="{{ $locParent->id }}">{{ $locParent->name }}</option>' +
			        		@endforeach
			        	'</select>'
			        );
				}else {
					$("div.parent").empty();
				}
			});

			// Validate Add Meta Form
		    $('#formMeta').validate({
		        rules: {
		            name: {
		                minlength: 3,
		                maxlength: 55,
		                required: true
		            },

		        },
		        highlight: function(element) {
		            $(element).closest('.form-group').addClass('has-error');
		        },
		        unhighlight: function(element) {
		            $(element).closest('.form-group').removeClass('has-error');
		        },
		        errorElement: 'span',
		        errorClass: 'help-block',
		        errorPlacement: function(error, element) {
		            if (element.parent('.input-group').length) {
		                error.insertAfter(element.parent());
		            } else {
		                error.insertAfter(element);
		            }
		        },
		        submitHandler: function(form) {
		        	// leave it blank here.
				}
		    });
			

			// Validate Edit Meta Form
		    $('#editMetaForm').validate({
		        rules: {
		            name: {
		                minlength: 3,
		                maxlength: 55,
		                required: true
		            }
		        },
		        highlight: function(element) {
		            $(element).closest('.form-group').addClass('has-error');
		        },
		        unhighlight: function(element) {
		            $(element).closest('.form-group').removeClass('has-error');
		        },
		        errorElement: 'span',
		        errorClass: 'help-block',
		        errorPlacement: function(error, element) {
		            if (element.parent('.input-group').length) {
		                error.insertAfter(element.parent());
		            } else {
		                error.insertAfter(element);
		            }
		        },
		        submitHandler: function(form) {
		        	// leave it blank here.
				}
		    });
		});
	</script>
@endsection