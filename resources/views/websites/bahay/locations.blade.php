@extends('layouts.website.bahay.app')

@section('website_bahay_content')
<div class="location">
	<div class="container">
		<h4>Browse Locations</h4>
		<div class="row">
	  		<div class="col-xs-12 col-sm-6 col-md-4">
	    		<div class="input-group">
	     			<input type="text" class="form-control" placeholder="Search for...">
	  				<span class="input-group-btn">
	    				<button class="btn btn-default" type="button">Search</button>
	  				</span>
	    		</div>
	  		</div>
	  		<div class="col-xs-12 col-sm-6 col-md-8">&nbsp;</div>
		</div>
		<div class="row">
			@foreach ($locParents as $location)
			<div class="col-xs-12 col-sm-6 col-md-4">
				<h3 class=""><a href="{{website_bahay_url()}}/location/{{ $location->slug }}">{{ $location->name }}</a></h3>
				<ul>
					@foreach ($locations as $item)
						@if ($location->id == $item->parent)
						<li><a href="{{website_bahay_url()}}/location/{{ $item->slug }}">{{ $item->name }}</a></a></li>
						@endif
					@endforeach
				</ul>
			</div>
			@endforeach	
		</div>
	</div>	
</div>	
@endsection
