@extends('layouts.front.app')

@section('content_front')
<div class="banner">
	<div class="container">
		<nav>
			<ul class="nav navbar-nav navbar-right">
                <li><a href="{{ support_url() }}">Support Center</a></li>
				<li><a href="#">Login</a></li>
				<li><a href="#">Register</a></li>
			</ul>
		</nav>
		<div class="banner-content">
			<h1 class="site-name">Deedel</h1>
			<h2>You can do almost everything and earn</h2>
			<button class="btn-started btn-lg">GET STARTED</button>
		</div>
	</div>
</div>
<header class="header-fixed">
	<div class="container">
		<a href="#" class="logo">
			<img src="assets/img/logo.png" alt="Deedel">
		</a>
		<nav class="nav">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{ websites_url() }}">Network</a></li>
				<li><a href="#">Earn More</a></li>
				<li><a href="#">Sign In</a></li>
			</ul>
		</nav>
	</div>	
</header>
<div class="h-header"></div>
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<img class="img-responsive" src="assets/img/real-estate.jpg" alt="">
			</div>
			<div class="col-xs-12 col-sm-6">
				<h2>Lorem ipsum</h4>
				<h4>Cum sociis natoque penatibus et magnis dis parturient montes, </h5>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate</p>
			</div>
		</div>
	</div>
</div>
<div class="section-reward">
	<div class="container">
		<h2 class="h1">Earn Points To Get Rewards</h2>
		<p>
			<a href="/reward" class="btn btn-lg btn-reward">Rewards</a>
		</p>
	</div> 
</div>
<div class="section section-popular">
	<div class="container">
		<h2 class="text-center">Popular in our Network</h2>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/booklulu.jpg" alt="booklulu">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_booklulu_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>	
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/deedel-homes.jpg" alt="deedel-homes">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_bahay_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/trabaho.jpg" alt="trabaho">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_trabaho_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/renta.jpg" alt="renta">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_rent_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/renta.jpg" alt="renta">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_rent_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/trabaho.jpg" alt="trabaho">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_trabaho_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/booklulu.jpg" alt="booklulu">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_booklulu_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="thumbnail pop-website">
					<img src="assets/img/deedel-homes.jpg" alt="deedel-homes">
					<div class="caption text-right">
						<div class="caption-text">
							<p>
								<a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
								<a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
								<a href="{{ website_bahay_url() }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer">
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="#"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-cc-amex" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-cc-paypal" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-cc-discover" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
		</ul>
	</div>
</div>
@endsection
