@extends('network.deedel.inc.app')

@section('content_deedel')
<div class="container">   
    <meta name="csrf-token" content="{{ csrf_token() }}" /> 
    <h2>Terms & Definition</h2>
    <p>Network
    <select name="webName" id="network">
        @foreach($websites as $website)
    	<option value="{{$website->id}}">{{$website->name}}({{$website->domain}})</option>
        @endforeach
    </select>
    <!-- when all terms are displayed -->
    <ul id="termAll">
        @foreach($terms as $term)
            <li>
                <h4>{{$term->name}}</h4>
                <div class="">
                    {{$term->desc}}
                </div>
                <p><a href="{{$term->website->type}}://www.{{$term->website->domain}}">{{$term->website->name}}</a></p>
            </li>
        @endforeach
    </ul>
    <!-- when all terms are by network -->
    <div id="output"></div>
    <!-- add pagination, display 2 definition per page -->
</div>
@endsection

@section('script_footer')
    <script type="text/javascript">
        $(function() {
            $('#network').on('change', function(e) {
                e.preventDefault(e);  
                    $.ajax({
                    type: "POST",
                    url: "/terms-definition/arr",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType : "html",
                    data: { selectedNetwork: $('#network').val() },
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        $("#termAll").hide();
                        var str = "<table class='table'>";
                        $.each( obj, function( key, value ) {
                            str = str + "<tr><td>" + value.name + "</td><td>" + value.desc + "</td><td>" + value.status + "</td></tr>";
                        });
                        str = str + "</table>";
                        $("#output").html(str);
                     }
                });           
            });
        });
    </script>

@endsection