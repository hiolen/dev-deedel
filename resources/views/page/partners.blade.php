@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <ul class="nav">
                <li><a href="#">Our Partners</a></li>
                <li><a href="#">Freelancers</a></li>
                <li><a href="#">Agencies</a></li>
                <li><a href="#">Affiliates</a></li>
            </ul>            
        </div>
        <div class="col-xs-12 col-sm-8">
            <h1>DeeDel Digital Partners</h1>
            <p>We partner with the best designers, developers and marketers in the world to help our merchants succeed.</p>
            <section id="freelancers">
                <h2>Freelancers</h2>
                <p>Earn commisions in every successful purchased from our marketplaces.</p>
            </section>
            <section id="agencies">
                <h2>Agencies</h2>
                <p>Earn commisions from all clients purchase.</p>
            </section>
            <section id="affiliates">
                <h2>Affiliate Partners</h2>
                <p>Earn commissions of up to a 30% of the coins/product purchased.</p>
            </section>
        </div>
    </div>
</div>
@endsection
