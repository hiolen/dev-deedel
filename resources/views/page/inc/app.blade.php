@include('page.inc.htmlhead')
<body class="page">
	<div class="page-wrapper">
		@include('page.inc.header')
	    @yield('content_page')
		@include('page.inc.footer')
	</div>
	@yield('modal_footer_page')
	@yield('script_footer_page')
</body>
</html>
