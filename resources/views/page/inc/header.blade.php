    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ deedel_url() }}">
                    DeeDel
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ websites_url() }}">Network</a></li>
                    <li><a href="{{ coins_url() }}">Buy Coins</a></li>
                    <li class="dropdown">
                        <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            More
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="{{ about_url() }}">About</a></li>
                            <li><a href="{{ contact_url() }}">Contact Us</a></li>
                            <li><a href="{{ faq_url() }}">FAQ</a></li>
                            <li><a href="{{ terms_definition_url() }}">Terms & Definition</a></li>
                            <li><a href="{{ blog_url() }}">Blog</a></li>
                            <li><a href="{{ support_url() }}">Support Center</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="#modalLogin" data-toggle="modal">Login</a></li>
                        <li><a href="#modalRegister" data-toggle="modal">Register</a></li>
                    @else
                        <li><a href="{{ account_wallet_url() }}"><i class="fa fa-money"></i></a></li>
                        <li><a href="{{ cart_url() }}"><i class="fa fa-shopping-cart"></i> ({{ Cart::instance('main')->count(false) }})</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                            @role('super-admin')
                                <li><a href="{{ super_admin_url() }}"><i class="fa fa-money"></i> SuperAdmin Dashboard</a></li>
                            @endrole
                                <li><a href="{{ account_url() }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{ account_network_url() }}"><i class="fa fa-dashboard"></i> My Network</a></li>
                                <li><a href="{{ account_url() }}/profile"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="{{ account_url() }}/settings"><i class="fa fa-gear"></i> Settings</a></li>
                                <li><a href="{{ logout_url() }}"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>