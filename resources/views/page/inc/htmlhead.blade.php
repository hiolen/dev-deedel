<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>DeeDel</title>

	{{ deedel_bootstrap_css() }}
	{{ deedel_css() }}
	{{ deedel_jquery_js() }}
	{{ deedel_bootstrap_js() }}
	{{ deedel_jquery_validate_js() }}

	@yield('script_header_page')
</head>