<?php
	if($page){
	    $page_template = $page->template;
	    if($page_template){
	    	$target = 'page.templates.'.$page_template;	
?>
	@if(View::exists($target))
	    @include($target)
	@else
	    @include('page.templates.index')
	@endif
<?php
	    } else {
?>
@include('page.inc.htmlhead')
<body class="page">
	<div class="page-wrapper">
		@include('page.inc.header')
		<div class="container">    
			<h2 class="page-title">{{ $page->title }}</h2>
			<div class="page-content">
			{{ $page->content }}
			</div>
		</div>
		@include('page.inc.footer')
	</div>
	@yield('modal_footer_page')
	@yield('script_footer_page')
</body>
</html>

<?php
	    }
	}
?>