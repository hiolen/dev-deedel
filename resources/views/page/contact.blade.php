@extends('network.deedel.inc.app')

@section('content_deedel')
<div class="container">
    @if (session()->has('success_message'))
    <div class="alert-msg">
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    </div>
    @endif

    @if (session()->has('error_message'))
    <div class="alert-msg">
        <div class="alert alert-danger">
            {{ session()->get('error_message') }}
        </div>
    </div>
    @endif

    <div class="contact-page">
        <h1>CONTACT</h1>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="admin-contact">
                    <img src="assets/img/deedel-logo.png" alt="Deedel Logo" class="img-responsive">
                    <div class="admin-info">
                        <h2><strong>Deedel Digital</strong></h2>
                        <span>
                            <a href="mailto: info@deedel.com" target="_blank">info@deedel.com</a> <br> 
                            <a href="telto: 032 261 2603">032 261 2603</a> <br>
                            <a href="#"><strong>Chat</strong>: @deedel</a>
                        </span>
                        <p class="h4">Hi, there! Thanks for dropping by.<br> Why don’t you send me a message so we can catch up?</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="contact-us">
                    <form action="#" class="form-horizontal" id="sendContact" method="post" autocomplete="off">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="name">Name</label> 
                            <input type="text" name="name" class="form-control">    
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label> 
                            <input type="email" name="email" class="form-control">    
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label> 
                            <input type="text" name="subject" class="form-control">    
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label> 
                            <textarea name="message" placeholder="Leave Your Message Here!" class="form-control"></textarea>    
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="form-cotrol btn btn-success btn-lg">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('script_footer_deedel')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(function() {
            // Validate Contact Form
            $('#sendContact').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    email: {
                        required: true
                    },
                    subject: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    message: {
                        required: true
                    }
                },
                messages: {
                    name: {
                        minlength: "Your Name must be at least 3 characters",
                        maxlength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name",
                    },
                    email: {
                        required: "Please provide your Name",
                    },
                    subject: {
                        minlength: "Your Subject must be at least 3 characters",
                        maxlength: "Your Subject must be at least 55 characters",
                        required: "Please provide your Subject",
                    },
                    message: {
                        required: "Please provide your Message",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });

            // send email contact
            $('#sendContact').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: "{{ url('/contact') }}",
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 100);
                      }, error:function() {
                        console.log("Error!");
                      }
                    }); 
                } else {
                    return false;
                }
            });
        });
    </script>
@endsection