@extends('layouts.app')

@section('content')

<div class="container">
<h2>Market your products and Services</h2>
<ul class="nav">
	<li><a href="/website/booklulu">Booklulu</a></li>
	<li><a href="/website/bahay">Real Estate</a></li>
	<li><a href="">Renta</a></li>
	<li><a href="">Lugar</a></li>
	<li><a href="">cebuzzz.com</a></li>
</ul>

<h2>Share your thoughts</h2>
<ul class="nav">
	<li><a href="">My Books.press</a></li>
	<li><a href="">reviewsbook.xyz</a></li>
	<li><a href="">Negosyo.pw</a></li>
</ul>

<h2>Hire the best contractor for your amazing projects!</h2>
<ul class="nav">
	<li><a href="/website/trabaho">Trabaho</a></li>
</ul>

<h2>Buy products and services & Eshop</h2>
<ul class="nav">
	<li><a href="">tindahan</a></li>
	<li><a href="">illustrationest.com</a></li>
	<li><a href="">themeswapper.com</a></li>
</ul>

<h2>Become a helping hand for the people in need.</h2>
<ul class="nav">
	<li><a href="">Bundle of joy</a></li>
	<li><a href="">Talento.pw</a></li>
	<li><a href="">Edukasyon</a></li>
</ul>

<h2>Exploring amazing places on earth.</h2>
<ul class="nav">
	<li><a href="">Torista</a></li>
	<li><a href="">Cebuzzz</a></li>
	<li><a href="">Locally</a></li>
</ul>
@endsection
