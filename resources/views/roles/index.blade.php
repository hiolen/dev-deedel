@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Role Management</h2>
	        </div>
	        <div class="pull-right">
	        	@permission('role-create')
	            <button id="btn-add" name="btn-add" class="btn btn-success btn-xs">Create New Role <i class="fa fa-plus"></i></button>
	            @endpermission
	        </div>
	    </div>
	</div>
	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th width="280px">Action</th>
			</tr>
		</thead>
		<tbody id="role-list" name="role-list">
			@foreach ($roles as $key => $role)
			<tr class="item{{ $role->id }}">
				<td>{{ $role->display_name }}</td>
				<td>{{ $role->description }}</td>
				<td>
					@permission('role-edit')
					<button class="btn btn-info btn-xs btn-detail open-modal" value="{{ $role->id }}"><i class="fa fa-files-o"></i> Edit</button>
					@endpermission
					@permission('role-delete')
					<a href="#modalDelRole" data-toggle="modal" data-id="{{ $role->id }}" data-name="{{ $role->name }}" class="btn-del-role btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
		        	@endpermission
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{!! $roles->render() !!}
</div>
@endsection

@section('modal')
	<!-- Add/Edit Role Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="modalRoleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalRoleLabel">Role <span class="role-title">Editor</span></h4>
                </div>
                <form action="#" method="POST" id="formRoles" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" name="name" placeholder="Name" class="form-control" id="name" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Display Name:</strong>
                            <input type="text" name="display_name" placeholder="Display Name" class="form-control" id="display_name" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            <textarea name="description" placelholder="Description" class="form-control" id="description" value=""></textarea>
                        </div>
                        <div class="form-group">
                        	<strong>Permission:</strong><br />
                        	@foreach($permissions as $permission)
                        		<label>
                        			<input type="checkbox" name="permissions[]" class="name" value="{{ $permission->id }}" />
                        			{{ $permission->display_name }}
                        		</label><br />
                        	@endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="role_id" name="role_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Role Modal -->
    <div class="modal fade modalDelRole" tabindex="-1" role="dialog" id="modalDelRole" aria-labelledby="modalDelRoleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelRoleLabel">Delete <span class="role-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delRoleForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer')
	<!-- Jquery Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

    <!-- script for role -->
    <script type="text/javascript">
    	$(function() {
    		//display modal form for creating new task
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#formRoles').trigger("reset");
                $('#myModal').modal('show');
            });

            // Edit User
            $('.open-modal').click(function() {
                var role_id = $(this).val();

                $.get('role' + '/' + role_id, function (data) {
                    //success data
                    console.log(data);
                    $('#role_id').val(data.id);
                    $('#name').val(data.name);
                    $('#display_name').val(data.display_name);
                    $('#description').val(data.description);
                    $('#btn-save').val("update");

                    $('#myModal').modal('show');
                }); 
            });

            //create new task / update existing task
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var role_id = $('#role_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'role';

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + role_id;
                }

           		if ($('#formRoles').valid()) {
	                $.ajax({
	                    type: type,
	                    url: my_url,
	                    data: $('#formRoles').serialize(),
	                    dataType: 'json',
	                    success: function (data) {
	                        console.log(data);

	                        var role = '<tr class="item' + data.role.id + '"><td>' + data.role.display_name + '</td><td>' + data.role.description + '</td>';
	                        role += '<td><button class="btn btn-info btn-xs btn-detail open-modal" value="' + data.role.id + '"><i class="fa fa-files-o"></i> Edit</button>&nbsp;';
	                        role += '<a href="#modalDelRole" data-toggle="modal" data-id="' + data.role.id + '" data-name="' + data.role.name + '" class="btn-del-role btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a></td></tr>';

	                        if (state == "add"){ //if role added a new record
	                            $('#role-list').prepend(role);
	                        }else{ //if role updated an existing record
	                            $(".item" + role_id).replaceWith( role );
	                        }

	                        $('#formRoles').trigger("reset");

	                        $('#myModal').modal('hide')
	                    },
	                    error: function (data) {
	                        console.log('Error:', data);
	                    }
	                });
	            }else
                    return false;               

            });

            // DELETE Role Ajax
            $('.btn-del-role').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);
                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: 'role/delete',
                    data: data,
                    success: function(data) {
                        console.log(data);
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelRole').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

			// Validate Add Role Form
            $('#formRoles').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    display_name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
    	});
    </script>
@endsection