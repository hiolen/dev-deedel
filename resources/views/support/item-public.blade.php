@extends('support.layout.app')

@section('content_support')
<div class="support supCat-item">
    <div class="support-banner text-center">
        <div class="container">
            <h2><strong>Find helpful articles</strong></h2>
            @include('support.layout.search')
            @include('support.layout.banner')
        </div>
    </div>
    <div class="section">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="">Support Home</a></li>
                <li><a href="">Category Name</a></li>
                <li>Article Title</li>
            </ul>
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <div class="content">
                        <p><a href="domain">network name</a></p>
                        <h3>Articles title</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula leo ac mollis dapibus. Pellentesque vitae maximus nunc. Duis ante est, tempus vel nisl non, blandit mollis enim. Etiam vitae mi id dui dignissim pulvinar. Nunc mollis blandit lacus quis aliquet. Sed lobortis imperdiet massa, nec feugiat ligula pulvinar ac. Praesent commodo molestie lacus. Morbi placerat ornare orci eget blandit. Aenean vel accumsan mi. Etiam quis risus ligula. Praesent imperdiet bibendum risus, id venenatis turpis hendrerit vitae. Sed luctus purus lacus, vel finibus est ultricies sed. Proin eget turpis turpis.</p>
                        <p>Praesent ut tortor ut dui ullamcorper vulputate at vel enim. Cras scelerisque nibh sed posuere molestie. Curabitur eleifend ultrices tristique. Vestibulum pharetra est non efficitur viverra. Curabitur ac sem lectus. Vestibulum vitae faucibus augue, vel commodo quam. Etiam lobortis maximus sapien vitae auctor.</p>
                        <p>Duis sit amet eros eu tellus placerat dignissim. Nullam commodo eros vitae sapien tristique, et imperdiet neque placerat. Praesent dignissim ante arcu, sit amet venenatis ligula vestibulum sit amet. Nam a arcu sit amet ex maximus porta. Pellentesque metus mauris, rutrum ut tellus nec, condimentum fringilla felis. Vestibulum accumsan tortor metus, ac sollicitudin turpis elementum id. Sed faucibus ac turpis in laoreet. Pellentesque vel gravida nibh, non consequat risus. Nunc condimentum sem a cursus ultrices. Nam eleifend vel tellus eget consequat. Cras finibus, mi et blandit porttitor, justo nisi dapibus diam, quis dictum est elit in ligula. Sed nunc urna, blandit et tortor at, sodales blandit turpis. Curabitur ac lectus sit amet ligula egestas hendrerit a at velit. Integer ut maximus neque, nec condimentum tortor. Vestibulum luctus scelerisque augue vitae lobortis. Praesent id mattis justo, vestibulum suscipit lectus.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="support-sidebar">
                        <h3><strong>Category</strong></h3>
                        @include('support.layout.categories')
                    </div>
                </div>
            </div>
            <footer class="supItem-footer">
                Is this helpful to you? <a href="">Yes</a> | <a href="">No</a>
            </footer>
        </div>
    </div>
</div>
@endsection
