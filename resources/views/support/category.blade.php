@extends('support.layout.app')

@section('content_support')
<div class="support support-category">
    <div class="support-banner text-center">
        <div class="container">
            <h2><strong>Find helpful articles</strong></h2>
            @include('support.layout.search')
            @include('support.layout.banner')
        </div>
    </div>
    <div class="section">
        <div class="container">
             <ul class="breadcrumb">
                <li><a href="">Support Home</a></li>
                <li><a href="">Category Name</a></li>
            </ul>
            <h3 class="supCat-name">Category Name Articles</h3>
            <div class="row">
                <div class="col-xs-2 col-sm-9">
                       
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <ul class="sup-cat">
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <ul class="sup-cat">
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                    <li>
                                        <h4><a href="">Can I pay using money transfer/remittances</a></h4>
                                        <a href="">Payment</a>
                                        <p>do you want to pay using western union, etc?</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="support-sidebar">
                        <h3><strong>Category</strong></h3>
                        @include('support.layout.categories')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
