<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DeeDel</title>
    {{ icons() }}
    {{ global_css() }}
    {{ network_style_css() }}
    {{ message_style_css() }}
    {{ support_style_css() }}
    {{ support_cat_style_css() }}
    {{ support_ticket_style_css() }}

    <?php $url_jquery = URL::asset('assets/js/jquery.min.js'); ?>
    <?php $url_bootstrap = URL::asset('assets/js/bootstrap.min.js'); ?>
    <script src="{{ $url_jquery }}"></script>
    <script src="{{ $url_bootstrap }}"></script>
    @yield('header_script')
</head>
<body id="app-layout">
    @include('layouts.header')

    @yield('content_support')

    @if (Auth::guest())
        @include('auth.modalLogin')
        @include('auth.modalRegister')
    @endif
    @include('support.layout.footer')
</body>
</html>
