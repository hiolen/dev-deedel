<footer>
    <p>DeeDel &copy; 2016</p>
    <nav>
        <ul>
            <li><a href="{{ faq_url() }}">FAQ</a></li>
            <li><a href="{{ terms_url() }}">Terms</a></li>
            <li><a href="{{ privacy_url() }}">Privacy Policy</a></li>
        </ul>
    </nav>
</footer>
@yield('script_footer')
