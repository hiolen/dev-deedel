@extends('network.deedel.inc.app')

@section('content_deedel')
<div class="support">
    <div class="support-banner text-center">
        <div class="container">
            <h2><strong>Find helpful articles</strong></h2>
            @include('support.layout.search')
            @include('support.layout.banner')
        </div>
    </div>
    <div class="section">
        <div class="container">  
            <div class="support-list-category">
                <li>
                    <a href="#" class="sup-square sup-general">
                        <i data-icon="r" class="linea-icon linea-basic"></i>
                        <span>General</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="sup-square sup-network">
                        <i data-icon="s" class="linea-icon linea-basic"></i>
                        Network
                    </a>
                </li>
                <li>
                    <a href="#" class="sup-square sup-coins">
                        <i data-icon="k" class="linea-icon linea-basic"></i>
                        Coins
                    </a>
                </li>
                <li>
                    <a href="#" class="sup-square sup-member">Membership</a>
                </li>
                <li>
                    <a href="#" class="sup-square sup-payments">Payments</a>
                </li>
                <li>
                    <a href="#" class="sup-square sup-rewards">Rewards</a>
                </li>
                <li>
                    <a href="#" class="sup-square sup-more">More</a>
                    <!--
                        Coupons
                        Contest
                        Ads
                        Paid Content
                    -->
                </li>
            </div>
        </div>
    </div>
    <div class="section sec-popArticles">
        <div class="container">
             <h2>Popular Articles</h2>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <ul class="">
                        <li class="comment-body">
                            <div class="white-box">
                                <h5><a href="#">Can I pay using money transfer/remittances</a></h5>
                                <a href="#" class="label label-megna label-rounded">Payment</a>
                                <p>do you want to pay using western union, etc?</p>
                            </div>
                        </li>
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="">
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                        <li>
                            <h4><a href="#">Can I pay using money transfer/remittances</a></h4>
                            <a href="#" class="sup-payment">Payment</a>
                            <p>do you want to pay using western union, etc?</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
