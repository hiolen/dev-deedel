@extends('layouts.app')

@section('content')
<div class="sup-ticket">
    <div class="container">
        <div class="section-header">
            <h2>Support Tickets</h2>
            <a href="/support/ticket/new" class="btn-ticket">Submit Ticket</a>
        </div>
        <div class="row">
            <div class="col-header col-xs-12 col-sm-8">
                <h4>Subject</h4>
            </div>
            <div class="col-header col-xs-12 col-sm-2">
                <h4>Date</h4>
            </div>
            <div class="col-header col-xs-12 col-sm-2">
                <h4>Status</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <p><a href="{{ support_url() }}/ticket/t/slug">Ticket Title</a></p>
                <p><a href="{{ support_url() }}/ticket/t/slug">Ticket Title</a></p>
                <p><a href="{{ support_url() }}/ticket/t/slug">Ticket Title</a></p>
            </div>
            <div class="col-xs-12 col-sm-2">
                <p>date</p>
                <p>date</p>
                <p>date</p>
            </div>
            <div class="col-xs-12 col-sm-2">
                <p><a href="#">open</a> | <a href="#">close</a> | <a href="#">hold</a></p>
                <p><a href="#">open</a> | <a href="#">close</a> | <a href="#">hold</a></p>
                <p><a href="#">open</a> | <a href="#">close</a> | <a href="#">hold</a></p>
            </div>
        </div>
    </div>
</div>
@endsection
