@extends('layouts.app')

@section('content')
<div class="sup-ticket">
	<div class="container">
	    <h2>Create Ticket</h2>
	        <form>
	            <p><input type="text" name="" class="form-control" placeholder="Subject"></p>
	            <div>
	                <textarea class="form-control tinymce" placeholder="Message"></textarea>
	            </div>
	        </form>
	    </div>
	</div>
</div>
@endsection
