@extends('support.layout.app')

<?php $support_url = Config::get('deedel_variables.support_url'); ?>
@section('content_support')
<div class="container">
	<div class="alert alert-danger">
    	<h2>When this page is visited the user will be forced to login. Only login users can view this page and other support pages</h2>
    </div>
    <h1>Support Center</h1>
    <div class="row">
    	<div class="col-xs-12 col-sm-3 col-md-2">
    		@include('support.sidebar')
    	</div>
    	<div class="col-xs-12 col-sm-9 col-md-10">
    		<ul class="nav">
    			<li><a href="{{ support_url() }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>
    			<li><a href="{{ $support_url }}/ticket/slug">Ticket Title</a></li>v
    		</ul>
    	</div>
    </div>
</div>
@endsection
