@extends('layouts.app')

<?php $support_url = Config::get('deedel_variables.support_url'); ?>
@section('content')
<div class="container">
    <h1>Support Inbox</h1>
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-2">
            @include('support.sidebar')
        </div>
        <div class="col-xs-12 col-sm-9 col-md-10">
            <ul class="nav">
                <li><a href="{{ $support_url }}/inbox/slug">Message Title</a></li>
                <li><a href="{{ $support_url }}/inbox/slug">Message Title</a></li>
                <li><a href="{{ $support_url }}/inbox/slug">Message Title</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection
