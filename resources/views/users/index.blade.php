@extends('layouts.app')

@section('content')
<div class="container">
    @if (session()->has('success_message'))
    <div class="alert-msg">
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    </div>
    @endif

    @if (session()->has('error_message'))
    <div class="alert-msg">
        <div class="alert alert-danger">
            {{ session()->get('error_message') }}
        </div>
    </div>
    @endif
   <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Users Management</h2>
            </div>
            <div class="pull-right">
                <button id="btn-add" name="btn-add" class="btn btn-success btn-xs">Create New User <i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Roles</th>
                <th width="280px">Action</th>
            </tr>
        </thead>
        <tbody id="user-list" name="user-list">
            @foreach ($data as $key => $user)
            <tr class="item{{ $user->id }}">
                <td>{{ $user->name }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if(!empty($user->roles))
                        @foreach($user->roles as $v)
                            <label class="label label-success">{{ $v->display_name }}</label>
                        @endforeach
                    @endif
                </td>
                <td>
                    <button class="btn btn-info btn-xs btn-detail open-modal" value="{{ $user->id }}"><i class="fa fa-files-o"></i> Edit</button>
                    <a href="#modalDelUser" data-toggle="modal" data-id="{{ $user->id }}" data-name="{{ $user->name }}" class="btn-del-user btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $data->render() !!}
</div>
@endsection

@section('modal')
    <!-- Add/Edit User Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="modalAddUserLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddUserLabel">User <span class="user-add-title">Editor</span></h4>
                </div>
                <form action="#" method="POST" id="formUsers" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" name="name" placeholder="Name" class="form-control" id="name" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Username:</strong>
                            <input type="text" name="username" placeholder="Username" class="form-control" id="username" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Email:</strong>
                            <input type="email" name="email" placeholder="Email" class="form-control" id="email" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Password:</strong>
                            <input type="password" name="password" placeholder="Password" class="form-control" id="password" value="" />
                        </div>
                        <div class="form-group">
                            <strong>Confirm Password:</strong>
                            <input type="password" name="confirm_password" placeholder="Confirm Password" class="form-control" />
                        </div>
                        <div class="form-group">
                            <strong>Role:</strong>
                            <select name="userRoles[]" class="form-control" multiple="multiple" id="userRoles">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="user_id" name="user_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete User Modal -->
    <div class="modal fade modalDelUser" tabindex="-1" role="dialog" id="modalDelUser" aria-labelledby="modalDelUserLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelUserLabel">Delete <span class="user-del-title">User</span></h4>
                </div>
                <form action="#" method="POST" id="delUserForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

    <!-- script for user -->
    <script type="text/javascript">
        $(function() {
            //display modal form for creating new task
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#formUsers').trigger("reset");
                $('#myModal').modal('show');
            });

            // Edit User
            $('.open-modal').click(function() {
                var user_id = $(this).val();

                $.get('user' + '/' + user_id, function (data) {
                    //success data
                    console.log(data);
                    $('#user_id').val(data.id);
                    $('#username').val(data.username);
                    $('#name').val(data.name);
                    $('#email').val(data.email);
                    $('#btn-save').val("update");

                    $('#myModal').modal('show');
                }); 
            });

            //create new task / update existing task
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var user_id = $('#user_id').val();
                var type = "POST"; //for creating new resource
                var my_url = '{{ super_admin_members_user_url() }}';

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + user_id;
                }

                if ($('#formUsers').valid()) {
                    $.ajax({
                        type: type,
                        url: my_url,
                        data: $('#formUsers').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            var text = '';
                            for(var i = 0;i < data.roles.length;i++)
                            {
                                text += '<label class="label label-success">' + data.roles[i] + '</label>&nbsp;';
                            }

                            var user = '<tr class="item' + data.user.id + '"><td>' + data.user.name + '</td><td>' + data.user.username + '</td><td>' + data.user.email + '</td><td>' + text + '</td>';
                            user += '<td><button class="btn btn-info btn-xs btn-detail open-modal" value="' + data.user.id + '"><i class="fa fa-files-o"></i> Edit</button>&nbsp;';
                            user += '<a href="#modalDelUser" data-toggle="modal" data-id="' + data.user.id + '" data-name="' + data.user.name + '" class="btn-del-user btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a></td></tr>';

                            if (state == "add"){ //if user added a new record
                                $('#user-list').prepend(user);
                            }else{ //if user updated an existing record
                                $(".item" + user_id).replaceWith( user );
                            }

                            $('#formUsers').trigger("reset");

                            $('#myModal').modal('hide')
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }else
                    return false;

            });

            // Validate Add User Form
            $('#formUsers').validate({
                rules: {
                    username: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    email: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    password: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    confirm_password: {
                        minlength: 3,
                        maxlength: 55,
                        required: true,
                        equalTo : "#password"
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });

            // DELETE User Ajax
            $('.btn-del-user').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);
                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: '{{ super_admin_members_user_delete_url() }}',
                    data: data,
                    success: function(data) {
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelUser').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

        });

        
    </script>
@endsection
