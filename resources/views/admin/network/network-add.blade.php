@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <div class="network-add">
        <h2>Add Network</h2>
        <form action="{{ super_admin_network_save_url() }}" method="POST" id="addNetworkForm" role="form">
            {!! csrf_field() !!}
            <div class="network-input">
                <label>Type</label>
                <Select class="form-control" name="type">
                    <option value="http">http</option>
                    <option value="https">https</option>
                </Select>
                {{ form_input("text","name","","Name","form-control","Name") }}
                {{ form_input("text","slug","","Slug","form-control","Slug") }}
                {{ form_input("text","domain","","Domain","form-control","Domain") }}
                {{ form_input("text","image","","Domain","form-control","Image") }}
                {{ form_texteditor("details","","Add Description","form-control text-editor","Description") }}
            </div>
            <div class="network-btn">
                {{ form_submit("Save") }}
            </div>
        </form>
    </div>

</div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}
    <script>
        $(function() {
            $('#addNetworkForm').on('submit', function(e) {
                e.preventDefault(e);
                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_network_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false; 
                }
            });
        });
    </script>
@endsection

