@extends('admin.network.inc.app')

@section('content_admin_network')
<div class="container">
    <h2>Booklulu Admin Dashboard</h2>    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            Books
            <ul>
                <li><a href="{{ url('/admin/books') }}">All Books</a></li>
                <li><a href="{{ url('/admin/books/add') }}">Add a Book</a></li>
                <li><a href="{{ url('/admin/books/genre') }}">Books Genre</a></li>
                <li><a href="{{ url('/admin/books/genre/add') }}">Books Genre</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection
