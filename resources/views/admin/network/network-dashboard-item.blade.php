@extends('admin.network.inc.app')
@section('content_admin_network')
    <?php
        $network = $network->slug;
        $target = 'admin.network.'.$network.'.index';
    ?>
    @include($target)
@endsection