@extends('admin.inc.app')

@section('content_admin')
<div class="network-dashboard clearfix">
    <div class="container">
        <div class="row clearfix">
            @foreach($network as $net)
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="thumbnail">
                    <a href="{{ super_admin_network_dashboard_url() }}/{{$net->id}}">
                        <img src="{{ deedel_url() }}{{$net->image}}" alt="{{$net->name}}" class="img-responsive">
                        <h4>{{$net->name}} <span>({{$net->domain}})</span> </h4>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

