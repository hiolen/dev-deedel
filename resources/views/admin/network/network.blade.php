@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
        <h2>
            Network 
            <a href="#" id="btn-add" data-toggle="modal" data-title="Category Meta" class="btn-add-meta">
                <i class="fa fa-plus"></i> Add New Network</a>
        </h2>

                        <div class="white-box">
                            <div class="comment-center">
                            @foreach($websites as $website)
                                <div class="comment-body item{{ $website->id }}">
                                    <div class="user-img"><img src="{{ deedel_url() }}{{$website->image}}" alt=""></div>
                                    <div class="mail-contnet">
                                        <h5><a href="{{$website->type}}://www.{{$website->domain}}" target="_blank">{{$website->name}} ({{$website->domain}})</a></h5> <span class="mail-desc">{{$website->details}}</span> <span class="label label-rouded label-info">Active</span><a href="#modalDelNetwork" data-toggle="modal" data-id="{{ $website->id }}" data-name="{{ $website->name }}" class="btn-del-network"><i class="ti-close text-danger"></i></a> <a href="#" class="btn-detail open-modal" data-value="{{ $website->id }}"><i class="ti-pencil text-success"></i></a> <a href="#network-details-{{ $website->id }}" data-toggle="modal"><i class="ti-eye text-success"></i></a> <a href="{{$website->type}}://www.{{$website->domain}}{{ network_admin_url() }}" target="_blank"><i class="ti-settings text-success"></i></a>
                                    </div>
                                    <div class="modal fade" tabindex="-1" role="dialog" id="network-details-{{$website->id}}" aria-labelledby="modalViewNetworkLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="modalAddNetworkLabel">{{$website->name}} ({{$website->domain}})</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p><strong>ID</strong>: {{$website->id}}</p>
                                                    <p><strong>Type</strong>: {{$website->type}}</p>
                                                    <p><strong>Slug</strong>: {{$website->slug}}</p>
                                                    <p><strong>Description</strong></p>
                                                    <p>{{$website->details}}</p>
                                                    <p><img src="{{ deedel_url() }}{{$website->image}}" alt="" class="img-responsive"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>

</div>


@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addNetwork" aria-labelledby="modalAddNetworkLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddNetworkLabel">Add Network</h4>
                </div>
                <form action="#" method="POST" id="addNetworkForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <Select class="form-control" name="type" id="type">
                            <option value="http">http</option>
                            <option value="https">https</option>
                        </Select>
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        {{ form_input("text","slug","","Slug","form-control","Slug") }}
                        {{ form_input("text","domain","","Domain","form-control","Domain") }}
                        {{ form_input("text","image","","Domain","form-control","Image") }}
                        {{ form_texteditor("details","","Add Description","form-control text-editor","Description") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="website_id" name="website_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Role Modal -->
    <div class="modal fade modalDelNetwork" tabindex="-1" role="dialog" id="modalDelNetwork" aria-labelledby="modalDelRoleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelRoleLabel">Delete <span class="role-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delRoleForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}
    {{ admin_network_footer_js() }}

    <script>
        //display modal form for creating new task
        $('#btn-add').click(function(){
            $('#btn-save').val("add");
            $('#addNetworkForm').trigger("reset");
            $('#addNetwork').modal('show');
        });

        // Edit User
        $('.open-modal').click(function() {
            var network_id = $(this).attr('data-value');

            $.get('network' + '/' + network_id, function (data) {
                //success data
                console.log(data);
                $('#website_id').val(data.id);
                $('#type').val(data.type);
                $('#name').val(data.name);
                $('#slug').val(data.slug);
                $('#domain').val(data.domain);
                $('#image').val(data.image);

                tinymce.EditorManager.execCommand('mceRemoveEditor',true, 'details');
                $('#details').val(data.details);

                var details_editor_config_edit = {
                    selector: '.text-editor',
                    setup: function (editor) {
                        editor.on('change', function () {
                            tinymce.triggerSave();
                        });
                    },
                    menubar: false,
                    statusbar: false,
                    plugins: [
                      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                      'save table contextmenu directionality emoticons template paste textcolor',
                      'code advlist'
                    ],
                    a_plugin_option: true,
                    toolbar: 'code | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media preview fullpage',
                };

                tinymce.init(details_editor_config_edit);

                $('#btn-save').val("update");
                $('#addNetwork').modal('show');
            }); 
        });

        //create new task / update existing task
        $("#btn-save").click(function (e) {
            e.preventDefault();
            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();
            var website_id = $('#website_id').val();
            var type = "POST"; //for creating new resource
            var my_url = 'network';

            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + website_id;
            }

            if ($('#addNetworkForm').valid()) {
                $.ajax({
                    type: type,
                    url: my_url,
                    data: $('#addNetworkForm').serialize(),
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }else
                return false;               

        });

        // Delete Meta
        $('.btn-del-network').on('click', function() {
            $('.dname').html($(this).data('name'));
            $('.did').text($(this).data('id'));
        });

        $('.modal-footer').on('click', '.delete', function(e) {
            e.preventDefault(e);
            
            var data = {
                _token: $('input[name=_token]').val(),
                id: $('.did').text()
            }

            $.ajax({
                type: 'post',
                url: '{{ super_admin_network_delete_url() }}',
                data: data,
                success: function(data) {
                    console.log(data);
                    $('.item' + $('.did').text()).remove();
                    $('#modalDelNetwork').modal('hide');
                }, error:function() {
                    console.log("Error!");
                }
            });
        });

        // Validate Add Role Form
        $('#addNetworkForm').validate({
            rules: {
                name: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                domain: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                image: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                // leave it blank here.
            }
        });
    </script>
@endsection