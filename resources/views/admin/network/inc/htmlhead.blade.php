<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DeeDel</title>
    {{ icons() }}
    {{ global_css() }}
    {{ admin_style_css() }}


    {{ admin_css() }}
    <?php $url_jquery = URL::asset('assets/js/jquery.min.js'); ?>
    <?php $url_bootstrap = URL::asset('assets/js/bootstrap.min.js'); ?>
    <script src="{{ $url_jquery }}"></script>
    <script src="{{ $url_bootstrap }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
	@yield('script_header_admin')
</head>