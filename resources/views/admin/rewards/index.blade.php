@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Rewards</h2>
    <ul>
        <li><a href="">Add Gifts</a></li>
        <li><a href="">Add Points</a></li>
        <li><a href="">Add Coins</a></li>
    </ul>
    <!-- Database: marketingdeedel ; Table: rewards -->
    <h3>Type Gifts</h3>
    <table>
    	<thead>
    		<tr>
                <th>ID</th>
    			<th>Name</th>
    			<th>Value</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Icon</th>
                <th>Network</th>
    			<th>Action</th>
    		</tr>
            @foreach($rewards as $reward)
            <tr>
                <th>{{$reward->id}}</th>
                <th>{{$reward->name}}</th>
                <th>{{$reward->value}}</th>
                <th>{{$reward->amount}}</th>
                <th>{{$reward->price}}</th>
                <th>{{$reward->icon}}</th>
                <th>{{$reward->network_id}}</th>
                <th>Action</th>
            </tr>
            @endforeach
    	</thead>
    	<tbody>

    	</tbody>
    </table>
</div>
@endsection
