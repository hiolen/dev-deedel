@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Gifts</h2>
    <a href="#addGift" data-toggle="modal" data-title="Category Meta" class="btn-add-meta"><i class="fa fa-plus"></i> Add New Gift</a>
    <!-- DB: AllDeeDel ; Table: websites -->
    <table>
    	<thead>
    		<tr>
                <th>ID</th>
    			<th>Type</th>
    			<th>Name</th>
    			<th>Slug</th>
                <th>Desc</th>
                <th>Domain</th>
                <th>Image</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
            @foreach($websites as $website)
    		<tr>
                <th>{{$website->id}}</th>
                <td>{{$website->type}}</td>
                <td>{{$website->name}}</td>
                <td>{{$website->slug}}</td>
                <td>{{$website->details}}</td>
                <td><a href="{{$website->type}}://www.{{$website->domain}}" target="_blank">{{$website->domain}}</a></td>
                <td>Website Image</td>
    			<td><a href="">Edit</a> | <a href="">Delete</a></td>
    		</tr>
            @endforeach
    	</tbody>
    </table>
</div>
@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addGift" aria-labelledby="modalAddGiftLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddGiftLabel">Add Network</h4>
                </div>
                <form action="{{ website_bahay_admin_add_meta_url() }}" method="POST" id="addNetworkForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        {{ form_input("text","slug","","Slug","form-control","Slug") }}
                        {{ form_texteditor("description","","Add Description","form-control text-editor","Description") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_submit("Save") }}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Network Modal -->
    <div class="modal fade modalDeleteGift" tabindex="-1" role="dialog" id="modalDeleteNetwork" aria-labelledby="modalDeleteNetworkLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDeleteNetworkLabel">Delete Network</h4>
                </div>
                <!--<form action=" admin_delete_network_url() }}" method="POST" id="deleteNetworkForm" role="form">-->
                <form action="" method="POST" id="deleteNetworkForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {
            // Add Network
            $('#addMetaForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ website_bahay_admin_add_meta_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });

            $(".btn-add-meta").click(function(){
                var parents = [];
                var t = $(this);
                var id = t.attr("data-type");
                var title = t.attr("data-title");

                $('#modalAddMeta').on('show.bs.modal', function () {
                  $("#meta-id").val(id);
                  $(".meta-add-title").text(title);
                });

                $('#modalAddMeta').on('hide.bs.modal', function () {
                  $("#meta-id").val("");
                  $(".meta-add-title").text("Meta");
                });

            });

            // Edit Meta
            $('#editMetaForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                        type: "POST",
                        url: '{{ website_bahay_admin_edit_meta_url() }}',
                        data: $(this).serialize(),
                        success: function(data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        }, error: function() {
                            console.log("Error!");
                        }
                    });
                } else {
                    return false;
                }
            });
    
            // Delete Network Ajax
            $('.btn-del-meta').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);

                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: '{{ website_bahay_admin_delete_meta_url() }}',
                    data: data,
                    success: function(data) {
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelMeta').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

            // Validate Add Network Form
            $('#addMetaForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                messages: {
                    name: {
                        minLength: "Your Name of must be at least 3 characters",
                        maxLength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name of Meta",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
            

            // Validate Edit Meta Form
            $('#editMetaForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                messages: {
                    name: {
                        minLength: "Your Name of must be at least 3 characters",
                        maxLength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name of Meta",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
        });
    </script>
@endsection