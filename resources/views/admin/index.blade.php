@extends('admin.inc.app')

@section('content_admin')
<div class="container">

            <div class="section cat-msg">
                <h3><a href="{{ super_admin_messages_url() }}">Messages</a></h3>
                <ul class="list-unstyled clearfix">
                    <li><a href=""><i class="fa fa-envelope" aria-hidden="true"></i> Inbox</a></li>
                    <li><a href="{{ super_admin_messages_reports_url() }}"><i class="fa fa-file-text" aria-hidden="true"></i> Reports</a></li>
                    <li><a href="{{ super_admin_messages_support_url() }}"><i class="fa fa-ticket" aria-hidden="true"></i> Support Tickets</a></li>
                    <li><a href="{{ super_admin_messages_support_ticket_add_url() }}"><i class="fa fa-ticket" aria-hidden="true"></i> Create Support Tickets</a></li>
                </ul>
            </div>

            <div class="section cat-network">
                <h3><a href="{{ super_admin_network_url() }}">Network</a></h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_network_add_url() }}">Add a Network</a></li>
                    <li><a href="{{ super_admin_network_url() }}">All Network</a></li>
                    <li><a href="{{ super_admin_network_dashboard_url() }}">All Network Dashboard</a></li>
                </ul>
            </div>

            <div class="section cat-coupons">
                <h3><a href="{{ super_admin_coupons_url() }}">Coupons</a></h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_coupons_add_url() }}"><i class="fa fa-plus" aria-hidden="true"></i> Add a coupon</a></li>
                    <li><a href="{{ super_admin_coupons_upload_url() }}"><i class="fa fa-upload" aria-hidden="true"></i> Upload coupons</a></li>
                    <li><a href="{{ super_admin_coupons_url() }}"><i class="fa fa-tags" aria-hidden="true"></i> All Coupons</a></li>
                </ul>
            </div>

            <div class="section cat-pricing">
                <h3><a href="{{ super_admin_pricing_url() }}">Pricing</a></h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_pricing_coins_type_url() }}">Coins Type</a></li>
                    <li><a href="{{ super_admin_pricing_coins_set_url() }}">Set coins type</a></li>
                    <li><a href="{{ super_admin_pricing_coins_add_url() }}">add coins</a></li>
                    <li><a href="{{ super_admin_pricing_coins_url() }}">coins</a></li>
                    <li><a href="{{ super_admin_pricing_membership_add_url() }}">Add membership</a></li>
                    <li><a href="{{ super_admin_pricing_membership_url() }}">membership</a></li>
                    <li><a href="{{ super_admin_pricing_activities_add_url() }}">Add Activities</a></li><!-- pay using coins -->
                    <li><a href="{{ super_admin_pricing_activities_url() }}">Activities</a></li><!-- pay using coins -->
                </ul> 
            </div>

            <div class="section cat-payment">
                <h3>Payments</h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_payments_coins_url() }}">Coins</a></li>
                    <li><a href="{{ super_admin_payments_membership_url() }}">Membership</a></li>
                    <li><a href="{{ super_admin_payments_activities_url() }}">Activities</a></li>
                    <li><a href="{{ super_admin_payments_rewards_url() }}">Rewards</a></li>
                    <li><a href="{{ super_admin_payments_contest_url() }}">Contest</a></li>
                </ul>
            </div>

            <div class="section cat-rewards">
                <h3>Rewards</h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_rewards_points_url() }}"><i class="fa fa-star" aria-hidden="true"></i> Point (RP)</a></li>
                    <!-- Token can be used using any coins -->
                    <li><a href="{{ super_admin_rewards_gifts_url() }}"><i class="fa fa-gift" aria-hidden="true"></i> Gifts (RG)</a></li>
                    <!-- Token can only be used using green coins -->
                    <li><a href="{{ super_admin_rewards_coins_url() }}"><i class="fa fa-gift" aria-hidden="true"></i> Coins (RC)</a></li>
                    <!-- Token can only be used using green coins -->
                    <li><a href="{{ super_admin_rewards_token_url() }}"><i class="fa fa-gift" aria-hidden="true"></i> Token</a></li>
                    <!-- Token can only be used using points -->
                </ul>
            </div>

            <div class="section cat-cont">
                <h3>Contest</h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="">Add Contest Type</a></li>
                    <li><a href="">Create a Contest</a></li>
                    <li><a href="">All Contest</a></li>
                </ul>   
            </div>

            <div class="section cat-members">
                <h3>Members</h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_members_url() }}"><i class="fa fa-users" aria-hidden="true"></i> Members</a></li>
                    <li><a href="{{ super_admin_members_roles_url() }}"><i class="fa fa-users" aria-hidden="true"></i> Roles</a></li>
                    <li><a href="{{ super_admin_members_permissions_url() }}"><i class="fa fa-users" aria-hidden="true"></i> Permissions</a></li>
                    <li><a href="{{ super_admin_members_users_premium_url() }}"><i class="fa fa-certificate" aria-hidden="true"></i> Premium Members</a></li>
                </ul>
            </div>

            <div class="section cat-cont">
                <h3>Contents</h3>
                <ul class="list-unstyled clearfix">
                    <li><a href="{{ super_admin_content_support_url() }}">Support Articles</a></li>
                    <li><a href="{{ super_admin_content_terms_url() }}">Terms & Definition</a></li>
                    <li><a href="{{ super_admin_content_pages_url() }}">Pages</a></li>
                    <li><a href="{{ super_admin_content_blog_url() }}">Blog</a></li>
                    <li><a href="{{ super_admin_content_landing_pages_url() }}">Landing Pages</a></li>
                    <li><a href="{{ super_admin_content_emails_url() }}">Email Templates</a></li>
                    <li><a href="{{ super_admin_content_forms_url() }}">Forms</a></li>
                    <li><a href="{{ super_admin_content_panels_url() }}">Panels</a></li>
                </ul>   
            </div>

            <div class="section cat-reports">
                <h3>Reports</h3>
            </div>

            <div class="section cat-reports">
                <h3>Ads System</h3>
                <h1>Coming Soon</h1>
            </div>
            
    
    <!--
    <ul class="nav">
        <li><a href="">Messages</a></li>
        <!--
            Requests
            Feedback
            Contact Message
        -->
        <!--
        <li><a href="">Support Ticket</a></li>
        <!--
            User can send tickets
            deedel personnel respond to tickets (status:open,solved,closed)
        -->
        <!--
        <li><a href="">Network</a></li>
        <!-- add/edit/delete a network
            name,type,domain,image,slug
        -->
        <!--
        <li><a href="">Coupons</a></li>
        <!-- add new coupon manually and upload csv -->
        <!--
        <li><a href="">Pricing</a></li>
        <!--
            coins
            - set coins and prices
            account upgrades
            - set premium account/upgrade name, details and prices
        -->
        <!--
        <li><a href="">Payments</a></li>
        <!--
            Coins
            - Successful Coins Orders
            - Total Coins Active (all type of coins)
            - Total Coins Spend (per coins)
            - Coins Payment Methods
            - Account Wallet Payment Methods used
            Daily/Weekly/Monthly Signup
            Daily/Weekly/Monthly Upgrades
        -->
        <!--
        <li><a href="">Members</a></li>
        <!--
            List all Users
            List all premium members
            List all members by coins, by points, by gifts, by rewards, by reviews, by etc.
            send gift to member
            message member
            block member
            link to member public profile (include latest public activity)
        -->
        <!--
        <li><a href="">Contest</a></li>
        <!--
            List all contest
            create contest type
            add a contest
            contest rewards
            selecting winners
        -->
        <!--
        <li><a href="">Blog</a></li>
        <!-- create/update/delete blog
            approve blog post from authors and guest authors
            assign blog author
        -->
        <!--
        <li><a href="">Help Articles/Tutorials</a></li>
        <!-- Add/Edit/Delete Help
            Assign Author and Approve Post
        -->
        <!--
    </ul>
     -->
</div>
@endsection
