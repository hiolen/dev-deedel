@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Blog Name Posts</h2>
	<p><a href="{{ account_contents_blog_url() }}/1/posts/add" class="btn btn-primary">Create Post</a>
	<a href="{{ account_contents_blog_url() }}/1/meta/add" class="btn btn-primary">Create Meta</a>
	<a href="{{ account_contents_blog_url() }}/1/meta" class="btn btn-primary">All Meta</a></p>
	<form>
		<input type="text" name="" class="form-control">
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
	<p>
		Display by Status
		<select class="form-control">
			<option>Draft</option>
			<option>Pending</option>
			<option>Publish</option>
			<option>Deleted</option>
		</select>
	</p>
	<p>
		Display by Month
		<strong>calendar</strong>
	</p>
	<p>Sort
		<a href="">Newest</a> | <a href="">Oldest</a><br>
		<a href="">ABC asc</a> | <a href="">ABC desc</a>
		<a href="">comments asc</a> | <a href="">comments desc</a>
	</p>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>Title</td>
				<td>Meta</td>
				<td>Comments</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="slug">title blog post</a></td>
				<td>
					<p>meta category: <a href="">category</a>,<a href="">category</a>,<a href="">category</a></p>
					<p>meta tag: <a href="">tag</a>,<a href="">tag</a>,<a href="">tag</a></p>
				</td>
				<td><a href="">0 comment</a></td>
				<td><a href="">View</a>|<a href="">Edit</a>}<a href="">Delete</a></td>
			</tr>
			<tr>
				<td><a href="slug">title blog post</a></td>
				<td>
					<p>meta category: <a href="">category</a>,<a href="">category</a>,<a href="">category</a></p>
					<p>meta tag: <a href="">tag</a>,<a href="">tag</a>,<a href="">tag</a></p>
				</td>
				<td><a href="">0 comment</a></td>
				<td><a href="">View</a>|<a href="">Edit</a>}<a href="">Delete</a></td>
			</tr>
			<tr>
				<td><a href="slug">title blog post</a></td>
				<td>
					<p>meta category: <a href="">category</a>,<a href="">category</a>,<a href="">category</a></p>
					<p>meta tag: <a href="">tag</a>,<a href="">tag</a>,<a href="">tag</a></p>
				</td>
				<td><a href="">0 comment</a></td>
				<td><a href="">View</a>|<a href="">Edit</a>}<a href="">Delete</a></td>
			</tr>
			<tr>
				<td><a href="slug">title blog post</a></td>
				<td>
					<p>meta category: <a href="">category</a>,<a href="">category</a>,<a href="">category</a></p>
					<p>meta tag: <a href="">tag</a>,<a href="">tag</a>,<a href="">tag</a></p>
				</td>
				<td><a href="">0 comment</a></td>
				<td><a href="">View</a>|<a href="">Edit</a>}<a href="">Delete</a></td>
			</tr>
			<tr>
				<td><a href="slug">title blog post</a></td>
				<td>
					<p>meta category: <a href="">category</a>,<a href="">category</a>,<a href="">category</a></p>
					<p>meta tag: <a href="">tag</a>,<a href="">tag</a>,<a href="">tag</a></p>
				</td>
				<td><a href="">0 comment</a></td>
				<td><a href="">View</a>|<a href="">Edit</a>}<a href="">Delete</a></td>
			</tr>
		</tbody>
	</table>
    <ul class="pagination">
        <li><a href="">1</a></li>
        <li class="active"><a href="">2</a></li>
        <li><a href="">3</a></li>
        <li><a href="">4</a></li>
    </ul>
</div>
@endsection