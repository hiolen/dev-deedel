@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>All Meta - Blog Name</h2>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Description</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>meta name</td>
				<td>meta-name</td>
				<td>description</td>
				<td><a href="meta/1">View</a>|<a href="meta/1/add">Add Item</a>|<a href="">Edit</a>|<a href="">Delete</a>|</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection