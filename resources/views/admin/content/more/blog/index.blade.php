@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Blog</h2>
	<a href="{{ account_contents_blog_add_url() }}" class="btn btn-primary">Create Blog</a>
	<form>
		<input type="text" name="" class="form-control">
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Network</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($blogs as $blog)
				@if($user->id == $blog->author)
				<tr>
					<td>{{$blog->id}}</td>
					<td>{{$blog->title}}</td>
					<td>{{$blog->slug}}</td>
					<td>{{$blog->website->domain}}</td>
					<td><a href="blog/1/posts">View</a>|<a href="">Edit</a>|<a href="">Delete</a>|</td>
				</tr>
				@endif
			@endforeach
		</tbody>
	</table>
    <ul class="pagination">
        <li><a href="">1</a></li>
        <li class="active"><a href="">2</a></li>
        <li><a href="">3</a></li>
        <li><a href="">4</a></li>
    </ul>
</div>
@endsection