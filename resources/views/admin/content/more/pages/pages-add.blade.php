@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Add a page</h2>	
    <form>
        {{ form_input("text","name","","Name","form-control","Name") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
        <p>Network
        <select class="form-control">
            <option>network name (domain)</option>
            <option>network name (domain)</option>
            <option>network name (domain)</option>
        </select>
        </p>
        <p>membership type
        <select class="form-control">
            <option value="">membership name</option>
            <option value="">membership name</option>
            <option value="">membership name</option>
        </select>
        </p>
        {{ form_submit("Save") }}
        {{ form_submit("Publish") }}
    </form>
</div>
@endsection

@section('script_footer_account')
    {{ account_footer_tinymce_js() }}
    {{ account_footer_tinymce_1_js() }}
@endsection