@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Pages</h2>
	<a href="pages/add" class="btn btn-primary">Create Page</a>
	<p>network <select class="form-control">
		<option>network name</option>
		<option>network name</option>
		<option>network name</option>
	</select></p>
	<p>membership type <select class="form-control">
		<option>agent</option>
		<option>realtor</option>
		<option>developer</option>
	</select></p>
	<p>Status <a href="">Draft</a>|<a href="">Publish</a>|<a href="">Delete</a></p>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>Name</td>
				<td>Slug</td>
				<th>desc</th>
				<th>Network url/domain</th>
				<th>Membership url</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="full-page-url">About</a></td>
				<td>about</td>
				<td>desc</td>
				<td><a href="">deedelhomes.com</a></td>
				<td><a href="">agent</a></td>
				<td>draft/publish/deleted</td>
				<td><a href="">view</a>|<a href="">Edit</a>|<a href="">delete</a></td>
			</tr>
			<tr>
				<td><a href="full-page-url">The team</a></td>
				<td>team</td>
				<td>desc</td>
				<td><a href="">deedelhomes.com</a></td>
				<td><a href="">agent</a></td>
				<td>draft/publish/deleted</td>
				<td><a href="">view</a>|<a href="">Edit</a>|<a href="">delete</a></td>
			</tr>
			<tr>
				<td><a href="full-page-url">The Press</a></td>
				<td>press</td>
				<td>desc</td>
				<td><a href="">deedelhomes.com</a></td>
				<td><a href="">agent</a></td>
				<td>draft/publish/deleted</td>
				<td><a href="">view</a>|<a href="">Edit</a>|<a href="">delete</a></td>
			</tr>
		</tbody>
	</table>
	full-page-url : domain.com/membership-slug/membership-name/page-slug
</div>
@endsection