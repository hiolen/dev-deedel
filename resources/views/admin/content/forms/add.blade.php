@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Add a form</h2>	
    <form>
        <p>Field Type
        <select class="form-control">
            <option>text</option>
            <option>email</option>
            <option>hidden</option>
            <option>textarea</option>
        </select>
        </p>
        <p>
            <input type="text" name="formname" placeholder="Name" class="form-control">
        </p>
        <p>membership type
        <select class="form-control">
            <option value="">membership name</option>
            <option value="">membership name</option>
            <option value="">membership name</option>
        </select>
        </p>
        {{ form_submit("Save") }}
        {{ form_submit("Publish") }}
    </form>
</div>
@endsection

@section('script_footer_account')
    {{ account_footer_tinymce_js() }}
    {{ account_footer_tinymce_1_js() }}
@endsection