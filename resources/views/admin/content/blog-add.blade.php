@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Add New Blog Post</h2>
    @include('admin.content.inc.blog-nav')
    <form>
        {{ form_input("text","title","","Title","form-control","Title") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
        <p>
        <select class="form-control">
            <option value="">category</option>
        </select>
        </p>
        <p>Network
        <select class="form-control">
            <option>network name (domain)</option>
        </select>
        </p>
        {{ form_submit("Save") }}
    </form>
</div>
@endsection
