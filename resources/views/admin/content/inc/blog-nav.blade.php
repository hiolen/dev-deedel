
    <a href="{{ super_admin_content_blog_add_url() }}" class="btn btn-primary">Add New Blog Post</a>
    <a href="{{ super_admin_content_blog_url() }}" class="btn btn-primary">All Blog Posts</a>
    <a href="{{ super_admin_content_blog_category_add_url() }}" class="btn btn-primary">Add New Category</a>
    <a href="{{ super_admin_content_blog_category_url() }}" class="btn btn-primary">All Categories</a>
    <a href="{{ super_admin_content_blog_tag_add_url() }}" class="btn btn-primary">Add New Tag</a>
    <a href="{{ super_admin_content_blog_tag_url() }}" class="btn btn-primary">All Tags</a>