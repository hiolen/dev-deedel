@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Blog Posts</h2>
	@include('admin.content.inc.blog-nav')
    <p>Display by Network
    <select class="form-control">
        <option>network name (domain)</option>
    </select></p>
    <p>Display by category
    <select class="form-control">
        <option value="category_id">category name</option>
    </select></p>
    <p>Display by tag
    <select class="form-control">
        <option value="tag_id">tag name</option>
    </select></p>
    <p>Display by status
    <select class="form-control">
        <option value="draft">Draft</option>
        <option value="pending">Pending</option>
        <option value="publish">Publish</option>
        <option value="deleted">Deleted</option>
    </select></p>
    <form>    
	    <p>Search by
	    <select class="form-control">
	        <option value="title">title</option>
	        <option value="user">user</option>
	        <option value="date">date</option>
	    </select></p>
        <input type="search" name="" placeholder="Search Support Articles" class="form-control">
        {{ form_submit("Search") }}
    </form>
    <table>
    	<thead>
    		<tr>
    			<th>Title</th>
    			<th>Short Description</th>
    			<th>Full Description</th>
    			<th>Categories</th>
    			<th>Tags</th>
    			<th>Network</th>
    			<th>Posted by</th>
    			<th>Date Posted</th>
    			<th>Status</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td><a href="slug">title</a></td>
    			<td>Short desc</td>
    			<td>Full desc</td>
    			<td><a href="categoryslug">category</a>,<a href="categoryslug">category</a>,<a href="categoryslug">category</a></td>
    			<td><a href="tagslug">tag</a>,<a href="tagslug">tag</a>,<a href="tagslug">tag</a></td>
    			<td><a href="domain">network name</a></td>
    			<td><a href="userprofilepage">username</a></td>
    			<td>date</td>
    			<td>status</td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    		<tr>
    			<td><a href="slug">title</a></td>
    			<td>Short desc</td>
    			<td>Full desc</td>
    			<td><a href="categoryslug">category</a>,<a href="categoryslug">category</a>,<a href="categoryslug">category</a></td>
    			<td><a href="tagslug">tag</a>,<a href="tagslug">tag</a>,<a href="tagslug">tag</a></td>
    			<td><a href="domain">network name</a></td>
    			<td><a href="userprofilepage">username</a></td>
    			<td>date</td>
    			<td>status</td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    	</tbody>
    </table>
    <ul class="pagination">
        <li><a href="">1</a></li>
        <li class="active"><a href="">2</a></li>
        <li><a href="">3</a></li>
        <li><a href="">4</a></li>
    </ul>
</div>
@endsection
