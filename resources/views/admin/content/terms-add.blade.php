@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Add Terms & Definition</h2>
    <a href="{{ super_admin_content_terms_url() }}" class="btn btn-primary">All Terms</a>
    <form action="{{super_admin_content_terms_save_url()}}" method="POST" id="addPricingCoinsForm" role="form">
        {!! csrf_field() !!}
        {{ form_input("text","name","","Name","form-control","Name") }}
        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
        <p>
        <select class="form-control" name="website_id">
            @foreach($websites as $website)
            <option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
            @endforeach
        </select>
        </p>
        {{ form_submit("Save") }}
    </form>
</div>
@endsection
