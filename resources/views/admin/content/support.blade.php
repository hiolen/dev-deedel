@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Support</h2>
    <a href="{{ super_admin_content_support_add_url() }}" class="btn btn-primary">Add New Support Article</a>
    <a href="{{ super_admin_content_support_category_add_url() }}" class="btn btn-primary">Add New Support Category</a>
    <a href="{{ super_admin_content_support_category_url() }}" class="btn btn-primary">All Support Categories</a>
    <p>Display by Category
    <select class="form-control"> 
    	<option value="category_id">category name</option>
    	<option value="category_id">category name</option>
    	<option value="category_id">category name</option>
    </select></p>
    <p>Display by Network
    <select class="form-control"> 
        @foreach($websites as $website)
    	<option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
        @endforeach
    </select></p>
    <form>
        <input type="search" name="" placeholder="Search Support Articles" class="form-control">
        {{ form_submit("Search") }}
    </form>

    <table>
    	<thead>
    		<tr>
    			<th>Title</th>
    			<th>category</th>
    			<th>Full Description</th>
    			<th>Network</th>
    			<th>Posted by</th>
    			<th>Date Posted</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td><a href="slug">title</a></td>
    			<td>category</td>
    			<td>Full desc</td>
    			<td><a href="domain">network name</a></td>
    			<td><a href="userprofilepage">username</a></td>
    			<td>date</td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    		<tr>
    			<td><a href="slug">title</a></td>
    			<td>category</td>
    			<td>Full desc</td>
    			<td><a href="domain">network name</a></td>
    			<td><a href="userprofilepage">username</a></td>
    			<td>date</td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
            @foreach($supports as $support)
                <tr>
                    <td>{{$support->title}}</td>
                    <td>{{$support->content}}</td>
                    <td>{{$support->author}}</td>
                    <td>{{$support->website_id}}</td>
                    <td>{{$support->date_added}}</td>
                    <td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
                </tr>
            @endforeach
    	</tbody>
    </table>
</div>
@endsection