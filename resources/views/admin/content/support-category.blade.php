@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Support Articles Category</h2>
    <a href="{{ super_admin_content_support_category_add_url() }}" class="btn btn-primary">Add New Support Category</a>
    <a href="{{ super_admin_content_support_url() }}" class="btn btn-primary">View All Support Articles</a>
    <a href="{{ super_admin_content_support_add_url() }}" class="btn btn-primary">Add New Support Article</a>
    <p>Display by Network
    <select class="form-control"> 
    	<option value="network_id">network name (domain)</option>
    	<option value="network_id">network name (domain)</option>
    </select></p>
    <form>
        <input type="search" name="" class="form-control">
        {{ form_submit("Search") }}
    </form>
    <table>
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>Short Description</th>
    			<th>Full Description</th>
    			<th>Network</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td><a href="slug">Name</a></td>
    			<td>Short desc</td>
    			<td>Full desc</td>
    			<td><a href="domain">network name</a></td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    		<tr>
    			<td><a href="slug">Name</a></td>
    			<td>Short desc</td>
    			<td>Full desc</td>
    			<td><a href="domain">network name</a></td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    	</tbody>
    </table>
</div>
@endsection