@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Add New Blog Category</h2>
    @include('admin.content.inc.blog-nav')
    <form>
        {{ form_input("text","name","","Name","form-control","Name") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("shortdesc","","Short Description","form-control text-editor","Short Description") }}
        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
        <p>Parent Category
        <select class="form-control">
            <option value="category_id">category name</option>
        </select>
        <p>Network
        <select class="form-control">
            <option>network name (domain)</option>
        </select>
        </p>
        {{ form_submit("Save") }}
        {{ form_submit("Publish") }}
        <!-- make sure to store author_id=login user, post_type=blog, meta_type=category, status=draft when save, status=publish when publish is clicked , date added -->
    </form>
</div>
@endsection
