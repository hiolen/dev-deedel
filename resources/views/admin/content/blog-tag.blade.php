@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Blog Tags</h2>
	@include('admin.content.inc.blog-nav')
    <p>Display by Network
    <select class="form-control">
        <option>network name (domain)</option>
    </select></p>
    <p>Display by status
    <select class="form-control">
        <option value="draft">Draft</option>
        <option value="pending">Pending</option>
        <option value="publish">Publish</option>
        <option value="deleted">Deleted</option>
    </select></p>
    <form>
        <input type="search" name="" placeholder="Search Blog Tag" class="form-control">
        {{ form_submit("Search") }}
    </form>
    <table>
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>Short Description</th>
    			<th>Full Description</th>
    			<th>Network</th>
    			<th>Parent Tag</th>
    			<th>Posted by</th>
    			<th>Date Posted</th>
    			<th>Status</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td><a href="slug">name</a></td>
    			<td>Short desc</td>
    			<td>Full desc</td>
    			<td><a href="domain">network name</a></td>
    			<td>parent tag name</td>
    			<td><a href="userprofilepage">username</a></td>
    			<td>date</td>
    			<td>draft/pending/publish/deleted</td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    		<tr>
    			<td><a href="slug">name</a></td>
    			<td>Short desc</td>
    			<td>Full desc</td>
    			<td><a href="domain">network name</a></td>
    			<td>parent tag name</td>
    			<td><a href="userprofilepage">username</a></td>
    			<td>date</td>
    			<td>draft/pending/publish/deleted</td>
    			<td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
    		</tr>
    	</tbody>
    </table>
    <ul class="pagination">
        <li><a href="">1</a></li>
        <li class="active"><a href="">2</a></li>
        <li><a href="">3</a></li>
        <li><a href="">4</a></li>
    </ul>
</div>
@endsection
