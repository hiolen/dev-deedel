@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Terms</h2>
    <a href="{{ super_admin_content_terms_add_url() }}" class="btn btn-primary">Add New Term & Definition</a>
    <form>
        <input type="search" name="" class="form-control">
        {{ form_submit("Search") }}
    </form>
    <table>
    	<thead>
    		<tr>
    			<th>Term Name</th>
    			<th>Definition</th>
                <th>Network</th>
                <th>Status</th>
                <th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
            @foreach($terms as $term)
                <tr class="item{{$term->id}}">
                    <td>{{$term->name}}</td>
                    <td>{{$term->desc}}</td>
                    <td>{{$term->website->name}}</td>
                    <td>{{$term->status}}</td>
                    <td>
                        <button class="btn btn-info btn-xs btn-detail open-modal" value="{{ $term->id }}"><i class="fa fa-files-o"></i> Edit</button> | 
                        <a href="#modalDelMembership" data-toggle="modal" data-id="{{ $term->id }}" data-name="{{ $term->name }}" class="btn-del-member btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                </tr>
            @endforeach
    		
    	</tbody>
    </table>
    {{$terms->links()}}
</div>
@endsection

@section('modal_footer_admin')
<!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addMemberType" aria-labelledby="modalAddCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalMemberTypeLabel">Add New Term</h4>
                </div>
                <form action="#" method="POST" id="addMemberTypeForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" id="membership-id">
                    <div class="modal-body">
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
                        <p>Network</p>
                        <select name="website_id" id="website_id">
                            @foreach($websites as $website)
                                <option value="{{$website->id}}">{{$website->name}}({{$website->domain}})
                                </option>
                            @endforeach
                        </select>
                        {{ form_input("text","status","","Status","form-control","Status") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="membership_id" name="membership_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Role Modal -->
    <div class="modal fade modalDelMembership" tabindex="-1" role="dialog" id="modalDelMembership" aria-labelledby="modalDelMembershipLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelMembershipLabel">Delete <span class="membership-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delMembershipForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}
    <script type="text/javascript">
        $(function() {
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#addMemberTypeForm').trigger("reset");
                $('#addMemberType').modal('show');
            });
            //create new Network / update new Network
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var membership_id = $('#membership_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'terms';
                var dataType = $("#membership-id").val();

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + membership_id;
                }

                // if ($('#formMeta').valid()) {
                    $.ajax({
                        type: type,
                        url: my_url,
                        data: $('#addMemberTypeForm').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                // }else
                //     return false;
            });  

            // Edit User
            $('.open-modal').click(function() {
                var membership_id = $(this).val();

                $.get('terms' + '/' + membership_id, function (data) {
                    //success data
                    console.log(data);
                    $('#membership_id').val(data.id);
                    $('#name').val(data.name);
                    $('#website_id').val(data.website_id);
                    $('#status').val(data.status);
                    $('#btn-save').val("update");
                    $('#addMemberType').modal('show');
                }); 
            });

            $('.btn-del-member').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: 'terms/delete',
                    data: data,
                    success: function(data) {
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelMembership').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });  
        });
    </script>
@endsection