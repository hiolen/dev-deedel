@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Add Pages</h2>
    @include('admin.content.inc.pages-nav')
    <form action="{{ super_admin_content_pages_save_url() }}" method="POST" id="addPricingCoinsForm" role="form">
        {!! csrf_field() !!}
        {{ form_input("text","title","","Title","form-control","Title") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("content","","Short Description","form-control text-editor","Short Description") }}
        <p><select name="website_id" class="form-control">
            @foreach($websites as $website)
        	<option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
            @endforeach
        </select></p>
        {{ form_input("text","status","","Slug","form-control","Slug") }}
        @include('admin.content.inc.pages-template')
        {{ form_submit("Save") }}
        {{ form_submit("Publish") }}
        <!-- make sure to store author_id=login user, content_type=page, status=draft when save, status=publish when publish is clicked , date added -->
    </form>
</div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}
@endsection