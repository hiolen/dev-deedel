@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Contents</h2>
    <ul>
        <li><a href="{{ super_admin_content_pages_url() }}">Pages</a></li>
        <li><a href="{{ super_admin_content_landing_pages_url() }}">Landing Pages</a></li>
        <li><a href="{{ super_admin_content_emails_url() }}">Email Templates</a></li>
        <li><a href="{{ super_admin_content_blog_url() }}">Blogs</a></li>
        <li><a href="{{ super_admin_content_support_url() }}">Support Articles</a></li>
        <li><a href="{{ super_admin_content_terms_url() }}">Terms & Definition</a></li>
    </ul>
    
</div>
@endsection
