@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Add Support Articles Category</h2>
    <a href="{{ super_admin_content_support_url() }}" class="btn btn-primary">All Support Articles</a>
    <a href="{{ super_admin_content_support_category_add_url() }}" class="btn btn-primary">Add New Support Category</a>
    <a href="{{ super_admin_content_support_category_url() }}" class="btn btn-primary">All Support Categories</a>
    <form>
        {{ form_input("text","name","","Name","form-control","Name") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("shortdesc","","Short Description","form-control text-editor","Short Description") }}
        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
        <p>Network
        <select class="form-control">
            <option>network name (domain)</option>
        </select>
        </p>
        {{ form_submit("Save") }}
        {{ form_submit("Publish") }}
        <!-- make sure to store author_id=login user, post_type=support, status=draft when save, status=publish when publish is clicked , date added -->
    </form>
</div>
@endsection
