@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Pages</h2>
    @include('admin.content.inc.pages-nav')
    <p>Display by Network
    <meta name="csrf-token" content="{{ csrf_token() }}" /> 
    <select id="network" class="form-control"> 
        @foreach($websites as $website)
        <option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
        @endforeach
    </select></p>
    <form>
        <input type="search" name="" placeholder="Search Support Articles" class="form-control">
        {{ form_submit("Search") }}
    </form>
    <table class="table bordered" id="termAll">
    	<thead>
    		<tr>
    			<th>Title</th>
    			<th>Status</th>
    			<th>Network</th>
    			<th>Date Posted</th>
    			<th>Posted by</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
            @foreach($pages as $page)
                <tr>
                    <td>{{$page->title}}</td>
                    <td>{{$page->status}}</td>
                    <td>{{$page->website['name']}}</td>
                    <td>{{$page->date_added}}</td>
                    <td>{{$page->authorName['name']}}</td>
                    <td><a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
                </tr>
            @endforeach
    	</tbody>
    </table>
    <table class="table bordered" id="output">
        
    </table>
</div>
@endsection

@section('script_footer_admin')
    <script type="text/javascript">
        $(function() {
            $('#network').on('change', function(e) {
                e.preventDefault(e);  
                    $.ajax({
                    type: "POST",
                    url: "/account/admin/content/pages/arr",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType : "html",
                    data: { selectedNetwork: $('#network').val() },
                    success: function(data) {
                        var obj = $.parseJSON(data);
                        $("#termAll").hide();
                        var str = "<thead><tr><th>Title</th><th>Status</th><th>Network</th><th>Date Posted</th><th>Posted by</th><th>Action</th></tr></thead><tbody>";
                        $.each( obj, function( key, value ) {
                            str = str + "<tr><td>" + value.title + "</td><td>" + value.status + "</td><td>" + value.webName + "</td><td>" + value.date_added + "</td><td>" + value.authorName +"</td><td><a href=#>View</a>|<a href=#>Edit</a>|<a href=#>Delete</a></td></tr>";
                        });
                        str = str + "</tbody>";
                        $("#output").html(str);
                        
                     }
                });           
            });
        });
    </script>
@endsection

            
                