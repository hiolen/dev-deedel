@extends('admin.inc.app')

@section('content_admin')
<div class="container">
    <h1>My Support Tickets</h1>
    <p><a href="#" id="btn-add" name="btn-add" class="btn btn-primary">Add Ticket</a></p>
    <p>Display by <select class="form-control">
        <option value="user">User</option>
        <option value="assignee">Assignee</option>
        <option value="data">Date</option>
        <option value="priority">Priority</option>
        <option value="status">Status</option>
    </select></p>
    <form>
        <input type="text" name="" class="form-control" placeholder="username">
    </form>
    <form>
        <input type="text" name="" class="form-control" placeholder="staffusername">
    </form>
    <form>
        <input type="text" name="" class="form-control" placeholder="calendar here">
    </form>
    <form>
        <select class="form-control">
            <option value="0">Select Priority</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </form>
    <form>
        <select class="form-control">
            <option value="all">Select Status</option>
            <option value="hold">hold</option>
            <option value="open">open</option>
            <option value="close">close</option>
        </select>
    </form>
    <table>
        <thead>
            <tr>
                <th>Subject</th>
                <th>Category</th>
                <th>User</th>
                <th>Assignee</th>
                <th>Priority</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="support-list" name="support-list">
            @foreach ($supports as $key => $support)
            <tr class="item{{ $support->id }}">
                <td><a href="#">{{ $support->title }}</a></td>
                <td>category name</td>
                <td><a href="userprofile">username</a></td>
                <td>staff username</td>
                <th>0</th>
                <td>date</td>
                <td>open/close/hold</td>
                <td><a href="">reply</a>|<a href="">assign</a>|<a href="">view</a>|<a href="">delete</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {!! $supports->render() !!}
  
</div>
@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="myModal" aria-labelledby="modalSupportLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalSupportLabel">Add Support</h4>
                </div>
                <form action="#" method="POST" id="supportForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        {{ form_input("text","subject","","Subject","form-control","Subject") }}
                        {{ form_input("text","slug","","Slug","form-control","Slug") }}
                        {{ form_texteditor("content","","Content","form-control text-editor","Content") }}
                        <p><select name="category" class="form-control">
                            <option value="support_category_id">Support Category</option>
                            <option value="support_category_id">Support Category</option>
                        </select></p>
                        <p><select name="assignee" class="form-control">
                            <option value="0">Support Admin</option>
                            <option value="staff_user_id">Support Staff</option>
                        </select></p>
                        <p><select name="priority" class="form-control">
                            <option value="0">Set Priority</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select></p>
                        <input type="text" name="" placeholder="username" class="form-control">
                        <div>
                            user details display when user textbox is fill in by keyup
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="support_id" name="support_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Role Modal -->
    <div class="modal fade modalDelSupport" tabindex="-1" role="dialog" id="modalDelSupport" aria-labelledby="modalDelSupportLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelSupportLabel">Delete <span class="support-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delSupportForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_header_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}
    {{ admin_network_footer_js() }}

    <script type="text/javascript">
        $(function() {
            //display modal form for creating new task
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#supportForm').trigger("reset");
                $('#myModal').modal('show');
            });

            //create new task / update existing task
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var support_id = $('#support_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'support';

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + website_id;
                }

                // if ($('#supportForm').valid()) {
                    $.ajax({
                        type: type,
                        url: my_url,
                        data: $('#supportForm').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                // }else
                //     return false;               

            });
        });
    </script>

@endsection