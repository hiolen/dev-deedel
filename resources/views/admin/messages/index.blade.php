@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <ul>
        <li><a href="{{ super_admin_messages_inbox_url() }}"><i class="fa fa-envelope" aria-hidden="true"></i> Inbox</a></li>
        <li><a href="{{ super_admin_messages_reports_url() }}"><i class="fa fa-file-text" aria-hidden="true"></i> Reports</a></li>
        <li><a href="{{ super_admin_messages_support_url() }}"><i class="fa fa-ticket" aria-hidden="true"></i> Support Tickets</a></li>
        <li><a href="{{ super_admin_messages_support_ticket_add_url() }}"><i class="fa fa-ticket" aria-hidden="true"></i> Create Support Tickets</a></li>
    </ul>
</div>
@endsection