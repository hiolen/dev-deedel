@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h1>Create Ticket</h1>
    <div>
        <form>
            {{ form_input("text","subject","","Subject","form-control","Subject") }}
            {{ form_input("text","slug","","Slug","form-control","Slug") }}
            {{ form_texteditor("content","","Content","form-control text-editor","Content") }}
            <p><select name="category" class="form-control">
                <option value="support_category_id">Support Category</option>
                <option value="support_category_id">Support Category</option>
            </select></p>
            <p><select name="assignee" class="form-control">
                <option value="0">Support Admin</option>
                <option value="staff_user_id">Support Staff</option>
            </select></p>
            <p><select name="priority" class="form-control">
                <option value="0">Set Priority</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select></p>
            <input type="text" name="" placeholder="username" class="form-control">
            <div>
                user details display when user textbox is fill in by keyup
            </div>
        </form>
    </div>
</div>
@endsection
