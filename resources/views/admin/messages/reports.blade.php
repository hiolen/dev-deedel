@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
	<h2>Reports</h2>
	<table>
		<thead>
			<tr>
				<th>reported by</th>
				<th>page reported</th>
				<th>details</th>
				<th>status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><a href="">user name</a></td>
				<td><a href="slug">page name</a></td>
				<td>report content</td>
				<td>new/invistigating/fixed/denied/deleted</td>
				<td><a href="#">reply</a>|<a href="#">delete</a></td>
			</tr>
			<tr>
				<td><a href="">user name</a></td>
				<td><a href="slug">page name</a></td>
				<td>report content</td>
				<td>new/invistigating/fixed/denied/deleted</td>
				<td><a href="#">reply</a>|<a href="#">delete</a></td>
			</tr>
		</tbody>
	</table>
</div>
@endsection