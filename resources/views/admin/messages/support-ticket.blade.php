@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <p>Status: Hold</p>
    <div class="content">
        <h3>Articles title</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula leo ac mollis dapibus. Pellentesque vitae maximus nunc. Duis ante est, tempus vel nisl non, blandit mollis enim. Etiam vitae mi id dui dignissim pulvinar. Nunc mollis blandit lacus quis aliquet. Sed lobortis imperdiet massa, nec feugiat ligula pulvinar ac. Praesent commodo molestie lacus. Morbi placerat ornare orci eget blandit. Aenean vel accumsan mi. Etiam quis risus ligula. Praesent imperdiet bibendum risus, id venenatis turpis hendrerit vitae. Sed luctus purus lacus, vel finibus est ultricies sed. Proin eget turpis turpis.</p>
        <p>Praesent ut tortor ut dui ullamcorper vulputate at vel enim. Cras scelerisque nibh sed posuere molestie. Curabitur eleifend ultrices tristique. Vestibulum pharetra est non efficitur viverra. Curabitur ac sem lectus. Vestibulum vitae faucibus augue, vel commodo quam. Etiam lobortis maximus sapien vitae auctor.</p>
        <p>Duis sit amet eros eu tellus placerat dignissim. Nullam commodo eros vitae sapien tristique, et imperdiet neque placerat. Praesent dignissim ante arcu, sit amet venenatis ligula vestibulum sit amet. Nam a arcu sit amet ex maximus porta. Pellentesque metus mauris, rutrum ut tellus nec, condimentum fringilla felis. Vestibulum accumsan tortor metus, ac sollicitudin turpis elementum id. Sed faucibus ac turpis in laoreet. Pellentesque vel gravida nibh, non consequat risus. Nunc condimentum sem a cursus ultrices. Nam eleifend vel tellus eget consequat. Cras finibus, mi et blandit porttitor, justo nisi dapibus diam, quis dictum est elit in ligula. Sed nunc urna, blandit et tortor at, sodales blandit turpis. Curabitur ac lectus sit amet ligula egestas hendrerit a at velit. Integer ut maximus neque, nec condimentum tortor. Vestibulum luctus scelerisque augue vitae lobortis. Praesent id mattis justo, vestibulum suscipit lectus.</p>
    </div>
    <footer>
    <a href="#" class="btn btn-primary">Reply</a>
    </footer>
</div>
@endsection
