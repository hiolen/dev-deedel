@include('admin.inc.htmlhead')
<body class="page-admin">
	<div class="page-wrapper">
		@include('admin.inc.header')
	    @yield('content_admin')
		@include('admin.inc.footer')
	</div>
	@yield('modal_footer_admin')
	@yield('script_footer_admin')
</body>
</html>
