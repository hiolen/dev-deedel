<div class="admin-nav">
	<div>
		<a href="#networkNavItems" data-toggle="collapse" class="network-dashboard-menu">Menu</a>
	</div>
	<ul class="admin-nav-items collapse" id="networkNavItems">
		<li>
			<a href="#adminMessage" data-toggle="collapse">Messages</a>
			<ul class="nav-subItems collapse" id="adminMessage">
				<li><a href="{{ super_admin_messages_inbox_url() }}"><i class="fa fa-envelope" aria-hidden="true"></i> Inbox</a></li>
				<li><a href="{{ super_admin_messages_reports_url() }}"><i class="fa fa-file-text" aria-hidden="true"></i> Reports</a></li>
				<li><a href="{{ super_admin_messages_support_url() }}"><i class="fa fa-ticket" aria-hidden="true"></i> Support Tickets</a></li>
				<li><a href="{{ super_admin_messages_support_ticket_add_url() }}"><i class="fa fa-ticket" aria-hidden="true"></i> Create Support Tickets</a></li>
			</ul>
				
		</li>
		<li>
			<a href="#adminnNetwork" data-toggle="collapse">Network</a>
			<ul class="nav-subItems collapse" id="adminnNetwork">
				<li><a href="{{ super_admin_network_add_url() }}"Add a Network</a></li>
	            <li><a href="{{ super_admin_network_url() }}">All Network</a></li>
	            <li><a href="{{ super_admin_network_dashboard_url() }}">All Network Dashboard</a></li>
			</ul>
		</li>
		<li>
	        <a href="#adminCoupons" data-toggle="collapse">Coupons</a>
	        <ul class="nav-subItems collapse" id="adminCoupons">
	            <li><a href="{{ super_admin_coupons_add_url() }}"><i class="fa fa-plus" aria-hidden="true"></i> Add a coupon</a></li>
	            <li><a href="{{ super_admin_coupons_upload_url() }}"><i class="fa fa-upload" aria-hidden="true"></i> Upload coupons</a></li>
	            <li><a href="{{ super_admin_coupons_url() }}"><i class="fa fa-tags" aria-hidden="true"></i> All Coupons</a></li>
	        </ul>
		</li>
	</ul>
</div>
