<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>DeeDel</title>
	{{ icons() }}
	{{ global_css() }}
	{{ admin_style_css() }}
	{{ admin_css() }}
	{{ adminNetwork_style_css() }}
	{{ admin_jquery_js() }}
	{{ admin_bootstrap_js() }}
	{{ admin_jquery_validate_js() }}

	@yield('script_header_admin')
</head>