@include('admin.inc.htmlhead')
<body>
    @yield('content_admin')
	@include('admin.inc.footer')
	@yield('modal_footer_admin')
	@yield('script_footer_admin')
</body>
</html>
