<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">

      <!-- .Logo -->
      <div class="top-left-part"><a class="logo" href="{{ deedel_url() }}"><img src="{{ deedel_url() }}/assets/elite/eliteadmin-small-logo.png" alt="home" class="light-logo" /></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="logotext"><!--This is logo text--><img src="{{ deedel_url() }}/assets/elite/eliteadmin-small-text.png" alt="home" class="light-logo" alt="home" /></a></li>
      </ul>
      <!-- /.Logo -->

      <!-- top right panel -->
      <ul class="nav navbar-top-links navbar-right pull-right">
        

        @if (Auth::guest())
            <li><a href="#modalLogin" data-toggle="modal">Login</a></li>
            <li><a href="#modalRegister" data-toggle="modal">Register</a></li>
        @else
            <!-- .dropdown -->
            <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
              <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
              </a>
              <ul class="dropdown-menu mailbox animated bounceInDown">
                <li>
                  <div class="drop-title">You have 4 new messages</div>
                </li>
                <li>
                  <div class="message-center"> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Pavan kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                    </a> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Sonu Nigam</h5>
                      <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                    </a> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Arijit Sinh</h5>
                      <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                    </a> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Pavan kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                    </a> </div>
                </li>
                <li> <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
              </ul>
              <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li><a href="{{ account_wallet_url() }}"><i class="fa fa-money"></i></a></li>
            <!-- .dropdown -->
            <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{ deedel_url() }}/assets/elite/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ Auth::user()->name }}</b> </a>
              <ul class="dropdown-menu dropdown-user animated flipInY">
                <li><a href="{{ account_url() }}"><i class="ti-user"></i> Dashboard</a></li>
                <li><a href="{{ account_url() }}/profile"><i class="ti-user"></i> My Profile</a></li>
                <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ account_url() }}/settings"><i class="ti-settings"></i> Account Setting</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ logout_url() }}"><i class="fa fa-power-off"></i> Logout</a></li>
              </ul>
              <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->

            <!-- .right toggle -->
            <li class="right-side-toggle"><a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-arrow-right"></i></a></li>
            <!-- /.right toggle -->
        @endif
      </ul>
      <!-- top right panel -->

    </div>
  </nav>
  <!-- End Top Navigation -->
  
@if (Auth::guest())
@else
  <!-- .Side panel -->
  <div class="side-mini-panel">
       <ul class="mini-nav">

              <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="ti-menu"></i></a></div>

  <li class="selected">
    <a href="javascript:void(0)"><i data-icon=")" class="linea-icon linea-basic"></i></a>
    <div class="sidebarmenu">
      <h3 class="menu-title">Messages</h3>
      <div class="searchable-menu">
        <form role="search" class="menu-search">
          <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a>
        </form>
      </div>
      <ul class="sidebar-menu">
        <li><a href="{{ super_admin_messages_inbox_url() }}">Inbox</a></li>
        <li><a href="compose">Compose mail</a></li>
        <li><a href="chat">Chat message</a></li>
        <li><a href="contacts">Contacts</a></li>
      </ul>
    </div>
  </li>
  <li>
    <a href="javascript:void(0)"><i data-icon="7" class="linea-icon linea-basic"></i></a>
    <div class="sidebarmenu">
      <h3 class="menu-title">Get Help</h3>
      <div class="searchable-menu">
        <form role="search" class="menu-search">
          <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a>
        </form>
      </div>
      <ul class="sidebar-menu">
        <li><a href="{{ super_admin_messages_support_url() }}">Tickets</a></li>
        <li><a href="{{ super_admin_messages_support_url() }}">Articles</a></li>
      </ul>
    </div>
  </li>
  <li>
    <a href="javascript:void(0)"><i data-icon="&#xe006;" class="linea-icon linea-basic fa-fw"></i></a>
    <div class="sidebarmenu">
      <h3 class="menu-title">Network</h3>
      <div class="searchable-menu">
        <form role="search" class="menu-search">
          <input type="text" placeholder="Search..." class="form-control">
          <a href=""><i class="fa fa-search"></i></a>
        </form>
      </div>
      <ul class="sidebar-menu">
        <li><a href="{{ super_admin_network_url() }}">All Network</a></li>
        <li><a href="{{ super_admin_network_add_url() }}">Add Network</a></li>
      </ul>
    </div>
  </li>
  <!-- /.Charts -->
              <!-- .Dashboard -->
              <li>
                <a href="javascript:void(0)"><i class="linea-icon linea-basic" data-icon="v"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Dashboard</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                  </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="index.html">Minimalistic</a></li>
                              <li><a href="index2.html">Demographical</a></li>
                              <li><a href="index3.html">Analitical</a></li>
                              <li><a href="index4.html">Simpler</a></li>
                              <hr>
                              <h3 class="menu-title">Other Demos</h3>
                              <li><a href="../eliteadmin-inverse/index.html" target="_blank">Eliteadmin Inverse</a></li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div>
              </li>
              <!-- /.Dashboard -->

              <!-- .Ui Elemtns -->
              <li><a href="javascript:void(0)"><i data-icon="/" class="linea-icon linea-basic"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Rewards</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="panels-wells.html">Panels and Wells</a></li> <li><a href="panel-ui-block.html">Panels With BlockUI</a></li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div> 
              </li>
              <!-- /.Ui Elemtns -->

              <!-- .Forms -->
              <li class=""><a href="javascript:void(0)"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Contents</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="form-basic.html">Basic Forms</a></li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div> 
              </li>  
              <!-- /.Forms -->

              <!-- .Sample Pages -->
              <li class=""><a href="javascript:void(0)"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Assets</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="starter-page.html">Starter Page</a></li>
                              <li><a href="blank.html">Blank Page</a></li>
                              <li class="menu"><a href="javascript:void(0)">Email Templates <i class="fa fa-angle-left pull-right"></i></a>
                                <!-- Second level submenu -->
                                <ul class="sub-menu">
                                  <li> <a href="../email-templates/basic.html">Basic</a></li>
                                  <li> <a href="../email-templates/alert.html">Alert</a></li>
                                  <li> <a href="../email-templates/billing.html">Billing</a></li>
                                  <li> <a href="../email-templates/password-reset.html">Reset Pwd</a></li>
                                </ul>
                                <!-- Second level submenu -->
                              </li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div>
              </li>
              <!-- /.Sample Pages -->


              <li class=""><a href="javascript:void(0)"><i data-icon="O" class="linea-icon linea-software fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Members</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="basic-table.html">Basic Tables</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>

              <li class=""><a href="javascript:void(0)"><i data-icon="O" class="linea-icon linea-software fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Pricing</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="basic-table.html">Basic Tables</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              


              <!-- .Widgets -->
              <li class=""><a href="javascript:void(0)"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Reports</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="widgets.html">All widgets</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Widgets -->

              <!-- .Icons -->
              <li class=""><a href="javascript:void(0)"><i data-icon="7" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Payments</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="fontawesome.html">Font awesome</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Icons -->

              <!-- .Maps -->
              <li class=""><a href="javascript:void(0)"><i data-icon="Q" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Coupons</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="map-google.html">Google Maps</a></li>
                            <li><a href="map-vector.html">Vector Maps</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Maps -->
           </ul>
  </div>
  <!-- /.Side panel -->
@endif

  <!-- Page Content -->
  <div class="page-wrapper">

    <nav class="navbar navbar-default navbar-static-top" style="display:none">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ deedel_url() }}">
                    DeeDel
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ websites_url() }}">Network</a></li>
                    <li><a href="{{ coins_url() }}">Buy Coins</a></li>
                    <li class="dropdown">
                        <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            More
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="{{ about_url() }}">About</a></li>
                            <li><a href="{{ contact_url() }}">Contact Us</a></li>
                            <li><a href="{{ faq_url() }}">FAQ</a></li>
                            <li><a href="{{ terms_definition_url() }}">Terms & Definition</a></li>
                            <li><a href="{{ blog_url() }}">Blog</a></li>
                            <li><a href="{{ support_url() }}">Support Center</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="#modalLogin" data-toggle="modal">Login</a></li>
                        <li><a href="#modalRegister" data-toggle="modal">Register</a></li>
                    @else
                        <li><a href="{{ account_wallet_url() }}"><i class="fa fa-money"></i></a></li>
                        <li><a href="{{ cart_url() }}"><i class="fa fa-shopping-cart"></i> ({{ Cart::instance('main')->count(false) }})</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ account_url() }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{ account_network_url() }}"><i class="fa fa-dashboard"></i> My Network</a></li>
                                <li><a href="{{ account_url() }}/profile"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="{{ account_url() }}/settings"><i class="fa fa-gear"></i> Settings</a></li>
                                <li><a href="{{ logout_url() }}"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>