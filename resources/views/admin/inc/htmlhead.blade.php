<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DeeDel</title>
    {{ bootstrap_front_css() }}
    {{ front_bower_components_toast_css() }}
    {{ front_bower_components_morrisjs_css() }}
    {{ front_animate_css() }}
    {{ front_style_css() }}
    {{ front_blue_css() }}
    {{ deedel_account_style_css() }}
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('script_header_admin')
</head>
<body id="app-layout" class="fix-header page-admin">