@include('admin.inc.htmlhead')
    @include('admin.inc.header')

    @yield('content_admin')
    @include('admin.inc.footer')
    @yield('modal_footer_admin')

    {{ jquery_js() }}
    {{ bootstrap_front_js() }}
    {{ slimscroll_js() }}
    {{ waves_js() }}
    {{ waypoints_js() }}
    {{ counterup_js() }}
    {{ raphael_js() }}
    {{ custom_js() }}
    {{ dashboard1_js() }}
    {{ sparkline_js() }}
    {{ charts_sparkline_js() }}
    {{ toast_js() }}
    {{ admin_jquery_validate_js() }}
    <script type="text/javascript">
      
      $(document).ready(function() {
          $.toast({
            heading: 'Welcome to Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'info',
            hideAfter: 3500, 
            
            stack: 6
          });
          $('.vcarousel').carousel({
                interval: 3000
           });
        });
        
    </script>
    @yield('script_footer_admin')
</body>
</html>
