@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Pricing Membership</h2>
    <a href="#" data-toggle="modal" class="btn btn-primary btn-add-meta" id="btn-add"><i class="fa fa-plus"></i>Add New Membership Type</a>
    <form>
        <input type="text" name="" placeholder="Search by name or description" class="form-control">
        {{ form_submit("Save") }}
    </form>
    <form>
        Display by Network
        <select name="website_id" class="form-control">
            @foreach($websites as $website)
                <option value="{{$website->id}}">{{$website->name}}({{$website->domain}})
                </option>
            @endforeach
        </select>
    </form>
    <form>
        Display by Pricing Type
        <select name="pricing_type" class="form-control">
            <option value="monthly">Monthly</option>
        </select>
    </form>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Slug</th><!-- doesnt need to be unique -->
                <th>Description</th>
                <th>Network</th><!-- use website_id -->
                <th>Price</th><!-- this represent green coins -->
                <th>Discount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($members as $member)
            <tr class="item{{$member->id}}">
                <td>{{$member->id}}</td>
                <td>{{$member->name}}</td>
                <td>{{$member->slug}}</td>
                <td>{{$member->desc}}</td>
                <td>{{$member->website->name}} (<a href="" target="_blank">{{$member->website->domain}}</a>)</td>
                <td>{{$member->price}}</td>
                <td>{{$member->discount}}</td>
                <td>
                    <button class="btn btn-info btn-xs btn-detail open-modal" value="{{ $member->id }}"><i class="fa fa-files-o"></i> Edit</button> | 
                    <a href="#modalDelMembership" data-toggle="modal" data-id="{{ $member->id }}" data-name="{{ $member->name }}" class="btn-del-member btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addMemberType" aria-labelledby="modalAddCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalMemberTypeLabel">Add New Activity Type</h4>
                </div>
                <form action="#" method="POST" id="addMemberTypeForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" id="membership-id">
                    <div class="modal-body">
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        {{ form_input("text","slug","","Slug","form-control","Slug") }}
                        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
                        <p>Network</p>
                        <select name="website_id" id="website_id">
                            @foreach($websites as $website)
                                <option value="{{$website->id}}">{{$website->name}}({{$website->domain}})
                                </option>
                            @endforeach
                        </select>
                        {{ form_input("number","price","","Price","form-control","Price") }}
                        {{ form_input("decimal","discount","","Discount","form-control","Discount") }}
                        <p>Pricing Type</p>
                        <p><select name="pricing_type" id="pricing_type">
                            <option value="">Choose type...</option>
                            <option value="onetime">One Time</option>
                            <option value="monthly">Monthly</option>
                            <option value="quarterly">Quarterly</option>
                            <option value="semiunnualy">Semi-Unnualy</option>
                            <option value="annualy">Annualy</option>
                            <option value="triennially">Triennially</option>
                        </select></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="membership_id" name="membership_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Role Modal -->
    <div class="modal fade modalDelMembership" tabindex="-1" role="dialog" id="modalDelMembership" aria-labelledby="modalDelMembershipLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelMembershipLabel">Delete <span class="membership-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delMembershipForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {
            //display modal form for creating new task
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#addMemberTypeForm').trigger("reset");
                $('#addMemberType').modal('show');
            });
            
            //create new Network / update new Network
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var membership_id = $('#membership_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'membership';
                var dataType = $("#membership-id").val();

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + membership_id;
                }

                // if ($('#formMeta').valid()) {
                    $.ajax({
                        type: type,
                        url: my_url,
                        data: $('#addMemberTypeForm').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                // }else
                //     return false;
            });  

            // Edit User
            $('.open-modal').click(function() {
                var membership_id = $(this).val();

                $.get('membership' + '/' + membership_id, function (data) {
                    //success data
                    console.log(data);
                    $('#membership_id').val(data.id);
                    $('#name').val(data.name);
                    $('#slug').val(data.slug);
                    $('#description').val(data.description);
                    $('#website_id').val(data.website_id);
                    $('#price').val(data.price);
                    $('#discount').val(data.discount);
                    $('#pricing_type').val(data.pricing_type);
                    $('#btn-save').val("update");

                    $('#addMemberType').modal('show');
                }); 
            });

            // Delete Network Ajax
            $('.btn-del-member').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);
                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: 'membership/delete',
                    data: data,
                    success: function(data) {
                        console.log(data);
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelMembership').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });            

            // Validate Add Role Form
            $('#addMemberTypeForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });

        });
    </script>
@endsection