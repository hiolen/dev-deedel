@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <h2>Pricing Add Coins</h2> 
    <form action="{{ super_admin_pricing_coins_save_url() }}" method="POST" id="addPricingCoinsForm" role="form">
        {!! csrf_field() !!}
    	Coins Type
    	<select name="coins_type_id" id="cointype">
            @foreach($coins_type as $coin_type)
    		<option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
            @endforeach
    	</select>
    	{{ form_input("text","name","","Name","form-control","Name") }}
        <p>Value</p>
    	<input type="number" name="coins_value" placeholder="Value" class="form-control" id="coinvalue">
        <p>Price</p>
        <input type="number" name="price" placeholder="Price" class="form-control" id="price">
        {{ form_texteditor("desc","","Add Description","form-control text-editor","Description") }}
    	<p>Discount Type</p>
    	<select name="discount_type">
    		<option value="percent">Percentage</option>
    		<option value="fixed">Fixed Value</option>
    	</select>
    	{{ form_input("number","discount_value","","Discount Value","form-control","Discount Value") }}
        {{ form_submit("Save") }}
    </form> 

</div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}
    <!-- Jquery Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

    <script type="text/javascript">
        
        $(function() {

            $('#cointype,#coinvalue').on('change keyup', function(e) {
                e.preventDefault(e);  
                if ($(this).valid()) {
                    $.ajax({
                    type: "POST",
                    url: "/account/admin/pricing/coins/adds",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {selectedCoinType:$('#cointype').val()},
                    success: function(data) {
                         var coin_price = data.price;
                         var coin_value = $('#coinvalue').val();
                         var price = coin_price * coin_value;
                        $('#price').val(price);
                     }
                 });
                } else {
                    return false;
                }
            });
           
            // Add Network
            
            $('#addPricingCoinsForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_pricing_coins_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });
           
        });
    </script>
@endsection