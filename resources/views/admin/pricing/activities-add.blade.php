@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Pricing Add Activities</h2>
    <form action="{{ super_admin_pricing_activities_save_url() }}" method="POST" id="addActivityTypeForm" role="form">
        {!! csrf_field() !!}
        <div class="modal-body">
            {{ form_input("text","name","","Name","form-control","Name") }}
            {{ form_input("text","activity_code","","Activity Code","form-control","Activity Code") }}
            {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
            <p>Coins Type</p>
            <select name="coins_type_id">
                @foreach($coins_type as $coin_type)
                <option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
                @endforeach
            </select>
            {{ form_input("number","amount","","Amount","form-control","Amount") }}
            <p>Pricing Type</p>
            <p><select name="pricing_type">
                <option value="onetime">One Time</option>
                <option value="monthly">Monthly</option>
                <option value="quarterly">Quarterly</option>
                <option value="semiunnualy">Semi-Unnualy</option>
                <option value="annualy">Annualy</option>
                <option value="triennially">Triennially</option>
            </select></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ form_submit("Save") }}
        </div>
    </form>
</div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {
            // Add Network
            $('#addActivityTypeForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_pricing_activities_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });
           
        });
    </script>
@endsection