@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Pricing Activities</h2>
    <a href="#" id="btn-add" class="btn btn-primary btn-add-activity"><i class="fa fa-plus"></i>Add New Activity Type</a>
    <table>
        <thead>
            <tr>
                <th>Activity Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Coins Type</th>
                <th>Amount</th>
                <th>Pricing Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="activity-list" name="activity-list">  
            @foreach($price_acts as $price_act)
                <tr class="item{{ $price_act->id }}">
                    <td>{{$price_act->activity_code}}</td>
                    <td>{{$price_act->name}}</td>
                    <td>{{$price_act->desc}}</td>
                    <td>{{$price_act->coinsType->name}}</td>
                    <td>{{$price_act->amount}}</td>
                    <td>{{$price_act->pricing_type}}</td>
                    <td>
                        <a href="#" class="btn-detail open-modal" data-value="{{ $price_act->id }}">Edit</a> | 
                        <a href="#modalDelActivity" data-toggle="modal" data-id="{{ $price_act->id }}" data-name="{{ $price_act->name }}" class="btn-del-activity">Delete</a>
                    </td>
               </tr>
            @endforeach        
        </tbody>
    </table>
    {!! $price_acts->render() !!}
</div>
@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addActivityType" aria-labelledby="modalAddCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddCouponLabel">Add New Activity Type</h4>
                </div>
                <form action="#" method="POST" id="addActivityTypeForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        {{ form_input("text","activity_code","","Activity Code","form-control","Activity Code") }}
                        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
                        <p>Coins Type</p>
                        <select name="coins_type_id" id="coins_type_id">
                            @foreach($coins_type as $coin_type)
                            <option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
                            @endforeach
                        </select>
                        {{ form_input("number","amount","","Amount","form-control","Amount") }}
                        <p>Pricing Type</p>
                        <p><select name="pricing_type" id="pricing_type">
                            <option value="onetime">One Time</option>
                            <option value="monthly">Monthly</option>
                            <option value="quarterly">Quarterly</option>
                            <option value="semiunnualy">Semi-Unnualy</option>
                            <option value="annualy">Annualy</option>
                            <option value="triennially">Triennially</option>
                        </select></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="activity_id" name="activity_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Activity Modal -->
    <div class="modal fade modalDelActivity" tabindex="-1" role="dialog" id="modalDelActivity" aria-labelledby="modalDelActivityLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelRoleLabel">Delete <span class="role-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delActivityForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {

            //display modal form for creating new Activity
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#addActivityTypeForm').trigger("reset");
                $('#addActivityType').modal('show');
            });

            // Edit Activity
            $('.open-modal').click(function() {
                var activity_id = $(this).attr('data-value');

                $.get('activities' + '/' + activity_id, function (data) {
                    //success data
                    console.log(data);
                    $('#activity_id').val(data.id);
                    $('#name').val(data.name);
                    $('#activity_code').val(data.activity_code);
                    $('#desc').val(data.desc);
                    $('#coins_type_id').val(data.coins_type_id);
                    $('#amount').val(data.amount);
                    $('#pricing_type').val(data.pricing_type);
                    $('#btn-save').val("update");

                    $('#addActivityType').modal('show');
                }); 
            });

            //create new task / update existing task
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var activity_id = $('#activity_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'activities';

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + activity_id;
                }

                if ($('#addActivityTypeForm').valid()) {
                    $.ajax({
                        type: type,
                        url: my_url,
                        data: $('#addActivityTypeForm').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }else
                    return false;               

            });

            // DELETE Role Ajax
            $('.btn-del-activity').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);
                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: 'activities/delete',
                    data: data,
                    success: function(data) {
                        console.log(data);
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelActivity').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

            // Validate Add Role Form
            $('#addActivityTypeForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    activity_code: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
           
        });
    </script>
@endsection