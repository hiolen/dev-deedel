@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Pricing Coins</h2>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <a href="#addPricingCoins" data-toggle="modal" class="btn btn-primary btn-add-meta"><i class="fa fa-plus"></i>Add New Coin Type</a>
    <table>
        <thead>
            <tr>
                <th>Coins Type</th>
                <th>Coins Name</th>
                <th>Coins Value</th>
                <th>Coins Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="coin-list" name="coin-list">
            @foreach($coins as $key => $coin)
            <tr class="item{{$coin->id}}">
                <!-- <th>Coins Type</th> -->
                <th>{{ $coin->coinsType->name }}</th>
                <th>{{ $coin->name }}</th>
                <th>{{ $coin->coins_value}}</th>
                <th>{{ $coin->price }}</th>
                <td>
                    <a href="#" class="btn-detail open-modal" data-value="{{ $coin->id }}">Edit</a> | 
                    <a href="#modalDelCoin" data-toggle="modal" data-id="{{ $coin->id }}" data-name="{{ $coin->name }}" class="btn-del-coin">Delete</a>
                </td>
            </tr>
            @endforeach   

        </tbody>
    </table>
    {!! $coins->render() !!}
</div>
@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addPricingCoins" aria-labelledby="modalAddCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddPricingCoins">Pricing Coins</h4>
                </div>
                <form action="#" method="POST" id="addPricingCoinsForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        Coins Type
                        <select name="coins_type_id" id="cointype">
                            @foreach($coins_type as $coin_type)
                            <option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
                            @endforeach
                        </select>
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        <p>Value</p>
                        <input type="number" name="coins_value" placeholder="Value" class="form-control" id="coinvalue">
                        <p>Price</p>
                        <input type="number" name="price" placeholder="Price" class="form-control" id="price">
                        {{ form_texteditor("desc","","Add Description","form-control text-editor","Description") }}
                        
                        <p>Discount Type</p>
                        <select name="discount_type">
                            <option value="percent">Percentage</option>
                            <option value="fixed">Fixed Value</option>
                        </select>
                        {{ form_input("number","discount_value","","Discount Value","form-control","Discount Value") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-save" value="add">Submit</button>
                        <input type="hidden" id="coin_id" name="coin_id" value="0">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Role Modal -->
    <div class="modal fade modalDelCoin" tabindex="-1" role="dialog" id="modalDelCoin" aria-labelledby="modalDelCoinLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDelCoinLabel">Delete <span class="coin-del-title">Role</span></h4>
                </div>
                <form action="#" method="POST" id="delCoinForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {
            $('#cointype,#coinvalue').on('change', function(e) {
                e.preventDefault(e);  
                if ($(this).valid()) {
                    $.ajax({
                    type: "POST",
                    url: "/account/admin/pricing/coins/adds",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {selectedCoinType:$('#cointype').val()},
                    success: function(data) {
                         var coin_price = data.price;
                         var coin_value = $('#coinvalue').val();
                         var price = coin_price * coin_value;
                        $('#price').val(price);
                     }
                 });
                } else {
                    return false;
                }
            });
            
            //display modal form for creating new task
            $('#btn-add').click(function(){
                $('#btn-save').val("add");
                $('#addPricingCoinsForm').trigger("reset");
                $('#addPricingCoins').modal('show');
            });

            // Edit User
            $('.open-modal').click(function() {
                var coin_id = $(this).attr('data-value');

                $.get('coin' + '/' + coin_id, function (data) {
                    //success data
                    console.log(data);
                    $('#coin_id').val(data.id);
                    $('#name').val(data.name);
                    $('#price').val(data.price);
                    $('#desc').val(data.desc);
                    $('#btn-save').val("update");

                    $('#addPricingCoins').modal('show');
                }); 
            });

            //create new task / update existing task
            $("#btn-save").click(function (e) {
                e.preventDefault();
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var coin_id = $('#coin_id').val();
                var type = "POST"; //for creating new resource
                var my_url = 'coin';

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/' + coin_id;
                }

                if ($('#addPricingCoinsForm').valid()) {
                    $.ajax({
                        type: type,
                        url: my_url,
                        data: $('#addPricingCoinsForm').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }else
                    return false;               

            });

            // Delete Meta
            $('.btn-del-coin').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);
                
                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: 'coin/delete',
                    data: data,
                    success: function(data) {
                        console.log(data);
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelCoin').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

            // Validate Add Role Form
            $('#addPricingCoinsForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    },
                    coins_value: {
                        number: true,
                        required: true
                    },
                    price: {
                        number: true,
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
           
        });
    </script>
@endsection