@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Pricing</h2>
    <ul>
        <li><a href="{{ super_admin_pricing_coins_type_url() }}">Coins Type</a></li>
        <li><a href="{{ super_admin_pricing_coins_url() }}">Coins</a></li>
        <li><a href="{{ super_admin_pricing_membership_url() }}">Membership</a></li>
        <li><a href="{{ super_admin_pricing_activities_url() }}">Activities</a></li>
    </ul>    
</div>
@endsection
@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addCoinType" aria-labelledby="modalAddCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddCouponLabel">Set New Coins Type</h4>
                </div>
                <form action="{{ super_admin_pricing_coins_set_save_url() }}" method="POST" id="addCoinTypeForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        {{ form_input("text","name","","Name","form-control","Name") }}
                        {{ form_texteditor("desc","","Add Description","form-control text-editor","Description") }}
                        {{ form_input("decimal","price","","Price","form-control","Price") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_submit("Save") }}
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {
            // Add Network
            $('#addCoinTypeForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_pricing_coins_set_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });

            $(".btn-add-meta").click(function(){
                var parents = [];
                var t = $(this);
                var id = t.attr("data-type");
                var title = t.attr("data-title");

                $('#addCoinType').on('show.bs.modal', function () {
                  $("#meta-id").val(id);
                  $(".meta-add-title").text(title);
                });

                $('#addCoinType').on('hide.bs.modal', function () {
                  $("#meta-id").val("");
                  $(".meta-add-title").text("Meta");
                });

            });
           
        });
    </script>
@endsection