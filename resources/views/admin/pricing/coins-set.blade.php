@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h2>Set New Coins Type</h2>
    <form action="{{ super_admin_pricing_coins_set_save_url() }}" method="POST" id="addCoinTypeForm" role="form">
   		{!! csrf_field() !!}
    	{{ form_input("text","name","","Name","form-control","Name") }}
        {{ form_texteditor("desc","","Add Description","form-control text-editor","Description") }}
        {{ form_input("decimal","price","","Price","form-control","Price") }}
        {{ form_submit("Save") }}
    </form> 
</div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script type="text/javascript">
        $(function() {
            // Add Network
            $('#addCoinTypeForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_pricing_coins_set_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });
           
        });
    </script>
@endsection