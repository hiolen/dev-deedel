@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    <h4>Add Coupon</h4>
     <form action="{{ super_admin_coupons_save_url() }}" method="POST" id="addCouponForm" role="form">
        {!! csrf_field() !!}
        <div class="modal-body">
            Coupon Type
            <select id="couponType" name="types">
                <option value="coins">Coins</option>
                <option value="gifts">Gifts</option>
                <option value="points">Points</option>
            </select>
            <p>
            Coupon Value
            <!-- Will display when coins is selected -->
            <select id="coins" name="value">
                @foreach($coins_type as $coin_type)
                    <option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
                @endforeach
            <!-- display all coins -->
            </select>
            <!-- Will display when points/gifts is selected -->
            <select id="gifts" name="value">
                @foreach($websites as $website)
                    <option value="{{$website->id}}">{{$website->name}}({{$website->domain}})</option>
                @endforeach
            <!-- display all network -->
            </select>
            {{ form_input("text","amount","","Amount","form-control","Amount") }}
            <p>Coupon Code</p>
                <input class="form-control" type="hidden" name="coupon_code" value="{{$coupon_code}}">
            <p>{{$coupon_code}}</p>
            <p>Pin Code</p>
            <input class="form-control" type="hidden" name="pin_code" value="{{$pin_code}}">
            <p>{{$pin_code}}</p>
            <select name="status">
                <option value="active">Active</option>
                <option value="used">Used</option>
            </select>
        </div>
        <div class="modal-footer">
            {{ form_submit("Save") }}
        </div>
    </form>
</div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script>
        $(function() {
            //select coins
            $('#gifts').hide(); 
            $('#couponType').change(function(){
                if($('#couponType').val() == 'coins') {
                    $('#coins').show(); 
                    $('#gifts').hide(); 
                } else {
                    $('#coins').hide(); 
                    $('#gifts').show(); 
                } 
            });
            // Add Network
            $('#addCouponForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_coupons_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });

            $(".btn-add-meta").click(function(){
                var parents = [];
                var t = $(this);
                var id = t.attr("data-type");
                var title = t.attr("data-title");

                $('#modalAddMeta').on('show.bs.modal', function () {
                  $("#meta-id").val(id);
                  $(".meta-add-title").text(title);
                });

                $('#modalAddMeta').on('hide.bs.modal', function () {
                  $("#meta-id").val("");
                  $(".meta-add-title").text("Meta");
                });

            });

            // Edit Meta
            $('#editMetaForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                        type: "POST",
                        url: '{{ website_bahay_admin_edit_meta_url() }}',
                        data: $(this).serialize(),
                        success: function(data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        }, error: function() {
                            console.log("Error!");
                        }
                    });
                } else {
                    return false;
                }
            });
    
            // Delete Network Ajax
            $('.btn-del-meta').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);

                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: '{{ website_bahay_admin_delete_meta_url() }}',
                    data: data,
                    success: function(data) {
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelMeta').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

            // Validate Add Network Form
            $('#addMetaForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                messages: {
                    name: {
                        minLength: "Your Name of must be at least 3 characters",
                        maxLength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name of Meta",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
            

            // Validate Edit Meta Form
            $('#editMetaForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                messages: {
                    name: {
                        minLength: "Your Name of must be at least 3 characters",
                        maxLength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name of Meta",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
        });
    </script>
@endsection