@extends('admin.inc.app')

@section('script_header_admin')
@endsection
@section('content_admin')
<div class="container">
    @if (session()->has('success_message'))
    <div class="alert-msg">
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    </div>
    @endif

    @if (session()->has('error_message'))
    <div class="alert-msg">
        <div class="alert alert-danger">
            {{ session()->get('error_message') }}
        </div>
    </div>
    @endif

    <h2>Coupons</h2>

    <a href="#addCoupon" data-toggle="modal" class="btn-add-meta"><i class="fa fa-plus"></i> Add New Coupon</a>
    <a href="#modalUploadCoupon" data-toggle="modal" class="btn-upload-coupon"><i class="fa fa-plus"></i> Upload Coupons</a>
    <p>Display by Status <select class="form-control">
        <option value="active">Active</option>
        <option value="used">Used</option>
    </select></p>
    <p>Display rows by
        <select class="form-control">
            <option value="100">100</option>
            <option value="250">250</option>
            <option value="500">500</option>
        </select>
    </p>
    <p>Display by coupon type
        <select id="couponType" name="types" class="form-control">
            <option value="coins">Coins</option>
            <option value="gifts">Gifts</option>
            <option value="points">Points</option>
        </select>
    </p>
            Coupon Value
            <!-- Will display when coins is selected -->
            <select id="coins" name="value" class="form-control">
                @foreach($coins_type as $coin_type)
                    <option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
                @endforeach
            <!-- display all coins -->
            </select>
            <!-- Will display when points/gifts is selected -->
            <select id="gifts" name="value" class="form-control">
                @foreach($websites as $website)
                    <option value="{{$website->id}}">{{$website->name}}({{$website->domain}})</option>
                @endforeach
            <!-- display all network -->
            </select>
    <p>
        <a href="coupons/download-coupon/xls"><button class="btn btn-success">Download Excel xls</button></a>
        <a href="coupons/download-coupon/xlsx"><button class="btn btn-success">Download Excel xlsx</button></a>
        <a href="coupons/download-coupon/csv"><button class="btn btn-success">Download CSV</button></a>
    </p>
    <table>
    	<thead>
    		<tr>
                <th>ID</th>
                <th>Type</th>
                <th>Value</th>
                <th>Amount</th>
    			<th>Coupon CODE</th>
                <th>PIN CODE</th>
                <th>Status</th>
    		</tr>
    	</thead>
    	<tbody>
            @foreach($coupons as $coupon)
            <tr>
                <td>{{$coupon->id}}</td>
                <td>{{$coupon->types}}</td>
                <td>{{$coupon->coinsType->name}}</td>
                <td>{{$coupon->amount}}</td>
                <th>{{$coupon->coupon_code}}</th>
                <th>{{$coupon->pin_code}}</th>
                <td>{{$coupon->status}}</td>
            </tr>
            @endforeach
           
    	</tbody>
    </table>
    {!! $coupons->render() !!}
</div>
@endsection

@section('modal_footer_admin')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="addCoupon" aria-labelledby="modalAddCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalAddCouponLabel">Add Coupon</h4>
                </div>
                <form action="{{ super_admin_coupons_save_url() }}" method="POST" id="addCouponForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        Coupon Type
                        <select id="couponType" name="types">
                            <option value="coins">Coins</option>
                            <option value="gifts">Gifts</option>
                            <option value="points">Points</option>
                        </select>
                        <p>
                        Coupon Value
                        <!-- Will display when coins is selected -->
                        <select id="coins" name="value">
                            @foreach($coins_type as $coin_type)
                                <option value="{{$coin_type->id}}">{{$coin_type->name}}</option>
                            @endforeach
                        <!-- display all coins -->
                        </select>
                        <!-- Will display when points/gifts is selected -->
                        <select id="gifts" name="value">
                            @foreach($websites as $website)
                                <option value="{{$website->id}}">{{$website->name}}({{$website->domain}})</option>
                            @endforeach
                        <!-- display all network -->
                        </select>
                        {{ form_input("text","amount","","Amount","form-control","Amount") }}
                        <p>Coupon Code</p>
                            <input class="form-control" type="hidden" name="coupon_code" value="{{$coupon_code}}">
                        <p>{{$coupon_code}}</p>
                        <p>Pin Code</p>
                        <input class="form-control" type="hidden" name="pin_code" value="{{$pin_code}}">
                        <p>{{$pin_code}}</p>
                        <select name="status">
                            <option value="active">Active</option>
                            <option value="used">Used</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_submit("Save") }}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Network Modal -->
    <div class="modal fade modalDeleteGift" tabindex="-1" role="dialog" id="modalDeleteNetwork" aria-labelledby="modalDeleteNetworkLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalDeleteNetworkLabel">Delete Network</h4>
                </div>
                <!--<form action=" admin_delete_network_url() }}" method="POST" id="deleteNetworkForm" role="form">-->
                <form action="" method="POST" id="deleteNetworkForm" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-body">
                        Are you sure you want to delete this <span class="dname"></span>? <span
                            class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_del_submit("Delete") }}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Upload Coupons -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalUploadCoupon" aria-labelledby="modalUploadCouponLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalUploadCouponTitle">Upload Coupon</h4>
                </div>
                <form action="coupons/import-coupon" method="post" id="modalUploadCoupon" role="form" enctype="multipart/form-data">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <input type="file" name="import_file" class="form-control">    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-default">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script_footer_admin')
    {{ admin_footer_tinymce_js() }}
    {{ admin_footer_tinymce_1_js() }}

    <script>
        $(function() {
            $('#gifts').hide(); 
            $('#couponType').change(function(){
                if($('#couponType').val() == 'coins') {
                    $('#coins').show(); 
                    $('#gifts').hide(); 
                } else {
                    $('#coins').hide(); 
                    $('#gifts').show(); 
                } 
            });
            // Add Network
            $('#addCouponForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                      type: "POST",
                      url: '{{ super_admin_coupons_save_url() }}',
                      data: $(this).serialize(),
                      success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 500);
                      }, error:function() {
                        console.log("Error!");
                      }
                    });
                } else {
                    return false;
                }
            });

            $(".btn-add-meta").click(function(){
                var parents = [];
                var t = $(this);
                var id = t.attr("data-type");
                var title = t.attr("data-title");

                $('#addCoupon').on('show.bs.modal', function () {
                  $("#meta-id").val(id);
                  $(".meta-add-title").text(title);
                });

                $('#addCoupon').on('hide.bs.modal', function () {
                  $("#meta-id").val("");
                  $(".meta-add-title").text("Meta");
                });

            });

            // Edit Meta
            $('#editMetaForm').on('submit', function(e) {
                e.preventDefault(e);

                if ($(this).valid()) {
                    $.ajax({
                        type: "POST",
                        url: '{{ website_bahay_admin_edit_meta_url() }}',
                        data: $(this).serialize(),
                        success: function(data) {
                            console.log(data);
                            setTimeout(function() {window.location = data.redirect_url;}, 500);
                        }, error: function() {
                            console.log("Error!");
                        }
                    });
                } else {
                    return false;
                }
            });
    
            // Delete Network Ajax
            $('.btn-del-meta').on('click', function() {
                $('.dname').html($(this).data('name'));
                $('.did').text($(this).data('id'));
            });

            $('.modal-footer').on('click', '.delete', function(e) {
                e.preventDefault(e);

                var data = {
                    _token: $('input[name=_token]').val(),
                    id: $('.did').text()
                }

                $.ajax({
                    type: 'post',
                    url: '{{ website_bahay_admin_delete_meta_url() }}',
                    data: data,
                    success: function(data) {
                        $('.item' + $('.did').text()).remove();
                        $('#modalDelMeta').modal('hide');
                    }, error:function() {
                        console.log("Error!");
                    }
                });
            });

            // Upload Coupons
            // $('.btn-upload').on('click', function() {
                // $.ajax({
                //     type: 'post',
                //     url: 'coupons/import-coupon',
                //     data: new FormData('#uploadCouponForm'),
                //     processData: false,
                //     contentType: false,
                //     success: function(data) {
                //         console.log(data);
                //     }, 
                //     error: function() {
                //         console.log("Error!");
                //     }
                // });
            // });

            // Validate Add Network Form
            $('#addMetaForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                messages: {
                    name: {
                        minLength: "Your Name of must be at least 3 characters",
                        maxLength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name of Meta",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
            

            // Validate Edit Meta Form
            $('#editMetaForm').validate({
                rules: {
                    name: {
                        minlength: 3,
                        maxlength: 55,
                        required: true
                    }
                },
                messages: {
                    name: {
                        minLength: "Your Name of must be at least 3 characters",
                        maxLength: "Your Name must be at least 55 characters",
                        required: "Please provide your Name of Meta",
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    // leave it blank here.
                }
            });
        });
    </script>
@endsection