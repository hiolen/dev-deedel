<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DeeDel</title>
    {{ bootstrap_front_css() }}
    {{ front_bower_components_toast_css() }}
    {{ front_bower_components_morrisjs_css() }}
    {{ front_animate_css() }}
    {{ front_style_css() }}
    {{ front_default_css() }}
    {{ deedel_style_css() }}
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('script_header_front')
</head>
<body id="app-layout" class="fix-header">
    @include('layouts.front.header')

    @yield('content_front')
    @yield('modal_front')

    @if (Auth::guest())
        @include('auth.modalLogin')
        @include('auth.modalRegister')
    @endif
    @include('layouts.front.footer')
    @yield('script_footer_front')
    {{ jquery_js() }}
    {{ bootstrap_front_js() }}
    {{ slimscroll_js() }}
    {{ waves_js() }}
    {{ waypoints_js() }}
    {{ counterup_js() }}
    {{ raphael_js() }}
    {{ custom_js() }}
    {{ dashboard1_js() }}
    {{ sparkline_js() }}
    {{ charts_sparkline_js() }}
    {{ toast_js() }}
    <script type="text/javascript">
      
      $(document).ready(function() {
          $.toast({
            heading: 'Welcome to Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'info',
            hideAfter: 3500, 
            
            stack: 6
          });
          $('.vcarousel').carousel({
                interval: 3000
           });
        });
        
    </script>
</body>
</html>
