
  <!-- .right panel -->
  <div class="right-side-panel">
      <div class="scrollable-right">
          <!-- .Theme settings -->
          <h3 class="title-heading">Colors</h3>
          <ul id="themecolors" class="p-20">
              <li><b>Choose your theme color</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme working">1</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              
              
            </ul>

          <!-- .Notes -->
          <h3 class="title-heading">Tasks</h3>
          <div class="notes-widgets">
             <!-- .Notes header --> 
             <div class="header-part">
               <div class="btn-group">
                  <button aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle daydrop" type="button"> This week <span class="caret"></span></button>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Todays</a></li>
                    <li><a href="#">Monthly</a></li>
                    <li><a href="#">Yearly</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <div class="pull-right">
                    <a href="javascript:void(0)" class="text-inverse m-r-5" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" title="Add New" data-placement="left" class="text-inverse"><i class="ti-plus"></i></a>
                </div> 
              </div> 
              <!-- /.Notes header --> 
              <!-- .Notes body --> 
              <ul class="list-task list-group">
                  <li class="list-group-item">
                    <div class="checkbox checkbox-success">
                      <input type="checkbox" id="inputSchedule1" name="inputCheckboxesSchedule">
                      <label for="inputSchedule1"> <span>Schedule meeting</span> </label>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="checkbox checkbox-success">
                      <input type="checkbox" id="inputCall1" name="inputCheckboxesCall">
                      <label for="inputCall1"> <span>Give Purchase report</span> </label>
                    </div>
                  </li>
                  <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                      <input type="checkbox" id="inputBook1" name="inputCheckboxesBook">
                      <label for="inputBook1"> <span>Book flight for holiday</span> </label>
                    </div>
                  </li>
                  <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                      <input type="checkbox" id="inputForward1" name="inputCheckboxesForward">
                      <label for="inputForward1"> <span>Forward all tasks</span> </label>
                    </div>
                  </li>
                  <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                      <input type="checkbox" id="inputRecieve1" name="inputCheckboxesRecieve">
                      <label for="inputRecieve1"> <span>Recieve shipment</span> </label>
                    </div>
                  </li>
                  <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                      <input type="checkbox" id="inputForward21" name="inputCheckboxesd">
                      <label for="inputForward21"> <span>Important tasks</span> </label>
                    </div>
                  </li>
              </ul>
              <!-- /.Notes body -->
            <a class="text-muted db text-center m-t-10" href="javascript:void(0)">View all notes</a> 
          </div>

          <!-- .Notification -->
          <h3 class="title-heading">Notifications <span class="label label-rouded label-danger pull-right">4</span></h3>
          <div class="message-center p-20"> 
              <a href="javascript:void(0)">
              <div class="mail-contnet">
                <h5>Sonu your title here</h5>
                <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
              </a> <a href="javascript:void(0)">
              <div class="mail-contnet">
                <h5>Arijit your title here</h5>
                <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
              </a> <a href="javascript:void(0)">
              
              <div class="mail-contnet">
                <h5>Your title here Genelia</h5>
                <span class="mail-desc">I love to do acting and dancing</span> <span class="time">9:08 AM</span> </div>
              </a> <a href="javascript:void(0)" class="b-none">
              
              <div class="mail-contnet">
                <h5>Pavan kumar</h5>
                <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
              </a> </div>            
          <!-- /.Notification -->

          <!-- .Chat notification -->
          <h3 class="title-heading">Chat <span class="label label-rouded label-megna pull-right">3</span></h3>
          <ul class="m-t-20 chatonline">
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a></li>
            </ul>
            <!-- /.Chat notification -->
      </div>
  </div>
  <!-- /.right panel -->


<footer class="footer">
<div class="container">
<div class="row">
  <div class="col-md-3 col-sm-6"><img src="images/footer-logo.png"/><p class="m-t-30"><font class="text-white">Eliteadmin</font> is premium quality admin dashboard template with flat design. It is fully responsive admin dashboard template built with Bootstrap Framework, HTML5 & CSS3, Media query. </p></div>
  <div class="col-md-3 col-sm-6"> 
  <ul class="footer-link list-icons">
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/index.html"><i class="ti-angle-right text-megna"></i> Minimal Dashboard</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/index2.html"><i class="ti-angle-right text-megna"></i> Demographical Dashboad</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/index3.html"><i class="ti-angle-right text-megna"></i> Analytical Dashboad</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/index4.html"><i class="ti-angle-right text-megna"></i> Simpler Dashboard</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-ecommerce/index.html"><i class="ti-angle-right text-megna"></i> eCommerce Dashboard</a></li>
  </ul>
  </div>
  <div class="col-md-3 col-sm-6">
  <ul class="footer-link list-icons">
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/widgets.html"><i class="ti-angle-right text-megna"></i> Widget Sections</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/morris-chart.html"><i class="ti-angle-right text-megna"></i> Chart Options</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/form-layout.html"><i class="ti-angle-right text-megna"></i> Lots of Form Layouts</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/data-table.html"><i class="ti-angle-right text-megna"></i> Data Table Options</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/calendar.html"><i class="ti-angle-right text-megna"></i> Calendar Designs</a></li>
  </ul> 
  </div>
  <div class="col-md-3 col-sm-6">
  <ul class="footer-link list-icons">
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/inbox.html"><i class="ti-angle-right text-megna"></i> Inbox Layout</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/treeview.html"><i class="ti-angle-right text-megna"></i> Tree View Options</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/carousel.html"><i class="ti-angle-right text-megna"></i> Carousel Slider Option</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/gallery.html"><i class="ti-angle-right text-megna"></i> Gallery Option</a></li>
  <li><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/starter-page.html"><i class="ti-angle-right text-megna"></i> Starter Pages</a></li>
  </ul>
  </div>

</div>
</div>
</div>
            
          <div class="col-md-12 sub-footer">
              <span>Copyright 2016. All Rights Reserved by <a class="text-white" href="https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820?ref=suniljoshi" target="_blank">Elite Admin</a></span>
              <span class="pull-right">Design & Developed by <a class="text-white" href="http://themedesigner.in" target="_blank">Theme Designer</a></span>
          </div>
    </footer>

    <footer class="footer" style="display:none">
        <p>&nbsp;</p>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <img src="" alt="DeeDel">
                    <p>DeeDel &copy; 2016</p>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <h5>Pages</h5>
                    <nav>
                        <ul>
                            <li><a href="{{ faq_url() }}">FAQ</a></li>
                            <li><a href="{{ terms_url() }}">Terms</a></li>
                            <li><a href="{{ privacy_url() }}">Privacy Policy</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <h5>Highlight</h5>
                    <nav>
                        <!-- create videos, infographics and contents for the following presentations -->
                        <ul>
                            <li><a href="">How to earn points</a></li>
                            <!-- earn points from website activities, affiliates and more -->
                            <li><a href="">how to get rewards</a></li>
                            <!-- get rewards from contest and selected website activities like sharing posts, affiliate and more -->
                            <li><a href="">how to claim rewards</a></li>
                            <!-- claiming rewards is depending on how you gathered points -->
                            <li><a href="">how to join to contest</a></li>
                            <li><a href="">do you have affiliate program</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <h5>Get in touch</h5>
                    <form>
                        <input type="text" name="" placeholder="Name" class="form-control">
                        <input type="email" name="" placeholder="Email" class="form-control">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </footer>
</div>
@yield('script_footer')
