<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>DeeDel</title>
    <?php $url_css_bootstrap = URL::asset('assets/css/bootstrap.min.css'); ?>
    <?php $url_jquery = URL::asset('assets/js/jquery.min.js'); ?>
    <?php $url_bootstrap = URL::asset('assets/js/bootstrap.min.js'); ?>
    <link rel="stylesheet" type="text/css" href="{{ $url_css_bootstrap }}">
    <script src="{{ $url_jquery }}"></script>
    <script src="{{ $url_bootstrap }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
    @yield('script')
    <!-- Fonts 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">


    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        
    $(document).ready(function(){
        //TinyMCE
        if($("textarea.tinymce").length>0){
            tinymce.init({
              selector: 'textarea.tinymce',
              height: 500,
              plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
              ],
              toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            });
        }
    });
    </script>
    -->
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    DeeDel Digital
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ websites_url() }}">Websites</a></li>
                    <li><a href="{{ coins_url() }}">Buy Coins</a></li>
                    <li class="dropdown">
                        <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            More
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="{{ support_url() }}">Support Center</a></li>
                            <li><a href="{{ help_url() }}">Help Center</a></li>
                            <li><a href="{{ forum_url() }}">Forum</a></li>
                            <li><a href="{{ blog_url() }}">Blog</a></li>
                            <li><a href="{{ about_url() }}">About</a></li>
                            <li><a href="{{ careers_url() }}">Careers</a></li>
                            <li><a href="{{ partners_url() }}">Partners</a></li>
                        </ul>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="#modalLogin" data-toggle="modal">Login</a></li>
                        <li><a href="#modalRegister" data-toggle="modal">Register</a></li>
                    @else
                        <!-- <li class=""><a href="/wishlist">Wishlist ({{ Cart::instance('wishlist')->count(false) }})</a></li> -->
                        <li class=""><a href="/cart">Cart ({{ Cart::instance('main')->count(false) }})</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ account_url() }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{ account_websites_url() }}"><i class="fa fa-dashboard"></i> Accounts</a></li>
                                <li><a href="{{ account_url() }}/profile"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="{{ account_url() }}/settings"><i class="fa fa-gear"></i> Settings</a></li>
                                <li><a href="{{ logout_url() }}"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    @yield('modal')

    @if (Auth::guest())
        @include('auth.modalLogin')
        @include('auth.modalRegister')
    @endif
</body>
</html>
