@include('layouts.admin.htmlhead')
<body id="app-layout">
@include('layouts.admin.header')

    @yield('superadmin_content')
</body>
</html>
