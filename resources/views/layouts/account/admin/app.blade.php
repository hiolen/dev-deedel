@include('layouts.account.admin.htmlhead')
<body id="app-layout">
@include('layouts.account.admin.header')

    @yield('super_admin_content')

</body>
</html>
