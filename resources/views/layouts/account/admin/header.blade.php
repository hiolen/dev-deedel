
<?php
    $super_admin_url = Config::get('deedel_variables.super_admin_url');
?>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ $super_admin_url }}">
                    Super Admin
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ $super_admin_url }}/deedel">Deedel</a></li>
                    <li><a href="{{ $super_admin_url }}/websites">websites</a></li>
                    <li><a href="{{ $super_admin_url }}/users">Users</a></li>
                    <li><a href="{{ $super_admin_url }}/activity">Activities</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="#">Login</a></li>
                    @else
                    @endif
                </ul>
            </div>
        </div>
    </nav>