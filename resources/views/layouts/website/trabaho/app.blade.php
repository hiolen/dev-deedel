@include('layouts.website.trabaho.htmlhead')
<body id="app-layout">
@include('layouts.website.trabaho.header')

    @yield('website_trabaho_content')

</body>
</html>
