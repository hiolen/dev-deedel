    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ website_bahay_url() }}">
                    Bahay
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="#findby" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Find<span class="caret"></span></a>
                        <ul class="dropdown-menu" aria-labelledby="findby">
                            <li><a href="{{ website_bahay_url() }}/locations">By Locations</a></li>
                            <li><a href="{{ website_bahay_url() }}/tags">By Tags</a></li>
                            <li><a href="{{ website_bahay_url() }}/developers">By Developers</a></li>
                            <li><a href="{{ website_bahay_url() }}/realtors">By Realtors</a></li>
                            <li><a href="{{ website_bahay_url() }}/agents">By Agents</a></li>
                            <li><a href="{{ website_bahay_url() }}/search">Advance Search</a></li>
                        </ul>
                    </li> 
                    <li><a href="{{ website_bahay_url() }}/category/lot-only">Lot Only</a></li>
                    <li><a href="{{ website_bahay_url() }}/category/house-and-lot">House and Lot</a></li>
                    <li><a href="{{ website_rent_url() }}">For Rent/Lease</a></li>
                    <li><a href="{{ website_bahay_url() }}/category/condominiums">Condominiums</a></li>
                    <li class="dropdown"><a href="#blog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Articles<span class="caret"></span></a>
                        <ul class="dropdown-menu" aria-labelledby="blog">
                            <li><a href="{{ website_bahay_url() }}/blog">Blog</a></li>
                            <li><a href="{{ website_bahay_url() }}/news">News</a></li>
                            <li><a href="{{ website_bahay_url() }}/promos">Promos</a></li>
                            <li><a href="{{ website_bahay_url() }}/events">Events</a></li>
                        </ul>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ website_bahay_url() }}/login">Login</a></li>
                        <li><a href="{{ website_bahay_url() }}/join">Join</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ account_url() }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{ account_url() }}/profile"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="{{ account_url() }}/settings"><i class="fa fa-gear"></i> Settings</a></li>
                                <li><a href="{{ account_url() }}/logout"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>