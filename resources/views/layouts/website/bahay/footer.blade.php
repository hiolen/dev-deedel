    <footer class="footer">
        <ul class="nav">
            <li><a href="#">About</a></li>
            <li><a href="#">Privacy policy</a></li>
            <li><a href="#">Terms and Conditions</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">How it Works</a></li>
            <li><a href="#">FAQs</a></li>
        </ul>
    </footer>
    @yield('script_footer')