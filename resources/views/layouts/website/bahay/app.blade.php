<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DeeDel</title>
    @yield('style_css')
    {{ bahayLocation_style_css() }}
    
@include('layouts.website.bahay.htmlhead')
<body id="app-layout">
@include('layouts.website.bahay.header')
    @yield('website_bahay_content')
    @yield('modal')
@include('layouts.website.bahay.footer')
</body>
</html>
