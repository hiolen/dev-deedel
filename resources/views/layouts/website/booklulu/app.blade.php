@include('layouts.website.booklulu.htmlhead')
<body id="app-layout">
@include('layouts.website.booklulu.header')

    @yield('website_booklulu_content')

</body>
</html>
