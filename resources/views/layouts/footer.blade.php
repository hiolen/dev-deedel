<footer class="footer">
    <p>&nbsp;</p>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <img src="" alt="DeeDel">
                <p>DeeDel &copy; 2016</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <h5>Pages</h5>
                <nav>
                    <ul>
                        <li><a href="{{ faq_url() }}">FAQ</a></li>
                        <li><a href="{{ terms_url() }}">Terms</a></li>
                        <li><a href="{{ privacy_url() }}">Privacy Policy</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <h5>Highlight</h5>
                <nav>
                    <!-- create videos, infographics and contents for the following presentations -->
                    <ul>
                        <li><a href="">How to earn points</a></li>
                        <!-- earn points from website activities, affiliates and more -->
                        <li><a href="">how to get rewards</a></li>
                        <!-- get rewards from contest and selected website activities like sharing posts, affiliate and more -->
                        <li><a href="">how to claim rewards</a></li>
                        <!-- claiming rewards is depending on how you gathered points -->
                        <li><a href="">how to join to contest</a></li>
                        <li><a href="">do you have affiliate program</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <h5>Get in touch</h5>
                <form>
                    <input type="text" name="" placeholder="Name" class="form-control">
                    <input type="email" name="" placeholder="Email" class="form-control">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</footer>
@yield('script_footer')
