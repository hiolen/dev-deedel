<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DeeDel</title>
    {{ icons() }}
    {{ global_css() }}
    {{ home_style_css() }}
    {{ network_style_css() }}
    {{ message_style_css() }}
    {{ contact_style_css() }}
    {{ support_ticket_style_css() }}

    <?php $url_jquery = URL::asset('assets/js/jquery.min.js'); ?>
    <?php $url_bootstrap = URL::asset('assets/js/bootstrap.min.js'); ?>
    <?php $url_homeScript = URL::asset('assets/js/home-script.js'); ?>
    <script src="{{ $url_jquery }}"></script>
    <script src="{{ $url_bootstrap }}"></script>
    <script src="{{ $url_homeScript }}"></script>
    @yield('script')
    <!-- Fonts 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">


    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        
    $(document).ready(function(){
        //TinyMCE
        if($("textarea.tinymce").length>0){
            tinymce.init({
              selector: 'textarea.tinymce',
              height: 500,
              plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
              ],
              toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            });
        }
    });
    </script>
    -->
</head>
<body id="app-layout">
    @include('layouts.header')

    @yield('content')
    @yield('modal')

    @if (Auth::guest())
        @include('auth.modalLogin')
        @include('auth.modalRegister')
    @endif
    @include('layouts.footer')
</body>
</html>
