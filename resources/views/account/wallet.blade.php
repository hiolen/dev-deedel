@extends('layouts.app')

@section('content')
<div class="container">
    <h2>My Wallet</h2>
    <a href="#modalLoadCoins" data-toggle="modal" data-title="Load Coins" class="btn-add-meta"><i class="fa fa-plus"></i> Load Coins</a>
	<h3><strong>Gold Coins: @if($user->coinType('gold')){{ $user->coinType('gold') }}@else 0 @endif</strong></h3>
	<h3><strong>Green Coins: @if($user->coinType('green')){{ $user->coinType('green') }}@else 0 @endif</strong></h3>
	<h3><strong>Silver Coins: @if($user->coinType('silver')){{ $user->coinType('silver') }}@else 0 @endif</strong></h3>
	<h3><strong>Pink Coins: @if($user->coinType('pink')){{ $user->coinType('pink') }}@else 0 @endif</strong></h3>
</div>
<!--
coins   -   equivalent
gold    -   0.05
silver  -   0.01    
pink    -   1.25
green   -   0.10

point   -   0.001
-->
@endsection


@section('modal')
    <!-- Add Network Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalLoadCoins" aria-labelledby="modalLoadCoinsLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLoadCoinsLabel">Load Coins</h4>
                </div>
                <form action="{{ website_bahay_admin_add_meta_url() }}" method="POST" id="addNetworkForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        {{ form_input("text","code","","Code","form-control","Code") }}
                        {{ form_input("text","pin","","Pin","form-control","Pin") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ form_submit("Load Now") }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
