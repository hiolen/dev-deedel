@extends('layouts.app')

<?php $account_url = Config::get('deedel_variables.account_url'); ?>
@section('content')
<div class="container">
    
            <ul class="nav">
                <li><a href="{{ $account_url }}/websites">My Accounts</a>
                    <ul class="nav">
                        <li><a href="#">website name</a></li>
                        <li><a href="{{ $account_url }}/booklulu">Booklulu</a>
                            <ul class="nav">
                                <li><a href="{{ $account_url }}/booklulu/books">My Books</a></li>
                                <li><a href="{{ url('/dashboard/book/add') }}">Add a book</a></li>
                                <li><a href="{{ url('/dashboard/book/reviews/pending') }}">Pending Book Reviews</a></li>
                                <li><a href="{{ url('/user/messages') }}">Inbox</a></li>
                                <li><a href="{{ url('/user/messages/sent') }}">Sent</a></li>
                                <li><a href="{{ url('/user/messages/inquiry') }}">Inquiries</a></li>
                            </ul>
                        </li>
                        <li><a href="#">website (hire a staff)</a></li>
                        <li><a href="#">website (hire a freelancer)</a></li>
                        <li><a href="#">website name</a></li>
                    </ul>
                </li>
            </ul>
</div>
@endsection
