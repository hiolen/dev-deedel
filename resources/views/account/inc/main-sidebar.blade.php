<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Auth::user()->image }}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
    @endif

    <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
          <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                        class="fa fa-search"></i></button>
          </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- USE {{ Request::is('route-name*') ? 'active' : '' }} to dynamically set active tab -->
            <li class="{{ Request::is('dashboard*') ? 'active' : '' }}"><a href="{{ route('account.deedel.home') }}"><i class='fa fa-tachometer'></i> <span>Dashboard</span></a></li>
            <li class="treeview">
                <a href="{{ Request::is('admin*') ? 'active' : '' }}"><i class='fa fa-link'></i> <span>Contents</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('account.deedel.contentsBlog') }}">Blog</a></li>
                    <li><a href="{{ route('account.deedel.page') }}">Pages</a></li>
                    <!-- <li><a href="#">Landing Pages</a></li>
                    <li><a href="#">Email Templates</a></li>
                    <li><a href="#">Forms</a></li>
                    <li><a href="#">Panel</a></li> -->
                </ul>
            </li>
            <!-- <li class="{{ Request::is('profile*') ? 'active' : '' }}"><a href="{{ route('account.deedel.profile') }}"><i class='fa fa-user'></i> <span>My Profile</span></a></li> -->
            <!-- <li class="{{ Request::is('messages*') ? 'active' : '' }}"><a href="{{ route('account.deedel.messages') }}"><i class='fa fa-cogs'></i> <span>Messages</span></a></li> -->
            <li class="{{ Request::is('network*') ? 'active' : '' }}"><a href="{{ route('account.deedel.network') }}"><i class='fa fa-link'></i> <span>Network</span></a></li>
            <li class="{{ Request::is('wallet*') ? 'active' : '' }}"><a href="{{ route('account.deedel.wallet') }}"><i class='fa fa-link'></i> <span>Wallet</span></a></li>
            <li class="{{ Request::is('admin*') ? 'active' : '' }}"><a href="{{ route('account.deedel.settings') }}"><i
                            class='fa fa-cogs'></i> <span>Settings</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>