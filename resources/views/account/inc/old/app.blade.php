@include('account.inc.htmlhead')
<body id="app-layout" class="fix-header page-account">
	@include('account.inc.header')
    @yield('content_account')
    {{ jquery_js() }}
    {{ bootstrap_front_js() }}
    {{ slimscroll_js() }}
    {{ waves_js() }}
    {{ waypoints_js() }}
    {{ counterup_js() }}
    {{ raphael_js() }}
    {{ custom_js() }}
    {{ dashboard1_js() }}
    {{ sparkline_js() }}
    {{ charts_sparkline_js() }}
    {{ toast_js() }}
    <script type="text/javascript">
      
      $(document).ready(function() {
          $.toast({
            heading: 'Welcome to Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'info',
            hideAfter: 3500, 
            
            stack: 6
          });
          $('.vcarousel').carousel({
                interval: 3000
           });
        });
        
    </script>
		@include('account.inc.footer')
	</div>
	@yield('modal_footer_account')
	@yield('script_footer_account')
</body>
</html>