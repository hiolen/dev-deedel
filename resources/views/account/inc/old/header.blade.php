<div class="account navbar-fixed-top" style="display:none;">
    <div class="container">
        <ul>
            <li><a href="{{ account_messages_url() }}">Messages</a></li>
            <li><a href="{{ account_wallet_url() }}">Wallet</a></li>
            <li><a href="{{ account_assets_url() }}">Assets</a></li>
            <li><a href="{{ account_contents_url() }}">Contents</a></li>
            <li><a href="{{ account_rewards_url() }}">Rewards</a></li>
            <li><a href="{{ account_orders_url() }}">Orders</a></li>
            <li><a href="{{ account_contests_url() }}">Contests</a></li>
            <li><a href="{{ account_ads_url() }}">Ads</a></li>
            <li><a href="{{ account_affiliate_url() }}">Affiliate</a></li>
            <li><a href="{{ account_network_url() }}">Network</a></li>
            <li><a href="{{ account_premium_url() }}">Premium Accounts</a></li>
        </ul>
    </div>
</div>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">

      <!-- .Logo -->
      <div class="top-left-part"><a class="logo" href="{{ deedel_url() }}"><img src="{{ deedel_url() }}/assets/elite/eliteadmin-small-logo.png" alt="home" class="light-logo" /></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="logotext"><!--This is logo text--><img src="{{ deedel_url() }}/assets/elite/eliteadmin-small-text.png" alt="home" class="light-logo" alt="home" /></a></li>
      </ul>
      <!-- /.Logo -->

      <!-- top right panel -->
      <ul class="nav navbar-top-links navbar-right pull-right">
        

        @if (Auth::guest())
            <li><a href="#modalLogin" data-toggle="modal">Login</a></li>
            <li><a href="#modalRegister" data-toggle="modal">Register</a></li>
        @else
            <!-- .dropdown -->
            <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
              <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
              </a>
              <ul class="dropdown-menu mailbox animated bounceInDown">
                <li>
                  <div class="drop-title">You have 4 new messages</div>
                </li>
                <li>
                  <div class="message-center"> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Pavan kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                    </a> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Sonu Nigam</h5>
                      <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                    </a> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Arijit Sinh</h5>
                      <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                    </a> <a href="#">
                    <div class="user-img"> <img src="{{ deedel_url() }}/assets/elite/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                    <div class="mail-contnet">
                      <h5>Pavan kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                    </a> </div>
                </li>
                <li> <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
              </ul>
              <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li><a href="{{ account_wallet_url() }}"><i class="fa fa-money"></i></a></li>
            <!-- .dropdown -->
            <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{ deedel_url() }}/assets/elite/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ Auth::user()->name }}</b> </a>
              <ul class="dropdown-menu dropdown-user animated flipInY">
                <li><a href="{{ account_url() }}"><i class="ti-user"></i> Dashboard</a></li>
                <li><a href="{{ account_url() }}/profile"><i class="ti-user"></i> My Profile</a></li>
                <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ account_url() }}/settings"><i class="ti-settings"></i> Account Setting</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ logout_url() }}"><i class="fa fa-power-off"></i> Logout</a></li>
              </ul>
              <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->

            <!-- .right toggle -->
            <li class="right-side-toggle"><a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-arrow-right"></i></a></li>
            <!-- /.right toggle -->
        @endif
      </ul>
      <!-- top right panel -->

    </div>
  </nav>
  <!-- End Top Navigation -->
  
@if (Auth::guest())
@else
  <!-- .Side panel -->
  <div class="side-mini-panel">
       <ul class="mini-nav">

              <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="ti-menu"></i></a></div>
              <!-- .Apps -->
              <li class=""><a href="javascript:void(0)"><i data-icon=")" class="linea-icon linea-basic"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                      <h3 class="menu-title">Messages</h3>
                      <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                      </div>
                      <ul class="sidebar-menu">
                          <li><a href="chat.html">Chat-message</a></li>
                          <li><a href="calendar.html">Calendar</a></li>
                          <li><a href="inbox.html">Mail box</a></li>
                          <li><a href="inbox-detail.html">Inbox detail</a></li>
                          <li><a href="compose.html">Compose mail</a></li>
                          <li class="menu">
                              <a href="javascript:void(0)"> <span>Contacts</span> <span class="label label-rouded label-megna">3</span><i class="fa fa-angle-left pull-right"></i> </a>
                              <ul class="sub-menu">
                                  <li><a href="contact.html">Contact1</a></li>
                                  <li><a href="contact2.html">Contact2</a></li>
                                  <li><a href="contact-detail.html">Contact Detail</a></li>
                              </ul>
                          </li>
                      </ul>
                      <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Apps -->

              <!-- .Dashboard -->
              <li class="selected">
                <a href="javascript:void(0)"><i class="linea-icon linea-basic" data-icon="v"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Dashboard</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                  </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="index.html">Minimalistic</a></li>
                              <li><a href="index2.html">Demographical</a></li>
                              <li><a href="index3.html">Analitical</a></li>
                              <li><a href="index4.html">Simpler</a></li>
                              <hr>
                              <h3 class="menu-title">Other Demos</h3>
                              <li><a href="../eliteadmin-inverse/index.html" target="_blank">Eliteadmin Inverse</a></li>
                              <li><a href="../eliteadmin-mini-sidebar/index3.html" target="_blank">Eliteadmin Mini Sidebar</a></li>
                              <li><a href="../eliteadmin/index.html" target="_blank">Eliteadmin</a></li>
                              <li><a href="../eliteadmin-mini-sidebar/index3.html" target="_blank">Eliteadmin Dark</a></li>
                              <li><a href="../eliteadmin-iconbar/index4.html" target="_blank">Eliteadmin Iconbar</a></li>
                              <li><a href="../eliteadmin-ecommerce/index.html" target="_blank">Eliteadmin eCommerce</a></li>
                              <li><a href="../eliteadmin-horizontal-navbar/index3.html" target="_blank">Horizontal nav & Box</a></li>
                              <li><a href="../eliteadmin-horizontal-nav-fullwidth/index.html" target="_blank">Horizontal nav & Fullwidth</a></li>
                              <li><a href="../eliteadmin-rtl/index.html" target="_blank">Eliteadmin RTL</a></li>
                              <li><a href="../eliteadmin-inverse-rtl/index.html" target="_blank">Inverse RTL</a></li>
                              <li><a href="https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820?clickthrough_id=%2016750820&license=regular&open_purchase_for_item_id=%2016750820&purchasable=source&redirect_back=true&ref=suniljoshi&utm_source=item_desc_link" class="btn btn-rounded btn-danger cst-btn">Buy Elite admin</a></li>
                              <li><a href="https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820?clickthrough_id=%2016750820&license=regular&open_purchase_for_item_id=%2016750820&purchasable=source&redirect_back=true&ref=suniljoshi&utm_source=item_desc_link" class="btn btn-rounded btn-outline btn-danger">Buy This admin @ $24</a></li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div>
              </li>
              <!-- /.Dashboard -->

              <!-- .multi -->
              <li class=""><a href="javascript:void(0)"><i data-icon="7" class="linea-icon linea-basic text-danger"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                      <h3 class="menu-title text-danger">Multipurpose</h3>
                      <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                      </div>
                      <ul class="sidebar-menu">
                          <li> <a href="../eliteadmin-hospital/index.html">Hospital Admin</a> </li>
                          <li> <a href="../eliteadmin-crm/index.html">CRM Admin</a> </li>
                          <li> <a href="../eliteadmin-university/index.html">University Admin</a> </li>
                          <li> <a href="../eliteadmin-music/index.html">Music Admin</a> </li>
                          <li> <a href="../eliteadmin-real-estate/index.html">Real Estate Admin</a> </li>
                          <li role="separator" class="divider"></li>
                          <li> <a href="../eliteadmin-inverse/index.html">Inverse</a></li>
                          <li> <a href="../eliteadmin-colors/index.html">Colors</a></li>
                          <li> <a href="../eliteadmin/index.html">Eliteadmin</a></li>
                          <li> <a href="../eliteadmin-wpmenu/index.html">Wpmenu</a></li>
                          <li> <a href="../eliteadmin-ecommerce/index.html">Ecommerce</a></li>
                          
                          <li> <a href="../eliteadmin-inverse-php/index.php">Basic PHP</a></li>
                          <li> <a href="../eliteadmin-material/index3.html">Material Design</a></li>
                          <li> <a href="../eliteadmin-dark/index.html">Dark</a></li>
                          <li> <a href="../eliteadmin-mini-sidebar/index3.html">Mini Sidebar</a></li>
                          <li> <a href="../eliteadmin-horizontal-navbar/index3.html">Horizontal Boxed Nav</a></li>
                          <li> <a href="../eliteadmin-horizontal-nav-fullwidth/index.html">Horizontal Full Nav</a></li>
                          <li> <a href="../eliteadmin-iconbar/index4.html">Iconbar</a></li>
                          <li> <a href="../eliteadmin-rtl/index.html">Eliteadmin RTL</a></li>
                          <li> <a href="../eliteadmin-inverse-rtl/index.html">Inverse RTL</a></li>
                      </ul>
                      <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.multi -->

              <!-- .Ui Elemtns -->
              <li><a href="javascript:void(0)"><i data-icon="/" class="linea-icon linea-basic"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">UI Elements</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="panels-wells.html">Panels and Wells</a></li> <li><a href="panel-ui-block.html">Panels With BlockUI</a></li>
                              <li><a href="buttons.html">Buttons</a></li>
                              <li><a href="sweatalert.html">Sweat alert</a></li>
                              <li><a href="typography.html">Typography</a></li>
                              <li><a href="grid.html">Grid</a></li>
                              <li><a href="tabs.html">Tabs</a></li>
                              <li><a href="tab-stylish.html">Stylish Tabs</a></li>
                              <li><a href="modals.html">Modals</a></li>
                              <li><a href="progressbars.html">Progress Bars</a></li>
                              <li><a href="notification.html">Notifications</a></li>
                              <li><a href="carousel.html">Carousel</a></li>
                              <li><a href="list-style.html">List & Media object</a></li>
                              <li><a href="user-cards.html">User Cards</a></li>
                              <li><a href="timeline.html">Timeline</a></li>
                              <li><a href="timeline-horizontal.html">Horizontal Timeline</a></li>
                              <li><a href="nestable.html">Nesteble</a></li>
                              <li><a href="range-slider.html">Range Slider</a></li>
                              <li><a href="tooltip-stylish.html">Stylish Tooltip</a></li>
                              <li><a href="bootstrap.html">Bootstrap UI</a></li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div> 
              </li>
              <!-- /.Ui Elemtns -->

              <!-- .Forms -->
              <li class=""><a href="javascript:void(0)"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Contents</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="form-basic.html">Basic Forms</a></li>
                              <li><a href="form-layout.html">Form Layout</a></li>
                              <li><a href="form-advanced.html">Form Addons</a></li>
                              <li><a href="form-material-elements.html">Form Material</a></li> <li><a href="form-float-input.html">Form Float Input</a></li>
                              <li><a href="form-upload.html">File Upload</a></li>
                              <li><a href="form-mask.html">Form Mask</a></li>
                              <li><a href="form-img-cropper.html">Image Cropping</a></li>
                              <li><a href="form-validation.html">Form Validation</a></li>
                              <li><a href="form-dropzone.html">File Dropzone</a></li>
                              <li><a href="form-pickers.html">Form-pickers</a></li>
                              <li><a href="form-wizard.html">Form-wizards</a></li>
                              <li><a href="form-typehead.html">Typehead</a></li>
                              <li><a href="form-xeditable.html">X-editable</a></li>
                              <li><a href="form-summernote.html">Summernote</a></li>
                              <li><a href="form-bootstrap-wysihtml5.html">Bootstrap wysihtml5</a></li>
                              <li><a href="form-tinymce-wysihtml5.html">Tinymce wysihtml5</a></li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div> 
              </li>  
              <!-- /.Forms -->

              <!-- .Sample Pages -->
              <li class=""><a href="javascript:void(0)"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                          <h3 class="menu-title">Assets</h3>
                          <div class="searchable-menu">
                            <form role="search" class="menu-search">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form>
                          </div>
                          <ul class="sidebar-menu">
                              <li><a href="starter-page.html">Starter Page</a></li>
                              <li><a href="blank.html">Blank Page</a></li>
                              <li class="menu"><a href="javascript:void(0)">Email Templates <i class="fa fa-angle-left pull-right"></i></a>
                                <!-- Second level submenu -->
                                <ul class="sub-menu">
                                  <li> <a href="../email-templates/basic.html">Basic</a></li>
                                  <li> <a href="../email-templates/alert.html">Alert</a></li>
                                  <li> <a href="../email-templates/billing.html">Billing</a></li>
                                  <li> <a href="../email-templates/password-reset.html">Reset Pwd</a></li>
                                </ul>
                                <!-- Second level submenu -->
                              </li>
                          </ul>
                          <!-- Left navbar-header end -->
                    </div>
              </li>
              <!-- /.Sample Pages -->

              <!-- .Charts -->
              <li class=""><a href="javascript:void(0)"><i data-icon="&#xe006;" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Charts</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="flot.html">Flot Charts</a> </li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Charts -->

              <!-- .Tables -->
              <li class=""><a href="javascript:void(0)"><i data-icon="O" class="linea-icon linea-software fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Tables</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="basic-table.html">Basic Tables</a></li> <li><a href="table-layouts.html">Table Layouts</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Tables -->

              <!-- .Widgets -->
              <li class=""><a href="javascript:void(0)"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Widgets</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="widgets.html">All widgets</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Widgets -->

              <!-- .Icons -->
              <li class=""><a href="javascript:void(0)"><i data-icon="7" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Icons</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="fontawesome.html">Font awesome</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Icons -->

              <!-- .Maps -->
              <li class=""><a href="javascript:void(0)"><i data-icon="Q" class="linea-icon linea-basic fa-fw"></i></a>
                  <div class="sidebarmenu">
                      <!-- Left navbar-header -->
                        <h3 class="menu-title">Maps</h3>
                        <div class="searchable-menu">
                          <form role="search" class="menu-search">
                              <input type="text" placeholder="Search..." class="form-control">
                              <a href=""><i class="fa fa-search"></i></a>
                          </form>
                        </div>
                        <ul class="sidebar-menu">
                            <li><a href="map-google.html">Google Maps</a></li>
                            <li><a href="map-vector.html">Vector Maps</a></li>
                        </ul>
                        <!-- Left navbar-header end -->
                  </div>
              </li>
              <!-- /.Maps -->
           </ul>
  </div>
  <!-- /.Side panel -->
@endif

  <!-- Page Content -->
  <div class="page-wrapper">

    <nav class="navbar navbar-default navbar-static-top" style="display:none">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ deedel_url() }}">
                    DeeDel
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ websites_url() }}">Network</a></li>
                    <li><a href="{{ coins_url() }}">Buy Coins</a></li>
                    <li class="dropdown">
                        <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            More
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="{{ about_url() }}">About</a></li>
                            <li><a href="{{ contact_url() }}">Contact Us</a></li>
                            <li><a href="{{ faq_url() }}">FAQ</a></li>
                            <li><a href="{{ terms_definition_url() }}">Terms & Definition</a></li>
                            <li><a href="{{ blog_url() }}">Blog</a></li>
                            <li><a href="{{ support_url() }}">Support Center</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="#modalLogin" data-toggle="modal">Login</a></li>
                        <li><a href="#modalRegister" data-toggle="modal">Register</a></li>
                    @else
                        <li><a href="{{ account_wallet_url() }}"><i class="fa fa-money"></i></a></li>
                        <li><a href="{{ cart_url() }}"><i class="fa fa-shopping-cart"></i> ({{ Cart::instance('main')->count(false) }})</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ account_url() }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{ account_network_url() }}"><i class="fa fa-dashboard"></i> My Network</a></li>
                                <li><a href="{{ account_url() }}/profile"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="{{ account_url() }}/settings"><i class="fa fa-gear"></i> Settings</a></li>
                                <li><a href="{{ logout_url() }}"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>