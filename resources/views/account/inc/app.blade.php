    <!-- html header -->
    @include('account.inc.htmlhead')

    <!-- header -->
    @include('account.inc.header')

    <!-- main sidebar -->
    @include('account.inc.main-sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('contentheader_title', 'Page Header here')
                <small>@yield('contentheader_description', 'Page Description here')</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            <div class="row">
                <!-- page content -->
                @yield('content_account')
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->        

    <!-- control sidebar -->
    @include('account.inc.control-sidebar')

    <!-- footer -->
    @include('account.inc.footer')

    <!-- extra script -->
    @yield('extra_script');

    <!-- moodal -->
    @yield('modal_account')

</body>
</html>