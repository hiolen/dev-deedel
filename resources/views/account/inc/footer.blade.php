<!-- jQuery 2.1.4  -->
<script src="{{ asset('/assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/assets/js/account/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/assets/js/account/app.min.js') }}" type="text/javascript"></script>
<!-- Toastr -->
<!-- <script src="{{ asset('/assets/js/account/toastr.min.js') }}" type="text/javascript"></script> -->
<!-- Jquery Validation -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
<!-- CKEDITOR -->
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<!-- <script src="https://checkout.stripe.com/checkout.js"></script> -->
<!-- SweetAlert2 -->
<script src="{{ asset('/assets/js/account/sweetalert2.min.js') }}" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout.