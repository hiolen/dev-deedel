<div class="account-affiliate">
	<ul>
		<li class="menu-home"><a href="{{ account_affiliate_url() }}">Affiliate Home</a></li>
		<li class="menu-banners"><a href="{{ account_affiliate_banners_url() }}">Banners</a></li>
		<li class="menu-referrals"><a href="{{ account_affiliate_referrals_url() }}">Referrals</a></li>
		<li class="menu-earnings"><a href="{{ account_affiliate_earnings_url() }}">Earnings</a></li>
	</ul>
</div>