<!-- Modal -->
<div id="update-page" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Update Page</h4>
            </div>

            <div class="modal-body">
                <form action="{{ route('account.deedel.contentPagesUpdate') }}" method="POST" role="form" id="formPageUpdate">
                    {!! csrf_field() !!}

                    <input type="hidden" name="id" id="cpb-update-id">

                    <div class="form-group">
                        <label for="cpb-update-title" class="control-label">Name</label>
                        <input type="text" name="title" class="form-control" id="cpb-update-title">
                    </div>

                    <div class="form-group">
                        <label for="cpb-update-desc" class="control-label">Description:</label>
                        <textarea type="text" class="form-control" id="contentPageUpdate" name="description" rows="10"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="cpb-update-network" class="control-label">Network url/domain</label>
                        <select class="form-control" name="network" id="cpb-update-network">
                            @foreach($websites as $website)
                                <option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="cpb-update-role" class="control-label">Membership url</label>
                            <select class="form-control" name="membership" id="cpb-update-role">
                                @foreach($user->roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light" id="btn-blog-add">Update Page</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>