<!-- Modal -->
<div id="add-blog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Blog</h4>
            </div>
            <form id="formBlogAdd" method="post" action="{{ route('account.deedel.contentsBlogAdd') }}">
            	<div class="modal-body">
            		{{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="cba-title" class="control-label">Title:</label>
                        <input type="text" class="form-control" id="cba-title" name="title">
                    </div>

                    <div class="form-group">
                        <label for="cba-desc" class="control-label">Description:</label>
                        <textarea type="text" class="form-control" id="contentBlogAdd" name="description" rows="10"></textarea>
                    </div>

                    <div class="checkbox">
                        <label>
                          <input type="checkbox" name="featured" id="cba-feature"> Set Featured
                        </label>
                    </div>

                    <div class="form-group">
                		<select name="website_id" class="form-control" id="cba-web">
                			@foreach($websites as $website)	
                			<option value="{{ $website->id }}">{{ $website->name }}</option>
                			@endforeach
                		</select>
                    </div>
            	</div>
                
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-danger waves-effect waves-light" id="btn-blog-add">Add Blog</button>
	            </div>
	        </form>
        </div>
    </div>
</div>