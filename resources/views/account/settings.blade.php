@extends('account.inc.app')

@section('htmlheader_title')
DeeDel - Settings
@endsection

@section('content_account')
<div class="col-md-8">
	<div class="box box-primary">
		<div class="box-body">
			Settings here...
		</div>
	</div>
</div>
<!-- /.box -->
@endsection
