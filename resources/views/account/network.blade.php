@extends('account.inc.app')

@section('contentheader_title')
Network
@endsection

@section('contentheader_description')
Network Description ...
@endsection

@section('content_account')
<div class="col-md-12">
	<div class="box">
		<div class="box-body">
			<ul>
			@foreach($networks as $network)
				@foreach($websites as $website)
					@if($network == $website->id)
						<li>
							<a href="#">{{ $website->name }}</a>
							<!-- <br>add network notifications<br> -->
						</li>
					@endif
				@endforeach
			@endforeach
			</ul>
		</div>
	</div>
	<!-- /.box -->
</div>
@endsection
