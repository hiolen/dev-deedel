@extends('account.inc.app')

@section('content_account')
<div class="col-md-8">
	<div class="box box-primary">
		<div class="box-body">
			<h2>Network</h2>
			<ul>
				@foreach($networks as $network)
				<li><a href="{{ $network->domain }}">{{ $network->name }}</a> - <span>{{ $network->details }}</span></li>
				@endforeach
			</ul>
		</div>
	</div>
</div>
@endsection
