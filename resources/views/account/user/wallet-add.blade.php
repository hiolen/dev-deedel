@extends('account.inc.app')

@section('htmlheader_title')
DeeDel - Add Wallet
@endsection

@section('contentheader_title')
Wallet
@endsection

@section('contentheader_description')
Wallet Description ...
@endsection

@section('content_account')
<div class="col-md-8">
	@include('account.inc.sessions')
	<div class="box box-primary">
 		<div class="box-header with-border">
  		<h2>Add Money to your account</h2>
  	</div>

 		<div class="box-body">
		  <ul class="nav nav-pills">
		  	<li>
			    <a href="#money-paypal" data-toggle="modal">
					<img src="{{ deedel_url() }}/assets/elite/lp/images/ui-component.png"/>
					<p>Paypal</p>
				</a>	
		  	</li>
		  	<li>
			    <a href="#money-stripe" data-toggle="modal">
					<img src="{{ deedel_url() }}/assets/elite/lp/images/sidebars.png"/>
					<p>Stripe</p>
				</a>	
		  	</li>
		  	<li>
			    <a href="#money-bank" data-toggle="modal">
					<img src="{{ deedel_url() }}/assets/elite/lp/images/pages.png"/>
					<p>Bank</p>
				</a>	
		  	</li>
		  	<li>
			    <a href="#money-remittances" data-toggle="modal">
					<img src="{{ deedel_url() }}/assets/elite/lp/images/e-com.png"/>
					<p>Money Remittances</p>
				</a>	
		  	</li>
		  </ul>
		</div>
	</div>
</div>
@endsection

@section('modal_account')
<!-- Use Paypal -->
<div id="money-paypal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Money Via Paypal</h4> 
            </div>

            <form action="{{ route('account.deedel.addWalletPaypal') }}" method="post" id="formViaPaypal" role="form">
            	<div class="modal-body">
            		{!! csrf_field() !!}
                    <div class="form-group">
                        <label for="amount-paypal" class="control-label">Amount:</label>
                        <input type="number" class="form-control" name="amount" id="amount-paypal">
                    </div>
            	</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-danger waves-effect waves-light" id="btnPaypal">Add Now</button>
	            </div>
	        </form>
        </div>
    </div>
</div>

	<!-- Use Stripe -->
<div id="money-stripe" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Money Via Stripe</h4> 
            </div>
            <form action="add_via_stripe" method="post" id="formViaStripe" role="form">
            	<div class="modal-body">
            		{!! csrf_field() !!}
                    <div class="form-group">
                        <label for="amount-stripe" class="control-label">Amount:</label>
                        <input type="number" class="form-control" id="amount-stripe" name="amount">
                    </div>
                    <input type="hidden" id="stripeToken" name="stripeToken"/>
  					<input type="hidden" id="stripeEmail" name="stripeEmail"/>
            	</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-danger waves-effect waves-light" id="btnStripe">Add Now</button>
	            </div>
	        </form>
        </div>
    </div>
</div>

	<!-- Use Bank -->
<div id="money-bank" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Money Via Bank</h4>
            </div>
           	<form action="{{ route('account.deedel.addWalletBank') }}" method="post" id="formViaBank">
            	<div class="modal-body">
            		{!! csrf_field() !!}
                    <div class="form-group">
                        <label for="by-bank" class="control-label">Bank:</label>
                        <select class="form-control" id="by-bank" name="bankName">
                        	<option value="BPI">Bank of the Philippine Islands (BPI)</option>
                        	<option value="BDO">BDO</option>
                        	<option value="unionBank">UnionBank</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="by-name" class="control-label">Sender (Name):</label>
                        <input type="text" class="form-control" id="by-name" name="sender">
                    </div>
                    <div class="form-group">
                        <label for="by-ref" class="control-label">Reference #:</label>
                        <input type="text" class="form-control" id="by-ref" name="reference">
                    </div>
                    <div class="form-group">
                        <label for="amount-bank" class="control-label">Amount:</label>
                        <input type="number" class="form-control" id="amount-bank" name="amount">
                    </div>
                    <h4>DeeDel Bank and Money Remittances Details</h4>
										<p>Account/Bank #</p>
										<p>Name: Diane Jison Apao</p>
										<p>Address: Unit 118 Bldg. 14 Decca Homes H. Cortes</p>
            	</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-danger waves-effect waves-light" id="btn-save-bank">Add Now</button>
	            </div>
	        </form>
        </div>
    </div>
</div>

	<!-- Use Remittance -->
<div id="money-remittances" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Money Via Money Remittances</h4>
            </div>
            <form id="formViaRemittance" method="post" action="{{ route('account.deedel.addWalletRemittance') }}">
            	<div class="modal-body">
            		{!! csrf_field() !!}
                    <div class="form-group">
                        <label for="by-remittances" class="control-label">Provider:</label>
                        <select class="form-control" id="by-remittances" name="remittanceName">
                        	<option id="xoom">Xoom</option>
                        	<option id="moneyGram">MoneyGram</option>
                        	<option id="westernUnion">westernUnion</option>
                        	<option id="palawan">Palawan</option>
                        	<option id="cebuanaLulhier">CubuanaLulhier</option>
                        	<option id="mLhulier">MLhulier</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="byr-name" class="control-label">Sender (Name):</label>
                        <input type="text" class="form-control" id="byr-name" name="sender">
                    </div>
                    <div class="form-group">
                        <label for="byr-ref" class="control-label">Reference #:</label>
                        <input type="text" class="form-control" id="byr-ref" name="reference">
                    </div>
                    <div class="form-group">
                        <label for="amount-remittances" class="control-label">Amount:</label>
                        <input type="number" class="form-control" id="amount-remittances" name="amount">
                    </div>
                    <h4>DeeDel Bank and Money Remittances Details</h4>
					<p>Account/Bank #</p>
					<p>Name: Diane Jison Apao</p>
					<p>Address: Unit 118 Bldg. 14 Decca Homes H. Cortes</p>
            	</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-danger waves-effect waves-light" id="btn-save-remittance">Add Now</button>
	            </div>
	        </form>
        </div>
    </div>
</div>
@endsection

@section('extra_script')
<script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_6pRNASCoBOKtIshFeQd4XMUh"
    data-amount="999"
    data-name="Stripe.com"
    data-description="Example charge"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto"
    data-zip-code="true">
</script>
<script type="text/javascript">
	$(function() {
		var handler = StripeCheckout.configure({
			key: 'pk_test_4O2nUot8OB8FP32S2XorPsfo',
			image: '/assets/network/deedel/img/logo-deedel.png',
			token: function(token) {
				$("#stripeToken").val(token.id);
        		$("#stripeEmail").val(token.email);
				$('#formViaStripe').get(0).submit();
			}
		});

		// Close Checkout on page navigation
		$(window).on('popstate', function() {
		    handler.close();
		});

        // Add Stripe
        $('#btnStripe').on('click', function(e) {
            var amount = $("#amount-stripe").val() *100;
            
            if ($('#formViaStripe').valid()){
                // Open Checkout with further options
                handler.open({
                  name: 'Add Money',
                  description: 'to your account',
                  amount: amount
                });

                $('#money-stripe').modal('hide');
                e.preventDefault();
            }
        });

		// add bank
		$('#btn-save-bank').on('click', function() {
            var url = $('#formViaBank').attr('action');
            var post = $('#formViaBank').attr('method');
            var data = $('#formViaBank').serialize();

			if ($('#formViaBank').valid()) {
				$.ajax({
	                type: post,
	                url: url,
	                data: data,
	                dataType: 'json',
	                success: function (data) {
	                    console.log(data);
	                    setTimeout(function() {window.location = data.redirect_url;}, 1000);
	                },
	                error: function (data) {
	                    console.log('Error:', data);
	                }
	            });
	        } else
	        	return false;
		});

		// add remittance
		$('#btn-save-remittance').on('click', function() {
            var url = $('#formViaRemittance').attr('action');
            var post = $('#formViaRemittance').attr('method');
            var data = $('#formViaRemittance').serialize();

			if ($('#formViaRemittance').valid()) {
                $.ajax({
                    type: post,
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            } else
                return false;
		});

        // Validate Send Stripe Form
        $('#formViaStripe').validate({
            rules: {
                amount: {
                	required: true,
                	number: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                // leave it blank here.
            }
        });

    	// Validate Send Bank Form
        $('#formViaBank').validate({
            rules: {
                sender: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                reference: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                amount: {
                	required: true,
                	number: true,
                    digits: true,
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                // leave it blank here.
            }
        });

		// Validate Send Remittance Form
        $('#formViaRemittance').validate({
            rules: {
                sender: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                reference: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                amount: {
                	required: true,
                	number: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                // leave it blank here.
            }
        });
	});
</script>
@endsection