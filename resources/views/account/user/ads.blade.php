@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Ads</h2>
	<a href="" class="btn btn-primary">Create Ad Banner</a>
	<a href="ads/type" class="btn btn-primary">Ads Type</a>
	<!--		
		duration 
		impression - pricing - red
		click - pricing - green
	-->
	<form>
		<input type="text" name="" placeholder="search" class="form-control">
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
	<select class="form-control">
		<option>ads type</option>
		<option>ads type</option>
		<option>ads type</option>
	</select>
	<select class="form-control">
		<option>status</option>
		<option>status</option>
		<option>status</option>
	</select>
	<p>expiry 
	<select class="form-control">
		<option>30 days</option>
		<option>60 days</option>
	</select></p>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Ads Title</th>
				<th>Ads Desc</th>
				<th>URL</th>
				<th>Image</th>
				<th>Date Expiry</th>
				<th>Ads type</th>
				<th>Ads status</th>
				<th># of impressions</th>
				<th># of clicks</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>ads name</td>
				<td>ads title</td>
				<td>ads desc</td>
				<td>target url</td>
				<td>image src</td>
				<td>30 days</td>
				<td>size200x200</td>
				<td>active/pending/pause</td>
				<td>2000</td>
				<td>100</td>
			</tr>
			<tr>
				<td>ads name</td>
				<td>ads title</td>
				<td>ads desc</td>
				<td>target url</td>
				<td>image src</td>
				<td>30 days</td>
				<td>card200x200</td>
				<td>active/pending/pause</td>
				<td>5000</td>
				<td>500</td>
			</tr>
		</tbody>
	</table>
    <ul class="pagination">
        <li><a href="">1</a></li>
        <li class="active"><a href="">2</a></li>
        <li><a href="">3</a></li>
        <li><a href="">4</a></li>
    </ul>
</div>
@endsection