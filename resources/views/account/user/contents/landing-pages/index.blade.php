@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Landing Pages</h2>
	<a href="landing-pages/add" class="btn btn-primary">Create a landing page</a>
	<form>
		<input type="text" name="" class="form-control">
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
	<p>Network 
		<select class="form-control">
			<option>network name</option>
			<option>network name</option>
			<option>network name</option>
		</select>
	</p>
	<p>Membership Type 
		<select class="form-control">
			<option>membership type</option>
			<option>membership type</option>
			<option>membership type</option>
		</select>
	</p>
	<p>Status: <a href="">Draft</a>|<a href="">Active</a>|<a href="">Expired</a></p>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Network url/domain</th>
				<th>Membership url</th>
				<th>Short code</th>
				<th>Email Template</th>
				<th>Email List</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td><a href="landing-pages/item/1">Name</a></td>
				<td>Slug</td>
				<td>Network name(domain)</td>
				<td>membership-url</td>
				<td><textarea class="form-control" disabled>short code</textarea></td>
				<td><a href="">email template id</a></td>
				<td><a href="">email list id</a></td>
				<td>Draft/Active/Expired</td>
				<td><a href="">Renew</a>|<a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
			</tr>
			<tr>
				<td>2</td>
				<td><a href="item/1">Name</a></td>
				<td>Slug</td>
				<td>Network name(domain)</td>
				<td>membership-url</td>
				<td><textarea class="form-control" disabled>short code</textarea></td>
				<td><a href="">email template id</a></td>
				<td><a href="">email list id</a></td>
				<td>Draft/Active/Expired</td>
				<td><a href="">Renew</a>|<a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
			</tr>
			<tr>
				<td>3</td>
				<td><a href="item/1">Name</a></td>
				<td>Slug</td>
				<td>Network name(domain)</td>
				<td>membership-url</td>
				<td><textarea class="form-control" disabled>short code</textarea></td>
				<td><a href="">email template id</a></td>
				<td><a href="">email list id</a></td>
				<td>Draft/Active/Expired</td>
				<td><a href="">Renew</a>|<a href="">View</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
			</tr>
		</tbody>
	</table>
</div>
@endsection