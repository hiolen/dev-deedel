@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Forms</h2>
	<a href="forms/add" class="btn btn-primary">Create a form</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>ID</td>
				<td>Name</td>
				<td>Slug</td>
				<td>Status</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td><a href="">form name</a></td>
				<td>form-slug</td>
				<td>draft/publish/deleted</td>
				<td><a href="">Preview</a>|<a href="">Edit</a>|<a href="">Delete</a>|</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection