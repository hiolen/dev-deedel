@extends('account.inc.app')

@section('htmlheader_title')
DeeDel - Pages
@endsection

@section('contentheader_title')
Pages
@endsection

@section('contentheader_description')
Pages Description ...
@endsection

@section('content_account')
@include('account.inc.pageAdd')
@include('account.inc.pageUpdate')
<div class="col-md-8">
	<div class="box box-primary	">
		<div class="box-header with-border">
			Pages
			<span class="pull-right">
				<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#add-page">Create Page</button>
			</span>
			
		</div>

		<div class="box-body">
			<p id="table-session"></p>
			<p>network <select class="form-control" id="network">
				@foreach($websites as $website)
				<option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
				@endforeach
			</select></p>

			<p>membership type <select class="form-control" id="membership">
				@foreach($user->roles as $role)
					<option value="{{$role->id}}">{{$role->name}}</option>
				@endforeach
			</select></p>
			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Network url/domain</th>
						<th>Membership url</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="page-info">
					@foreach($pages as $page)
					<tr id="{{ $page->id }}">
						<td>{{$page->id}}</td>
						<td>{{$page->title}}</td>
						<td>{!! str_limit($page->content, 150) !!}</td>
						<td>
							<select id="website_id" data-id="{{$page->id}}">
								@foreach ($websites as $website)
								<option value="{{$website->id}}" @if($page->website->id == $website->id)selected @endif >{{ $website->name }}</option>
								@endforeach
							</select>
						</td>
						<td>
							<select id="role" data-id="{{$page->id}}">
								@foreach ($roles as $role)
								<option value="{{ $role->id }}" @if($page->role->id == $role->id)selected @endif >{{ $role->display_name }}</option>
								@endforeach
							</select>
						</td>
						<td>
							<select id="status" data-id="{{$page->id}}">
								<option value="draft" @if($page->status == 'draft')selected @endif >Draft</option>
								<option value="pending" @if($page->status == 'pending')selected @endif >Pending</option>
								<option value="published" @if($page->status == 'published')selected @endif >Published</option>
							</select>
						</td>
						<td>
							<a href="" class="btn btn-xs btn-success" data-id="{{$page->id}}" id="edit">Edit</a>
							<a href="" class="btn btn-xs btn-danger" data-id="{{$page->id}}" id="delete">Delete</a>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	
</div>
@endsection

@section('extra_script')

<script type="text/javascript">
$(function() {
	CKEDITOR.replace( 'contentPageAdd' );
	CKEDITOR.replace( 'contentPageUpdate' );
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });

	// Add Page
	$('#formPageAdd').on('submit', function(e) {
		e.preventDefault();

		// form atrributes
    var url = $('#formPageAdd').attr('action');
    var post = $('#formPageAdd').attr('method');
    var data = $('#formPageAdd').serialize();

    // initialize data
    var title = $('#cpb-add-title').val();
    var desc = CKEDITOR.instances.contentPageAdd.getData();
    var network = $('#cpb-add-network').val();
    var role = $('#cpb-add-role').val();
    var token = '{{csrf_token()}}';

    // Form data
    var form_data = new FormData();
    form_data.append('title', title);
    form_data.append('description', desc);
    form_data.append('network_id', network);
    form_data.append('role_id', role);
    form_data.append('_token', token);

    if ($('#formPageAdd').valid()) {
    	$.ajax({
    		type: post,
    		url: url,
    		data: form_data,
    		processData: false,
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        success: function(data) {
        	console.log(data);

        	var tr = $('<tr id="'+data.page.id+'"/>');

        	tr.append($("<td/>", {
        		text: data.page.id
        	})).append($("<td/>", {
        		text: data.page.title
        	})).append($("<td/>", {
            html: data.page.content.substring(0, 150)
        	})).append($("<td/>", {
        		text: data.domain
        	})).append($("<td/>", {
        		text: data.role
        	})).append($("<td/>", {
        		html: '<select>' + 
        			'<option value="draft"' + ((data.page.status == 'draft')) + '>Draft</option>' +
        			'<option value="pending"' + ((data.page.status == 'pending')) + '>Pending</option>' +
        			'<option value="published"' + ((data.page.status == 'published')) + '>Published</option>' +
        		'</select></td>'
        	})).append($("<td/>", {
        		html: '<a href="#" class="btn btn-xs btn-success" id="edit" data-id="'+data.page.id+'">Edit</a> ' + 
        		'<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="'+data.page.id+'">Delete</a> '
        	}))

        	var message = '<p id="table-session">' +
        		'<div class="alert-msg">' +
					    '<div class="alert alert-success">' +
					        data.message
					    '</div>' +
						'</div>'+
					'</p>';


        	$('#page-info').append(tr);
        	$('#table-session').replaceWith(message);
        	$('#add-page').modal('hide');
          
          // Fade out alert
          $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-msg").slideUp(500);
            $(".alert-msg").remove();
          });
        	$('#formPageAdd').trigger("reset");
        },
        error: function(data) {
        	console.log('Error', data);
        	location.reload();
        }
    	});
    }else
    	return false;
	});

	// Edit Blog
	$('body').delegate('#page-info #edit', 'click', function(e){
		e.preventDefault();
		var id = $(this).data('id');

		$.get("{{ route('account.deedel.contentPagesEdit') }}", {id:id}, function(data){
			var id = data.page.id;
			var title = data.page.title;
			var content = data.page.content;
			var network = data.page.website_id;
			var role = data.page.role_id;
			
      var data = $('#formPageUpdate');
      data.find('#cpb-update-id').val(id);
      data.find('#cpb-update-title').val(title);
			data.find('#cpb-update-network').val(network);
			data.find('#cpb-update-role').val(role);
      CKEDITOR.instances.contentPageUpdate.setData(content);
		
			$('#update-page').modal('show');
		});
	})

	// Update Blog
	$('#formPageUpdate').on('submit', function(e) {
		e.preventDefault();

		// form atrributes
    var url = $('#formPageUpdate').attr('action');
    var post = $('#formPageUpdate').attr('method');
    var data = $('#formPageUpdate').serialize();

    // initialize data
    var id = $('#cpb-update-id').val();
    var title = $('#cpb-update-title').val();
    var desc = CKEDITOR.instances.contentPageUpdate.getData();
    var network = $('#cpb-update-network').val();
    var role = $('#cpb-update-role').val();
    var token = '{{csrf_token()}}';
    
    // Form data
    var form_data = new FormData();
    form_data.append('id', id);
    form_data.append('title', title);
    form_data.append('description', desc);
    form_data.append('network_id', network);
    form_data.append('role_id', role);
    form_data.append('_token', token);
    
    if ($('#formPageUpdate').valid()) {
    	$.ajax({
    		type: post,
    		url: url,
    		data: form_data,
    		processData: false,
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        success: function(data) {

        	var tr = $('<tr id="'+data.page.id+'"/>');

        	tr.append($("<td/>", {
        		text: data.page.id
        	})).append($("<td/>", {
        		text: data.page.title
        	})).append($("<td/>", {
            html: data.page.content.substring(0, 150)
        	})).append($("<td/>", {
        		text: data.domain
        	})).append($("<td/>", {
        		text: data.role
        	})).append($("<td/>", {
        		html: '<select>' + 
        			'<option value="draft"' + ((data.page.status == 'draft')?'selected':'') + '>Draft</option>' +
        			'<option value="pending"' + ((data.page.status == 'pending')?'selected':'') + '>Pending</option>' +
        			'<option value="published"' + ((data.page.status == 'published')?'selected':'') + '>Published</option>' +
        		'</select></td>'
        	})).append($("<td/>", {
        		html: '<a href="#" class="btn btn-xs btn-success" id="edit" data-id="'+data.page.id+'">Edit</a> ' + 
        		'<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="'+data.page.id+'">Delete</a> '
        	}))

        	var message = '<p id="table-session">' +
        		'<div class="alert-msg">' +
					    '<div class="alert alert-success">' +
					        data.message
					    '</div>' +
						'</div>'+
					'</p>';

					// append data
        	$('#page-info tr#'+data.page.id).replaceWith(tr);
        	// alert message
        	$('#table-session').replaceWith(message);
        	// close modal
        	$('#update-page').modal('hide');
          
          // Fade out alert
          $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-msg").slideUp(500);
            $(".alert-msg").remove();
          });
          // reset form
        	$('#formPageUpdate').trigger("reset");
        },
        error: function(data) {
        	console.log('Error', data);
        	location.reload();
        }
    	});
    }else
    	return false;
	});

	// Delete Blog
	$('body').delegate('#page-info #delete', 'click', function(e){
		e.preventDefault();
    
    var id = $(this).data('id');
		var token = '{{csrf_token()}}';
		
		$.post("{{route('account.deedel.contentPagesDestroy')}}", {id:id, _token:token}, function(data){
			var message = '<p id="table-session">' +
    		'<div class="alert-msg">' +
			    '<div class="alert alert-warning">' +
			        data.message
			    '</div>' +
				'</div>'+
			'</p>';

			$('#table-session').replaceWith(message);
      // Fade out alert
      $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-msg").slideUp(500);
        $(".alert-msg").remove();
      });
			$('tr#'+data.id).remove();
		});
	});

	// Change Status
  $('body').on('change', '#page-info #status', function(e){
    e.preventDefault();

    var id = $(this).data('id');
    var data = $(this).val();
    var token = '{{ csrf_token() }}';

    $.post("{{ route('account.deedel.contentPagesStatus') }}", {id:id, _token:token, val:data}, function(data){
    	console.log(data);
      var message = '<p id="table-session">' +
        '<div class="alert-msg">' +
          '<div class="alert alert-warning">' +
              data.message
          '</div>' +
        '</div>'+
      '</p>';

      $('#table-session').replaceWith(message);
      // Fade out alert
      $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-msg").slideUp(500);
        $(".alert-msg").remove();
      });
    });
  });

  // Change Website
  $('body').on('change', '#page-info #website_id', function(e){
    e.preventDefault();

    var id = $(this).data('id');
    var data = $(this).val();
    var token = '{{ csrf_token() }}';

    $.post("{{ route('account.deedel.contentPagesWebsite') }}", {id:id, _token:token, val:data}, function(data){
    	console.log(data);
      var message = '<p id="table-session">' +
        '<div class="alert-msg">' +
          '<div class="alert alert-warning">' +
              data.message
          '</div>' +
        '</div>'+
      '</p>';

      $('#table-session').replaceWith(message);
      // Fade out alert
      $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-msg").slideUp(500);
        $(".alert-msg").remove();
      });
    });
  });

  // Change Role
  $('body').on('change', '#page-info #role', function(e){
    e.preventDefault();

    var id = $(this).data('id');
    var data = $(this).val();
    var token = '{{ csrf_token() }}';

    $.post("{{ route('account.deedel.contentPagesRole') }}", {id:id, _token:token, val:data}, function(data){
    	console.log(data);
      var message = '<p id="table-session">' +
        '<div class="alert-msg">' +
          '<div class="alert alert-warning">' +
              data.message
          '</div>' +
        '</div>'+
      '</p>';

      $('#table-session').replaceWith(message);
      // Fade out alert
      $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-msg").slideUp(500);
        $(".alert-msg").remove();
      });
    });
  });

	// Validate Add Page
  $('#formPageAdd').validate({
      rules: {
          name: {
              minlength: 3,
              maxlength: 55,
              required: true
          },
          description: {
              required: true
          },
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if (element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function(form) {
          // leave it blank here.
      }
  });	

  // Validate Update Page
  $('#formPageUpdate').validate({
      rules: {
          name: {
              minlength: 3,
              maxlength: 55,
              required: true
          },
          description: {
              required: true
          },
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
          if (element.parent('.input-group').length) {
              error.insertAfter(element.parent());
          } else {
              error.insertAfter(element);
          }
      },
      submitHandler: function(form) {
          // leave it blank here.
      }
  });	
});
</script>
@endsection