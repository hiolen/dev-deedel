@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Add a page</h2>	
    <form action="{{account_contents_pages_save_url()}}" method="POST" role="form">
        {!! csrf_field() !!}
        {{ form_input("text","title","","Name","form-control","Name") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("content","","Description","form-control text-editor","Description") }}
        <p>Network
        <select class="form-control" name="website_id">
            @foreach($websites as $website)
                <option value="{{$website->id}}">{{$website->name}} ({{$website->domain}})</option>
            @endforeach
        </select>
        </p>
        <p>membership type
        <select class="form-control" name="role_id">
            @foreach($roles->roles as $role)
            <option value="{{$role->id}}">{{$role->name}}</option>
            @endforeach
        </select>
        </p>
        <input class="btn btn-primary" type="submit" name="btn-draft" value="draft">
        <input class="btn btn-primary" type="submit" name="btn-publish" value="publish">
    </form>
</div>
@endsection

@section('script_footer_account')
    {{ account_footer_tinymce_js() }}
    {{ account_footer_tinymce_1_js() }}
@endsection