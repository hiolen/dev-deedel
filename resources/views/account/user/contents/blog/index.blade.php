@extends('account.inc.app')

@section('htmlheader_title')
DeeDel - Blog
@endsection

@section('contentheader_title')
Blog
@endsection

@section('contentheader_description')
Blog Description ...
@endsection

@section('content_account')
@include('account.inc.blogAdd')
@include('account.inc.blogUpdate')
<div class="col-md-8">
	@include('account.inc.sessions')
	<div class="box box-primary">
		<div class="box-header with-border">
			<h2>
				Blog 
				<span class="pull-right">
					<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#add-blog">+ Create</button>
				</span>
			</h2>
		</div>

		<div class="box-body">
			<p id="table-session"></p>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>content</th>
							<th>Network</th>
							<th>featured</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody id="blog-info">
						@foreach($blogs as $blog)
							<tr id="{{$blog->id}}">
								<td>{{$blog->id}}</td>
								<td>{{$blog->title}}</td>
								<td>{!! str_limit($blog->content, 150) !!}</td>
								<td>{{$blog->website->domain}}</td>
								<td>
									<select id="featured" data-id="{{$blog->id}}">
										<option value="1" @if($blog->featured == 1)selected @endif >Yes</option>
										<option value="0" @if($blog->featured == 0)selected @endif >No</option>
									</select>
								</td>
								<td>
									<select id="status" data-id="{{$blog->id}}">
										<option value="draft" @if($blog->status == 'draft')selected @endif >Draft</option>
										<option value="pending" @if($blog->status == 'pending')selected @endif >Pending</option>
										<option value="published" @if($blog->status == 'published')selected @endif >Published</option>
									</select>
								</td>
								<td>
									<a href="" class="btn btn-xs btn-success" data-id="{{$blog->id}}" id="edit">Edit</a>
									<a href="" class="btn btn-xs btn-danger" data-id="{{$blog->id}}" id="delete">Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
        {{ $blogs->links() }}
			</div>
		</div>		
	</div>
</div>
@endsection

@section('modal_account')

@endsection

@section('extra_script')
<script type="text/javascript">
	$(function() {
		CKEDITOR.replace( 'contentBlogAdd' );
    CKEDITOR.replace( 'contentBlogUpdate' );

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    // Add Blog
		$('#formBlogAdd').on('submit', function(e) {
			e.preventDefault();
      
      // form atrributes
      var url = $('#formBlogAdd').attr('action');
      var post = $('#formBlogAdd').attr('method');
      var data = $('#formBlogAdd').serialize();

      // initialize data
      var title = $('#cba-title').val();
      var desc = CKEDITOR.instances.contentBlogAdd.getData();
      var featured = $('#cba-feature').val();
      var web = $('#cba-web').val();
      var token = '{{csrf_token()}}';

      // Form data
      var form_data = new FormData();
      form_data.append('title', title);
      form_data.append('description', desc);
      form_data.append('featured', featured);
      form_data.append('website_id', web);
      form_data.append('_token', token);

			if ($('#formBlogAdd').valid()) {
        $.ajax({
          type: post,
          url: url,
          data: form_data,
          dataType: 'json',
          processData: false,
          contentType: false, // The content type used when sending data to the server.
          cache: false, // To unable request pages to be cached
          success: function(data) {
            var tr = $('<tr id="'+data.post.id+'"/>');

          	tr.append($("<td/>", {
          		text: data.post.id
          	})).append($("<td/>", {
          		text: data.post.title
          	})).append($("<td/>", {
          		// text: data.post.content.substring(0, 150)
              html: data.post.content.substring(0, 150)
          	})).append($("<td/>", {
          		text: data.domain
          	})).append($("<td/>", {
          		html: '<select>' + 
          			'<option value="1"' + ((data.post.featured == 1)?'selected':'') + '>Yes</option>' + 
          			'<option value="0"' + ((data.post.featured == 0)?'selected':'') + '>No</option>' + 
          		'</select></td>'
          	})).append($("<td/>", {
          		html: '<select>' + 
          			'<option value="draft"' + ((data.post.status == 'draft')) + '>Draft</option>' +
          			'<option value="pending"' + ((data.post.status == 'pending')) + '>Pending</option>' +
          			'<option value="published"' + ((data.post.status == 'published')) + '>Published</option>' +
          		'</select></td>'
          	})).append($("<td/>", {
          		html: '<a href="#" class="btn btn-xs btn-success" id="edit" data-id="'+data.post.id+'">Edit</a> ' + 
          		'<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="'+data.post.id+'">Delete</a> '
          	}))

          	var message = '<p id="table-session">' +
          		'<div class="alert-msg">' +
						    '<div class="alert alert-success">' +
						        data.message
						    '</div>' +
							'</div>'+
						'</p>';


          	$('#blog-info').append(tr);
          	$('#table-session').replaceWith(message);
          	$('#add-blog').modal('hide');
            
            // Fade out alert
            $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
              $(".alert-msg").slideUp(500);
              $(".alert-msg").remove();
            });
          	$('#formBlogAdd').trigger("reset");
          },
          error: function (data) {
            console.log('Error:', data);
            location.reload();
          }
        });
	    } else
	        return false;
		});

		// Edit Blog
		$('body').delegate('#blog-info #edit', 'click', function(e){
			e.preventDefault();
			var id = $(this).data('id');

			$.get("{{ route('account.deedel.contentsBlogEdit') }}", {id:id}, function(data){

				var id = data.blog.id;
				var title = data.blog.title;
				var content = data.blog.content;
				var status = data.blog.status;
				var featured = data.blog.featured;
				var website_id = data.blog.website_id;

        var data = $('#formBlogUpdate');
        data.find('#title').val(title);
				data.find('#id').val(id);
				data.find('#web').val(website_id);
        CKEDITOR.instances.contentBlogUpdate.setData(content);

				if(featured == 1)
					$('#formBlogUpdate #feature').prop("checked", true);

				$('#update-blog').modal('show');
			});
		})

		// Update Blog
		$('#formBlogUpdate').on('submit', function(e) {
			e.preventDefault();

			// form atrributes
      var url = $('#formBlogUpdate').attr('action');
      var post = $('#formBlogUpdate').attr('method');
      var data = $('#formBlogUpdate').serialize();

      // initialize data
      var id = $('#id').val();
      var title = $('#title').val();
      var desc = CKEDITOR.instances.contentBlogUpdate.getData();
      var featured = $('#feature').val();
      var web = $('#web').val();
      var token = '{{csrf_token()}}';

      // Form data
      var form_data = new FormData();
      form_data.append('id', id);
      form_data.append('title', title);
      form_data.append('description', desc);
      form_data.append('featured', featured);
      form_data.append('website_id', web);
      form_data.append('_token', token);
      
      if ($('#formBlogUpdate').valid()) {
        $.ajax({
          type: post,
          url: url,
          data: form_data,
          contentType: false, // The content type used when sending data to the server.
          cache: false, // To unable request pages to be cached
          processData: false,
          success: function(data) {
            console.log(data);
          	var tr = $('<tr id="'+data.post.id+'"/>');

          	tr.append($("<td/>", {
          		text: data.post.id
          	})).append($("<td/>", {
          		text: data.post.title
          	})).append($("<td/>", {
          		html: data.post.content.substring(0, 150)
          	})).append($("<td/>", {
          		text: data.domain
          	})).append($("<td/>", {
          		html: '<select>' + 
          			'<option value="1"' + ((data.post.featured == 1)?'selected':'') + '>Yes</option>' + 
          			'<option value="0"' + ((data.post.featured == 0)?'selected':'') + '>No</option>' + 
          		'</select></td>'
          	})).append($("<td/>", {
          		html: '<select>' + 
          			'<option value="draft"' + ((data.post.status == 'draft')) + '>Draft</option>' +
          			'<option value="pending"' + ((data.post.status == 'pending')) + '>Pending</option>' +
          			'<option value="published"' + ((data.post.status == 'published')) + '>Published</option>' +
          		'</select></td>'
          	})).append($("<td/>", {
          		html: '<a href="#" class="btn btn-xs btn-success" id="edit" data-id="'+data.post.id+'">Edit</a> ' + 
          		'<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="'+data.post.id+'">Delete</a> '
          	}))
            

          	var message = '<p id="table-session">' +
          		'<div class="alert-msg">' +
						    '<div class="alert alert-warning">' +
						        data.message
						    '</div>' +
							'</div>'+
						'</p>';

            // Append data
          	$('#blog-info tr#'+data.post.id).replaceWith(tr);
            // alert message
          	$('#table-session').replaceWith(message);
            // Close Modal
          	$('#update-blog').modal('hide');
            // Fade out alert
            $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
              $(".alert-msg").slideUp(500);
              $(".alert-msg").remove();
            });
            // Reset Form
          	$('#formBlogUpdate').trigger("reset");

          },
          error: function (data) {
            console.log('Error:', data);
            location.reload();
          }
        });
      } else
        return false;
		});

		// Delete Blog
		$('body').delegate('#blog-info #delete', 'click', function(e){
			e.preventDefault();
      
      var id = $(this).data('id');
			var token = '{{csrf_token()}}';
			
			$.post("{{route('account.deedel.contentsBlogDestroy')}}", {id:id, _token:token}, function(data){
				var message = '<p id="table-session">' +
      		'<div class="alert-msg">' +
				    '<div class="alert alert-warning">' +
				        data.message
				    '</div>' +
					'</div>'+
				'</p>';

				$('#table-session').replaceWith(message);
        // Fade out alert
        $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
          $(".alert-msg").slideUp(500);
          $(".alert-msg").remove();
        });
				$('tr#'+data.id).remove();
			});
		});

    // Change Status
    $('body').on('change', '#blog-info #status', function(e){
      e.preventDefault();

      var id = $(this).data('id');
      var data = $(this).val();
      var token = '{{ csrf_token() }}';

      $.post("{{ route('account.deedel.contentsBlogStatus') }}", {id:id, _token:token, val:data}, function(data){
        var message = '<p id="table-session">' +
          '<div class="alert-msg">' +
            '<div class="alert alert-warning">' +
                data.message
            '</div>' +
          '</div>'+
        '</p>';

        $('#table-session').replaceWith(message);
        // Fade out alert
        $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
          $(".alert-msg").slideUp(500);
          $(".alert-msg").remove();
        });
      });
    });

    // Change featured
    $('body').on('change', '#blog-info #featured', function(e){
      e.preventDefault();

      var id = $(this).data('id');
      var data = $(this).val();
      var token = '{{ csrf_token() }}';

      $.post("{{ route('account.deedel.contentsBlogFeatured') }}", {id:id, _token:token,val:data}, function(data){
        console.log(data);
        var message = '<p id="table-session">' +
          '<div class="alert-msg">' +
            '<div class="alert alert-warning">' +
                data.message
            '</div>' +
          '</div>'+
        '</p>';

        $('#table-session').replaceWith(message);
        // Fade out alert
        $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
          $(".alert-msg").slideUp(500);
          $(".alert-msg").remove();
        });
      });
    });

    // Validate Add Blog
    $('#formBlogAdd').validate({
        rules: {
            title: {
                minlength: 3,
                maxlength: 55,
                required: true
            },
            description: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            // leave it blank here.
        }
    });

     // Validate Update Blog
    $('#formBlogUpdate').validate({
        rules: {
            title: {
                minlength: 3,
                maxlength: 55,
                required: true
            },
            description: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            // leave it blank here.
        }
    });
	});
</script>
@endsection