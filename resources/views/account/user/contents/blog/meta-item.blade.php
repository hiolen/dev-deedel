@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Meta Name - Blog Name</h2>
	<a href="" class="btn btn-primary">Add Meta Item</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Description</th>
				<th>Parent</th>
				<th>Posts</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>meta item name</td>
				<td>meta-item-name</td>
				<td>description</td>
				<td>parent</td>
				<td>20</td>
				<td><a href="meta/1">View</a>|<a href="">Edit</a>|<a href="">Delete</a>|</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection