@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Blog Name Posts</h2>
	<p><a href="{{ account_contents_blog_url() }}/1/posts/add" class="btn btn-primary">Create Post</a>
	<a href="{{ account_contents_blog_url() }}/1/meta/add" class="btn btn-primary">Create Meta</a>
	<a href="{{ account_contents_blog_url() }}/1/meta" class="btn btn-primary">All Meta</a></p>
	<form>
		<input type="text" name="" class="form-control">
		<button type="submit" class="btn btn-primary">Search</button>
	</form>
	<p>
		Display by Status
		<select class="form-control">
			<option>Draft</option>
			<option>Pending</option>
			<option>Publish</option>
			<option>Deleted</option>
		</select>
	</p>
	<p>
		Display by Month
		<strong>calendar</strong>
	</p>
	<p>Sort
		<a href="">Newest</a> | <a href="">Oldest</a><br>
		<a href="">ABC asc</a> | <a href="">ABC desc</a>
		<a href="">comments asc</a> | <a href="">comments desc</a>
	</p>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>Title</td>
				<td>Meta</td>
				<td>Comments</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach($posts as $post)
				@if($user->id == $post->author)
				<tr>
					<td>{{$post->title}}</td>
					<td>
						meta category: @foreach(explode(',', $post->meta_id) as $z)
							@foreach($meta_cats as $meta_cat)
								@if($meta_cat->id == $z)
									<a href="#">{{$meta_cat->name}}</a>,
								@endif 
							@endforeach
           				@endforeach
           				<br>
						meta tags:@foreach(explode(',', $post->meta_id) as $z)
							@foreach($meta_tags as $meta_tag)
								@if($meta_tag->id == $z)
									<a href="#">{{$meta_tag->name}}</a>,
								@endif 
							@endforeach
           				@endforeach
           			</td>
           			<td><a href="">0 comment</a></td>
					<td><a href="">View</a>|<a href="">Edit</a>}<a href="">Delete</a></td>
				</tr>
				@endif
			@endforeach
			
		</tbody>
	</table>
    <ul class="pagination">
        <li><a href="">1</a></li>
        <li class="active"><a href="">2</a></li>
        <li><a href="">3</a></li>
        <li><a href="">4</a></li>
    </ul>
</div>
@endsection