@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Add Meta - Blog Name</h2>	
    <form>
        {{ form_input("text","title","","Title","form-control","Title") }}
        {{ form_input("text","slug","","Slug","form-control","Slug") }}
        {{ form_texteditor("desc","","Description","form-control text-editor","Description") }}
        {{ form_submit("Save") }}
        {{ form_submit("Publish") }}
    </form>
</div>
@endsection

@section('script_footer_account')
    {{ account_footer_tinymce_js() }}
    {{ account_footer_tinymce_1_js() }}
@endsection