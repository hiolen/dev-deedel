@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>All Meta - Blog Name</h2>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Slug</th>
				<th>Description</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		
		@foreach($metas as $meta)
		<tr>
		@foreach($post_metas as $post_meta)
			@if($post_meta == $meta->id)
			<td>{{$meta->id}}</td>
			<td>{{$meta->name}}</td>
			<td>{{$meta->slug}}</td>
			<td>{{$meta->content}}</td>
			<td><a href="meta/1">View</a>|<a href="meta/1/add">Add Item</a>|<a href="">Edit</a>|<a href="">Delete</a>|</td>
			@endif
		@endforeach
		
		</tr>
		@endforeach
		</tbody>
	</table>
</div>
@endsection