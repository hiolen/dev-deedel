@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Email Templates</h2>
	<a href="emails/add" class="btn btn-primary">Create Email Template</a>
	<a href="emails/list" class="btn btn-primary">Email Lists</a>
	<a href="emails/list/add" class="btn btn-primary">Add New Email List</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>ID</td>
				<td>Name</td>
				<td>Slug</td>
				<td>email list</td>
				<td>Status</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>Template Name</td>
				<td>slug</td>
				<td>email list id and name</td>
				<td>draft/publish/deleted</td>
				<td><a href="">Preview</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
			</tr>
			<tr>
				<td>1</td>
				<td>Template Name</td>
				<td>slug</td>
				<td>email list id and name</td>
				<td>draft/publish/deleted</td>
				<td><a href="">Preview</a>|<a href="">Edit</a>|<a href="">Delete</a></td>
			</tr>
		</tbody>
	</table>
</div>
@endsection