@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Assets</h2>
	<ul>
		<li><a href="#">Add Assets</a></li>
		<li><a href="#">Links</a></li>
		<li><a href="#">Files</a></li>
		<li><a href="#">Embed Media</a></li>
	</ul>
	<div class="links">
		<ul>
			<li><input type="text" name="" class="form-control">
				<button class="btn btn-primary">Add Link</button></li>
			<li><input type="text" name="" class="form-control">
				<button class="btn btn-primary">Add Link</button></li>
			<li><input type="text" name="" class="form-control">
				<button class="btn btn-primary">Add Link</button></li>
		</ul>
		<button class="btn btn-primary">Add more links</button>
	</div>
	<div class="file">
		upload image (jpg/gif/png)
		upload pdf
		upload xls
		<input type="file" name="">
	</div>
	<div class="embed">
		<ul>
			<li><a href="">Embed Video</a></li>
			<li><a href="">Embed Audio</a></li>
			<li><a href="">Embed Image</a></li>
			<li><a href="">Embed Photo Gallery</a></li>
		</ul>
		
		<div class="video">
			<select class="form-control">
				<option value="youtube">youtube</option>
				<option value="vimeo">vimeo</option>
			</select>
			<input type="text" name="" placeholder="Add Video Link" class="form-control">
		</div>
		<div class="audio">
			<input type="text" name="" placeholder="Add audio/mp3 link" class="form-control">
		</div>
		<div class="image">
			accepted images files (jpng/png/gif)
			<input type="text" name="" placeholder="Enter Image Link" class="form-control">
		</div>
		<div class="gallery">
			<select class="form-control">
				<option>gallery provider here</option>
				<option>gallery provider here</option>
				<option>gallery provider here</option>
			</select>
		</div>
	</div>
</div>
@endsection