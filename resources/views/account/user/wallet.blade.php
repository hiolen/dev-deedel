@extends('account.inc.app')

@section('htmlheader_title')
DeeDel - Wallet
@endsection

@section('contentheader_title')
Wallet
@endsection

@section('contentheader_description')
Wallet Description ...
@endsection

@section('content_account')
    <div class="col-md-8">
        @include('account.inc.sessions')

        <div class="row">
            <div class="col-wallet col-md-3">
                <div class="box box-primary">
                    <div class="box-body">
                        <h2>Wallet</h2>
                        <div class="wallet">
                            <h4><i class="fa fa-plus" aria-hidden="true"></i> <a href="{{ route('account.deedel.walletAdd') }}">Money</a></h4>
                            <p>Account Balance = ${{ $user->getTotalBalance() }} </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="box box-primary">
                    <div class="box-header">
                        <h2>Coins 
                            <a href="#modalLoadCoins" data-toggle="modal" data-title="Load Coins" class="btn btn-primary btn-add-meta load-btn"><i class="fa fa-plus"></i> Load Coins</a>
                        </h2>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="coin">
                                @foreach($coins as $coin)
                            
                                <div class="col-xs-6">
                                    <h4>{{ $coin->name }}</h4>
                                </div>
                                <div class="col-xs-6">
                                    <div class="pull-right">
                                        <a href="#modalBuyCoins-{{ $coin->name }}" data-toggle="modal" class="btn btn-primary btn-buyCoins">Buy Now</a>
                                        <p>Coins Earn: @if($user->coinType($coin->name)) {{ $user->coinType($coin->name) }} @else 0 @endif </p>
                                    </div>
                                </div>
                            
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal_account')
    <!-- Load Coins Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalLoadCoins" aria-labelledby="modalLoadCoinsLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLoadCoinsLabel">Load Coins</h4>
                </div>
                <form action="{{ route('account.deedel.loadCoins') }}" method="POST" id="loadForm" role="form">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        {{ form_input("text","code","","Code","form-control","Code") }}
                        {{ form_input("text","pin","","Pin","form-control","Pin") }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="loadBtn">Buy</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @foreach($coins as $coin)
		<div class="modal fade" tabindex="-1" role="dialog" id="modalBuyCoins-{{ $coin->name }}" aria-labelledby="modalBuyCoinsLabel-{{ $coin->name }}" data-coin-type="{{ $coin->name }}">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title" id="modalLoadCoinsLabel">Buy {{ $coin->name }} Coins</h4>
	                </div>
                    <form action="{{ route('account.deedel.buyCoins') }}" id="buyForm" role="form">
                        <div class="modal-body">
                            {!! csrf_field() !!}
                            <input type="hidden" value="{{$coin->name}}" name="type">
                            <div class="form-group">
                                <label class="control-label">{{ $coin->name }} Coins:</label>
                                <select class="form-control" id="coins" name="price_id">
                                    @foreach($coins_pricing as $pricing)
                                        @if($pricing->coins_type_id==$coin->id)
                                            <option value="{{ $pricing->id }}">{{ $pricing->coins_value }} - ${{ $pricing->price }}</option>
                                        @else
                                            <option>No available coins</option>
                                            <?php break 1; ?>
                                        @endif
                                    @endforeach              
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="buyBtn">Buy</button>
                        </div>
                    </form>
	            </div>
	        </div>
	    </div>
    @endforeach
@endsection

@section('extra_script')
<script type="text/javascript">
    $(function() {

        // Buy Button
        $('#buyBtn').on('click', function() {
            $.ajax({
                type: 'post',
                url: 'wallet/buy_coins',
                data: $('#buyForm').serialize(),
                datatype: 'json',
                success: function(data) {
                    console.log(data);
                    setTimeout(function() {window.location = data.redirect_url;}, 1000);
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });

        // Load Button
        $('#loadBtn').on('click', function() {
            var url = $('#loadForm').attr('action');
            var post = $('#loadForm').attr('method');
            var data = $('#loadForm').serialize();

            if ($('#loadForm').valid()) {
                $.ajax({
                    type: post,
                    url: url,
                    data: data,
                    datatype: 'json',
                    success: function(data) {
                        console.log(data);
                        setTimeout(function() {window.location = data.redirect_url;}, 1000);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            } else
                return false;      
        });

        // Validate Add User Form
        $('#loadForm').validate({
            rules: {
                code: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                },
                pin: {
                    minlength: 3,
                    maxlength: 55,
                    required: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                // leave it blank here.
            }
        });
    });
</script>
@endsection
