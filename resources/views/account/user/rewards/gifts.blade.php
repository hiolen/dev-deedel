@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Gifts</h2>
	<p><select class="form-control">
		<option class="category_id">category name</option>
		<option class="category_id">category name</option>
		<option class="category_id">category name</option>
		<option class="category_id">category name</option>
	</select></p>
	<ul>
		<li><button data-id="1" data-category="1" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="2" data-category="1" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="3" data-category="2" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="4" data-category="4" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="5" data-category="1" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="6" data-category="2" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="7" data-category="5" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
		<li><button data-id="8" data-category="3" class="btn"><svg width="20" height="20">gift icon</svg> gift name</button></li>
	</ul>
</div>
@endsection