@extends('account.inc.app')

@section('content_account')
<div class="user-affiliate">
	<div class="container">
		@include('account.inc.nav-affiliate')
		<div class="affiliate-referrals">
			<div class="row row-header">
				<div class="col-xs-12 col-sm-6">
					<strong>Email</strong>
				</div>
				<div class="col-xs-12 col-sm-3">
					<strong>Url</strong>
				</div>
				<div class="col-xs-12 col-sm-3">
					<strong>Status</strong>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<p><a href="#">user email</a></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p><a href="#">link</a></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p><a href="#">signup</a> | <a href="#">pending</a></p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p><a href="#">user email</a></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p><a href="#">link</a></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p><a href="#">signup</a> | <a href="#">pending</a></p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p><a href="#">user email</a></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p><a href="#">link</a></p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p><a href="#">signup</a> | <a href="#">pending</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection