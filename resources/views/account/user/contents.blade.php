@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Contents</h2>
	<ul>
		<li><a href="{{ account_contents_blog_url() }}">Blog</a></li>
		<li><a href="{{ account_contents_pages_url() }}">Pages</a></li>
		<li><a href="{{ account_contents_landing_pages_url() }}">Landing Pages</a></li>
		<li><a href="{{ account_contents_emails_url() }}">Email Templates</a></li>
		<li><a href="{{ account_contents_forms_url() }}">Forms</a></li>
		<li><a href="{{ account_contents_panels_url() }}">Panels</a></li>
	</ul>
</div>
@endsection