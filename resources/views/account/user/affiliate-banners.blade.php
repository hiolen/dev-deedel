@extends('account.inc.app')

@section('content_account')
<div class="user-affiliate ">
	<div class="container">
		@include('account.inc.nav-affiliate')
		<section>
			<h4>Banner 1</h4>
			<p><a href="#"><img src="" alt=""></a></p>
			<textarea class="form-control" disabled></textarea>
			<button type="button" class="btn btn-primary btn-copy">Copy</button>
		</section>
		<section>
			<h4>Banner 2</h4>
			<p><a href="#"><img src="" alt=""></a></p>
			<textarea class="form-control" disabled></textarea>
			<button type="button" class="btn btn-primary btn-copy">Copy</button>
		</section>
	</div>
</div>
@endsection