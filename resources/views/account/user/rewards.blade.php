@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Rewards</h2>
	<ul>
		<li><a href="rewards/points">Points</a></li>
		<li><a href="rewards/coins">Coins</a></li>
		<li><a href="rewards/gifts">Gifts</a></li>
		<li><a href="rewards/tokens">Tokens</a></li>
	</ul>
    <h3><strong>My Points:</strong></h3>
    <h3><strong>My Gifts:</strong></h3>
    <h3><strong>My Coins:</strong></h3>
    <h3><strong>My Tokens:</strong></h3>
</div>
@endsection