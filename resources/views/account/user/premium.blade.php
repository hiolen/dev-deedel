@extends('account.inc.app')

@section('content_account')
<div class="container">
	<h2>Premium</h2>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Network</th>
				<th>Membership Type</th>
				<th>Membership Image</th>
				<th>Network Membership URL</th>
			</tr>
		</thead>
		<tbody>
			@foreach($networks as $network)
				@foreach($websites as $website)
					@if($network == $website->id)
					<tr>
						<td>{{ $website->name }} ({{ $website->domain }})</td>
						<td>developer</td>
						<td>developer-image</td>
						<td>/account/premium/deedelhomes/developer</td>
					</tr>
					@endif
				@endforeach
			@endforeach
		</tbody>
	</table>
</div>
@endsection