@extends('account.inc.app')

@section('content_account')
<div class="user-affiliate text-center">
	<div class="container">
		@include('account.inc.nav-affiliate')
		<div class="affiliate-content">
			<p>Earn points when new sign up, membership upgrades, first coins purchase</p>
			<h5>Your affiliate link <strong>{{ deedel_url() }}?username</strong> <button type="button" class="btn btn-primary">Copy</button></h5>
			<p>
				<a href="" class="btn btn-primary btn-share btn-fb"><i class="fa fa-facebook fa-fb" aria-hidden="true"></i> facebook</a>
				<a href="" class="btn btn-primary btn-share btn-tweet"><i class="fa fa-twitter fa-tweet" aria-hidden="true"></i> tweet</a>
				<a href="" class="btn btn-primary btn-share btn-google"><i class="fa fa-google-plus fa-google" aria-hidden="true"></i> google plus</a>
			</p>
			<form>
				<p>Share via Email
					<textarea class="form-control"></textarea>
					<button type="submit" class="btn btn-primary">Share</button>
				</p>
			</form>
		</div>
	</div>
</div>
@endsection