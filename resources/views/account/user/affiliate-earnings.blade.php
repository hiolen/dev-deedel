@extends('account.inc.app')

@section('content_account')
<div class="user-affiliate">
	<div class="container">
		@include('account.inc.nav-affiliate')
		<div class="affiliate-earnings">
			<div class="row row-header">
				<div class="col-xs-12 col-sm-6">
					<strong>Activity</strong>
				</div>
				<div class="col-xs-12 col-sm-3">
					<strong>Earning</strong>
				</div>
				<div class="col-xs-12 col-sm-3">
					<strong>Date</strong>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<p>New signup</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>10 points</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>Date</p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p>first coins purchase</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>10 gold coins</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>Date</p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p>New signup from sharing</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>20 points</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>Date</p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p>New signup from sharing</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>20 points</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<p>Date</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection