@extends('layouts.app')

@section('content')
<div class="container">
    <ul>
        <li><a href="{{ url('/dashboard/books') }}">My Books</a></li>
        <li><a href="{{ url('/dashboard/book/add') }}">Add a book</a></li>
        <li><a href="{{ url('/dashboard/book/reviews/pending') }}">Pending Book Reviews</a></li>
        <li><a href="{{ url('/user/messages') }}">Inbox</a></li>
        <li><a href="{{ url('/user/messages/sent') }}">Sent</a></li>
        <li><a href="{{ url('/user/messages/inquiry') }}">Inquiries</a></li>
    </ul>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <ul>
            <li>
                <a href="#"><i class="fa fa-user"></i></a>
            </li>
        </ul>
    </div>
</div>
@endsection
