@extends('layouts.account.admin.app')

<?php
    $super_admin_deedel_url = Config::get('deedel_variables.super_admin_deedel_url');
?>
@section('super_admin_content')
<div class="container">
    <h2>DeeDel Dashboard</h2>

    <ul>
    	<li><a href="{{ $super_admin_deedel_url }}/coins">Coins</li>
    	<li><a href="{{ $super_admin_deedel_url }}/pricing">Pricing</li>
    	<li><a href="{{ $super_admin_deedel_url }}/coupons">Coupons</li>
    	<li><a href="{{ $super_admin_deedel_url }}/payments">Payments</li>
    	<li><a href="{{ $super_admin_deedel_url }}/forum">Forum</li>
    	<li><a href="{{ $super_admin_deedel_url }}/blog">Blog</li>
    	<li><a href="{{ $super_admin_deedel_url }}/help">Help</li>
    	<li><a href="{{ $super_admin_deedel_url }}/careers">Careers</li>
    </ul>
</div>
@endsection
