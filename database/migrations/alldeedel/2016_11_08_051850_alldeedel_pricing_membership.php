<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlldeedelPricingMembership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_membership', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('shortdesc');
            $table->text('desc');
            $table->integer('website_id')->unsigned();
            $table->integer('price');
            $table->integer('discount');
            $table->enum('pricing_type', ['onetime', 'monthly', 'quarterly', 'semiunnualy', 'annualy','triennially']);
            $table->integer('role')->unsigned();
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pricing_membership');
    }
}
