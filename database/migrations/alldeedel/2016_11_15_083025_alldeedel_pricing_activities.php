<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlldeedelPricingActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_activities', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('activity_code')->unique();
            $table->text('desc');
            $table->integer('coins_type_id')->unsigned();
            $table->integer('amount');
            $table->enum('pricing_type', ['onetime', 'monthly', 'quarterly', 'semiunnualy', 'annualy','triennially']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pricing_activities');
    }
}
