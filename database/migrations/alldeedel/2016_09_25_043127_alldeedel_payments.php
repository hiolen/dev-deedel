<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlldeedelPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->float('amount')->unsigned();
            $table->enum('status', ['payed', 'unpayed']);
            $table->enum('types', ['coins', 'purse', 'wallet', 'bag']);
            $table->enum('transaction', ['paypal', 'stripe', 'remittance', 'purchased']);
            $table->string('token');
            $table->string('payment_id');
            $table->timestamps();

            // $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
