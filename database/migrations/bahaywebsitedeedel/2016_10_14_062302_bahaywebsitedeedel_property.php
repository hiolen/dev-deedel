<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BahaywebsitedeedelProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->longText('details');
            $table->text('files');
            $table->text('links');
            $table->integer('price');
            $table->integer('meta_id')->unsigned();
            $table->integer('developer_id')->unsigned();
            $table->integer('payment_id')->unsigned();
            $table->integer('amenities_id')->unsigned();
            $table->integer('persqmprice');//if lot only
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property');
    }
}
