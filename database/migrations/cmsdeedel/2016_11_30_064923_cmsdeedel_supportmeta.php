<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsdeedelSupportmeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('cmsdeedel')->create('supportmeta', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('slug');
            $table->string('name');
            $table->string('excerpt');
            $table->string('content');
            $table->enum('status', ['draft', 'pending', 'published', 'deleted']);
            $table->integer('author')->unsigned();
            $table->timestamp('date_added');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supportmeta');
    }
}
