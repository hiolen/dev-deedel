<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsdeedelTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('cmsdeedel')->create('terms', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('desc');
            $table->enum('status', ['draft', 'pending', 'publish']);
            $table->integer('website_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('terms');
    }
}
