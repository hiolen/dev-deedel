<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsdeedelSupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('cmsdeedel')->create('support', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->string('excerpt');
            $table->string('content');
            $table->enum('status', ['draft', 'pending', 'published', 'deleted']);
            $table->timestamp('date_added');
            $table->integer('author')->unsigned()->index();
            $table->integer('website_id')->unsigned();
            $table->integer('meta_id');
            $table->string('template');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('support');
    }
}
