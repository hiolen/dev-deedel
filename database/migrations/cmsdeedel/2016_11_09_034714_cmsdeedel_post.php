<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsdeedelPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('cmsdeedel')->create('post', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->string('excerpt');
            $table->string('content');
            $table->enum('status', ['draft', 'pending', 'published', 'deleted']);
            $table->timestamp('date_added');
            $table->integer('author')->unsigned()->index();
            $table->integer('website_id')->unsigned();
            $table->integer('blog_id')->unsigned();
            $table->text('meta_id');
            $table->string('template');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post');
    }
}
