<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsdeedelBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('cmsdeedel')->create('blog', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->longText('content');
            $table->enum('status', ['draft', 'pending', 'published', 'deleted'])->default('draft');
            $table->enum('featured', [1, 0])->default(0);
            $table->integer('author')->unsigned()->index();
            $table->integer('website_id')->unsigned();
            $table->string('template');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog');
    }
}
