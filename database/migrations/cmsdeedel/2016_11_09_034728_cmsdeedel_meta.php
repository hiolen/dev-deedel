<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsdeedelMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('cmsdeedel')->create('meta', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('slug');
            $table->string('name');
            $table->string('excerpt');
            $table->string('content');
            $table->enum('status', ['draft', 'pending', 'published', 'deleted']);
            $table->enum('post_type', ['blog', 'support']);
            $table->integer('author')->unsigned()->index();
            $table->integer('website_id')->unsigned();
            $table->enum('meta_type', ['category', 'tag']);
            $table->timestamp('date_added');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meta');
    }
}
