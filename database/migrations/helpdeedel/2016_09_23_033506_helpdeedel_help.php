<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HelpdeedelHelp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help', function($table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('subheading');
            $table->string('slug')->unique();
            $table->string('content');
            $table->string('status');
            $table->integer('helpCategoryID')->unsigned()->index();
            $table->timestamp('dateAdded');
            $table->smallInteger('revised');
            $table->timestamps();
            $table->integer('revisedBy')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('help');
    }
}
