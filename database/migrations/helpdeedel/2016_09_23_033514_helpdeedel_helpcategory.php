<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HelpdeedelHelpcategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpcategory', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('details');
            $table->string('slug')->unique();
            $table->string('status');
            $table->timestamp('dateAdded');
            $table->smallInteger('revised');
            $table->integer('revisedBy')->unsigned()->index();
            $table->timestamps();
            $table->integer('WebsiteID')->unsigned()->index();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('helpcategory');
    }
}
