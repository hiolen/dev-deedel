<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessagedeedelMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message', function($table)
        {
            $table->increments('id');
            $table->string('subject');
            $table->string('content');
            $table->integer('byUserID')->unsigned()->index();
            $table->integer('toUserID')->unsigned()->index();
            $table->integer('messageTypeID')->unsigned()->index();
            $table->timestamp('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('message');
    }
}
