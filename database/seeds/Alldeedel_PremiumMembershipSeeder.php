<?php

use Illuminate\Database\Seeder;

class Alldeedel_PremiumMembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('premium_membership')->insert([
            'user_id' => 1,
            'membership_id' => 1,
            'expiry_days' => 30,
        ]);
        DB::connection('alldeedel')->table('premium_membership')->insert([
            'user_id' => 1,
            'membership_id' => 2,
            'expiry_days' => 30,
        ]);
        DB::connection('alldeedel')->table('premium_membership')->insert([
            'user_id' => 1,
            'membership_id' => 3,
            'expiry_days' => 30,
        ]);
    }
}
