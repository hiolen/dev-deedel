<?php

use Illuminate\Database\Seeder;

class TrabahoWebsite_BidSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('trabahowebsitedeedel')->table('bid')->insert([
            'message' => 'This is a sample bid message to a project',
            'amount' => 500,
            'projectID' => 1,
            'userID' => 1,
        ]);
        DB::connection('trabahowebsitedeedel')->table('bid')->insert([
            'message' => 'This is a sample bid message 2 to a project',
            'amount' => 300,
            'projectID' => 1,
            'userID' => 2,
        ]);
    }
}
