<?php

use Illuminate\Database\Seeder;

class AllDeedel_PricingCoinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('pricing_coins')->insert([
            'name' => 'Gold Coins',
            'desc' => '',
            'coins_type_id' => 1,
            'coins_value' => 100,
            'price' => 4.88,
            'discount_type' => 'percent',
            'discount_value' => 5,
        ]);
        DB::connection('alldeedel')->table('pricing_coins')->insert([
            'name' => 'Gold Coins',
            'desc' => '',
            'coins_type_id' => 1,
            'coins_value' => 200,
            'price' => 8.88,
            'discount_type' => 'percent',
            'discount_value' => 10,
        ]);
        DB::connection('alldeedel')->table('pricing_coins')->insert([
            'name' => 'Gold Coins',
            'desc' => '',
            'coins_type_id' => 1,
            'coins_value' => 500,
            'price' => 19.88,
            'discount_type' => 'fixed',
            'discount_value' => 5,
        ]);
        DB::connection('alldeedel')->table('pricing_coins')->insert([
            'name' => 'Gold Coins',
            'desc' => '',
            'coins_type_id' => 1,
            'coins_value' => 500,
            'price' => 19.88,
            'discount_type' => 'fixed',
            'discount_value' => 15,
        ]);
        DB::connection('alldeedel')->table('pricing_coins')->insert([
            'name' => 'Gold Coins',
            'desc' => '',
            'coins_type_id' => 1,
            'coins_value' => 500,
            'price' => 19.88,
            'discount_type' => 'fixed',
            'discount_value' => 5.22,
        ]);
    }
}
