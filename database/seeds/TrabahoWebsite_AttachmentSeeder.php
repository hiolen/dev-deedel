<?php

use Illuminate\Database\Seeder;

class TrabahoWebsite_AttachmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('trabahowebsitedeedel')->table('attachment')->insert([
            'name' => 'Attachment 1',
            'filename' => 'file name',
            'projectID' => 1,
        ]);
        DB::connection('trabahowebsitedeedel')->table('attachment')->insert([
            'name' => 'pdf to html',
            'filename' => 'pdftohtml.pdf',
            'projectID' => 1,
        ]);
    }
}
