<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AllDeedel_WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 1,
            'name' => 'DeeDel',
            'details' => 'This website is about real estate projects',
            'slug' => 'deedel',
            'domain' => 'www.deedel.com',
            'featured' => '0',
            'image' => '/assets/img/network/deedel/deedel.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 1,
            'name' => 'DeeDel Homes',
            'details' => 'This website is about real estate projects',
            'slug' => 'deedelhomes',
            'domain' => 'www.deedelhomes.com',
            'featured' => '0',
            'image' => '/assets/img/network/deedelhomes/deedelhomes.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 1,
            'name' => 'Booklulu',
            'details' => 'This website will help you market your books',
            'slug' => 'booklulu',
            'domain' => 'www.booklulu.com',
            'featured' => '0',
            'image' => '/assets/img/network/booklulu/booklulu.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 2,
            'name' => 'Cebuzzz',
            'details' => 'This website is all about Cebu',
            'slug' => 'cebuzzz',
            'domain' => 'www.cebuzzz.com',
            'featured' => '0',
            'image' => '/assets/img/network/cebuzzz/cebuzzz.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 3,
            'name' => 'DeeDel Ace',
            'details' => 'This website is for filipino freelancers',
            'slug' => 'deedelace',
            'domain' => 'www.deedelace.com',
            'featured' => '0',
            'image' => '/assets/img/network/deedelace/deedelace.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 4,
            'name' => 'Trabaho',
            'details' => 'Freelance Work',
            'slug' => 'trabaho',
            'domain' => 'www.trabaho.pw',
            'featured' => '0',
            'image' => '/assets/img/network/trabaho/trabaho.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 1,
            'name' => 'Renta',
            'details' => 'This website will help you find the best home for you to rent',
            'slug' => 'renta',
            'domain' => 'www.renta.pw',
            'featured' => '0',
            'image' => '/assets/img/network/renta/renta.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('website')->insert([
            'user_id' => 1,
            'name' => 'Basahon',
            'details' => 'This website is free to create reading materials',
            'slug' => 'basahon',
            'domain' => 'www.basahon.com',
            'featured' => '0',
            'image' => '/assets/img/network/basahon/basahon.jpg',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
