<?php

use Illuminate\Database\Seeder;

class CMSDeedel_PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-1',
            'slug' => 'blog-1',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,6',
            'author' => 3,
            'website_id' => 1,
            'blog_id' => 1,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-2',
            'slug' => 'blog-2',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'published',
            'date_added' => '',
            'meta_id' => '7,2,4,5,6',
            'author' => 3,
            'website_id' => 2,
            'blog_id' => 1,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-3',
            'slug' => 'blog3',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,8',
            'author' => 3,
            'website_id' => 2,
            'blog_id' => 1,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-4',
            'slug' => 'blog-4',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,7,5,6',
            'author' => 3,
            'website_id' => 2,
            'blog_id' => 1,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-5',
            'slug' => 'blog-5',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,8,6',
            'author' => 1,
            'website_id' => 2,
            'blog_id' => 1,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-5',
            'slug' => 'blog-5',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,8',
            'author' => 1,
            'website_id' => 3,
            'blog_id' => 2,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-6',
            'slug' => 'blog-6',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,7',
            'author' => 1,
            'website_id' => 1,
            'blog_id' => 2,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-7',
            'slug' => 'blog-7',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,8',
            'author' => 1,
            'website_id' => 3,
            'blog_id' => 2,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-8',
            'slug' => 'blog-8',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,7',
            'author' => 3,
            'website_id' => 3,
            'blog_id' => 1,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-9',
            'slug' => 'blog-9',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,7',
            'author' => 1,
            'website_id' => 2,
            'blog_id' => 2,
            'template' => 'page',
        ]);
        DB::connection('cmsdeedel')->table('post')->insert([
            'title' => 'Blog-10',
            'slug' => 'blog-10',
            'excerpt' => '<h3>This is a short summary</h3>',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'date_added' => '',
            'meta_id' => '1,2,4,5,8',
            'author' => 1,
            'website_id' => 3,
            'blog_id' => 3,
            'template' => 'page',
        ]);
    }
}
