<?php

use Illuminate\Database\Seeder;

class CMSDeedel_SupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('support')->insert([
            'title' => 'Support Page 1',
            'slug' => 'support-page-1',
            'excerpt' => 'support page content summary',
            'content' => 'support page content full',
            'status' => 'draft',
            'author' => 1,
            'website_id' => 1,
            'template' => 'supportpage',
            'meta_id' => 1,
            'date_added' => '',
        ]);
    }
}
