<?php

use Illuminate\Database\Seeder;

class HelpDeedel_HelpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::connection('helpdeedel')->table('help')->insert([
            'title' => 'Help 1',
            'subheading' => 'This is a help',
            'slug' => 'help1',
            'content' => 'this is a content help',
            'status' => 'publish',
            'HelpCategoryID' => 5,
        ]);
	     DB::connection('helpdeedel')->table('help')->insert([
	        'title' => 'Help 2',
	        'subheading' => 'This is a help2',
	        'slug' => 'help2',
	        'content' => 'this is a content help',
	        'status' => 'publish',
	        'HelpCategoryID' => 6,
	    ]);
	     DB::connection('helpdeedel')->table('help')->insert([
            'title' => 'Help 3',
            'subheading' => 'This is help3',
            'slug' => 'help3',
            'content' => 'this is a content help',
            'status' => 'publish',
            'HelpCategoryID' => 5,
        ]);
	     DB::connection('helpdeedel')->table('help')->insert([
            'title' => 'Help 4',
            'subheading' => 'This is a help 4',
            'slug' => 'help4',
            'content' => 'this is a content help',
            'status' => 'publish',
            'HelpCategoryID' => 6,
        ]);
    }
}
