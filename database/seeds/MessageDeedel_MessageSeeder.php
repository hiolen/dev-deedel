<?php

use Illuminate\Database\Seeder;

class MessageDeedel_MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('messagedeedel')->table('message')->insert([
            'subject' => 'subject title',
            'content' => 'message text here',
            'byUserID' => 1,
            'toUserID' => 2,
            'messageTypeID' => 1,
        ]);
        DB::connection('messagedeedel')->table('message')->insert([
            'subject' => 'subject title 2',
            'content' => 'message text here 2',
            'byUserID' => 1,
            'toUserID' => 2,
            'messageTypeID' => 1,
        ]);
    }
}
