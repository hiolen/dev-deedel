<?php

use Illuminate\Database\Seeder;

class TrabahoWebsite_ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('trabahowebsitedeedel')->table('project')->insert([
            'slug' => 'project',
            'title' => 'project sample 1',
            'details' => 'This is a sample project detail',
            'budget' => '200-500',
            'skills' => '1,2,3,4,5',
            'categories' => '1,2,3',
            'attachment' => '1,2',
            'userID' => 1,
        ]);
        DB::connection('trabahowebsitedeedel')->table('project')->insert([
            'slug' => 'project-2',
            'title' => 'project sample 2',
            'details' => 'This is a sample project detail 2',
            'budget' => '100-500',
            'skills' => '1,4,5',
            'categories' => '1,3',
            'attachment' => '',
            'userID' => 1,
        ]);
        DB::connection('trabahowebsitedeedel')->table('project')->insert([
            'slug' => 'project-3',
            'title' => 'project sample 3',
            'details' => 'This is a sample project detail 3',
            'budget' => '200-1000',
            'skills' => '1,2,3',
            'categories' => '1,2',
            'attachment' => '1',
            'userID' => 1,
        ]);
    }
}
