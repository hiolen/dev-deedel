<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CMSDeedel_BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('blog')->insert([
            'title' => 'Blog 1',
            'slug' => 'blog-1',
            'content' => 'blog content',
            'status' => 'draft',
            'author' => 1,
            'featured' => 1,
            'website_id' => 1,
            'template' => 'blog',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('blog')->insert([
            'title' => 'Blog 2',
            'slug' => 'blog-2',
            'content' => 'blog content',
            'status' => 'draft',
            'author' => 3,
            'featured' => 1,
            'website_id' => 3,
            'template' => 'blog',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('blog')->insert([
            'title' => 'Blog 2',
            'slug' => 'blog-2',
            'content' => 'blog content',
            'status' => 'draft',
            'author' => 3,
            'featured' => 1,
            'website_id' => 3,
            'template' => 'blog',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('blog')->insert([
            'title' => 'Blog 1',
            'slug' => 'blog-1',
            'content' => 'blog content',
            'status' => 'draft',
            'author' => 1,
            'featured' => 1,
            'website_id' => 2,
            'template' => 'blog',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
