<?php

use Illuminate\Database\Seeder;

class MessageDeedel_ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('messagedeedel')->table('contact')->insert([
            'email' => 'dianeapao@gmail.com',
            'name' => 'diane',
            'subject' => 'subject title',
            'detail' => 'message text here',
        ]);
        DB::connection('messagedeedel')->table('contact')->insert([
            'email' => 'dianeapao@gmail.com',
            'name' => 'diane 2',
            'subject' => 'subject title 2',
            'detail' => 'message text here 2',
        ]);
        DB::connection('messagedeedel')->table('contact')->insert([
            'email' => 'dianeapao@gmail.com',
            'name' => 'diane 3',
            'subject' => 'subject title 3',
            'detail' => 'message text here 3',
        ]);
    }
}
