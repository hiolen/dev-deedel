<?php

use Illuminate\Database\Seeder;

class MarketingDeedel_RewardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('marketingdeedel')->table('rewards')->insert([
            'types' => 'coins',
            'name' => 'Free on Signup',
            'value' => 1,
            'amount' => 10,
            'price' => 4.8,
            'icon' => 'coins icon',
            'network_id' => 1,
        ]);
        DB::connection('marketingdeedel')->table('rewards')->insert([
            'types' => 'gifts',
            'name' => 'Free TV',
            'value' => 0,
            'amount' => 10,
            'price' => 4.8,
            'icon' => 'tv icon',
            'network_id' => 2,
        ]);
    }
}
