<?php

use Illuminate\Database\Seeder;

class AllDeedel_PricingMembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Developer',
            'slug' => 'developer',
            'desc' => 'DeedelHomes Developer',
            'website_id' => 1,
            'price' => 10,
            'discount' => 2,
            'pricing_type' => 'monthly',
        ]);
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Developer',
            'slug' => 'developer',
            'desc' => 'DeedelHomes Developer',
            'website_id' => 1,
            'price' => 20,
            'discount' => 2,
            'pricing_type' => 'quarterly',
        ]);
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Realtor',
            'slug' => 'realtor',
            'desc' => 'DeedelHomes Realtor',
            'website_id' => 1,
            'price' => 15,
            'discount' => 2,
            'pricing_type' => 'quarterly',
        ]);
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Agent',
            'slug' => 'agent',
            'desc' => 'DeedelHomes Agent',
            'website_id' => 1,
            'price' => 10,
            'discount' => 1,
            'pricing_type' => 'quarterly',
        ]);
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Indie Author',
            'slug' => 'indie-author',
            'desc' => 'Booklulu Indie Author',
            'website_id' => 2,
            'price' => 20,
            'discount' => 1,
            'pricing_type' => 'annually',
        ]);
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Professional Author',
            'slug' => 'professional-author',
            'desc' => 'Booklulu Professional Author',
            'website_id' => 2,
            'price' => 30,
            'discount' => 1,
            'pricing_type' => 'annually',
        ]);
        DB::connection('alldeedel')->table('pricing_membership')->insert([
            'name' => 'Publisher',
            'slug' => 'publisher',
            'desc' => 'Booklulu Publisher',
            'website_id' => 2,
            'price' => 40,
            'discount' => 1,
            'pricing_type' => 'annually',
        ]);
    }
}
