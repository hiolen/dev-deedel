<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	[
        		'name' => 'role-list',
        		'display_name' => 'Display Role Listing',
        		'description' => 'See only Listing Of Role'
        	],
        	[
        		'name' => 'role-create',
        		'display_name' => 'Create Role',
        		'description' => 'Create New Role'
        	],
        	[
        		'name' => 'role-edit',
        		'display_name' => 'Edit Role',
        		'description' => 'Edit Role'
        	],
        	[
        		'name' => 'role-delete',
        		'display_name' => 'Delete Role',
        		'description' => 'Delete Role'
        	],
        	[
        		'name' => 'project-list',
        		'display_name' => 'Display Project Listing',
        		'description' => 'See only Listing Of Project'
        	],
        	[
        		'name' => 'project-create',
        		'display_name' => 'Create Project',
        		'description' => 'Create New Project'
        	],
        	[
        		'name' => 'project-edit',
        		'display_name' => 'Edit Project',
        		'description' => 'Edit Project'
        	],
        	[
        		'name' => 'project-delete',
        		'display_name' => 'Delete Project',
        		'description' => 'Delete Project'
        	],
            [
                'name' => 'realtor-list',
                'display_name' => 'Display Realtor Listing',
                'description' => 'See only Listing Of Realtor'
            ],
            [
                'name' => 'realtor-create',
                'display_name' => 'Create Realtor',
                'description' => 'Create New Realtor'
            ],
            [
                'name' => 'realtor-edit',
                'display_name' => 'Edit Realtor',
                'description' => 'Edit Realtor'
            ],
            [
                'name' => 'realtor-delete',
                'display_name' => 'Delete Realtor',
                'description' => 'Delete Realtor'
            ],
            [
                'name' => 'approve-request-realtor',
                'display_name' => 'Approve request of Realtor',
                'description' => 'Approve request of Realtor'
            ],
            [
                'name' => 'approve-request-agent',
                'display_name' => 'Approve request of Agent',
                'description' => 'Approve request of Agent'
            ],
            [
                'name' => 'activity-report-list',
                'display_name' => 'Activity report listing',
                'description' => 'Activity report listing'
            ],
            [
                'name' => 'send-notification-realtor',
                'display_name' => 'Approve request of Realtor',
                'description' => 'Approve request of Realtor'
            ],
            [
                'name' => 'send-notification-agent',
                'display_name' => 'Approve request of Agent',
                'description' => 'Approve request of Agent'
            ],
            [
                'name' => 'gifts-and-rewards',
                'display_name' => 'Give gifts and rewards',
                'description' => 'Give gifts and rewards'
            ],
            [
                'name' => 'blog-list',
                'display_name' => 'Display Blog Listing',
                'description' => 'See only Listing Of Blog'
            ],
            [
                'name' => 'blog-create',
                'display_name' => 'Create Blog',
                'description' => 'Create New Blog'
            ],
            [
                'name' => 'blog-edit',
                'display_name' => 'Edit Blog',
                'description' => 'Edit Blog'
            ],
            [
                'name' => 'blog-delete',
                'display_name' => 'Delete Blog',
                'description' => 'Delete Blog'
            ]
        ];

        foreach ($permission as $key => $value) {
        	Permission::create($value);
        }
    }
}
