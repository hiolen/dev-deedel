<?php

use Illuminate\Database\Seeder;

class AllDeedel_CoinsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Gold',
            'desc' => 'Gold is used for giving rewards to a certain network activity. Gold has the highest value for rewards coins.',
            'price' => 0.10,
        ]);
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Silver',
            'desc' => 'Silver is used for giving rewards to a certain network activity same with gold where gold has the higher value.',
            'price' => 0.05,
        ]);
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Bronze',
            'desc' => 'Bronze is used for giving rewards to a certain network activity same with gold & silver where bronze has the lowest value.',
            'price' => 0.01,
        ]);
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Red',
            'desc' => 'Red, Blue and Green Coins are the only coins that can be converted to cash. Red has the lowest value compared to Green and Blue',
            'price' => 0.10,
        ]);
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Green',
            'desc' => 'Red, Blue and Green Coins are the only coins that can be converted to cash. Green has the 2nd highest value among Red and Blue',
            'price' => 1,
        ]);
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Blue',
            'desc' => 'Red, Blue and Green Coins are the only coins that can be converted to cash. Blue has the highest value compared to Red and Green.',
            'price' => 10,
        ]);
        DB::connection('alldeedel')->table('coins_type')->insert([
            'name' => 'Pink',
            'desc' => 'Pink can only be used in donating to charity/fundraising deedel network',
            'price' => 1.25,
        ]);
    }
}