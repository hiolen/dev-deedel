<?php

use Illuminate\Database\Seeder;

class HelpDeedel_HelpCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::connection('helpdeedel')->table('helpcategory')->insert([
            'name' => 'HelpCategory  1',
            'details' => 'This is a help category',
            'slug' => 'category1',
            'status' => 'publish',      
        ]);
         DB::connection('helpdeedel')->table('helpcategory')->insert([
            'name' => 'HelpCategory  2',
            'details' => 'This is a help category',
            'slug' => 'category2',
            'status' => 'publish',      
        ]);
    }
}
