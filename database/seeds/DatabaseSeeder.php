<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AllDeedel_UserSeeder::class);
        $this->call(AllDeedel_WebsiteSeeder::class);
        $this->call(AllDeedel_CoinsTypeSeeder::class);
        $this->call(AllDeedel_PricingCoinsSeeder::class);
        $this->call(AllDeedel_PricingMembershipSeeder::class);
        $this->call(Alldeedel_PremiumMembershipSeeder::class);

        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(Role_UserTableSeeder::class);
        $this->call(Permission_RoleTableSeeder::class);

        // $this->call(BahayWebsite_PropertySeeder::class);
        // $this->call(BahayWebsite_MetaSeeder::class);

        // $this->call(CMSDeedel_PostSeeder::class);
        // $this->call(CMSDeedel_MetaSeeder::class);
        // $this->call(CMSDeedel_PagesSeeder::class);

        // $this->call(MessageDeedel_MessageSeeder::class);
        // $this->call(MessageDeedel_ContactSeeder::class);

        // $this->call(MarketingDeedel_CouponsSeeder::class);
        // $this->call(MarketingDeedel_AffiliateSeeder::class);
        // $this->call(MarketingDeedel_RewardsSeeder::class);
        // $this->call(MarketingDeedel_PointsSeeder::class);
        // $this->call(MarketingDeedel_PromoSeeder::class);

        // $this->call(TrabahoWebsite_UserSeeder::class);
        // $this->call(TrabahoWebsite_SkillSeeder::class);
        // $this->call(TrabahoWebsite_CategorySeeder::class);
        // $this->call(TrabahoWebsite_ProjectSeeder::class);
        // $this->call(TrabahoWebsite_BidSeeder::class);
        // $this->call(TrabahoWebsite_ProjectSeeder::class);
        // $this->call(TrabahoWebsite_AttachmentSeeder::class);

        // $this->call(BahayWebsite_MetaSeeder::class);
        // $this->call(BahayWebsite_PropertySeeder::class);

        // $this->call(CMSDeedel_TermsSeeder::class);
        // $this->call(CMSDeedel_PostSeeder::class);
        // $this->call(CMSDeedel_BlogSeeder::class);
        // $this->call(CMSDeedel_MembershipSeeder::class);
        // $this->call(CMSDeedel_PagesSeeder::class);
        // $this->call(CMSDeedel_SupportSeeder::class);
        // $this->call(CMSDeedel_MetaSeeder::class);
        
    }
}
