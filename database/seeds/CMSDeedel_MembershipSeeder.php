<?php

use Illuminate\Database\Seeder;

class CMSDeedel_MembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('membership')->insert([
            'name' => 'Primary Homes',
            'slug' => 'primary-homes',
            'desc' => 'desc',
            'membership_id' => 1,
            'user_id' => 1,
        ]);
        DB::connection('cmsdeedel')->table('membership')->insert([
            'name' => 'Book Cafe',
            'slug' => 'bookcafe',
            'desc' => 'desc',
            'membership_id' => 2,
            'user_id' => 1,
        ]);
    }
}
