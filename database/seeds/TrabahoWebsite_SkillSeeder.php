<?php

use Illuminate\Database\Seeder;

class TrabahoWebsite_SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('trabahowebsitedeedel')->table('skill')->insert([
            'name' => 'Skill Name',
            'slug' => 'skill-slug',
            'desc' => 'This is Skill desc',
        ]);
        DB::connection('trabahowebsitedeedel')->table('skill')->insert([
            'name' => 'HTML',
            'slug' => 'html',
            'desc' => 'This is HTML skill desc',
        ]);
        DB::connection('trabahowebsitedeedel')->table('skill')->insert([
            'name' => 'CSS',
            'slug' => 'css',
            'desc' => 'This is CSS skill desc',
        ]);
        DB::connection('trabahowebsitedeedel')->table('skill')->insert([
            'name' => 'PHP',
            'slug' => 'php',
            'desc' => 'This is PHP skill desc',
        ]);
    }
}
