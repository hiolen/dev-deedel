<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
        	[
        		'name' => 'super-admin',
        		'display_name' => 'User Super Admin',
        		'description' => 'User Super Admin'
        	],
        	[
        		'name' => 'real-estate-developer',
        		'display_name' => 'Developer Estate User',
        		'description' => 'Developer Estate User'
        	],
            [
                'name' => 'real-estate-realtor',
                'display_name' => 'Realtor Estate User',
                'description' => 'Realtor Estate User'
            ],
            [
                'name' => 'real-estate-agent',
                'display_name' => 'Agent Estate User',
                'description' => 'Agent Estate User'
            ],
        ];

        foreach ($role as $key => $value) {
        	Role::create($value);
        }
    }
}
