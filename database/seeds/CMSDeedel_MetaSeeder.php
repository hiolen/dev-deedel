<?php

use Illuminate\Database\Seeder;

class CMSDeedel_MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership1',
            'slug' => 'membership1',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'published',
            'post_type' => 'support',
            'author' => 3,
            'website_id' => 2,
            'meta_type' => 'tag',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership2',
            'slug' => 'membership2',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'blog',
            'author' => 3,
            'website_id' => 2,
            'meta_type' => 'category',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership3',
            'slug' => 'membership3',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'blog',
            'author' => 1,
            'website_id' => 4,
            'meta_type' => 'tag',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership4',
            'slug' => 'membership4',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'support',
            'author' => 1,
            'website_id' => 2,
            'meta_type' => 'category',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership5',
            'slug' => 'membership5',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'support',
            'author' => 1,
            'website_id' => 1,
            'meta_type' => 'tag',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership6',
            'slug' => 'membership6',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'blog',
            'author' => 3,
            'website_id' => 2,
            'meta_type' => 'category',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership7',
            'slug' => 'membership7',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'support',
            'author' => 3,
            'website_id' => 2,
            'meta_type' => 'tag',
            'date_added' => ''
        ]);
        DB::connection('cmsdeedel')->table('meta')->insert([
            'parent_id' => 0,
            'name' => 'Membership8',
            'slug' => 'membership8',
            'excerpt' => 'This is an excerpt',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'post_type' => 'blog',
            'author' => 3,
            'website_id' => 1,
            'meta_type' => 'category',
            'date_added' => ''
        ]);
    }
}
