<?php

use Illuminate\Database\Seeder;

class TrabahoWebsite_UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('trabahowebsitedeedel')->table('user')->insert([
            'id' => 1,
            'name' => 'User Name / Company Name',
            'slug' => 'user-slug',
            'skills' => '1,2,3,4,5',
            'categories' => '1,2,3',
            'desc' => 'This is user/company short bio',
        ]);
        DB::connection('trabahowebsitedeedel')->table('user')->insert([
            'id' => 2,
            'name' => 'Deesignerd',
            'slug' => 'deesignerd',
            'skills' => '1,2,3,4,5',
            'categories' => '1,2,3',
            'desc' => 'This is Deesignerd short bio. This is Deesignerd short bio. This is Deesignerd short bio',
        ]);
        DB::connection('trabahowebsitedeedel')->table('user')->insert([
            'id' => 3,
            'name' => 'DeeDel Digital',
            'slug' => 'deedel-digital',
            'skills' => '1,2,3,4,5',
            'categories' => '1,2,3',
            'desc' => 'This is DeeDel Digital short bio',
        ]);
    }
}
