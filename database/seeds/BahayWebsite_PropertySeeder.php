<?php

use Illuminate\Database\Seeder;

class BahayWebsite_PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('bahaywebsitedeedel')->table('property')->insert([
            'name' => 'Property Name 1',
            'slug' => 'property-name-1',
            'details' => 'Property Name 1 Properties',
            'price' => 2000000,
            'persqmprice' => 2000,
            'meta_id' => 1,
            'user_id' => 1
        ]);

        DB::connection('bahaywebsitedeedel')->table('property')->insert([
            'name' => 'Property Name 2',
            'slug' => 'property-name-2',
            'details' => 'Property Name 2 Properties',
            'price' => 2500000,
            'persqmprice' => 2800,
            'meta_id' => 2,
            'user_id' => 1
        ]);

        DB::connection('bahaywebsitedeedel')->table('property')->insert([
            'name' => 'Property Name 3',
            'slug' => 'property-name-3',
            'details' => 'Property Name 3 Properties',
            'price' => 3000000,
            'persqmprice' => 3000,
            'meta_id' => 3,
            'user_id' => 1
        ]);

        DB::connection('bahaywebsitedeedel')->table('property')->insert([
            'name' => 'Property Name 4',
            'slug' => 'property-name-4',
            'details' => 'Property Name 4 Properties',
            'price' => 20000000,
            'persqmprice' => 20000,
            'meta_id' => 4,
            'user_id' => 1
        ]);
    }
}
