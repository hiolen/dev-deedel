<?php

use Illuminate\Database\Seeder;

class AllDeedel_PricingActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('pricing_activities')->insert([
            'name' => 'Adding Project (DeeDelHomes)',
            'activity_code' => 'DeeDelHomesADDProject',
            'desc' => 'adding new project in deedelhomes.com',
            'coins_type_id' => 1,
            'amount' => 20,
            'pricing_type' => 'monthly',
        ]);
        DB::connection('alldeedel')->table('pricing_activities')->insert([
            'name' => 'Adding Project (Booklulu)',
            'activity_code' => 'BookluluADDProject',
            'desc' => 'adding new project in deedelhomes.com',
            'coins_type_id' => 2,
            'amount' => 50,
            'pricing_type' => 'quarterly',
        ]);
        DB::connection('alldeedel')->table('pricing_activities')->insert([
            'name' => 'Adding Project (Trabaho)',
            'activity_code' => 'TrabahoADDProject',
            'desc' => 'adding new project in deedelhomes.com',
            'coins_type_id' => 3,
            'amount' => 500,
            'pricing_type' => 'anually',
        ]);
    }
}
