<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AllDeedel_UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('alldeedel')->table('users')->insert([
            'username' => 'Admin',
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('password'),
            'activated' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('users')->insert([
            'username' => 'Developer',
            'name' => 'Developer',
            'email' => 'developer@gmail.com',
            'password' => bcrypt('password'),
            'activated' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('users')->insert([
            'username' => 'Realtor',
            'name' => 'Realtor',
            'email' => 'realtor@gmail.com',
            'password' => bcrypt('password'),
            'activated' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('alldeedel')->table('users')->insert([
            'username' => 'Agent',
            'name' => 'Agent',
            'email' => 'agent@gmail.com',
            'password' => bcrypt('password'),
            'activated' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
