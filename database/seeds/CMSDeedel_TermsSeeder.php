<?php

use Illuminate\Database\Seeder;

class CMSDeedel_TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('terms')->insert([
            'name' => 'Coins',
            'desc' => 'coins is use in ',
            'status' => 'draft',
            'website_id' => 2,
        ]);
        DB::connection('cmsdeedel')->table('terms')->insert([
            'name' => 'Rewards',
            'desc' => 'the term reward is use when',
            'status' => 'publish',
            'website_id' => 2,
        ]);
        DB::connection('cmsdeedel')->table('terms')->insert([
            'name' => 'Coupons',
            'desc' => 'coupons is used when you want to clain rewards and coins',
            'status' => 'pending',
            'website_id' => 2,
        ]);
        DB::connection('cmsdeedel')->table('terms')->insert([
            'name' => 'payments',
            'desc' => 'payments are used in purchasing coins',
            'status' => 'publish',
            'website_id' => 2,
        ]);
    }
}
