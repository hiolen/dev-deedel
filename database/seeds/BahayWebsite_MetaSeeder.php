<?php

use Illuminate\Database\Seeder;

class BahayWebsite_MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'Lot Only',
            'slug' => 'lot-only',
            'desc' => 'Lot Only Properties',
            'parent' => 0,
            'types' => 'category'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'House and lot',
            'slug' => 'house-and-lot',
            'desc' => 'House and lot Properties',
            'parent' => 0,
            'types' => 'category'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'For Rent/Lease',
            'slug' => 'for-rent',
            'desc' => 'For Rent/Lease Properties',
            'parent' => 0,
            'types' => 'category'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'Condominiums',
            'slug' => 'condominiums',
            'desc' => 'Condominiums Properties',
            'parent' => 0,
            'types' => 'category'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'Rent to own',
            'slug' => 'rent-to-own',
            'desc' => 'You can pay monthly',
            'parent' => 0,
            'types' => 'tag'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'Ready for occupancy',
            'slug' => 'ready-for-occupancy',
            'desc' => 'Move-in immediately',
            'parent' => 0,
            'types' => 'tag'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'Cebu Province',
            'slug' => 'cebu-province',
            'desc' => 'Cebu Province Properties',
            'parent' => 0,
            'types' => 'location'
        ]);
        DB::connection('bahaywebsitedeedel')->table('meta')->insert([
            'name' => 'Cebu City',
            'slug' => 'cebu-city',
            'desc' => 'Cebu City Properties',
            'parent' => 4,
            'types' => 'location'
        ]);
    }
}
