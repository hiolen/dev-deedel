<?php

use Illuminate\Database\Seeder;

class MarketingDeedel_CouponsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('marketingdeedel')->table('coupons')->insert([
            'types' => 'coins',
            'coinsType_id' => 1,
            'amount' => 200,
            'coupon_code' => 'ADHIWERD1343ASDA',
            'pin_code' => '102394',
            'status' => 'active',
        ]);
        DB::connection('marketingdeedel')->table('coupons')->insert([
            'types' => 'gifts',
            'coinsType_id' => 1,
            'amount' => 1,
            'coupon_code' => 'ADHIYXRD1343ASDA',
            'pin_code' => '111394',
            'status' => 'active',
        ]);
    }
}
