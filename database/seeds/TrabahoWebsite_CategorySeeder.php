<?php

use Illuminate\Database\Seeder;

class TrabahoWebsite_CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('trabahowebsitedeedel')->table('category')->insert([
            'name' => 'Category Name',
            'slug' => 'category-name',
            'desc' => 'This is category desc',
        ]);
        DB::connection('trabahowebsitedeedel')->table('category')->insert([
            'name' => 'Web Development',
            'slug' => 'web-development',
            'desc' => 'This is Web Development category desc',
        ]);
        DB::connection('trabahowebsitedeedel')->table('category')->insert([
            'name' => 'Writing',
            'slug' => 'writing',
            'desc' => 'This is Writing category desc',
        ]);
    }
}
