<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CMSDeedel_PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Membership',
            'slug' => 'membership',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 1,
            'template' => 'membership',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'About',
            'slug' => 'about',
            'content' => 'this is the page full text test<p>This is a paragraph</p>',
            'status' => 'publish',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 2,
            'template' => '',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'FAQ',
            'slug' => 'faq',
            'content' => '',
            'status' => 'publish',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 3,
            'template' => 'faq',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Privacy Policy',
            'slug' => 'privacy',
            'content' => 'This is the Policy and privacy page',
            'status' => 'publish',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 4,
            'template' => '',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Terms & Conditions',
            'slug' => 'terms',
            'content' => 'This is the terms and conditions page',
            'status' => 'publish',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 1,
            'template' => '',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Page 1',
            'slug' => 'page-1',
            'content' => 'page content',
            'status' => 'draft',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 2,
            'template' => 'page',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Page 2',
            'slug' => 'page-2',
            'content' => 'page content',
            'status' => 'draft',
            'author' => 1,
            'website_id' => 1,
            'role_id' => 3,
            'template' => 'page2',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Membership',
            'slug' => 'membership',
            'content' => 'this is a blog post full text test<p>This is a paragraph</p>',
            'status' => 'draft',
            'author' => 2,
            'website_id' => 3,
            'role_id' => 4,
            'template' => 'membership',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'About',
            'slug' => 'about',
            'content' => 'this is the page full text test<p>This is a paragraph</p>',
            'status' => 'publish',
            'author' => 2,
            'website_id' => 3,
            'role_id' => 1,
            'template' => '',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'FAQ',
            'slug' => 'faq',
            'content' => '',
            'status' => 'publish',
            'author' => 2,
            'website_id' => 3,
            'role_id' => 2,
            'template' => 'faq',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Privacy Policy',
            'slug' => 'privacy',
            'content' => 'This is the Policy and privacy page',
            'status' => 'publish',
            'author' => 3,
            'website_id' => 4,
            'role_id' => 3,
            'template' => '',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Terms & Conditions',
            'slug' => 'terms',
            'content' => 'This is the terms and conditions page',
            'status' => 'publish',
            'author' => 3,
            'website_id' => 4,
            'role_id' => 4,
            'template' => '',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Page 1',
            'slug' => 'page-1',
            'content' => 'page content',
            'status' => 'draft',
            'author' => 3,
            'website_id' => 4,
            'role_id' => 1,
            'template' => 'page',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::connection('cmsdeedel')->table('pages')->insert([
            'title' => 'Page 2',
            'slug' => 'page-2',
            'content' => 'page content',
            'status' => 'draft',
            'author' => 3,
            'website_id' => 4,
            'role_id' => 2,
            'template' => 'page2',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
