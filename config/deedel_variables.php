<?php

    $user_account_url='/account';
    $user_account_contents_url=$user_account_url.'/contents';
    $admin_url='/account/admin';
    $admin_messages_url=$admin_url.'/messages';
    $admin_pricing_url=$admin_url.'/pricing';
    $admin_members_url=$admin_url.'/members';
    $deedelhomes_url='/network/deedelhomes';
    $admin_content_url=$admin_url.'/content';
    $admin_content_support_url=$admin_content_url.'/support';
    $admin_content_terms_url=$admin_content_url.'/terms';
    $admin_content_blog_url=$admin_content_url.'/blog';
    $admin_content_pages_url=$admin_content_url.'/pages';
    $admin_content_landing_pages_url=$admin_content_url.'/landing-pages';
    $admin_content_emails_url=$admin_content_url.'/emails';
    $admin_content_forms_url=$admin_content_url.'/forms';
    $admin_content_panels_url=$admin_content_url.'/panels';

    return array(
    // 'help_url' =>'help.deedeldigital.com',
    // 'support_url' =>'support.deedeldigital.com',
    // 'account_url' =>'account.deedeldigital.com',
    // 'forum_url' =>'forum.deedeldigital.com',
    // 'blog_url' =>'blog.deedeldigital.com',
    'websites_url' =>'/network',
    'pricing_url' =>'/pricing',
    'coins_url' =>'/coins',
    'terms_definition_url' =>'/terms-definition',
    //Admin
    // 'super_admin_url' =>'acount.deedeldigital.com/admin/admin',
    //Websites
    // 'website_booklulu_url' =>'www.booklulu.com',
    // 'website_trabaho_url' =>'www.trabaho.pw',
    // 'website_bahay_url' =>'www.bahay.pw',

//Local URL's
    'local_url' =>'http://localhost:8000',
    'deedel_url' =>'',
    'cart_url' =>'/cart',
    'contact_url' =>'/contact',
    'about_url' =>'/about',
    'careers_url' =>'/careers',
    'partners_url' =>'/partners',
    'help_url' =>'/help',
    'support_url' =>'/support',
    'forum_url' =>'/forum',
    'blog_url' =>'/blog',
    'faq_url' =>'/faq',
    'terms_url' =>'/terms',
    'privacy_url' =>'/privacy',

    'deedel_register_url' =>'/register',
    'deedel_login_url' =>'/login',
    'deedel_howto_url' =>'/howto',
    'deedel_network_url' =>'/network',
    
    //User
    'account_url' =>$user_account_url,
    'account_messages_url' =>$user_account_url.'/messages',
    'account_wallet_url' =>$user_account_url.'/wallet',
    'account_network_url' =>$user_account_url.'/network',
    'account_wallet_url' =>$user_account_url.'/wallet',
    'account_wallet_add_url' =>$user_account_url.'/wallet/add',
    'account_assets_url' =>$user_account_url.'/assets',
    'account_contents_url' =>$user_account_contents_url,
    'account_contents_blog_url' =>$user_account_contents_url.'/blog',
    'account_contents_blog_add_url' =>$user_account_contents_url.'/blog/add',
    'account_contents_pages_url' =>$user_account_contents_url.'/pages',
    'account_contents_pages_save_url' =>$user_account_contents_url.'/pages/save',
    'account_contents_landing_pages_url' =>$user_account_contents_url.'/landing-pages',
    'account_contents_emails_url' =>$user_account_contents_url.'/emails',
    'account_contents_forms_url' =>$user_account_contents_url.'/forms',
    'account_contents_panels_url' =>$user_account_contents_url.'/panels',
    'account_rewards_url' =>$user_account_url.'/rewards',
    'account_orders_url' =>$user_account_url.'/orders',
    'account_contests_url' =>$user_account_url.'/contests',
    'account_ads_url' =>$user_account_url.'/ads',
    'account_affiliate_url' =>$user_account_url.'/affiliate',
    'account_affiliate_banners_url' =>$user_account_url.'/affiliate/banners',
    'account_affiliate_referrals_url' =>$user_account_url.'/affiliate/referrals',
    'account_affiliate_earnings_url' =>$user_account_url.'/affiliate/earnings',
    'account_premium_url' =>$user_account_url.'/premium',
    'logout_url' =>'/logout',

    //Admin
    'super_admin_url' => $admin_url,
    'super_admin_messages_url' => $admin_messages_url,
    'super_admin_messages_inbox_url' => $admin_messages_url.'/inbox',
    'super_admin_messages_reports_url' => $admin_messages_url.'/reports',
    'super_admin_messages_support_url' => $admin_messages_url.'/support',
    'super_admin_messages_support_ticket_url' => $admin_messages_url.'/support/ticket',
    'super_admin_messages_support_ticket_add_url' => $admin_messages_url.'/support/ticket/add',
    'super_admin_network_url' => $admin_url.'/network',
    'super_admin_network_add_url' => $admin_url.'/network/add',
    'super_admin_network_save_url' => $admin_url.'/network/save',
    'super_admin_network_delete_url' => $admin_url.'/network/delete',
    'super_admin_network_dashboard_url' => $admin_url.'/network/dashboard',
    'super_admin_coupons_url' => $admin_url.'/coupons',
    'super_admin_coupons_add_url' => $admin_url.'/coupons/add',
    'super_admin_coupons_save_url' => $admin_url. '/coupons/save',
    'super_admin_coupons_upload_url' => $admin_url.'/coupons/upload',
    'super_admin_pricing_url' => $admin_pricing_url,
    'super_admin_pricing_coins_type_url' => $admin_pricing_url.'/coins-type',
    'super_admin_pricing_coins_set_url' => $admin_pricing_url.'/coins/set',
    'super_admin_pricing_coins_url' => $admin_pricing_url.'/coins',
    'super_admin_pricing_coins_set_save_url' => $admin_pricing_url.'/coins/set/save',
    'super_admin_pricing_coins_add_url' => $admin_pricing_url.'/coins/add',
    'super_admin_pricing_coins_save_url' => $admin_pricing_url.'/coins/save',
    'super_admin_pricing_membership_url' => $admin_pricing_url.'/membership',
    'super_admin_pricing_membership_add_url' => $admin_pricing_url.'/membership/add',
    'super_admin_pricing_membership_save_url' => $admin_pricing_url.'/membership/save',
    'super_admin_pricing_activities_url' => $admin_pricing_url.'/activities',
    'super_admin_pricing_activities_add_url' => $admin_pricing_url.'/activities/add',
    'super_admin_pricing_activities_save_url' => $admin_pricing_url.'/activities/save',
    'super_admin_payments_coins_url' => $admin_url.'/payments/coins',
    'super_admin_payments_membership_url' => $admin_url.'/payments/membership',
    'super_admin_payments_activities_url' => $admin_url.'/payments/activities',
    'super_admin_payments_rewards_url' => $admin_url.'/payments/rewards',
    'super_admin_payments_contest_url' => $admin_url.'/payments/contest',
    'super_admin_rewards_points_url' => $admin_url.'/rewards/points',
    'super_admin_rewards_gifts_url' => $admin_url.'/rewards/gifts',
    'super_admin_rewards_coins_url' => $admin_url.'/rewards/coins',
    'super_admin_rewards_token_url' => $admin_url.'/rewards/token',
    'super_admin_members_url' => $admin_members_url,
    'super_admin_members_users_premium_url' => $admin_members_url.'/users/premium',
    'super_admin_members_user_url' => $admin_members_url.'/user',
    'super_admin_members_user_delete_url' => $admin_members_url.'/user/delete',
    'super_admin_members_roles_url' => $admin_members_url.'/roles',
    'super_admin_members_permissions_url' => $admin_members_url.'/permissions',
    
    'super_admin_content_terms_url' => $admin_content_terms_url,
    'super_admin_content_terms_add_url' => $admin_content_terms_url.'/add',
    'super_admin_content_terms_save_url' => $admin_content_terms_url.'/save',
    'super_admin_content_support_url' => $admin_content_support_url,
    'super_admin_content_support_add_url' => $admin_content_support_url.'/add',
    'super_admin_content_support_category_url' => $admin_content_support_url.'/category',
    'super_admin_content_support_category_add_url' => $admin_content_support_url.'/category/add',
    'super_admin_content_blog_url' => $admin_content_blog_url,
    'super_admin_content_blog_add_url' => $admin_content_blog_url.'/add',
    'super_admin_content_blog_category_url' => $admin_content_blog_url.'/category',
    'super_admin_content_blog_category_add_url' => $admin_content_blog_url.'/category/add',
    'super_admin_content_blog_tag_url' => $admin_content_blog_url.'/tag',
    'super_admin_content_blog_tag_add_url' => $admin_content_blog_url.'/tag/add',
    'super_admin_content_pages_url' => $admin_content_pages_url,
    'super_admin_content_pages_add_url' => $admin_content_pages_url.'/add',
    'super_admin_content_pages_save_url' => $admin_content_pages_url.'/save',
    'super_admin_content_landing_pages_url' => $admin_content_landing_pages_url,
    'super_admin_content_emails_url' => $admin_content_emails_url,
    'super_admin_content_forms_url' => $admin_content_forms_url,
    'super_admin_content_panels_url' => $admin_content_panels_url,

    'super_admin_deedel_url' =>'/account/admin/admin/deedel',


    'admin_messages_url' => '/account/admin/messages/m',

    //Websites
    'booklulu_url' =>'/websites/booklulu',

    'trabaho_url' =>'/websites/trabaho',

    //Individual Websites
    'website_trabaho_url' =>'/website/trabaho',
    //DeeDelHomes.com
    'website_bahay_url' =>$deedelhomes_url,
    'website_bahay_admin_url' =>$deedelhomes_url.'/admin',
    'website_bahay_admin_meta_url' =>$deedelhomes_url.'/admin/meta',
    'website_bahay_admin_add_meta_url' =>$deedelhomes_url.'/admin/add/meta',
    'website_bahay_admin_edit_meta_url' =>$deedelhomes_url.'/admin/edit/meta',
    'website_bahay_admin_delete_meta_url' =>$deedelhomes_url.'/admin/delete/meta',
    //booklulu.com
    'website_booklulu_url' =>'/network/booklulu',
    //renta.pw
    'website_rent_url' =>'/network/rent',
    //themeswapper.com
    'website_themeswapper_url' =>'/network/themeswapper',
    'website_cebuzzz_url' =>'/network/cebuzzz',


    'network_admin_url' =>'/admin',

    );

?>