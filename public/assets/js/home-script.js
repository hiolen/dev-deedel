(function( $ ) {
    $(document).ready(function(){
        $(window).bind('scroll', function() {
            var navHeight = $(".banner").height() - 100;
            if ($(window).scrollTop() > navHeight) {
                $('body').addClass('h-fixed');
            }
            else {
                $('body').removeClass('h-fixed');
            }
        });
    });
})( jQuery );
