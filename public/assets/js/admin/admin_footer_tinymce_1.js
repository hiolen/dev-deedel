
$(function() {
    $(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
    });
    var details_editor_config = {
        selector: '.text-editor',
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        menubar: false,
        statusbar: false,
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor',
          'code advlist'
        ],
        a_plugin_option: true,
        toolbar: 'code | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media preview fullpage',
    };

    tinymce.init(details_editor_config);
});