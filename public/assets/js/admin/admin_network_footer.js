$(function() {

    $(".btn-add-meta").click(function(){
        var parents = [];
        var t = $(this);
        var id = t.attr("data-type");
        var title = t.attr("data-title");

        $('#addNetwork').on('show.bs.modal', function () {
          $("#meta-id").val(id);
          $(".meta-add-title").text(title);
        });

        $('#addNetwork').on('hide.bs.modal', function () {
          $("#meta-id").val("");
          $(".meta-add-title").text("Meta");
        });
    });

    // Delete Network Ajax
    $('.btn-del-meta').on('click', function() {
        $('.dname').html($(this).data('name'));
        $('.did').text($(this).data('id'));
    });


    // Validate Add Network Form
    $('#addMetaForm').validate({
        rules: {
            name: {
                minlength: 3,
                maxlength: 55,
                required: true
            }
        },
        messages: {
            name: {
                minLength: "Your Name of must be at least 3 characters",
                maxLength: "Your Name must be at least 55 characters",
                required: "Please provide your Name of Meta",
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            // leave it blank here.
        }
    });    

    // Validate Edit Meta Form
    $('#editMetaForm').validate({
        rules: {
            name: {
                minlength: 3,
                maxlength: 55,
                required: true
            }
        },
        messages: {
            name: {
                minLength: "Your Name of must be at least 3 characters",
                maxLength: "Your Name must be at least 55 characters",
                required: "Please provide your Name of Meta",
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            // leave it blank here.
        }
    });
});