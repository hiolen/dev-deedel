var app = angular.module("app", ["xeditable"]);

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

app.controller('Ctrl', function($scope) {
  $scope.chapter = {
  	id: 'ch1',
    title: 'chapter 1',
    content: 'chapter contents'
  };  
});