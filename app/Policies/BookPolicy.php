<?php

namespace App\Policies;

Use App\User;
Use App\Book;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can delete the given book.
     *
     * @param  User  $user
     * @param  Book  $book
     * @return bool
     */
    public function destroy(User $user, Book $book)
    {
        return $user->id === $book->user_id;
    }
}
