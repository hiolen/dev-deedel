<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSDeedel_Pages extends Model
{
    protected $connection = 'cmsdeedel';
    protected $table = 'pages';
    public $timestamps = false;
    
    public function website(){
        return $this->belongsTo('App\AllDeedel_Website', 'website_id');
    }
    public function author(){
    	return $this->belongsTo('App\AllDeedel_User', 'author');
    }
    public function role(){
    	return $this->belongsTo('App\Role','role_id');
    }
}
