<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_Wallet extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'alldeedel';
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wallets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sender', 'reference', 'amount'
    ];

    /**
     * Get the user that owns the wallet.
     */
    public function user()
    {
        return $this->belongsTo('App\AllDeedel_User', 'user_id');
    }
}
