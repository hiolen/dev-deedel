<?php
	function account_js_url(){
		return deedel_url().'/assets/js/account';
	}
	function account_css_url(){
		return deedel_url().'/assets/css/account';
	}
//Admin CSS
	function account_bootstrap_css(){
		echo '<link rel="stylesheet" href="'.account_css_url().'/bootstrap.min.css">';
	}
    function account_css(){
        echo '<link rel="stylesheet" href="'.account_css_url().'/style.css">';
    }
//JS
    function account_jquery_js(){
    	echo '<script src="'.account_js_url().'/jquery.min.js"></script>';
    }
    function account_bootstrap_js(){
    	echo '<script src="'.account_js_url().'/bootstrap.min.js"></script>';
    }
    function account_jquery_validate_js(){
    	echo '
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>';
    }
    function account_footer_tinymce_js(){
    	echo '<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>';
    }
    function account_footer_tinymce_1_js(){
    	echo '<script src="'.account_js_url().'/account_footer_tinymce_1.js"></script>';
    }



    function front_url(){
        return deedel_url().'/assets';
    }
    function bootstrap_front_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/bootstrap/css/bootstrap.min.css">';
    }
    function front_bower_components_toast_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/bower_components/toast-master/css/jquery.toast.css">';
    }
    function front_bower_components_morrisjs_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/bower_components/morrisjs/morris.css">';
    }
    function front_animate_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/front/css/animate.css">';
    }
    function front_style_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/front/css/style.css">';
    }
    function front_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/bootstrap/css/bootstrap.min.css">';
    }
    function front_default_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/front/css/colors/default.css">';
    }
    function front_blue_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/front/css/colors/blue.css">';
    }
    function jquery_js(){
        echo '<script src="'.front_url().'/bower_components/jquery/dist/jquery.min.js"></script>';
    }
    function bootstrap_front_js(){
        echo '<script src="'.front_url().'/bootstrap/js/bootstrap.min.js"></script>';
    }
    function front_js(){
        echo '<script src="'.front_url().'/bootstrap/js/bootstrap.min.js"></script>';
    }
    function waypoints_js(){
        echo '<script src="'.front_url().'/bower_components/waypoints/lib/jquery.waypoints.js"></script>';
    }
    function counterup_js(){
        echo '<script src="'.front_url().'/bower_components/counterup/jquery.counterup.min.js"></script>';
    }
    function slimscroll_js(){
        echo '<script src="'.front_url().'/front/js/jquery.slimscroll.js"></script>';
    }
    function waves_js(){
        echo '<script src="'.front_url().'/front/js/waves.js"></script>';
    }
    function raphael_js(){
        echo '<script src="'.front_url().'/bower_components/raphael/raphael-min.js"></script>';
    }
    function morris_js(){
        echo '<script src="'.front_url().'/bower_components/morrisjs/morris.js"></script>';
    }
    function custom_js(){
        echo '<script src="'.front_url().'/front/js/custom.min.js"></script>';
    }
    function dashboard1_js(){
        echo '<script src="'.front_url().'/front/js/dashboard1.js"></script>';
    }
    function sparkline_js(){
        echo '<script src="'.front_url().'/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>';
    }
    function charts_sparkline_js(){
        echo '<script src="'.front_url().'/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>';
    }
    function toast_js(){
        echo '<script src="'.front_url().'/bower_components/toast-master/js/jquery.toast.js"></script>';
    }
    function typed_js(){
        echo '<script src="'.front_url().'/bower_components//typed.js-master/dist/typed.min.js"></script>';
    }


    function deedel_style_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/network/deedel/css/style.css">';
    }

    function deedel_account_style_css(){
        echo '<link rel="stylesheet" href="'.front_url().'/network/deedel/css/account.css">';
    }
?>