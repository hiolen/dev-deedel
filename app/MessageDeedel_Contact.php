<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageDeedel_Contact extends Model
{
    //
    protected $connection = 'messagedeedel';
    protected $table = 'contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'subject', 'message'
    ];
}
