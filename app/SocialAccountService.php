<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\AllDeedel_SocialAccount;
use App\AllDeedel_User;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = AllDeedel_SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new AllDeedel_SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = AllDeedel_User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = AllDeedel_User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}