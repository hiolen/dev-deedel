<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSDeedel_Terms extends Model
{
	protected $connection = 'cmsdeedel';
    protected $table = 'terms';
    protected $fillable = ['name', 'desc', 'website_id'];
    public function website(){
    	return $this->belongsTo('App\AllDeedel_Website', 'website_id');
    }
}
