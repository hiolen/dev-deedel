<?php
	function admin_js_url(){
		return deedel_url().'/assets/js/admin';
	}
	function admin_css_url(){
		return deedel_url().'/assets/css/admin';
	}
//Admin CSS
	function admin_css(){
		echo '<link rel="stylesheet" href="'.admin_css_url().'/style.css">';
	}
//JS
    function admin_jquery_js(){
    	echo '<script src="'.admin_js_url().'/jquery.min.js"></script>';
    }
    function admin_bootstrap_js(){
    	echo '<script src="'.admin_js_url().'/bootstrap.min.js"></script>';
    }
    function admin_jquery_validate_js(){
    	echo '
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>';
    }
    function admin_footer_tinymce_js(){
    	echo '<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>';
    }
    function admin_footer_tinymce_1_js(){
    	echo '<script src="'.admin_js_url().'/admin_footer_tinymce_1.js"></script>';
    }
    function admin_network_footer_js(){
		echo '<script src="'.admin_js_url().'/admin_network_footer.js"></script>';
	}
    function admin_network_add_footer_js(){
		echo '<script src="'.admin_js_url().'/admin_network_add_footer.js"></script>';
	}
?>