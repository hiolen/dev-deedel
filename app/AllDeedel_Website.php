<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_Website extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'website';
    protected $fillable = ['type', 'domain','name', 'slug', 'details'];

    public function pricingMembership(){
    	return $this->hasMany('App\AllDeedel_PricingMembership', 'website_id');
    }
    public function terms(){
        return $this->hasMany('App\CMSDeedel_Terms', 'website_id');
    }
    public function pages(){
        return $this->hasMany('App\CMSDeedel_Pages', 'website_id');
    }
    public function blog(){
        return $this->hasMany('App\CMSDeedel_Blog', 'website_id');
    }
    /**
     * Get the websites for the user.
     */
    public function websites()
    {
        return $this->belongsTo('App\AllDeedel_Website', 'user_id');
    }
}
