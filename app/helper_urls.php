<?php

    function local_url(){
        return Config::get('deedel_variables.local_url');
    }
//DeeDel

    function deedel_register_url(){
        return Config::get('deedel_variables.deedel_register_url');
    }
    function deedel_login_url(){
        return Config::get('deedel_variables.deedel_login_url');
    }
    function deedel_howto_url(){
        return Config::get('deedel_variables.deedel_howto_url');
    }
    function deedel_network_url(){
        return Config::get('deedel_variables.deedel_network_url');
    }
    function deedel_url(){
        return Config::get('deedel_variables.deedel_url');
    }
    function about_url(){
        return Config::get('deedel_variables.about_url');
    }
    function contact_url(){
        return Config::get('deedel_variables.contact_url');
    }
    function faq_url(){
        return Config::get('deedel_variables.faq_url');
    }
    function terms_url(){
        return Config::get('deedel_variables.terms_url');
    }
    function privacy_url(){
        return Config::get('deedel_variables.privacy_url');
    }
    function careers_url(){
        return Config::get('deedel_variables.careers_url');
    }
    function partners_url(){
        return Config::get('deedel_variables.partners_url');
    }
    function websites_url(){
        return Config::get('deedel_variables.websites_url');
    }
    function pricing_url(){
        return Config::get('deedel_variables.pricing_url');
    }
    function coins_url(){
        return Config::get('deedel_variables.coins_url');
    }
    function cart_url(){
        return Config::get('deedel_variables.cart_url');
    }
    function help_url(){
        return Config::get('deedel_variables.help_url');
    }
    function support_url(){
        return Config::get('deedel_variables.support_url');
    }
    function forum_url(){
        return Config::get('deedel_variables.forum_url');
    }
    function terms_definition_url(){
        return Config::get('deedel_variables.terms_definition_url');
    }
    function blog_url(){
        return Config::get('deedel_variables.blog_url');
    }
    
//Admin
    function super_admin_url(){
        return Config::get('deedel_variables.super_admin_url');
    }
    function super_admin_messages_url(){
        return Config::get('deedel_variables.super_admin_messages_url');
    }
    function super_admin_messages_inbox_url(){
        return Config::get('deedel_variables.super_admin_messages_inbox_url');
    }
    function super_admin_messages_reports_url(){
        return Config::get('deedel_variables.super_admin_messages_reports_url');
    }
    function super_admin_messages_support_url(){
        return Config::get('deedel_variables.super_admin_messages_support_url');
    }
    function super_admin_messages_support_ticket_url(){
        return Config::get('deedel_variables.super_admin_messages_support_ticket_url');
    }
    function super_admin_messages_support_ticket_add_url(){
        return Config::get('deedel_variables.super_admin_messages_support_ticket_add_url');
    }
    function super_admin_network_url(){
        return Config::get('deedel_variables.super_admin_network_url');
    }
    function super_admin_network_add_url(){
        return Config::get('deedel_variables.super_admin_network_add_url');
    }
    function super_admin_network_save_url(){
        return Config::get('deedel_variables.super_admin_network_save_url');
    }
    function super_admin_network_dashboard_url(){
        return Config::get('deedel_variables.super_admin_network_dashboard_url');
    }
    function super_admin_network_delete_url(){
        return Config::get('deedel_variables.super_admin_network_delete_url');
    }
    function super_admin_coupons_url(){
        return Config::get('deedel_variables.super_admin_coupons_url');
    }
    function super_admin_coupons_add_url(){
        return Config::get('deedel_variables.super_admin_coupons_add_url');
    }
    function super_admin_coupons_save_url(){
        return Config::get('deedel_variables.super_admin_coupons_save_url');
    }
    function super_admin_coupons_upload_url(){
        return Config::get('deedel_variables.super_admin_coupons_upload_url');
    }
    function super_admin_pricing_url(){
        return Config::get('deedel_variables.super_admin_pricing_url');
    }
    function super_admin_pricing_coins_set_url(){
        return Config::get('deedel_variables.super_admin_pricing_coins_set_url');
    }
    function super_admin_pricing_coins_type_url(){
        return Config::get('deedel_variables.super_admin_pricing_coins_type_url');
    }
    function super_admin_pricing_coins_url(){
        return Config::get('deedel_variables.super_admin_pricing_coins_url');
    }
    function super_admin_pricing_coins_add_url(){
        return Config::get('deedel_variables.super_admin_pricing_coins_add_url');
    }
    function super_admin_pricing_coins_save_url(){
        return Config::get('deedel_variables.super_admin_pricing_coins_save_url');
    }
    function super_admin_pricing_coins_set_save_url(){
        return Config::get('deedel_variables.super_admin_pricing_coins_set_save_url');
    }
    function super_admin_pricing_membership_url(){
        return Config::get('deedel_variables.super_admin_pricing_membership_url');
    }
    function super_admin_pricing_membership_add_url(){
        return Config::get('deedel_variables.super_admin_pricing_membership_add_url');
    }
    function super_admin_pricing_membership_save_url(){
        return Config::get('deedel_variables.super_admin_pricing_membership_save_url');
    }
    function super_admin_pricing_activities_add_url(){
        return Config::get('deedel_variables.super_admin_pricing_activities_add_url');
    }
    function super_admin_pricing_activities_url(){
        return Config::get('deedel_variables.super_admin_pricing_activities_url');
    }
    function super_admin_pricing_activities_save_url(){
        return Config::get('deedel_variables.super_admin_pricing_activities_save_url');    
    }
    function super_admin_payments_coins_url(){
        return Config::get('deedel_variables.super_admin_payments_coins_url');
    }
    function super_admin_payments_membership_url(){
        return Config::get('deedel_variables.super_admin_payments_membership_url');
    }
    function super_admin_payments_activities_url(){
        return Config::get('deedel_variables.super_admin_payments_activities_url');
    }
    function super_admin_payments_rewards_url(){
        return Config::get('deedel_variables.super_admin_payments_rewards_url');
    }
    function super_admin_payments_contest_url(){
        return Config::get('deedel_variables.super_admin_payments_contest_url');
    }
    function super_admin_rewards_points_url(){
        return Config::get('deedel_variables.super_admin_rewards_points_url');
    }
    function super_admin_rewards_gifts_url(){
        return Config::get('deedel_variables.super_admin_rewards_gifts_url');
    }
    function super_admin_rewards_coins_url(){
        return Config::get('deedel_variables.super_admin_rewards_coins_url');
    }
    function super_admin_rewards_token_url(){
        return Config::get('deedel_variables.super_admin_rewards_token_url');
    }

    function super_admin_members_url(){
        return Config::get('deedel_variables.super_admin_members_url');
    }
    function super_admin_members_users_premium_url(){
        return Config::get('deedel_variables.super_admin_members_users_premium_url');
    }
    function super_admin_members_user_url(){
        return Config::get('deedel_variables.super_admin_members_user_url');
    }
    function super_admin_members_user_delete_url(){
        return Config::get('deedel_variables.super_admin_members_user_delete_url');
    }
    function super_admin_members_roles_url(){
        return Config::get('deedel_variables.super_admin_members_roles_url');
    }
    function super_admin_members_permissions_url(){
        return Config::get('deedel_variables.super_admin_members_permissions_url');
    }
    
    function super_admin_content_support_url(){
        return Config::get('deedel_variables.super_admin_content_support_url');
    }
    function super_admin_content_support_add_url(){
        return Config::get('deedel_variables.super_admin_content_support_add_url');
    }
    function super_admin_content_support_category_url(){
        return Config::get('deedel_variables.super_admin_content_support_category_url');
    }
    function super_admin_content_support_category_add_url(){
        return Config::get('deedel_variables.super_admin_content_support_category_add_url');
    }
    function super_admin_content_terms_url(){
        return Config::get('deedel_variables.super_admin_content_terms_url');
    }
    function super_admin_content_terms_add_url(){
        return Config::get('deedel_variables.super_admin_content_terms_add_url');
    }
    function super_admin_content_terms_save_url(){
        return Config::get('deedel_variables.super_admin_content_terms_save_url');
    }
    function super_admin_content_blog_url(){
        return Config::get('deedel_variables.super_admin_content_blog_url');
    }
    function super_admin_content_blog_add_url(){
        return Config::get('deedel_variables.super_admin_content_blog_add_url');
    }
    function super_admin_content_blog_category_url(){
        return Config::get('deedel_variables.super_admin_content_blog_category_url');
    }
    function super_admin_content_blog_category_add_url(){
        return Config::get('deedel_variables.super_admin_content_blog_category_add_url');
    }
    function super_admin_content_blog_tag_url(){
        return Config::get('deedel_variables.super_admin_content_blog_tag_url');
    }
    function super_admin_content_blog_tag_add_url(){
        return Config::get('deedel_variables.super_admin_content_blog_tag_add_url');
    }
    function super_admin_content_pages_url(){
        return Config::get('deedel_variables.super_admin_content_pages_url');
    }
    function super_admin_content_pages_add_url(){
        return Config::get('deedel_variables.super_admin_content_pages_add_url');
    }
    function super_admin_content_pages_save_url(){
        return Config::get('deedel_variables.super_admin_content_pages_save_url');
    }
    function super_admin_content_landing_pages_url(){
        return Config::get('deedel_variables.super_admin_content_landing_pages_url');
    }
    function super_admin_content_emails_url(){
        return Config::get('deedel_variables.super_admin_content_emails_url');
    }
    function super_admin_content_forms_url(){
        return Config::get('deedel_variables.super_admin_content_forms_url');
    }
    function super_admin_content_panels_url(){
        return Config::get('deedel_variables.super_admin_content_panels_url');
    }


    function super_admin_deedel_url(){
        return Config::get('deedel_variables.super_admin_deedel_url');
    }

    
    function admin_pricing_url(){
        return Config::get('deedel_variables.admin_pricing_url');
    }

    function admin_messages_url(){
        return Config::get('deedel_variables.admin_messages_url');
    }



    //Websites
    function booklulu_url(){
        return Config::get('deedel_variables.booklulu_url');
    }
    function trabaho_url(){
        return Config::get('deedel_variables.trabaho_url');
    }

    //Individual Websites
    function website_booklulu_url(){
        return Config::get('deedel_variables.website_booklulu_url');
    }
    function website_trabaho_url(){
        return Config::get('deedel_variables.website_trabaho_url');
    }
    //DeeDelHomes.com
    function website_bahay_url(){
        return Config::get('deedel_variables.website_bahay_url');
    }
    function website_bahay_admin_url(){
        return Config::get('deedel_variables.website_bahay_admin_url');
    }
    function website_bahay_admin_meta_url(){
        return Config::get('deedel_variables.website_bahay_admin_meta_url');
    }
    function website_bahay_admin_add_meta_url(){
        return Config::get('deedel_variables.website_bahay_admin_add_meta_url');
    }
    function website_bahay_admin_edit_meta_url(){
        return Config::get('deedel_variables.website_bahay_admin_edit_meta_url');
    }
    function website_bahay_admin_delete_meta_url(){
        return Config::get('deedel_variables.website_bahay_admin_delete_meta_url');
    }
    function website_cebuzzz_url(){
        return Config::get('deedel_variables.website_cebuzzz_url');
    }
    function website_themeswapper_url(){
        return Config::get('deedel_variables.website_themeswapper_url');
    }
    //rent.pw
    function website_rent_url(){
        return Config::get('deedel_variables.website_rent_url');
    }


//User Account    
    function account_url(){
        return Config::get('deedel_variables.account_url');
    }
    function account_messages_url(){
        return Config::get('deedel_variables.account_messages_url');
    }
    function account_network_url(){
        return Config::get('deedel_variables.account_network_url');
    }
    function account_wallet_url(){
        return Config::get('deedel_variables.account_wallet_url');      
    }
    function account_wallet_add_url(){
        return Config::get('deedel_variables.account_wallet_add_url');
    }
    function account_assets_url(){
        return Config::get('deedel_variables.account_assets_url');      
    }
    function account_contents_url(){
        return Config::get('deedel_variables.account_contents_url');      
    }
    function account_contents_blog_url(){
        return Config::get('deedel_variables.account_contents_blog_url');      
    }
    function account_contents_blog_add_url(){
        return Config::get('deedel_variables.account_contents_blog_add_url');      
    }
    function account_contents_pages_url(){
        return Config::get('deedel_variables.account_contents_pages_url');      
    }
    function account_contents_pages_save_url(){
        return Config::get('deedel_variables.account_contents_pages_save_url'); 
    }
    function account_contents_landing_pages_url(){
        return Config::get('deedel_variables.account_contents_landing_pages_url');
    }
    function account_contents_emails_url(){
        return Config::get('deedel_variables.account_contents_emails_url');      
    }
    function account_contents_forms_url(){
        return Config::get('deedel_variables.account_contents_forms_url');      
    }
    function account_contents_panels_url(){
        return Config::get('deedel_variables.account_contents_panels_url');      
    }
    function account_rewards_url(){
        return Config::get('deedel_variables.account_rewards_url');      
    }
    function account_orders_url(){
        return Config::get('deedel_variables.account_orders_url');      
    }
    function account_contests_url(){
        return Config::get('deedel_variables.account_contests_url');      
    }
    function account_ads_url(){
        return Config::get('deedel_variables.account_ads_url');      
    }
    function account_premium_url(){
        return Config::get('deedel_variables.account_premium_url');      
    }
    function account_affiliate_url(){
        return Config::get('deedel_variables.account_affiliate_url');      
    }
    function account_affiliate_banners_url(){
        return Config::get('deedel_variables.account_affiliate_banners_url');      
    }
    function account_affiliate_referrals_url(){
        return Config::get('deedel_variables.account_affiliate_referrals_url');      
    }
    function account_affiliate_earnings_url(){
        return Config::get('deedel_variables.account_affiliate_earnings_url');      
    }
    function logout_url(){
        return Config::get('deedel_variables.logout_url');
    }

    function network_admin_url(){
        return Config::get('deedel_variables.network_admin_url');
    }

?>