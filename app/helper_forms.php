<?php
    function form_input($type,$name,$value,$placeholder,$class,$label){
?>
	<div class="form-group">
		<label for="<?php echo $name; ?>"><?php echo $label; ?></label>
		<input type="<?php echo $type; ?>" class="<?php echo $class; ?>" id="<?php echo $name ?>" name="<?php echo $name; ?>" placeholder="<?php echo $placeholder; ?>" value="<?php echo $value; ?>">
	</div>
<?php
    }


    function form_texteditor($name,$value,$placeholder,$class,$label){
?>
	<div class="form-group">
		<label for="<?php echo $name; ?>"><?php echo $label; ?></label>
		<textarea class="<?php echo $class; ?>" name="<?php echo $name; ?>" placeholder="<?php echo $placeholder; ?>"><?php echo $value; ?></textarea>
	</div>
<?php
    }

    function form_submit($text){
?>
	<button type="submit" class="btn btn-primary"><?php echo $text; ?></button>
<?php } 
    function form_publish($text){
?>
	<button type="button" class="btn btn-primary"><?php echo $text; ?></button>
<?php } 
	function form_del_submit($text){
?>
	<button type="submit" class="btn btn-danger delete"><?php echo $text; ?></button>
<?php } 
	
?>
