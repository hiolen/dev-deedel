<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_SocialAccount extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'social_account';

    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    /**
     * Get the user that owns the Socail Account.
     */
    public function user()
    {
        return $this->belongsTo('App\AllDeedel_User', 'user_id');
    }
}
