<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingDeedel_Coupon extends Model
{
    protected $connection = "marketingdeedel";
    protected $table = "coupons";
    protected $fillable = ['types','coinsType_id', 'amount', 'coupon_code', 'pin_code', 'status'];

    public function coinsType(){
    	return $this->belongsTo('App\AllDeedel_CoinsType', 'coinsType_id');
    }
}
