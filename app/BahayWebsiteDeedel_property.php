<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahayWebsiteDeedel_Property extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'bahaywebsitedeedel';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;    

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'property';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['name', 'slug', 'desc', 'types'];

    /**
     * Get the meta that owns the property.
     */
    public function meta()
    {
        return $this->belongsTo('App\BahayWebsiteDeedel_Meta', 'meta_id');
    }
}
