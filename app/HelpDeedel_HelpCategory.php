<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpDeedel_HelpCategory extends Model
{
    protected $connection = 'helpdeedel';
	protected $table = 'helpcategory';

    public function help(){
    	return $this->hasMany(HelpDeedel_Help::class,'helpCategoryID' );
    }
}
