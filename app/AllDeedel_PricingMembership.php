<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_PricingMembership extends Model
{
    protected $table = 'pricing_membership';

    public function website(){
    	return $this->belongsTo('App\AllDeedel_Website', 'website_id');
    }
}
