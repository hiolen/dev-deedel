<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_PricingActivities extends Model
{
    protected $table = 'pricing_activities';

    public function coinsType(){
    	return $this->belongsTo('App\AllDeedel_CoinsType', 'coins_type_id');
    }

}
