<?php
	function deedel_js_url(){
		return deedel_url().'/assets/js/deedel';
	}
	function deedel_css_url(){
		return deedel_url().'/assets/css/deedel';
	}
//Admin CSS
	function deedel_bootstrap_css(){
		echo '<link rel="stylesheet" href="'.deedel_css_url().'/bootstrap.min.css">';
	}
    function deedel_css(){
        echo '<link rel="stylesheet" href="'.deedel_css_url().'/style.css">';
    }
//JS
    function deedel_jquery_js(){
    	echo '<script src="'.deedel_js_url().'/jquery.min.js"></script>';
    }
    function deedel_bootstrap_js(){
    	echo '<script src="'.deedel_js_url().'/bootstrap.min.js"></script>';
    }
    function deedel_jquery_validate_js(){
    	echo '
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>';
    }
    function deedel_footer_tinymce_js(){
    	echo '<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>';
    }
    function deedel_footer_tinymce_1_js(){
    	echo '<script src="'.deedel_js_url().'/admin_footer_tinymce_1.js"></script>';
    }
?>