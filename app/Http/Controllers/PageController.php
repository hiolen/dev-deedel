<?php

namespace App\Http\Controllers;

use Exception;
use Mail;

use Laracasts\Flash\Flash; 
use Session; 
use Cart;
use Omnipay\Omnipay; 
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect; 
use Illuminate\Contracts\Routing\ResponseFactory;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use App\MessageDeedel_Contact as Contact;
use App\CMSDeedel_Post as Post;
use App\CMSDeedel_Pages as Pages;
use App\CMSDeedel_Membership as UserMembership;
use App\CMSDeedel_Terms as Terms;
use App\AllDeedel_Website as Network;
use App\AllDeedel_PricingMembership as Membership;
use Response;

class PageController extends Controller
{
    protected $response;
    private $_api_context;

    public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
        $this->middleware('cors');

        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function pageStatic($slug){
        $page = Pages::where('slug', $slug)->first();
        return view('page.index', compact('page'));
    }
    // public function pageByUser($network_domain,$membership_slug,$membership_name,$page_slug){
    //     $network = Network::where('domain', $network_domain)->first();
    //     $membership = Membership::where('slug', $membership_slug)->first();
    //     $user_membership = UserMembership::where('name', $membership_name)->first();
    //     $page = Page::where('slug', $page_slug)->first();
    //     return view('page.by-user', compact('network','membership','page'));
    // }
    public function index(){
        $features = Network::where('featured', 1)
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();

        return view('network.deedel.index')->with('features', $features);
    }
    public function contact(){
        return view('page.contact');
    }
    public function pageNetwork(){
        $networks = Network::all();

        return view('network.deedel.page-network')->with('networks', $networks);
    }
    public function faq(){
        return view('page.templates.faq');
    }
    public function howto(){
        return view('page.templates.how-to');
    }
    public function privacy(){
        return view('page.templates.privacy');
    }
    public function sendContact(Request $request){
        try {
            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'msg' => $request->message
            );

            Mail::send('emails.contact', $data, function ($message) use ($request) {

                $message->from($request->email, 'Deedel');

                $message->to('rodel.hiolen@gmail.com')->subject($request->subject);

            });

            $contact = new Contact;
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->subject = $request->subject;
            $contact->detail = $request->message;
            $contact->save();

            session()->flash('success_message', 'Your email has been sent successfully');

        }catch (Exception $e) {
            session()->flash('error_message', $e->getMessage());
        }

        $response = array(
            'redirect_url' => url('/contact')
        );

        return $this->response->json($response);
    }
    public function termsDefinitionJson(Request $request){

        $samples = Terms::where('website_id', $request->selectedNetwork)->get();
        
        return Response::json($samples);
    }
    public function termsDefinition(){
        $websites = Network::get();
        $terms = Terms::paginate(3);
        return view('page.templates.terms-definition', compact('terms', 'websites'));
    }
    public function partners(){
        return view('page.partners');
    }
    public function support(){
        return view('support.index');
    }

    //Careers
    public function careers(){
        return view('careers.careers');
    }

    //Pricing
    public function coins(){
        return view('network.deedel.coins');
    }
    
    public function pricing(){
        return view('pricing.pricing');
    }
    public function booklulu(){
        return view('pricing.booklulu');
    }
    public function bookluluIndie(){
        return view('pricing.booklulu-indie');
    }
    public function bookluluProfessional(){
        return view('pricing.booklulu-professional');
    }
    public function bookluluPublisher(){
        return view('pricing.booklulu-publisher');
    }
}
