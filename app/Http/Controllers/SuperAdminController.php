<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Auth;
use DB;
use Excel;
use Carbon\Carbon;
use App\AllDeedel_PricingCoins as Price;
use App\AllDeedel_CoinsType as Coin;
use App\AllDeedel_Website as Website;
use App\AllDeedel_PricingActivities as Activity;
use App\AllDeedel_PricingMembership as Membership;
use App\AllDeedel_Website;
use App\CMSDeedel_Support as Support;
use App\MessageDeedel_Contact as Contact;
use App\MarketingDeedel_Coupon as Coupon;
use App\MarketingDeedel_Rewards as Reward;
use App\CMSDeedel_Terms as Terms;
use App\CMSDeedel_Pages as Pages;
use Response;
class SuperAdminController extends Controller
{
    protected $user;
    protected $response;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ResponseFactory $response)
    {   
        // Middleware 'Auth'     
        $this->middleware('auth');

        // Get the user
        $this->user = Auth::user(); 

        $this->response = $response;
    }


    public function index(){
        return view('admin.index');
    }

    //Pricing
    public function pricing(){
        $coins = Coin::get();
        return view('admin.pricing.index', compact('coins'));
    }
    public function pricingCoins(){
        $coins = Price::orderBy('id','DESC')->paginate(5);
        $coins_type = Coin::get();
        return view('admin.pricing.coins', compact('coins', 'coins_type'));
    }
    public function pricingCoinsAddAuto(Request $request){
        $cpt = Coin::find($request->selectedCoinType);
        return Response::json($cpt);
    }
    public function pricingCoinsAdd(Request $request){
        $coins_type = Coin::get();
        return view('admin.pricing.coins-add', compact('coins_type'));
    }
    public function pricingCoinsSave(Request $request){
        if ($request->isMethod('post')){
            $pricing_coin = new Price;
            $pricing_coin->coins_type_id = $request->coins_type_id;
            $pricing_coin->name = $request->name;
            $pricing_coin->desc = $request->desc;
            $pricing_coin->coins_value = $request->coins_value;
            $pricing_coin->price = $request->price;
            $pricing_coin->discount_type = $request->discount_type;
            $pricing_coin->discount_value = $request->discount_value;
            $pricing_coin->save();
            $ct = Coin::first();
            session()->flash('success_message', meta_save($pricing_coin->name));

            $response = array(
                'request' => $request->parent,
                'message' => 'Added coin type successful!',
                'redirect_url' => url(super_admin_pricing_coins_url())
            );
            return $this->response->json($response);
        }
    }
    public function pricingCoinsType(){
        $coins = Coin::get();
        return view('admin.pricing.coins-type', compact('coins'));
    }
    public function pricingCoinsSet(){
        return view('admin.pricing.coins-set');
    }
    public function pricingCoinsSetSave(Request $request){
        if ($request->isMethod('post')){
            $coin_type = new Coin;
            $coin_type->name = $request->name;
            $coin_type->desc = $request->desc;
            $coin_type->price = $request->price;
            $coin_type->save();

            session()->flash('success_message', meta_save($coin_type->name));

            $response = array(
                'request' => $request->parent,
                'message' => 'Added coin type successful!',
                'redirect_url' => url(super_admin_pricing_url())
            );
            return $this->response->json($response);
        }
    }

    public function editPricingCoins($id) {
        $coin = Coin::find($id);

        return $this->response->json($coin);
    }

    public function updatePricingCoins(Request $request, $id) {
        $coin_type = Coin::find($id);

        $coin_type->name = $request->name;
        $coin_type->desc = $request->desc;
        $coin_type->price = $request->price;
        $coin_type->save();

        $response = array(
            'coin' => $coin_type,
            'redirect_url' => 'coins'
        );

        return $this->response->json($response);
    }

    public function destroyPricingCoins(Request $request) {
        DB::table("pricing_coins")->where('id',$request->id)->delete();
        
        return response()->json ($request->id);
    }

    public function pricingMembership(){
        $members = Membership::get();
        $websites = Website::get();
        return view('admin.pricing.membership', compact('members', 'websites'));
    }
    // public function pricingMembershipAdd(){
    //     $websites = Website::get();
    //     return view('admin.pricing.membership-add', compact('websites'));
    // }
    public function pricingMembershipSave(Request $request){
        if ($request->isMethod('post')) {
            $member = new Membership;
            $member->name = $request->name;
            $member->name = $request->name;
            if($request->slug)
                $member->slug = str_slug($request->slug);
            else
                $member->slug = str_slug($request->name);
            $member->desc = $request->desc;
            $member->website_id = $request->website_id;
            $member->price = $request->price;
            $member->discount = $request->discount;
            $member->pricing_type = $request->pricing_type;
            $member->save();

            session()->flash('success_message', meta_save($member->name));
            $response = array(
                'request' => $request->parent,
                'message' => 'Added coin type successful!',
                'redirect_url' => url(super_admin_pricing_membership_url())
            );
            return $this->response->json($response);
        }
    }

    public function editMembership($id){
        $membership = Membership::find($id);

        return $this->response->json($membership);
    }

    public function updateMembership(Request $request, $id){
        $member = Membership::find($id);
        $member->name = $request->name;
        $member->name = $request->name;
        if($request->slug)
            $member->slug = str_slug($request->slug);
        else
            $member->slug = str_slug($request->name);
        $member->desc = $request->desc;
        $member->website_id = $request->website_id;
        $member->price = $request->price;
        $member->discount = $request->discount;
        $member->pricing_type = $request->pricing_type;
        $member->save();

        session()->flash('success_message', meta_update($member->name));
        $response = array(
            'request' => $request->parent,
            'message' => 'Added coin type successful!',
            'redirect_url' => url(super_admin_pricing_membership_url())
        );
        return $this->response->json($response);
    }

    public function destroyMembership(Request $request){
        if ($request->isMethod('post')) {
            Membership::find($request->id)->delete();

            return response ()->json ();
        } 
    }

    public function pricingActivities(Request $request){
        $price_acts = Activity::orderBy('id','ASC')->paginate(5);
        $coins_type = Coin::get();
        return view('admin.pricing.activities', compact('price_acts', 'coins_type'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function pricingActivitiesAdd(){
        $price_acts = Activity::get();
        $coins_type = Coin::get();
        return view('admin.pricing.activities-add', compact('price_acts', 'coins_type'));
    }

    public function pricingActivitiesSave(Request $request){
        if ($request->isMethod('post')){
            $activities = new Activity;
            $activities->name = $request->name;
            $activities->activity_code = $request->activity_code;
            $activities->desc = $request->desc;
            $activities->coins_type_id = $request->coins_type_id;
            $activities->amount = $request->amount;
            $activities->pricing_type = $request->pricing_type;
            $activities->save();

            session()->flash('success_message', meta_save($activities->name));

            $response = array(
                'request' => $request->parent,
                'message' => 'Added coin type successful!',
                'redirect_url' => url(super_admin_pricing_activities_url())
            );
            return $this->response->json($response);
        }
    }

    public function editActivities($id) {
        $activity = Activity::find($id);

        return $this->response->json($activity);
    }

    public function updateActivities(Request $request, $id) {
        $activities = Activity::find($id);

        $activities->name = $request->name;
        $activities->activity_code = $request->activity_code;
        $activities->desc = $request->desc;
        $activities->coins_type_id = $request->coins_type_id;
        $activities->amount = $request->amount;
        $activities->pricing_type = $request->pricing_type;
        $activities->save();

        $response = array(
            'request' => $request->parent,
            'message' => 'Added coin type successful!',
            'redirect_url' => url(super_admin_pricing_activities_url())
        );
        return $this->response->json($response);
    }

    public function destroyActivities(Request $request) {
        Activity::find($request->id)->delete();
        
        return response()->json ($request->id);
    }

    //Network
    public function network(){
        $websites = Website::get();
        return view('admin.network.network', compact('websites'));
    }
    public function networkAdd(){
        return view('admin.network.network-add');
    }
    public function networkSave(Request $request){
        if ($request->isMethod('post')){
            $meta = new Website;
            $meta->name = $request->name;
            if($request->slug)
                $meta->slug = str_slug($request->slug);
            else
                $meta->slug = str_slug($request->name);
            $meta->details = $request->details;
            $meta->domain = $request->domain;
            $meta->type = $request->type;
            $meta->image = $request->image;
            $meta->save();

            session()->flash('success_message', meta_save($meta->name));

            $response = array(
                'request' => $request->parent,
                'message' => 'Added meta successful!',
                'redirect_url' => url(super_admin_network_url())
            );
            return $this->response->json($response);
        }
    }

    public function editNetwork($id){
        $website = Website::find($id);

        return $this->response->json($website);
    }

    public function updateNetwork(Request $request, $id) {
        $website = Website::find($id);
        $website->name = $request->name;
        if($request->slug)
            $website->slug = str_slug($request->slug);
        else
            $website->slug = str_slug($request->name);
        $website->details = $request->details;
        $website->domain = $request->domain;
        $website->type = $request->type;
        $website->image = $request->image;
        $website->save();

        $response = array(
            'message' => 'Added coin type successful!',
            'redirect_url' => 'network'
        );

        return $this->response->json($response);
    }

    public function networkDashboard(){
        $network = Website::get();
        return view('admin.network.network-dashboard', compact('network'));
    }
    public function networkDashboardItem($id){
        $network = Website::find($id);
        return view('admin.network.network-dashboard-item', compact('network'));
    }

    public function destroyNetwork(Request $request) {
        Website::find($request->id)->delete();
        
        return response()->json ($request->id);
    }

    //Rewards
    public function rewards(){
        $rewards = Reward::get();
        return view('admin.rewards.index', compact('rewards'));
    }
    public function rewardsGifts(){
        return view('admin.rewards.gifts');
    }
    public function rewardsPoints(){
        return view('admin.rewards.points');
    }
    public function rewardsCoins(){
        return view('admin.rewards.coins');
    }

    //Coupons
    public function coupons(Request $request){
        $websites = Website::get();
        $coins_type = Coin::get();
        $pin_code = $this->generatePinCode();
        $coupon_code = $this->generateCouponCode();

        $coupons = Coupon::orderBy('id','ASC')->paginate(5);
        return view('admin.coupons.coupons', compact('coupons','websites', 'pin_code', 'coupon_code', 'coins_type'))
            ->with('i', ($request->input('page', 1) - 1) * 5);;
    }
    public function couponsAdd(){
        $websites = Website::get();
        $coins_type = Coin::get();
        $coupons = Coupon::get();
        $pin_code = $this->generatePinCode();
        $coupon_code = $this->generateCouponCode();
        return view('admin.coupons.coupons-add', compact('coupons','websites', 'pin_code','coupon_code', 'coins_type'));
    }
    public function couponsUpload(){
        return view('admin.coupons.coupons-upload');
    }
    public function importCoupon(Request $request) {
        if ($request->isMethod('post')){
            try {
                if($request->hasFile('import_file')){
                    $path = $request->import_file->getRealPath();
                    $data = Excel::load($path, function($reader) {
                    })->get();
                    if(!empty($data) && $data->count()){
                        foreach ($data as $key => $item) {
                            // Insert Coupons in DB
                            $coin = new Coupon;
                            $coin->types = $item->types;
                            $coin->value = $item->value;
                            $coin->amount = $item->amount;
                            $coin->coupon_code = $item->coupon_code;
                            $coin->pin_code = $item->pin_code;
                            $coin->status = $item->status;
                            $coin->created_at = Carbon::now();
                            $coin->updated_at = Carbon::now();
                            $coin->save();
                        }
                        return back()->with('success_message','Insert Record successfully.');
                    }
                }      
            } catch (Exception $e) {
                return back()->with('error_message',$e->getMessage());
            }
        }
    }
    public function downloadCoupon($type) {
        $data = Coupon::get()->toArray();
        return Excel::create('Coupons List', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    public function generateCouponCode(){
        $coupon_array = array_merge(range('a', 'z'),range('A', 'Z'), range(0, 9));
        $coupon_code = "";
            for($i = 0; $i < 16; $i++) {
                $coupon_code .= $coupon_array[rand(0, (count($coupon_array) - 1))];
            }  
        $check = Coupon::where('coupon_code' , $coupon_code)->first();
        if($check['coupon_code'] == $coupon_code)
        {
            $coupon_code2 ="";
            for($i = 0; $i < 16; $i++) {
                $coupon_code2 .= $coupon_array[rand(0, (count($coupon_array) - 1))];
            } 
            return $coupon_code2;  
        }
        else{
            return $coupon_code;
        }   
    }
    public function generatePinCode(){
        $pin_array = range(0,6);
        $pin_code = "";
            for($i = 0; $i < 6; $i++) {
                $pin_code .= $pin_array[rand(0, (count($pin_array) - 1))];
        }
        return $pin_code;
    }
    public function couponsSave(Request $request){
        if ($request->isMethod('post')){
            $coupon = new Coupon;
            $coupon->types = $request->types;
            $coupon->value = $request->value;
            $coupon->amount = $request->amount;
            $coupon->coupon_code = $request->coupon_code;
            $coupon->pin_code = $request->pin_code;
            $coupon->status = $request->status;
            $coupon->save();

            session()->flash('success_message', meta_save($coupon->types));

            $response = array(
                'request' => $request->parent,
                'message' => 'Added meta successful!',
                'redirect_url' => url(super_admin_coupons_url())
            );
            return $this->response->json($response);
        }
    }

    //Messages
    public function messages(){
        $messages = Contact::get();
        return view('admin.messages.index' , compact('messages'));
    }
    public function messagesChat(){
        return view('admin.messages.chat');
    }
    public function messagesCompose(){
        return view('admin.messages.compose');
    }
    public function messagesContacts(){
        return view('admin.messages.contact');
    }
    public function messagesContactsUser(){
        return view('admin.messages.contact-user');
    }
    public function messagesInbox(){
        $messages = Contact::get();
        return view('admin.messages.inbox' , compact('messages'));
    }
    public function messagesItem($id){
        $id = Contact::first();
        return view('admin.messages.message-item', compact('id'));
    }
    public function messagesReports(){
        return view('admin.messages.reports');
    }

    public function messagesSupport(Request $request){
        $supports = Support::orderBy('id','DESC')->paginate(10);
        return view('admin.messages.support',compact('supports'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function addSupport(Request $request) {
        if ($request->isMethod('post')){

            $support = new Support;
            $support->title = $request->subject;
            if($request->slug)
                $support->slug = str_slug($request->slug);
            else
                $support->slug = str_slug($request->name);
            $support->status = 'pending';
            $support->content = $request->content;
            $support->save();

            $response = array(
                'message' => 'Added coin type successful!',
                'redirect_url' => 'support'
            );

            return $this->response->json($response);
        }
    }

    public function messagesSupportTicketItem(){
        return view('admin.messages.support-ticket');
    }
    public function messagesSupportTicketAdd(){
        return view('admin.messages.support-ticket-add');
    }

    //Content
    public function content(){
        return view('admin.content.index');
    }
    public function contentBlogTagAdd(){
        return view('admin.content.blog-tag-add');
    }
    public function contentSupport(){
        $websites = Website::get();
        $supports = Support::get();
        return view('admin.content.support', compact('supports', 'websites'));
    }
    public function contentSupportAdd(){
        return view('admin.content.support-add');
    }
    public function contentSupportCategory(){
        return view('admin.content.support-category');
    }
    public function contentSupportCategoryAdd(){
        return view('admin.content.support-category-add');
    }
    public function contentTerms(){
        $terms = Terms::paginate(10);
        $websites = Website::get();
        return view('admin.content.terms', compact('terms', 'websites'));
    }
    public function contentTermsAdd(){
        $websites = Website::get();
        return view('admin.content.terms-add', compact('websites'));
    }
    public function contentTermsSave(Request $request){
        if($request->isMethod('post')){
            $terms = new Terms;
            $terms->name = $request->name;
            $terms->desc = $request->desc;
            $terms->website_id = $request->website_id;
            $terms->save();

            return redirect(super_admin_content_terms_url() );
        }
    }
    public function contentDestroyTerms(Request $request){
        if ($request->isMethod('post')) {
            Terms::find($request->id)->delete();
            return response ()->json ();
        } 
    }
    public function editTerms($id){
        $term = Terms::find($id);
        return $this->response->json($term);
    }

    public function updateTerms(Request $request, $id){
        $term = Terms::find($id);
        $term->name = $request->name;
        $term->website_id = $request->website_id;
        $term->desc = $request->desc;
        $term->status = $request->status;
        $term->save();

        session()->flash('success_message', meta_update($term->name));
        $response = array(
            'request' => $request->parent,
            'message' => 'Added term type successful!',
            'redirect_url' => url(super_admin_content_terms_url())
        );
        return $this->response->json($response);
    }
    public function contentBlog(){
        return view('admin.content.blog');
    }
    public function contentBlogAdd(){
        return view('admin.content.blog-add');
    }
    public function contentBlogCategory(){
        return view('admin.content.blog-category');
    }
    public function contentBlogCategoryAdd(){
        return view('admin.content.blog-category-add');
    }
    public function contentBlogTag(){
        return view('admin.content.blog-tag');
    }
     public function contentPages(){
        $pages = Pages::get();
        $websites = AllDeedel_Website::get();
        return view('admin.content.pages', compact('pages', 'websites'));
    }
    public function contentPagesJson(Request $request){
        // $pages = Pages::where('website_id', $request->selectedNetwork)->get();
        // $pages = DB::table('cmsdeedel.pages as db1')
        // ->select('db1.*')
        // ->join('alldeedel.website as db2', 'db1.website_id', '=', 'db2.id')
        // ->where('db2.id', $request->selectedNetwork)
        // ->get();
        $pages = DB::table('alldeedel.website as db1')->join('cmsdeedel.pages as db2', 'db2.website_id', '=', 'db1.id')->join('alldeedel.users as db3', 'db3.id', "=", "db2.author")->selectRaw('db1.name as webName, db3.name as authorName, db2.title as title, db2.status as status, db2.date_added as date_added')->where('db1.id', $request->selectedNetwork)->get();

        return Response::json($pages);
    }
    public function contentPagesAdd(){
        $websites = AllDeedel_Website::get();

        return view('admin.content.pages-add', compact('websites'));
    }
    public function contentPagesSave(Request $request){
        if($request->isMethod('post')){
            $page = new Pages;
            $page->title = $request->title;
            if($request->slug)
                $page->slug = str_slug($request->slug);
            else
                $page->slug = str_slug($request->title);
            $page->content = $request->content;
            $page->status = $request->status;
            $page->template = $request->template;
            $page->website_id = $request->website_id;
            $page->author = $this->user->id;
            $page->date_added = Carbon::now();
            $page->save();

            return redirect(super_admin_content_pages_url());
        }
    }
    public function contentLandingPages(){
        return view('admin.content.landing-pages');
    }
    public function contentEmails(){
        return view('admin.content.emails');
    }
    public function contentForms(){
        return view('admin.content.forms.index');
    }
    public function contentFormsAdd(){
        return view('admin.content.forms.add');
    }
    public function contentFormsItem(){
        return view('admin.content.forms.item');
    }
    public function contentPanels(){
        return view('admin.content.panels');
    }
}