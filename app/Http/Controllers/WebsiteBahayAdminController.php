<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\BahayWebsiteDeedel_Meta as Meta;

use Response;

class WebsiteBahayAdminController extends Controller
{    
    protected $response;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ResponseFactory $response)
    {
        $this->middleware('auth');
        $this->response = $response;
    }

    public function index(){
        return view('websites.bahay.admin.index', compact('validator'));
    } 
    public function meta(){
        
        // get parent for each meta [categories, tags, locations]
        $catParents = Meta::where('types', 'category')->where('parent', 0)->get();
        $tagParents = Meta::where('types', 'tag')->where('parent', 0)->get();
        $locParents = Meta::where('types', 'location')->where('parent', 0)->get();

        // get all categories
        $categories = Meta::where('types', 'category')->get();
        // get all tags
        $tags = Meta::where('types', 'tag')->get();
        // get all locations
        $locations = Meta::where('types', 'location')->get();
 
        return view('websites.bahay.admin.meta.index', compact('categories', 'tags', 'locations', 'catParents', 'tagParents', 'locParents'));
    }

    public function addMeta(Request $request) {
        if ($request->isMethod('post')){

            $meta = new Meta;
            $meta->name = $request->name;
            if($request->slug)
                $meta->slug = str_slug($request->slug);
            else
                $meta->slug = str_slug($request->name);
            $meta->desc = $request->description;
            $meta->parent = $request->parent;
            $meta->types = $request->type;
            $meta->save();

            session()->flash('success_message', meta_update($meta->name));

            $response = array(
                'request' => $request->id,
                'redirect_url' => url(website_bahay_admin_meta_url())
            );
            
            return $this->response->json($response);
        }
    }

    public function editMeta(Request $request) {
        if ($request->isMethod('post')){
            $meta = Meta::find($request->id);
            $meta->name = $request->name;
            if($request->slug)
                $meta->slug = str_slug($request->slug);
            else
                $meta->slug = str_slug($request->name);
            $meta->desc = $request->description;
            $meta->parent = $request->parent;
            $meta->save();
        }

        session()->flash('success_message', meta_update($meta->name));

        $response = array(
            'request' => $request->id,
            'redirect_url' => url(website_bahay_admin_meta_url())
        );
        return $this->response->json($response);
    }

    public function deleteMeta(Request $request) {        

        Meta::find($request->id)->delete();

        return response ()->json ();
    }
}