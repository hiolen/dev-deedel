<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\BahayWebsiteDeedel_property;
use JsValidator;
use DB;

class WebsiteBahayUserController extends Controller
{
    /**
     * Define your validation rules in a property in 
     * the controller to reuse the rules.
     */
    // protected $validationRules = ['title' => 'required|max:255'];
    protected $validationRules = ['name' => 'required'];

    protected $response;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ResponseFactory $response)
    {
        $this->middleware('auth');
        $this->response = $response;
    }

    public function account(){
        return view('websites.bahay.account');
    }
    public function settings(){
        return view('websites.bahay.settings');
    }
    public function profile(){
        return view('websites.bahay.profile');
    }
    public function editProfile(){
        return view('websites.bahay.profileEdit');
    }

    public function createProperty(){
        $validator = JsValidator::make($this->validationRules);
        return view('websites.bahay.propertyAdd', compact('validator'));
    }

    public function addProperty(Request $request){
        if ($request->isMethod('post')){
            // $property = new 
            $link = implode(", ", $request->link);
            $response = array(
                'title' => str_slug($request->title),
                'implode' => explode(", ", $link),
                'message' => 'Added property successful!',
                'redirect_url' => url('/buy-coins')
            );
            return $this->response->json($response);
        }
    }

    public function editProperty(){
        return view('websites.bahay.propertyEdit');
    }
    public function showProperty($metaSlug, $propertySlug) {
        $property = BahayWebsiteDeedel_property::where('slug', $propertySlug)->first();
        return view('websites.bahay.property-single',compact('property'));
    }
}