<?php
// https://github.com/drehimself/laravel-shopping-cart-example
namespace App\Http\Controllers;

use Cart;
use Validator;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Redirect; 
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;

class CartController extends Controller
{
	protected $response;

	public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.cart');
    }
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,20'
        ]);
         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return response()->json(['success' => false]);
         }
        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return redirect('cart')->withSuccessMessage('Item has been removed!');
    }
    /**
     * Remove the resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function emptyCart()
    {
        Cart::destroy();
        return redirect('cart')->withSuccessMessage('Your cart his been cleared!');
    }
    /**
     * Switch item from shopping cart to wishlist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToWishlist(Request $request, $id)
    {
    	if ($request->isMethod('post')){
            // add new item to cart
            $item = Cart::instance('coin')->get($id);
            Cart::instance('coin')->remove($id);

	        // if (Cart::instance('wishlist')->search(function ($cartItem, $rowId) { return $cartItem->id === $rowId; })) {
	        //     return redirect('cart')->withSuccessMessage('Item is already in your Wishlist!');
	        // }

	        Cart::instance('wishlist')->add(array('id' => $item->rowId, 'name' => $item->name, 'qty' => 1, 'price' => $item->price));
	        return redirect('cart')->withSuccessMessage('Item has been moved to your Wishlist!');
        }
        
    }

    public function cart(Request $request) {
        if ($request->isMethod('post')){
            // add new item to cart
            $token = $request->_value . $request->_type . $request->_token;
            if (Cart::search(['id' => $token])) {
	        	$response = array(
		            'token' => $token,
		            'message' => 'Item is already in your cart!',
		            'redirect_url' => url('/coins')
		        );
                session()->flash('warning_message', 'Item is already in your cart!');
		        return $this->response->json($response);
	        }
           	
            Cart::add($token, $request->_name, 1, $request->_price, [$request->_value, $request->_type, $request->_category]);

            $response = array(
                'redirect_url' => url('/coins')
            );
            session()->flash('success_message', 'Item has been moved to you cart!');
            return $this->response->json($response);
        }
    }
}
