<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Exception;
use Mail;
use Session;

use App\Http\Requests;
use Illuminate\Contracts\Routing\ResponseFactory;
use Response;
use Auth;
use Carbon\Carbon;
use DB;
use App\AllDeedel_User as User;
use App\Role as Role;
use App\AllDeedel_CoinsType as CoinsType;
use App\AllDeedel_Coin as Coin;
use App\AllDeedel_PricingCoins as PricingCoins;
use App\AllDeedel_Website as Website;
use App\AllDeedel_Wallet as Wallet;
use App\AllDeedel_Payment as Pay;
use App\CMSDeedel_Blog as Blog;
use App\CMSDeedel_Pages as Pages;
use App\CMSDeedel_Meta as Meta;
use App\CMSDeedel_Post as Post;
use App\MarketingDeedel_Coupon as Coupon;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class AccountController extends Controller
{
    private $_api_context;
    protected $user;
    protected $response;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ResponseFactory $response)
    {   
        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);

        // Middleware 'Auth'     
        $this->middleware('auth');

        // Get the user
        $this->user = Auth::user(); 

        $this->response = $response;
    }


    public function index(){
        return view('account.index');
    }

    public function referrals() {
        return view('account.referrals');
    }

    public function profile(){
        $profile = Auth::user();
        
        return view('account.profile', compact('profile'));
    }
    public function settings(){
        return view('account.settings');
    }
    public function network(){
        
        $networks = $this->user->websites;

        return view('account.user.network', compact('networks', 'websites'));
    }
    public function messages(){
        return view('account.user.messages');
    }
    public function assets(){
        return view('account.user.assets');
    }
    public function contents(){
        return view('account.user.contents');
    }
    public function contentsBlog(){
        $websites = Website::orderBy('created_at', 'desc')->get();

        $blogs = Blog::where('author', Auth::user()->id)->paginate(5);
        
        return view('account.user.contents.blog.index', compact('websites', 'blogs'));
    }

    public function contentsBlogAdd(Request $request){
        if ($request->ajax()){
            $post = new Blog;
            $post->title = $request->input('title');
            $post->slug = str_slug($request->input('title'));
            $post->content = $request->input('description');
            $post->author = $this->user->id;
            if ($request->input('featured') == 'on'){
                $post->featured = 1;
            }else{
                $post->featured = 0;
            }
            $post->website_id = $request->input('website_id');
            $post->status = 'draft';
            $post->template = 'blog';
            $post->save();

            $response = array(
                'post' => $post,
                'message' => 'You\'ve been added a new post!',
                'domain' => $post->website->domain
            );

            return $this->response->json($response);
        }
    }

    public function contentsBlogEdit(Request $request){
        if ($request->ajax()){
            $blog = Blog::find($request->id);

            $response = array(
                'blog' => $blog
            );

            return $this->response->json($response);
        }
    }

    public function contentsBlogUpdate(Request $request){
        if ($request->ajax()){
            $post = Blog::find($request->id);
            $post->title = $request->input('title');
            $post->slug = str_slug($request->input('title'));
            $post->content = $request->input('description');
            $post->author = $this->user->id;
            if ($request->input('featured') == 'on'){
                $post->featured = 1;
            }else{
                $post->featured = 0;
            }
            $post->website_id = $request->input('website_id');
            $post->status = 'draft';
            $post->template = 'blog';
            $post->save();

            $response = array(
                'post' => $post,
                'domain' => $post->website->domain,
                'message' => 'Edited Successfully!',
                'request' => $request->title
            );

            return $this->response->json($response);
        }
    }

    public function contentsBlogDestroy(Request $request){
        if ($request->ajax()){
            Blog::destroy($request->id);

            $response = array(
                'message' => 'You\'ve deleted a post!',
                'id' => $request->id
            );

            return $this->response->json($response);
        }
    }

    public function contentsBlogStatus(Request $request){
        if ($request->ajax()){
            $blog = Blog::find($request->id);
            $blog->status = $request->val;
            $blog->save();

            $response = array(
                'message' => 'You\'ve change a status!',
                'blog' => $blog
            );

            return $this->response->json($response);
        }
    }

    public function contentsBlogFeatured(Request $request){
        if ($request->ajax()){
            $blog = Blog::find($request->id);
            $blog->featured = $request->val;
            $blog->save();

            $response = array(
                'message' => 'You\'ve change featured!',
                'blog' => $blog
            );

            return $this->response->json($response);
        }
    }

    // public function contentsBlogPosts($id){
    //     $user = $this->user;
    //     $posts = Post::where('blog_id',$id)->get();
    //     $meta_tags = Meta::where('meta_type', 'tag')->get();
    //     $meta_cats = Meta::where('meta_type', 'category')->get();

    //     return view('account.user.contents.blog.posts', compact('meta_tags', 'meta_cats','posts', 'user'));
    // }
    // public function contentsBlogPostsAdd(){
    //     return view('account.user.contents.blog.posts-add');
    // }
    // public function contentsBlogMeta($id){
    //     // $posts = Post::pluck('meta_id');
    //     // $user = $this->user;
    //     // $post_length = count($posts);
    //     // $poste = array();
    //     // for( $i = 0; $i < $post_length; $i++){
    //     //     $poste[] = explode(",", $post_length[$i]);
    //     // }
    //     // $meta_length = count($hala);

    //     // for( $i = 0; $i < $meta_length; $i++){
    //     //     $samp = Meta::where('id', $i)->first();
    //     // }
    //     // $samp = array();
    //     // foreach($hala as $h){
    //     //    dd($h);
    //     // }

    //         // foreach(explode(',', $poset->meta_id) as $z){
    //         //     $samp = Meta::where('id', $z)->find($z);
    //         //      dd($samp);
    //         // }
    //     $post = Post::findOrFail($id);
    //     $post_metas = explode(',', $post->meta_id);
    //     $metas = Meta::get();

    //     return view('account.user.contents.blog.meta', compact('post_metas', 'metas'));
    // }
    // public function contentsBlogMetaAdd(){
    //     return view('account.user.contents.blog.meta-add');
    // }
    // public function contentsBlogMetaItem(){
    //     return view('account.user.contents.blog.meta-item');
    // }
    public function contentPages(){

        $pages = Pages::orderBy('created_at', 'desc')->get();
        $websites = Website::orderBy('created_at', 'desc')->get();
        $roles = Role::orderBy('created_at', 'desc')->get();
        $user = Auth::user();

        return view('account.user.contents.pages.index', compact('pages', 'websites', 'roles', 'user'));
    }

    public function contentPagesAdd(Request $request){
        if ($request->ajax()){
            $page = new Pages;
            $page->title = $request->input('title');
            $page->slug = str_slug($request->input('title'));
            $page->content = $request->input('description');
            $page->website_id = $request->input('network_id');
            $page->role_id = $request->input('role_id');
            $page->status = 'pending';
            $page->save();

            $response = array(
                'page' => $page,
                'message' => 'You\'ve been added a new page!',
                'domain' => $page->website->domain,
                'role' => $page->role->name,
            );

            return $this->response->json($response);
        }
    }

    public function contentPagesEdit(Request $request){
        if ($request->ajax()){
            $page = Pages::find($request->id);

            $response = array(
                'page' => $page
            );

            return $this->response->json($response);
        }
    }

    public function contentPagesUpdate(Request $request){
        if ($request->ajax()){
            $page = Pages::find($request->id);
            $page->title = $request->title;
            $page->slug = str_slug($request->title);
            $page->content = $request->description;
            $page->website_id = $request->network_id;
            $page->role_id = $request->role_id;
            $page->author = Auth::user()->id;
            $page->status = 'draft';
            $page->save();

            $response = array(
                'page' => $page,
                'message' => 'You\'ve been added a new page!',
                'domain' => $page->website->domain,
                'role' => $page->role->name,
            );

            return $this->response->json($response);
        }
    }

    public function contentPagesDestroy(Request $request){
        if ($request->ajax()){
            Pages::destroy($request->id);

            $response = array(
                'message' => 'You\'ve deleted a page!',
                'id' => $request->id
            );

            return $this->response->json($response);
        }
    }

    public function contentPagesStatus(Request $request){
        if ($request->ajax()){
            $page = Pages::find($request->id);
            $page->status = $request->val;
            $page->save();

            $response = array(
                'message' => 'You\'ve change a status!',
                'page' => $page
            );

            return $this->response->json($response);
        }
    }

    public function contentPagesWebsite(Request $request){
        if ($request->ajax()){
            $page = Pages::find($request->id);
            $page->website_id = $request->val;
            $page->save();

            $response = array(
                'message' => 'You\'ve change a website!',
                'page' => $page
            );

            return $this->response->json($response);
        }
    }

    public function contentPagesRole(Request $request){
        if ($request->ajax()){
            $page = Pages::find($request->id);
            $page->role_id = $request->val;
            $page->save();

            $response = array(
                'message' => 'You\'ve change a role!',
            );

            return $this->response->json($response);
        }
    }

    public function contentsPagesJson(Request $request){
        $pages = DB::table('alldeedel.website as db1')->join('cmsdeedel.pages as db2', 'db2.website_id', '=', 'db1.id')->join('alldeedel.roles as db3', 'db3.id', '=', 'db2.role_id')->selectRaw('db2.title as pageName, db2.slug as pageSlug, db2.content as pageContent, db2.status as pageStatus,db3.display_name as roleName ,db1.name as webName')->where('db2.author', $this->user->id)
            ->where( function ($query) use ($request)  {
                $query->orWhere('db1.id', $request->selectedNetwork)->orWhere('db2.role_id', $request->selectedMember);
            })->get();
        return Response::json($pages);
    }
    
    public function contentsPagesSave(Request $request){
        $page = new Pages;
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->content = $request->content;
        if($request->has('btn-draft')){
            $page->status = 'draft';
        }else if($request->has('btn-publish')){
            $page->status = 'published';
        }
        $page->author = $this->user->id;
        $page->website_id = $request->website_id;
        $page->role_id = $request->role_id;
        $page->save();

        return redirect(account_contents_pages_url());
    }
    public function contentsLandingPages(){
        return view('account.user.contents.landing-pages.index');
    }
    public function contentsLandingPagesAdd(){
        return view('account.user.contents.landing-pages.lp-add');
    }
    public function contentsLandingPagesItem(){
        return view('account.user.contents.landing-pages.lp-item');
    }
    public function contentsEmails(){
        return view('account.user.contents.emails.index');
    }
    public function contentsForms(){
        return view('account.user.contents.forms.index');
    }
    public function contentsPanels(){
        return view('account.user.contents.panels.index');
    }
    public function rewards(){
        return view('account.user.rewards');
    }
    public function rewardsPoints(){
        return view('account.user.rewards.points');
    }
    public function rewardsGifts(){
        return view('account.user.rewards.gifts');
    }
    public function rewardsCoins(){
        return view('account.user.rewards.coins');
    }
    public function rewardsTokens(){
        return view('account.user.rewards.tokens');
    }
    public function orders(){
        return view('account.user.orders');
    }
    public function contests(){
        return view('account.user.contests');
    }
    public function ads(){
        return view('account.user.ads');
    }
    public function adsAdd(){
        return view('account.user.ads-add');
    }
    public function adsType(){
        return view('account.user.ads-type');
    }
    public function adsTypeAdd(){
        return view('account.user.ads-type-add');
    }
    public function affiliate(){
        return view('account.user.affiliate');
    }
    public function affiliateBanners(){
        return view('account.user.affiliate-banners');
    }
    public function affiliateReferrals(){
        return view('account.user.affiliate-referrals');
    }
    public function affiliateEarnings(){
        return view('account.user.affiliate-earnings');
    }
    public function premium(){
        $roles = $this->user->roles;
        $networks = explode(",", $this->user->websites);
        $websites = Website::get();

        return view('account.user.premium', compact('networks', 'websites', 'roles'));
    }
    public function premiumNetworkMembership(){
        return view('account.user.premium.membership');
    }

    public function getWallet(){
        $user = $this->user;
        $coins = CoinsType::orderBy('id', 'asc')->get();
        $coins_pricing = PricingCoins::get();
        return view('account.user.wallet', compact('user','coins','coins_pricing'));
    }

    public function loadCoins(Request $request) {
        if ($request->isMethod('post')){
            $coupon = Coupon::where('coupon_code', $request->code)->where('pin_code', $request->pin)->first();

            if($coupon){
                if($coupon->types == 'coins' AND $coupon->status == 'active') {
                    // Insert Coins to db                    
                    $coin = new Coin;
                    $coin->user_id = $this->user->id;
                    $coin->types = $coupon->coinsType->name;
                    $coin->value = $coupon->amount;
                    $coin->created_at = Carbon::now();
                    $coin->updated_at = Carbon::now();
                    $coin->save();

                    // Update Coupon to db
                    $coupon->status = 'used';
                    $coupon->save();

                    session()->flash('success_message', 'Success Load Coupon');

                    $response = array(
                        'message' => 'Success Load Coupon',
                        'redirect_url' => 'wallet'
                    );
                    return $this->response->json($response);
                }else{
                    session()->flash('error_message', 'Coupon already in used.');

                    $response = array(
                        'message' => 'Coupon already in used.',
                        'redirect_url' => 'wallet'
                    );
                    return $this->response->json($response);
                }
            }else {
                session()->flash('error_message', 'Undefined Code or PIN');

                $response = array(
                    'message' => 'Undefined Code or PIN',
                    'redirect_url' => 'wallet'
                );
                return $this->response->json($response);
            }
        }
    }

    public function buyCoins(Request $request) {
        if ($request->isMethod('post')){
            $pc = PricingCoins::find($request->price_id);

            if($pc->discount_type == 'fixed')
                $price = $pc->price - $pc->discount_value;
            else if($pc->discount_type == 'percent')
                $price = $pc->price - ($pc->discount_value / 100 * $pc->price);

            if($price <= $this->user->getTotalBalance()) {
                // Insert Coins to db
                $coin = new Coin;
                $coin->user_id = $this->user->id;
                $coin->types = $request->type;
                $coin->value = $pc->coins_value;
                $coin->amount = $price;
                $coin->created_at = Carbon::now();
                $coin->updated_at = Carbon::now();
                $coin->save();

                // Insert Payment to db
                $payment = new Pay;
                $payment->user_id = $this->user->id;
                $payment->amount = $price;
                $payment->status = "payed";
                $payment->types = "coins";
                $payment->transaction = "purchased";
                $payment->token = $request->_token;
                $payment->created_at = Carbon::now();
                $payment->updated_at = Carbon::now();
                $payment->save();

                session()->flash('success_message', 'Success purchase');

                $response = array(
                    'request' => $pc->name,
                    'redirect_url' => 'wallet'
                );
                return $this->response->json($response);
            }else {
                session()->flash('error_message', 'Insufficient Funds');

                $response = array(
                    'request' => $pc->name,
                    'redirect_url' => 'wallet'
                );
                return $this->response->json($response);
            }

        }
    }

    public function addWallet(){
        return view('account.user.wallet-add');
    }

    public function addWalletBank(Request $request) {
        if ($request->isMethod('post')){
            try {
                $data = array(
                    'name' => $request->bankName,
                    'sender' => $request->sender,
                    'reference' => $request->reference,
                    'amount' => $request->amount,
                    'type' => 'Bank'
                );

                Mail::send('emails.bank_transfer', $data, function ($message) {
                    $message->from($this->user->email, 'Bank Transfer');
                    $message->to('rodel.hiolen@gmail.com')->subject('Bank Transfer');
                });

                Mail::send('emails.bank_transfer_requested', $data, function ($message) {
                    $message->from('no-reply@email.com', 'Bank Transfer Request');
                    $message->to($this->user->email)->subject('Bank Transfer Request');
                });

                // Insert Wallet to db
                $wallet = new Wallet;
                $wallet->user_id = $this->user->id;
                $wallet->name = $request->bankName;
                $wallet->type = 'bank';
                $wallet->sender = $request->sender;
                $wallet->reference = $request->reference;
                $wallet->amount = $request->amount;
                $wallet->status = 'requested';
                $wallet->created_at = Carbon::now();
                $wallet->updated_at = Carbon::now();
                $wallet->save();

                // Insert Payment to db
                $payment = new Pay;
                $payment->user_id = $this->user->id;
                $payment->amount = $request->amount;
                $payment->status = "unpayed";
                $payment->types = "wallet";
                $payment->transaction = "bank";
                $payment->token = $request->_token;
                $payment->created_at = Carbon::now();
                $payment->updated_at = Carbon::now();
                $payment->save();

                session()->flash('success_message', 'Your bank transfer will be pending for 2 - 3 days');

                $response = array(
                    'redirect_url' => '../wallet'
                );
                return $this->response->json($response);

            }catch (Exception $e) {
                session()->flash('error_message', $e->getMessage());
            }
        }
    }

    public function addWalletRemittance(Request $request) {
        if ($request->isMethod('post')){

            try {
                $data = array(
                    'name' => $request->bankName,
                    'sender' => $request->sender,
                    'reference' => $request->reference,
                    'amount' => $request->amount,
                    'type' => 'Remittance'
                );

                Mail::send('emails.remittance_transfer', $data, function ($message) {
                    $message->from($this->user->email, 'Remittance Transfer');
                    $message->to('rodel.hiolen@gmail.com')->subject('Remittance Transfer');
                });

                Mail::send('emails.remittance_transfer_requested', $data, function ($message) {
                    $message->from('no-reply@email.com', 'Remittance Transfer Request');
                    $message->to($this->user->email)->subject('Remittance Transfer Request');
                });

                // Insert Wallet to db
                $wallet = new Wallet;
                $wallet->user_id = $this->user->id;
                $wallet->name = $request->remittanceName;
                $wallet->type = 'remittance';
                $wallet->sender = $request->sender;
                $wallet->reference = $request->reference;
                $wallet->amount = $request->amount;
                $wallet->status = 'requested';
                $wallet->created_at = Carbon::now();
                $wallet->updated_at = Carbon::now();
                $wallet->save();

                // Insert Payment to db
                $payment = new Pay;
                $payment->user_id = $this->user->id;
                $payment->amount = $request->amount;
                $payment->status = "unpayed";
                $payment->types = "wallet";
                $payment->transaction = "remittance";
                $payment->token = $request->_token;
                $payment->created_at = Carbon::now();
                $payment->updated_at = Carbon::now();
                $payment->save();

                session()->flash('success_message', 'Your remittances will be pending for 2 - 3 days');

                $response = array(
                    'redirect_url' => '../wallet'
                );
                return $this->response->json($response);

            } catch (Exception $e) {
                session()->flash('error_message', $e->getMessage());
            }
        }
    }

    public function addWalletPaypal(Request $request) {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $price = $request->amount; // 10 € for example

        if($price == 0) { // ensure a price above 0
            session()->flash('error_message', 'Error: Please enter a value more than 0.');
            return redirect()->route('account.deedel.walletAdd');
        }

        // Set Item
        $item_1 = new Item();
        $item_1->setName('Add Money to your wallet')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($price);

        // add item to list
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($price); // price of all items together

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Money Transfer');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('account.deedel.addWalletPaypalSuccessful', ['success' => true]))
            ->setCancelUrl(route('account.deedel.addWalletPaypalSuccessful', ['success' => false]));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\config('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                Flash::error('Something went wrong, Sorry for inconvenience');
                return redirect('/');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('amount', $price);

        if(isset($redirect_url)) {
            // redirect to paypal
            return redirect($redirect_url);
        }

        Flash::error('Unknown error occurred');
        return redirect('/');
    }

    public function addWalletPaypalSuccessful(Request $request) {
        try {
            if (isset($request->success) && $request->success) {
                $amount = Session::pull('amount');
                $pay_id = Session::pull('paypal_payment_id');
                $data = array(
                    'link' => '/account'
                );

                Mail::send('emails.money_via_paypal', $data, function ($message) {
                    $message->from('no-reply@deedel.com', 'Deedel Purchased Coins');
                    $message->to($this->user->email)->subject('Deedel Purchased Coins');
                });

                // Insert Wallet to db
                $wallet = new Wallet;
                $wallet->user_id = $this->user->id;
                $wallet->name = 'Paypal';
                $wallet->type = 'paypal';
                $wallet->sender = $this->user->name;
                $wallet->amount = $amount;
                $wallet->status = 'release';
                $wallet->created_at = Carbon::now();
                $wallet->updated_at = Carbon::now();
                $wallet->save();

                // Insert Payment to db
                $payment = new Pay;
                $payment->user_id = $this->user->id;
                $payment->amount = $amount;
                $payment->status = "payed";
                $payment->types = "wallet";
                $payment->transaction = "paypal";
                $payment->token = $request->token;
                $payment->payment_id = $pay_id;
                $payment->created_at = Carbon::now();
                $payment->updated_at = Carbon::now();
                $payment->save();

                // clear the session payment ID
                Session::forget('paypal_payment_id');

                if (empty($request->input('PayerID')) || empty($request->input('token'))) {
                    Flash::error('Payment Failed');
                    return redirect()->route('account.deedel.home');
                }

                session()->flash('success_message', 'Wallet has been added successfully, please check your balance.');
                return redirect()->route('account.deedel.wallet');

            } else {
                session()->flash('error_message', 'User Cancelled the Approval!');
                
                return redirect()->route('account.deedel.walletAdd');
            }
            
        } catch (Exception $e) {
            session()->flash('error_message', $e->getMessage());
        }
    }

    public function addWalletStripe(Request $request) {
        try {
            $data = array(
                'link' => '/account'
            );

            Mail::send('emails.money_via_stripe', $data, function ($message) {
                $message->from('no-reply@deedel.com', 'Deedel Purchased Coins');
                $message->to($this->user->email)->subject('Deedel Purchased Coins');
            });

            // Insert Wallet to db
            $wallet = new Wallet;
            $wallet->user_id = $this->user->id;
            $wallet->name = 'stripe';
            $wallet->type = 'stripe';
            $wallet->sender = $this->user->name;
            $wallet->amount = $request->amount;
            $wallet->status = 'release';
            $wallet->created_at = Carbon::now();
            $wallet->updated_at = Carbon::now();
            $wallet->save();

            // Insert Payment to db
            $payment = new Pay;
            $payment->user_id = $this->user->id;
            $payment->amount = $request->amount;
            $payment->status = "payed";
            $payment->types = "wallet";
            $payment->transaction = "stripe";
            $payment->token = $request->_token;
            $payment->payment_id = $request->stripeToken;
            $payment->created_at = Carbon::now();
            $payment->updated_at = Carbon::now();
            $payment->save();

            session()->flash('success_message', 'Wallet has been added successfully, please check your balance.');

            return redirect()->route('account.deedel.wallet');
        } catch (Exception $e) {
            session()->flash('error_message', $e->getMessage());
        }
    }
    
}