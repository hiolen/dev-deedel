<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

use App\Http\Controllers\Controller;
use App\AllDeedel_User as User;
use App\AllDeedel_Website as Website;
use App\Role;

use DB;

class UserController extends Controller
{
    protected $response;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(10);
        $roles = Role::get();
        return view('users.index',compact('data', 'roles'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }
    public function usersPremium()
    {
        return view('users.premium');
    }

    /**
     * Add a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addUser(Request $request) {
        if ($request->isMethod('post')){
            $user = new User;
            $user->username = $request->username;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
            
            if(!is_null($request->userRoles)){
                foreach ($request->userRoles as $key => $value) {
                    $user->attachRole($value);
                }
            }

            $roles = [];
            if(!empty($user->roles)) {
                foreach($user->roles as $role)
                    $roles[] = $role->display_name;
            }

            $response = array(
                'user' => $user,
                'roles' => $roles
            );

            return $this->response->json($response);
        }
    }

    public function editUser($id){
        $user = User::find($id);

        return $this->response->json($user);
    }

    public function updateUser(Request $request, $id){
        $user = User::find($id);

        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if(!is_null($request->userRoles)){
            DB::table('role_user')->where('user_id',$id)->delete();
            foreach ($request->userRoles as $key => $value) {
                $user->attachRole($value);
            }
        }

        $roles = [];
        if(!empty($user->roles)) {
            foreach($user->roles as $role)
                $roles[] = $role->display_name;
        }
        

        $response = array(
            'user' => $user,
            'roles' => $roles
        );

        return $this->response->json($response);
    }

    public function destroyUser(Request $request) {        
        User::find($request->id)->delete();

        return response ()->json ();
    } 

    public function profile($profile)
    {
        return view('users.profile', compact('profile'));
    }

    public function settings(){
        return view('users.settings');
    }
}
