<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class WebsitesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('website.index');
    }
    public function booklulu(){
        return view('website.booklulu');
    }
    public function trabaho(){
        return view('website.trabaho');
    }
    public function bahay(){
        return view('websites.bahay.index');
    }
}
