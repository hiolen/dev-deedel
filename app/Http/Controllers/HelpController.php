<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\HelpDeedel_HelpCategory;
use App\HelpDeedel_Help;
use App\Http\Requests;

class HelpController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $helpCategory = DB::connection('helpdeedel')->table('helpcategory')->get();
        return view('help.index', compact('helpCategory'));
    }

    public function help(){

        $helpCategory = DB::connection('helpdeedel')->table('helpcategory')->get();
        return view('help.help', compact('helpCategory'));
    }

    public function helpCategory(HelpDeedel_HelpCategory $category){
        
        // dd($category);
        // $art = DB::connection('helpdeedel')->table('helpcategory')->where('slug',$slug )->first();
        // dd($art);
       //  $art = HelpDeedel_HelpCategory::where('slug', $category)->first();
       // dd($art);
        $helpCategory = DB::connection('helpdeedel')->table('helpcategory')->get();
        return view('help.help-category',compact('category','helpCategory'));
    }
    
    public function helpPost(HelpDeedel_HelpCategory $category,HelpDeedel_Help $post){

       
        // $art = HelpDeedel_Help::where('slug', $slug)->first();
        // dd($art);
        // dd($slug);
        return view('help.help-post', compact('post','category'));
    }
}
