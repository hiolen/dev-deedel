<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SupportController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('support.index');
    }
    public function category(){
        return view('support.category');
    }
    public function item(){
        return view('support.item-public');
    }
}
