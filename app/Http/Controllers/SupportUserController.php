<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SupportUserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function ticketAdd(){
        return view('support.ticket-add');
    }
    public function tickets(){
        return view('support.tickets');
    }
    public function ticketItem(){
        return view('support.item-private');
    }
}
