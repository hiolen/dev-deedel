<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class WebsiteTrabahoController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $category = DB::connection('trabahowebsitedeedel')->table('category')->get();
        $skill = DB::connection('trabahowebsitedeedel')->table('skill')->get();
        return view('websites.trabaho.index', compact('category','skill'));
    }
    public function projects(){
        $projects = DB::connection('trabahowebsitedeedel')->table('project')->get();
        return view('websites.trabaho.projects', compact('projects'));
    }
    public function project($project_id){
        $project = DB::connection('trabahowebsitedeedel')->table('project')->where('id', $project_id)->first();
        return view('websites.trabaho.project', compact('project'));
    }
}