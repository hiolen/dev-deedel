<?php

namespace App\Http\Controllers;

use Cart;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

class WishlistController extends Controller
{
	protected $response;

	public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.wishlist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::instance('wishlist')->remove($id);
        return redirect('wishlist')->withSuccessMessage('Item has been removed!');
    }

    /**
     * Remove the resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function emptyWishlist()
    {
        Cart::instance('wishlist')->destroy();
        return redirect('wishlist')->withSuccessMessage('Your wishlist his been cleared!');
    }

     /**
     * Switch item from wishlist to shopping cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToCart($id)
    {
        $item = Cart::instance('wishlist')->get($id);
        Cart::instance('wishlist')->remove($id);

        // if (Cart::instance('cart')->search(function ($cartItem, $rowId) { return $cartItem->id === $rowId; })) {
        //     return redirect('cart')->withSuccessMessage('Item is already in your Wishlist!');
        // }

        Cart::instance('cart')->add(array('id' => $item->rowId, 'name' => $item->name, 'qty' => 1, 'price' => $item->price));
        return redirect('wishlist')->withSuccessMessage('Item has been moved to your shopping cart!');
    }

    public function wishlist(Request $request) {

    	if ($request->isMethod('post')){
            //update/ add new item to cart
            Cart::instance('wishlist')->add(array('id' => $request->_token, 'name' => $request->_name, 'qty' => 1, 'price' => $request->_price, 'option' => array('value' => $request->_value, 'type' => $request->_type)));

            $response = array(
                'token' => $request->_token,
                'message' => 'Redirecting to Paypal',
                'redirect_url' => url('/wishlist')
            );
            return $this->response->json($response);
        }
    }
}
