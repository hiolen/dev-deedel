<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\Http\Requests;

use DB;
use Illuminate\Support\Facades\Input;
use App\BahayWebsiteDeedel_Meta as Meta;
use App\BahayWebsiteDeedel_Property as Property;
use App\AllDeedel_User as User;

class WebsiteBahayController extends Controller
{

    protected $response;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $tagParents = Meta::where('types', 'tag')->where('parent', 0)->get();
        $locParents = Meta::where('types', 'location')->where('parent', 0)->get();

        return view('websites.bahay.index', compact('tagParents', 'locParents'));
    }
    public function locations(){
        // get all locations
        $locations = Meta::where('types', 'location')->get();
        // get location parent
        $locParents = Meta::where('types', 'location')->where('parent', 0)->get();

        return view('websites.bahay.locations', compact('locations', 'locParents'));
    }
    public function tags(){
         // get all tags
        $tags = Meta::where('types', 'tag')->get();
        // get tag parent
        $tagParents = Meta::where('types', 'tag')->where('parent', 0)->get();

        return view('websites.bahay.tags', compact('tags', 'tagParents'));
    }
    public function developers(){
        $developers = User::get();
        return view('websites.bahay.developers', compact('developers'));
    }
    public function developer($name){
        $developer = User::where('name', $name)->first();

        if ($developer)
            return view('websites.bahay.developer', compact('developer'));
        else
            return abort('404');
    }
    public function realtors(){
        $realtors = User::get();
        return view('websites.bahay.realtors', compact('realtors'));
    }
    public function realtor($name){
        $realtor = User::where('name', $name)->first();

        if ($realtor)
            return view('websites.bahay.realtor', compact('realtor'));
        else
            return abort('404');
    }
    public function agents(){
        $agents = User::get();
        return view('websites.bahay.agents', compact('agents'));
    }
    public function agent($name){
        $agent = User::where('name', $name)->first();

        if ($agent)
            return view('websites.bahay.agent', compact('agent'));
        else
            return abort('404');
    }
    public function search(){
        $type = Input::get('pType');
        $price = (int)Input::get('pPrice');
        if($type) {
            $meta = Meta::where('name', $type)->first();
            $properties = $meta->properties;
            $response = array(
                'properties' => $properties,
                'meta' => $meta,
                'message' => 'Added coin type successful!'
            );
            return $this->response->json($response);
        }elseif($price){
            if($price <= 100000){
                $properties = Property::where('price', '>', 100000)->where('price', '>', 0)->get();
            }
            elseif($price <= 200000){
                $properties = Property::where('price', '>', 200000)->get();
            }
            elseif($price <= 300000){
                $properties = Property::where('price', '>', 300000)->get();
            }
            elseif($price <= 400000){
                $properties = Property::where('price', '>', 400000)->get();
            }
            elseif($price <= 500000){
                $properties = Property::where('price', '>', 500000)->get();
            }
            else
                $properties = Property::where('price', '>', 500000)->get();
            
            $response = array(
                'properties' => $properties,
                'message' => 'Added coin type successful!'
            );
            return $this->response->json($response);
        }else {
            $properties = Property::get();
            $metas = Meta::get();
            return view('websites.bahay.search', compact('properties', 'metas'));
        }
        
    }

    public function filter(Request $request) {
        $term = $request->get('term');
        $key = $request->get('key');
        $type = $request->get('type');
        $data = array();

        if($type){
            $metas = Meta::where($key, 'LIKE', '%'.$term.'%')->where('types', $type)->get();

            foreach ($metas as $meta) {
                $data[] = array('value' => $meta->name, 'properties' => $meta->properties);
            }
        }
        else {
            $properties = Property::where($key, 'LIKE', '%'.$term.'%')->get();

            foreach ($properties as $property) {
                $data[] = array('value' => $property->$key, 'id' => $property->id);
            }
        }

        if(count($data)) 
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function searchType(Request $request){
        $query = $request->get('term','');
        
        $metas = Meta::where('name','LIKE','%'.$query.'%')->get();
        
        $data = array();
        foreach ($metas as $meta) {
                $data[] = array('value' => $meta->name, 'id' => $meta->id);
        }
        if(count($data))
             return $data;
        else
            return ['value' => 'No Result Found', 'id' => '', 'query' => $query];
        // $results = array();
    
        // $queries = Meta::where('meta_id', 'LIKE', '%'.$id.'%')->get();

        // foreach ($queries as $query)
        // {
        //     $results[] = [ 'id' => $query->id, 'name' => $query->name ];
        // }
        
        // return $this->response->json($results);
    }

    public function upgrade(){
        return view('websites.bahay.upgrade');
    }
    public function membership(){
        return view('websites.bahay.membership');
    }

    //Static Pages
    public function about(){
        return view('websites.bahay.about');
    }
    public function faq(){
        return view('websites.bahay.faq');
    }
    public function contactus(){
        return view('websites.bahay.contactus');
    }

    public function showCategory($slug) {
        $category = Meta::where('slug', $slug)->where('types', 'category')->first();

        return view('websites.bahay.category-single', compact('category'));
    }

    public function showTag($slug) {
        $tag = Meta::where('slug', $slug)->where('types', 'tag')->first();

        return view('websites.bahay.tag-single', compact('tag'));
    }

    public function showLocation($slug) {
        $locParents = Meta::where('types', 'location')->where('parent', 0)->get();
        $locations = Meta::where('types', 'location')->get();
        $location = Meta::where('slug', $slug)->where('types', 'location')->first();
    return view('websites.bahay.location-single', compact('location', 'locations', 'locParents'));
    }
}