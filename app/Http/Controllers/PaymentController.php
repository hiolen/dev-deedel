<?php

namespace App\Http\Controllers;

use Exception;
use Mail;

use App\AllDeedel_Coin;
use App\AllDeedel_Payment;
use Cart;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect; 
use Illuminate\Contracts\Routing\ResponseFactory;

use Laracasts\Flash\Flash;
use Session;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaymentController extends Controller {
	private $_api_context;
	protected $response;
	protected $user;

	public function __construct(ResponseFactory $response) {
		// setup PayPal api context
		$paypal_conf = config('paypal');
		$this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
		$this->_api_context->setConfig($paypal_conf['settings']);

		// Get the user
		$this->user = Auth::user(); 

		// Ajax Response
		$this->response = $response;
	}

	public function postPaypal(Request $request) {
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');

		$count = 1;
		$stack = array();
		foreach (Cart::content() as $item) {
			$item_1 = new Item();
			$item_1->setName($item->name) // item name
			->setCurrency('USD')
			->setQuantity($item->qty)
			->setPrice($item->price); // unit price

			// push $stack array
			array_push($stack, $item_1);
			
		}

		// add item to list
		$item_list = new ItemList();
		$item_list->setItems($stack);

		$amount = new Amount();
		$amount->setCurrency('USD')
		->setTotal(Cart::total());

		$transaction = new Transaction();
		$transaction->setAmount($amount)
		->setItemList($item_list)
		->setDescription('Your transaction description');

		$redirect_urls = new RedirectUrls();
		$redirect_urls->setReturnUrl(url('pay_via_paypal_successful'))
		->setCancelUrl(url('payment/status'));

		$payment = new Payment();
		$payment->setIntent('Sale')
		->setPayer($payer)
		->setRedirectUrls($redirect_urls)
		->setTransactions(array($transaction));

		try {
			$payment->create($this->_api_context);
		} catch (\PayPal\Exception\PPConnectionException $ex) {
			if (\config('app.debug')) {
				echo "Exception: " . $ex->getMessage() . PHP_EOL;
				$err_data = json_decode($ex->getData(), true);
				exit;
			} else {
				Flash::error('Something went wrong, Sorry for inconvenience');
				return redirect('/');
			}
		}

		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}

		// add payment ID to session
		Session::put('paypal_payment_id', $payment->getId());

		if(isset($redirect_url)) {
			// redirect to paypal
			return redirect($redirect_url);
		}

		Flash::error('Unknown error occurred');
		return redirect('/');
	}

	public function getPaypalSuccessful(Request $request)
	{
		try {
			$data = array(
                'link' => '/account'
            );

			Mail::send('emails.coins_via_paypal', $data, function ($message) {
                $message->from('no-reply@deedel.com', 'Deedel Purchased Coins');
                $message->to($this->user->email)->subject('Deedel Purchased Coins');
            });

			// Get the payment ID before session clear
			$payment_id = Session::get('paypal_payment_id');

			if (sizeof(Cart::content()) > 0):
				foreach (Cart::content() as $item):
					// Insert Coin to db
					$coin = new AllDeedel_Coin;
					$coin->user_id = $this->user->id;
					$coin->types = $item->options[1];
					$coin->value = $item->options[0] * $item->qty;
					$coin->amount = $item->price;
					$coin->created_at = Carbon::now();
					$coin->updated_at = Carbon::now();
					$coin->save();

					// Insert Payment to db
					$payment = new AllDeedel_Payment;
					$payment->user_id = $this->user->id;
					$payment->amount = $item->price * $item->qty;
					$payment->status = "payed";
					$payment->types = $item->options[2];
					$payment->transaction = "paypal";
					$payment->token = $item->id;
					$payment->payment_id = $payment_id;
					$payment->created_at = Carbon::now();
					$payment->updated_at = Carbon::now();
					$payment->save();
				endforeach;
			endif;

			Cart::destroy();

			// clear the session payment ID
			Session::forget('paypal_payment_id');

			if (empty($request->input('PayerID')) || empty($request->input('token'))) {
				Flash::error('Payment Failed');
				return redirect('/');
			}
			
			session()->flash('success_message', 'Payment saved successfully!');
			return redirect('/cart');
		} catch (Exception $e) {
			session()->flash('error_message', $e->getMessage());
		}
	}

	public function postStripe(Request $request) {
		try {
			$data = array(
                'link' => '/account'
            );

			Mail::send('emails.coins_via_stripe', $data, function ($message) {
                $message->from('no-reply@deedel.com', 'Deedel Purchased Coins');
                $message->to($this->user->email)->subject('Deedel Purchased Coins');
            });

			if (sizeof(Cart::content()) > 0):
				foreach (Cart::content() as $item):
					// Insert Coin to db
					$coin = new AllDeedel_Coin;
					$coin->user_id = $this->user->id;
					$coin->types = $item->options[1];
					$coin->value = $item->options[0] * $item->qty;
					$coin->amount = $item->price;
					$coin->created_at = Carbon::now();
					$coin->updated_at = Carbon::now();
					$coin->save();

					// Insert Payment to db
					$payment = new AllDeedel_Payment;
					$payment->user_id = $this->user->id;
					$payment->amount = $item->price * $item->qty;
					$payment->status = "payed";
					$payment->types = $item->options[2];
					$payment->transaction = "stripe";
					$payment->token = $item->id;
					$payment->payment_id = $request->stripeToken;
					$payment->created_at = Carbon::now();
					$payment->updated_at = Carbon::now();
					$payment->save();
				endforeach;
			endif;

			Cart::destroy();
			session()->flash('success_message', 'Payment saved successfully!');

			return redirect('/cart');	
		} catch (Exception $e) {
			session()->flash('error_message', $e->getMessage());	
		}
	}

	// http://itsolutionstuff.com/post/laravel-5-stripe-example-using-laravel-cashier-from-scratchexample.html

}