<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;

use DB;

class RoleController extends Controller
{
    protected $response;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id','DESC')->paginate(5);
        $permissions = Permission::get();
        return view('roles.index',compact('roles', 'permissions'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Add a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addRole(Request $request) {
        if ($request->isMethod('post')){
            $role = new Role();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->save();

            if(!is_null($request->permissions)){
                foreach ($request->permissions as $key => $value) {
                    $role->attachPermission($value);
                }
            }

            $response = array(
                'role' => $role,
                'permissions' => $request->permissions
            );

            return $this->response->json($response);
        }
    }

    public function editRole($id) {
        $role = Role::find($id);

        return $this->response->json($role);
    }

    public function updateRole(Request $request, $id){
        $role = Role::find($id);

        $role->name = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->description = $request->input('description');
        $role->save();

        if(!is_null($request->permissions)){
            DB::table("permission_role")->where("permission_role.role_id",$id)->delete();
            foreach ($request->permissions as $key => $value) {
                $role->attachPermission($value);
            }
        }

        $response = array(
            'role' => $role,
            'permissions' => $role->permissions
        );

        return $this->response->json($response);
    }

    public function destroyRole(Request $request) {        
        // Role::find($request->id)->delete();
        DB::table("roles")->where('id',$request->id)->delete();
        
        return response()->json ($request->id);
    } 
}
