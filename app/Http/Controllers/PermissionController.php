<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

use App\Http\Requests;
use App\Permission;

class PermissionController extends Controller
{
    protected $response;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ResponseFactory $response)
    {
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = Permission::orderBy('id','DESC')->paginate(15);
        return view('permissions.index',compact('permissions'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Add a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addPermission(Request $request) {
        if ($request->isMethod('post')){
            $permission = new Permission();
            $permission->name = $request->input('name');
            $permission->display_name = $request->input('display_name');
            $permission->description = $request->input('description');
            $permission->save();

            $response = array(
                'permission' => $permission,
            );

            return $this->response->json($response);
        }
    }

    public function editPermission($id) {
        $permission = Permission::find($id);

        return $this->response->json($permission);
    }

    public function updatePermission(Request $request, $id){
        $permission = Permission::find($id);

        $permission->name = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->description = $request->input('description');
        $permission->save();

        $response = array(
            'permission' => $permission,
        );

        return $this->response->json($response);
    }

    public function destroyPermission(Request $request) {   
    	if ($request->isMethod('post')){     
	        Permission::find($request->id)->delete();
	        
	        return response()->json($request);
	    }
    } 
}
