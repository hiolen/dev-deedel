<?php

// CRUD Sample: https://github.com/avinashn/Ajax-CRUD-example-in-laravel

// Send Email
// Route::get('sendemail', function () {

//     $data = array(
//         'name' => "Learning Laravel",
//     );

//     Mail::send('emails.welcome', $data, function ($message) {

//         $message->from('rodel.hiolen@gmail.com', 'Learning Laravel');

//         $message->to('rodel.hiolen@gmail.com')->subject('Learning Laravel test email');

//     });
//     return "Your email has been sent successfully";

// });


// Route Auth
Route::get('login', ['as' => 'auth.deedel.login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('login', ['as' => 'auth.deedel.login', 'uses' => 'Auth\AuthController@login']);
Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

// Registration Routes...
Route::get('register', ['as' => 'auth.deedel.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::post('register', ['as' => 'auth.deedel.register', 'uses' => 'Auth\AuthController@register']);

// Password Reset Routes...
Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);

Route::get('user/activation/{token}', [
    'as' => 'auth.user.activate', 'uses' => 'Auth\AuthController@activateUser'
]);

// ----------------------------------------------------------------------------------------------------

//Static Pages
Route::get('/', ['as' => 'static.deedel.index', 'uses' => 'PageController@index']);
Route::get('/network', ['as' => 'static.deedel.network', 'uses' => 'PageController@pageNetwork']);
Route::get('/contact', ['as' => 'static.deedel.contact', 'uses' => 'PageController@contact']);
Route::post('/contact', ['as' => 'static.deedel.contact', 'uses' => 'PageController@sendContact']);
Route::get('/howto', ['as' => 'static.deedel.howto', 'uses' => 'PageController@howto']);
// Route::get('/about', 'WebsiteBahayController@about');
Route::get('/faq', ['as' => 'static.deedel.faq', 'uses' => 'PageController@faq']);
// Route::get('/how-it-works','WebsiteBahayController@howitworks');
// Route::get('/contact-us','WebsiteBahayController@contactus');
Route::get('/privacy', ['as' => 'static.deedel.privacy', 'uses' => 'PageController@privacy']);


// Terms
Route::get('/terms-definition', ['as' => 'static.deedel.termsDefinition', 'uses' => 'PageController@termsDefinition']);
Route::post('/terms-definition/arr', ['as' => 'static.deedel.termsDefinitionJson', 'uses' => 'PageController@termsDefinitionJson']);


Route::get('/partners', 'PageController@partners');

//Careers
Route::group(['prefix' => 'careers'], function () {
    Route::get('','PageController@careers');
});

//Pricing
Route::get('/coins', ['as' => 'pricing.deedel.home', 'uses' => 'PageController@coins']);

//Support Center
//Route::group(array('domain' => 'support.deedel.com'), function(){
Route::group(['prefix' => 'support'], function () {
    Route::get('', ['as' => 'support.deedel.home', 'uses' => 'SupportController@index']);
    Route::get('/category','SupportController@category');
    Route::get('/category/item','SupportController@item');
    Route::get('/ticket/new','SupportUserController@ticketAdd');
    Route::get('/tickets','SupportUserController@tickets');
    Route::get('/ticket', function () {
        return redirect('/support/tickets');
    });
    Route::get('/ticket/t/slug','SupportUserController@ticketItem');
    Route::get('/ticket/t', function () {
        return redirect('/support/tickets');
    });
});

//Super Admin
Route::group(['prefix' => 'account/admin'], function () {
    Route::get('', 'SuperAdminController@index');
    Route::group(['prefix' => 'messages'], function () {
        Route::get('', 'SuperAdminController@messages');
        Route::get('/chat', 'SuperAdminController@messagesChat');
        Route::get('/compose', 'SuperAdminController@messagesCompose');
        Route::get('/inbox', 'SuperAdminController@messagesInbox');
        Route::get('/m/{id}', 'SuperAdminController@messagesItem');
        Route::get('/contacts', 'SuperAdminController@messagesContacts');
        Route::get('/contacts/{id}', 'SuperAdminController@messagesContactsUser');
        Route::get('/reports', 'SuperAdminController@messagesReports');
        Route::group(['prefix' => 'support'], function () {
            Route::get('', 'SuperAdminController@messagesSupport');
            Route::post('', 'SuperAdminController@addSupport');
            Route::get('/ticket', function () {
                return redirect('/account/admin/messages/support');
            });
            Route::get('/ticket/id', 'SuperAdminController@messagesSupportTicketItem');
            Route::get('/ticket/add', 'SuperAdminController@messagesSupportTicketAdd');
        });
    });
    Route::group(['prefix' => 'network'], function () {
        Route::get('', 'SuperAdminController@network');
        Route::get('/add', 'SuperAdminController@networkAdd');
        Route::post('/', 'SuperAdminController@networkSave');
        Route::get('/{id?}', 'SuperAdminController@editNetwork');
        Route::put('/{id?}', 'SuperAdminController@updateNetwork');
        Route::get('/dashboard', 'SuperAdminController@networkDashboard');
        Route::get('/dashboard/{id}', 'SuperAdminController@networkDashboardItem');
        Route::post('/delete', 'SuperAdminController@destroyNetwork');
    });
    Route::group(['prefix' => 'coupons'], function () {
        Route::get('', 'SuperAdminController@coupons');
        Route::get('/add', 'SuperAdminController@couponsAdd');
        Route::get('/upload', 'SuperAdminController@couponsUpload');
        Route::get('download-coupon/{type}', 'SuperAdminController@downloadCoupon');
        Route::post('import-coupon', 'SuperAdminController@importCoupon');
        Route::post('/save', 'SuperAdminController@couponsSave');
    });
    Route::group(['prefix' => 'pricing'], function () {
        Route::get('', 'SuperAdminController@pricing');
        Route::get('/coins-type', 'SuperAdminController@pricingCoinsType');
        
        Route::get('/coins', 'SuperAdminController@pricingCoins');
        Route::get('/coins/add', 'SuperAdminController@pricingCoinsAdd');
        Route::post('/coins/adds', 'SuperAdminController@pricingCoinsAddAuto');
        Route::post('/coin', 'SuperAdminController@pricingCoinsSave');
        Route::get('coin/{id?}', 'SuperAdminController@editPricingCoins');
        Route::put('coin/{id?}', 'SuperAdminController@updatePricingCoins');
        Route::get('/coins/set', 'SuperAdminController@pricingCoinsSet');
        Route::post('/coins/set/save', 'SuperAdminController@pricingCoinsSetSave');
        Route::post('coin/delete', 'SuperAdminController@destroyPricingCoins');

        Route::get('/membership', 'SuperAdminController@pricingMembership');
        // Route::get('/membership/add', 'SuperAdminController@pricingMembershipAdd');
        Route::post('/membership', 'SuperAdminController@pricingMembershipSave');
        Route::get('membership/{id?}', 'SuperAdminController@editMembership');
        Route::put('membership/{id?}', 'SuperAdminController@updateMembership');
        Route::post('membership/delete', 'SuperAdminController@destroyMembership');

        Route::get('activities', 'SuperAdminController@pricingActivities');
        Route::get('/activities/add', 'SuperAdminController@pricingActivitiesAdd');
        Route::post('activities', 'SuperAdminController@pricingActivitiesSave');
        Route::get('activities/{id?}', 'SuperAdminController@editActivities');
        Route::put('activities/{id?}', 'SuperAdminController@updateActivities');
        Route::post('activities/delete', 'SuperAdminController@destroyActivities');
    });
    Route::group(['prefix' => 'rewards'], function () {
        Route::get('', 'SuperAdminController@rewards');
        Route::get('/gifts', 'SuperAdminController@gifts');
        Route::get('/gifts/add', 'SuperAdminController@giftsAdd');
        Route::get('/points', 'SuperAdminController@points');
        Route::get('/pointsAdd', 'SuperAdminController@pointsAdd');
    });
    Route::group(['prefix' => 'content'], function () {
        Route::get('', 'SuperAdminController@content');
        Route::get('/support', 'SuperAdminController@contentSupport');
        Route::get('/support/add', 'SuperAdminController@contentSupportAdd');
        Route::get('/support/category', 'SuperAdminController@contentSupportCategory');
        Route::get('/support/category/add', 'SuperAdminController@contentSupportCategoryAdd');
        Route::get('/terms', 'SuperAdminController@contentTerms');
        Route::get('/terms/add', 'SuperAdminController@contentTermsAdd');
        Route::get('/terms/{id?}', 'SuperAdminController@editTerms');
        Route::put('/terms/{id?}', 'SuperAdminController@updateTerms');
        Route::post('/terms/save', 'SuperAdminController@contentTermsSave');
        Route::post('/terms/delete', 'SuperAdminController@contentDestroyTerms');
        Route::get('/blog', 'SuperAdminController@contentBlog');
        Route::get('/blog/add', 'SuperAdminController@contentBlogAdd');
        Route::get('/blog/category', 'SuperAdminController@contentBlogCategory');
        Route::get('/blog/category/add', 'SuperAdminController@contentBlogCategoryAdd');
        Route::get('/blog/tag', 'SuperAdminController@contentBlogTag');
        Route::get('/blog/tag/add', 'SuperAdminController@contentBlogTagAdd');
        Route::get('/pages', 'SuperAdminController@contentPages');
        Route::post('/pages/arr', 'SuperAdminController@contentPagesJson');
        Route::get('/pages/add', 'SuperAdminController@contentPagesAdd');
        Route::post('/pages/save', 'SuperAdminController@contentPagesSave');
        Route::get('/landing-pages', 'SuperAdminController@contentLandingPages');
        Route::get('/emails', 'SuperAdminController@contentEmails');
        Route::get('/forms', 'SuperAdminController@contentForms');
        Route::get('/panels', 'SuperAdminController@contentPanels');


        Route::get('/forms/add', 'SuperAdminController@contentFormsAdd');
        Route::get('/forms/{id}', 'SuperAdminController@contentFormsItem');

    });

    Route::group(['prefix' => 'members', 'middleware' => ['auth', 'role:super-admin']], function () {
        // Route::resource('users','UserController');
        
        // For Admin
        Route::get('/', 'UserController@getIndex');

        // For User
        Route::get('users', 'UserController@getIndex');
        Route::get('users/premium', 'UserController@usersPremium');
        Route::post('user', 'UserController@addUser');
        Route::get('user/{id?}', 'UserController@editUser');
        Route::put('user/{id?}', 'UserController@updateUser');
        Route::post('user/delete', 'UserController@destroyUser');
        
        // For Role
        Route::get('roles',['as'=>'roles.index','uses'=>'RoleController@index','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
        Route::post('role',['as'=>'roles.store','uses'=>'RoleController@addRole','middleware' => ['permission:role-create']]);
        Route::get('role/{id?}',['as'=>'roles.edit','uses'=>'RoleController@editRole','middleware' => ['permission:role-edit']]);
        Route::put('role/{id?}',['as'=>'roles.update','uses'=>'RoleController@updateRole','middleware' => ['permission:role-edit']]);
        Route::post('role/delete',['as'=>'roles.destroy','uses'=>'RoleController@destroyRole','middleware' => ['permission:role-delete']]);    

        // For Permission
        Route::get('permissions', 'PermissionController@index');
        Route::post('permission', 'PermissionController@addPermission');
        Route::get('permission/{id?}', 'PermissionController@editPermission');
        Route::put('permission/{id?}', 'PermissionController@updatePermission');
        Route::post('permission/delete', 'PermissionController@destroyPermission'); 
    });

});

//Community Forum
//Route::group(array('domain' => 'forum.deedeldigital.com'), function(){
Route::group(['prefix' => 'forum'], function () {
    Route::get('','ForumController@index');
});

//Community Blog
//Route::group(array('domain' => 'blog.deedeldigital.com'), function(){
Route::group(['prefix' => 'blog'], function () {
    Route::get('','BlogController@index');
});

//Accounts
//Route::group(array('domain' => 'account.deedel.com'), function(){
Route::group(['prefix' => '/account'], function () {
    Route::get('', ['as' => 'account.deedel.home', 'uses' => 'AccountController@index']);
    Route::get('/profile', ['as' => 'account.deedel.profile', 'uses' => 'AccountController@profile']);
    Route::get('/settings', ['as' => 'account.deedel.settings', 'uses' => 'AccountController@settings']);
    Route::get('/network', ['as' => 'account.deedel.network', 'uses' => 'AccountController@network']);
    Route::get('/messages', ['as' => 'account.deedel.messages', 'uses' => 'AccountController@messages']);
    Route::get('/wallet', ['as' => 'account.deedel.wallet', 'uses' => 'AccountController@getWallet']);
    Route::post('/wallet/load_coins', ['as' => 'account.deedel.loadCoins', 'uses' => 'AccountController@loadCoins']);
    Route::post('/wallet/buy_coins', ['as' => 'account.deedel.buyCoins', 'uses' => 'AccountController@buyCoins']);
    Route::get('/wallet/add', ['as' => 'account.deedel.walletAdd', 'uses' => 'AccountController@addWallet']);

    Route::post('wallet/add_via_paypal', ['as' => 'account.deedel.addWalletPaypal', 'uses' => 'AccountController@addWalletPaypal']);

    Route::get('/wallet/add_via_paypal_successful', ['as' => 'account.deedel.addWalletPaypalSuccessful', 'uses' => 'AccountController@addWalletPaypalSuccessful']);

    Route::post('wallet/add_via_stripe', ['as' => 'account.deedel.addWalletStripe', 'uses' => 'AccountController@addWalletStripe']);
    Route::post('wallet/add_via_bank', ['as' => 'account.deedel.addWalletBank', 'uses' => 'AccountController@addWalletBank']);
    Route::post('wallet/add_via_remittance', ['as' => 'account.deedel.addWalletRemittance', 'uses' => 'AccountController@addWalletRemittance']);

    Route::get('/assets','AccountController@assets');
    // Route::get('/contents', ['as' => 'account.deedel.addWalletRemittance', 'uses' => 'AccountController@contents']);

    // Blog 
    Route::get('/content/blog', ['as' => 'account.deedel.contentsBlog', 'uses' => 'AccountController@contentsBlog']);

    Route::post('/content/blog/add', ['as' => 'account.deedel.contentsBlogAdd', 'uses' => 'AccountController@contentsBlogAdd']);

    Route::get('/content/blog/edit', ['as' => 'account.deedel.contentsBlogEdit', 'uses' => 'AccountController@contentsBlogEdit']);

    Route::post('/content/blog/update', ['as' => 'account.deedel.contentsBlogUpdate', 'uses' => 'AccountController@contentsBlogUpdate']);    

    Route::post('/content/blog/destroy', ['as' => 'account.deedel.contentsBlogDestroy', 'uses' => 'AccountController@contentsBlogDestroy']);

    Route::post('/content/blog/status', ['as' => 'account.deedel.contentsBlogStatus', 'uses' => 'AccountController@contentsBlogStatus']);

    Route::post('/content/blog/featured', ['as' => 'account.deedel.contentsBlogFeatured', 'uses' => 'AccountController@contentsBlogFeatured']);

    // Route::get('/contents/blog/{id}/posts','AccountController@contentsBlogPosts');
    // Route::get('/contents/blog/{id}/posts/{post_id}','AccountController@contentsBlogPostsItem');
    // Route::get('/contents/blog/{id}/posts/add','AccountController@contentsBlogPostsAdd');
    // Route::get('/contents/blog/{id}/meta','AccountController@contentsBlogMeta');
    // Route::get('/contents/blog/{id}/meta/add','AccountController@contentsBlogMetaAdd');
    // Route::get('/contents/blog/{id}/meta/{meta_id}','AccountController@contentsBlogMetaItem');

    // Page
    Route::get('/content/pages', ['as' => 'account.deedel.page', 'uses' => 'AccountController@contentPages']);

    Route::post('/content/pages/add', ['as' => 'account.deedel.contentPagesAdd', 'uses' => 'AccountController@contentPagesAdd']);

    Route::get('/content/pages/edit', ['as' => 'account.deedel.contentPagesEdit', 'uses' => 'AccountController@contentPagesEdit']);

    Route::post('/content/pages/update', ['as' => 'account.deedel.contentPagesUpdate', 'uses' => 'AccountController@contentPagesUpdate']);

    Route::post('/content/pages/destroy', ['as' => 'account.deedel.contentPagesDestroy', 'uses' => 'AccountController@contentPagesDestroy']);

    Route::post('/content/pages/status', ['as' => 'account.deedel.contentPagesStatus', 'uses' => 'AccountController@contentPagesStatus']);

    Route::post('/content/pages/website', ['as' => 'account.deedel.contentPagesWebsite', 'uses' => 'AccountController@contentPagesWebsite']);

    Route::post('/content/pages/role', ['as' => 'account.deedel.contentPagesRole', 'uses' => 'AccountController@contentPagesRole']);

    Route::post('/content/pages/arr','AccountController@contentsPagesJson');
    
    Route::post('/content/pages/save', 'AccountController@contentsPagesSave');
    Route::get('/content/landing-pages','AccountController@contentsLandingPages');
    Route::get('/content/landing-pages/add','AccountController@contentsLandingPagesAdd');
    Route::get('/content/landing-pages/item/{id}','AccountController@contentsLandingPagesItem');
    Route::get('/content/emails','AccountController@contentsEmails');
    Route::get('/content/forms','AccountController@contentsForms');
    Route::get('/content/panels','AccountController@contentsPanels');
    Route::get('/rewards','AccountController@rewards');
    Route::get('/rewards/points','AccountController@rewardsPoints');
    Route::get('/rewards/gifts','AccountController@rewardsGifts');
    Route::get('/rewards/coins','AccountController@rewardsCoins');
    Route::get('/rewards/tokens','AccountController@rewardsTokens');
    Route::get('/orders','AccountController@orders');
    Route::get('/contests','AccountController@contests');
    Route::get('/ads','AccountController@ads');
    Route::get('/ads/add','AccountController@adsAdd');
    Route::get('/ads/type','AccountController@adsType');
    Route::get('/ads/type/add','AccountController@adsTypeAdd');
    Route::get('/affiliate', 'AccountController@affiliate');
    Route::get('/affiliate/banners', 'AccountController@affiliateBanners');
    Route::get('/affiliate/referrals', 'AccountController@affiliateReferrals');
    Route::get('/affiliate/earnings', 'AccountController@affiliateEarnings');
    Route::get('/premium', 'AccountController@premium');
    Route::get('/premium/network/{slug}', 'AccountController@premiumNetworkMembership');
    Route::get('/premium/network', function () {
        return redirect('/account/premium');
    });
    
    // Route::group(['prefix' => 'admin'], function () {
 //     Route::get('','AccountController@admin');

    //  Route::group(['prefix' => 'admin'], function () {
 //         Route::get('','AccountController@superadmin');

    //      Route::group(['prefix' => 'deedel'], function () {
    //          Route::get('','AccountController@superadmin_deedel');
    //          Route::get('/blog','AccountController@superadmin_deedel_blog');
    //          Route::get('/forum','AccountController@superadmin_deedel_forum');
    //          Route::get('/help','AccountController@superadmin_deedel_help');
    //      });
    //  });
    // });
    
    // Route::group(['prefix' => 'user'], function () {
    //     Route::get('','AccountController@user');
    //     Route::get('/profile','AccountController@profile');
    // });
    
    Route::group(['prefix' => 'staff'], function () {
        Route::get('','AccountController@staff');
        Route::get('/admin','AccountController@staffAdmin');
    });
});


//User
// Route::get('/user', 'UserController@index');
// Route::get('/user/{profile}', 'UserController@profile');
// Route::get('/user/settings', 'UserController@settings');

//Websites
//Route::group(array('domain' => 'trabaho.pw'), function(){
// Route::group(['prefix' => 'website'], function () {
    // Route::group(['prefix' => 'trabaho'], function () {
    //     Route::get('/','WebsiteTrabahoController@index');
    //     Route::get('/projects','WebsiteTrabahoController@projects')->name('trabaho_projects');
    //     Route::get('/project/{project_id}','WebsiteTrabahoController@project');
    //     Route::get('/join', array('as' => 'trabaho.auth.register', 'uses' => 'Auth\AuthController@getRegister'));
    //     Route::post('/join', array('as' => 'trabaho.auth.register', 'uses' => 'Auth\AuthController@postRegister'));
    //     Route::get('/login', array('as' => 'trabaho.auth.login', 'uses' => 'Auth\AuthController@getLogin'));
    //     Route::post('/login', array('as' => 'trabaho.auth.login', 'uses' => 'Auth\AuthController@postLogin'));
    //     Route::get('/logout', array('as' => 'logout', 'uses' => 'Auth\AuthController@getLogout'));
    //     Route::get('/forgot', array('as' => 'forgot', 'uses' => 'Auth\AuthController@getLogin'));
    //     Route::post('/forgot', array('as' => 'forgot', 'uses' => 'Auth\AuthController@postLogin'));
    // });


// });

////Network

//DeeDelHomes
// Route::group(['prefix' => 'network/deedelhomes'], function () {
//     Route::get('/','WebsiteBahayController@index');
//     Route::get('/locations','WebsiteBahayController@locations');
//     Route::get('/category/{slug}','WebsiteBahayController@showCategory');
//     Route::get('/tag/{slug}','WebsiteBahayController@showTag');
//     Route::get('/location/{slug}','WebsiteBahayController@showLocation');
//     Route::get('/tags','WebsiteBahayController@tags');

//     Route::get('/developers','WebsiteBahayController@developers'); // list of developers
//     Route::get('/developer/{name}','WebsiteBahayController@developer'); // developer single page

//     Route::get('/realtors','WebsiteBahayController@realtors'); // list of realtors
//     Route::get('/realtor/{name}','WebsiteBahayController@realtor'); // list of realtors

//     Route::get('/agents','WebsiteBahayController@agents'); // list of agents
//     Route::get('/agent/{name}','WebsiteBahayController@agent'); // agent single page

//     Route::get('/search','WebsiteBahayController@search');
//     Route::get('/filter','WebsiteBahayController@filter');

//     Route::get('search/type', 'WebsiteBahayController@searchType');

//     Route::get('/upgrade','WebsiteBahayController@upgrade');

//     Route::get('/membership','WebsiteBahayController@membership'); 

//     Route::get('/account','WebsiteBahayUserController@account');
//     Route::get('/account/settings','WebsiteBahayUserController@settings');
//     Route::get('/account/profile','WebsiteBahayUserController@profile');
//     Route::get('/account/profile/edit','WebsiteBahayUserController@editProfile');

//     Route::get('/create/property','WebsiteBahayUserController@createProperty');
//     Route::post('/add/property','WebsiteBahayUserController@addProperty');
//     Route::get('/edit/property','WebsiteBahayUserController@editProperty');
//     Route::get('/category/{metaSlug}/{propertySlug}', 'WebsiteBahayUserController@showProperty');

//     //Admin Page
//     Route::group(['prefix' => 'admin'], function () {
//         Route::get('/','WebsiteBahayAdminController@index');  
              
//         Route::get('meta','WebsiteBahayAdminController@meta');
//         Route::post('meta','WebsiteBahayAdminController@addMeta');
//         Route::post('meta/edit','WebsiteBahayAdminController@editMeta');
//         Route::post('meta/delete','WebsiteBahayAdminController@deleteMeta');
//     });

//     //Static Pages
//     Route::get('/about','WebsiteBahayController@about');
//     Route::get('/faq','WebsiteBahayController@faq');
//     Route::get('/how-it-works','WebsiteBahayController@howitworks');
//     Route::get('/contact-us','WebsiteBahayController@contactus');
//     Route::get('/terms','WebsiteBahayController@terms');
//     Route::get('/privacy','WebsiteBahayController@privacy');
// });

// //Booklulu
// Route::group(['prefix' => 'network/booklulu'], function () {
//     Route::get('/','WebsiteBookluluController@index');
// });



//Payments

// Paypal Payment
Route::post('pay_via_paypal', 'PaymentController@postPaypal');
Route::get('pay_via_paypal_successful', 'PaymentController@getPaypalSuccessful');
// Stripe Payment
Route::post('pay_via_stripe', 'PaymentController@postStripe');
Route::get('pay_via_stripe_successful', 'PaymentController@getStripeSuccessful');

//Cart
Route::resource('cart','CartController');
Route::post('cart', 'CartController@cart');
Route::delete('emptyCart', 'CartController@emptyCart');
Route::post('switchToWishlist/{id}', 'CartController@switchToWishlist');

// Wish List
Route::resource('wishlist', 'WishlistController');
Route::post('wishlist', 'WishlistController@wishlist');
Route::delete('emptyWishlist', 'WishlistController@emptyWishlist');
Route::post('switchToCart/{id}', 'WishlistController@switchToCart');

// Social Auth
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

Route::get('/{slug}', 'PageController@pageStatic');
Route::get('/{network_domain}/{membership_slug}/{membership_name}/{page_slug}', 'PageController@pageByUser');
