<?php

namespace App\Repositories;

use App\User;

class BookRepository
{
    /**
     * Get all of the books for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return $user->books()
            ->orderBy('created_at', 'asc')
            ->get();
    }
}