<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSDeedel_Meta extends Model
{
    protected $connection = 'cmsdeedel';
    protected $table = 'meta';
}
