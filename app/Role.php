<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $table = 'roles';
    
    protected $fillable = ['name', 'display_name', 'description'];

    public function user(){
    	return $this->belongsToMany('App\AllDeedel_User', 'Role_User', 'role_id', 'user_id');
    }
    public function pages(){
    	return $this->belongsToMany('App\CMSDeedel_Pages');
    }
}