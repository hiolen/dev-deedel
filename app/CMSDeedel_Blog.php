<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSDeedel_Blog extends Model
{
    protected $connection = 'cmsdeedel';
    protected $table = 'blog';

    public function website(){
    	return $this->belongsTo('App\AllDeedel_Website', 'website_id');
    }
    
    public function post(){
    	return $this->hasMany('App\CMSDeedel_Post', 'blog_id');
    }

    public function user(){
    	return $this->belongsTo('App\AllDeedel_User', 'author');
    }
}
