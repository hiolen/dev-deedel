<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSDeedel_Post extends Model
{
    protected $connection = 'cmsdeedel';
    protected $table = 'post';

    public function blog(){
    	return $this->belongsTo('App\CMSDeedel_Blog');
    }
}
