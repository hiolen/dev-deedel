<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSDeedel_Support extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'cmsdeedel';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'support';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'content'];
}
