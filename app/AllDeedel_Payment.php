<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_Payment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the user that owns the payments.
     */
    public function user()
    {
        return $this->belongsTo('App\AllDeedel_User', 'user_id');
    }
}
