<?php
//CSS
    function icons(){
?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
    }
    function global_css(){
?>
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<?php
	}
	function home_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/home-style.css">
<?php
	}
	function admin_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/admin-style.css">
<?php
	}
	function network_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/network-style.css">
<?php
	}
	function message_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/message-style.css">
<?php
	}
	function support_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/support-style.css">
<?php
	}
	function support_cat_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/support-category-style.css">
<?php
	}
	function support_ticket_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/support-ticket-style.css">
<?php
	}
	function contact_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/contact-style.css">
<?php
	}
	function adminNetwork_style_css(){
?>
	<link rel="stylesheet" href="../../../assets/network/css/admin-network.css">
<?php
	}
	function bahayAdmin_style_css(){
?>
	<link rel="stylesheet" href="../../../assets/network/bahay/css/admin-styles.css">
<?php
	}
	function bahay_style_css(){
?>
	<link rel="stylesheet" href="../../../assets/network/bahay/css/bahay-style.css">
<?php
	}
	function bahayLocation_style_css(){
?>
	<link rel="stylesheet" href="../../../assets/network/bahay/css/location-style.css">
<?php
	}
	function bahayDevelopers_style_css(){
?>
	<link rel="stylesheet" href="../../../assets/network/bahay/css/developers-style.css">
<?php
	}
	function userAffiliate_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/user-affiliate-style.css">
<?php
	}
	function userWallet_style_css(){
?>
	<link rel="stylesheet" href="/assets/css/user-wallet-style.css">
<?php
	}
    

//JS
    function tinymce($ver=0){
?>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<?php
		if($ver==1){
?>
		    <script>
				$(function() {
					$(document).on('focusin', function(e) {
					    if ($(e.target).closest(".mce-window").length) {
					        e.stopImmediatePropagation();
					    }
					});
					var details_editor_config = {
						selector: '.text-editor',
						setup: function (editor) {
					        editor.on('change', function () {
					            tinymce.triggerSave();
					        });
					    },
						menubar: false,
						statusbar: false,
						plugins: [
					      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
					      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
					      'save table contextmenu directionality emoticons template paste textcolor',
					      'code advlist'
					    ],
						a_plugin_option: true,
						toolbar: 'code | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media preview fullpage',
					};

					tinymce.init(details_editor_config);
				});
		    </script>
<?php
		} else if($ver==2){
?>
		    <script>
				$(function() {
					var details_editor_config = {
						selector: '.text-editor-property',
						setup: function (editor) {
					        editor.on('change', function () {
					            tinymce.triggerSave();
					        });
					    },
						menubar: false,
						  setup: function(editor) {
						    editor.addButton('mylinks', {
						      type: 'menubutton',
						      text: 'My Links',
						      icon: false,
						      menu: [{
						        text: 'Link Name',
						        onclick: function() {
						          editor.insertContent('<a href="#link">Link 2</a>');
						        }
						      }, {
						        text: 'Link Name',
						        onclick: function() {
						          editor.insertContent('<a href="#link">Link 1</a>');
						        }
						      }]
						    });
						    editor.addButton('myfiles', {
						      type: 'menubutton',
						      text: 'My Files',
						      icon: false,
						      menu: [{
						        text: 'File Name',
						        onclick: function() {
						          editor.insertContent('<a href="#link">Link 2</a>');
						        }
						      }, {
						        text: 'File Name',
						        onclick: function() {
						          editor.insertContent('<a href="#link">Link 1</a>');
						        }
						      }]
						    });
						    editor.addButton('myembed', {
						      type: 'menubutton',
						      text: 'My Embed',
						      icon: false,
						      menu: [{
						        text: 'Embed Name',
						        onclick: function() {
						          editor.insertContent('<a href="#link">Link 2</a>');
						        }
						      }, {
						        text: 'Embed Name',
						        onclick: function() {
						          editor.insertContent('[{{ Embed code here }}]');
						        }
						      }]
						    });
						  },
						statusbar: false,
						plugins: [
					      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
					      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
					      'save table contextmenu directionality emoticons template paste textcolor',
					      'code advlist'
					    ],
						a_plugin_option: true,
						toolbar: 'code | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | mylinks myfiles myembed',
					};

					tinymce.init(details_editor_config);
				});
		    </script>
<?php
		}
    }
    function global_js(){
?>
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/react.min.js"></script>
	<script src="assets/js/react-dom.min.js"></script>
	<script src="assets/js/browser.min.js"></script>
<?php
	}
?>


