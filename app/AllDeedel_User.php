<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use App\AllDeedel_Coin as Coins;
use Auth;

use Zizaco\Entrust\Traits\EntrustUserTrait;

class AllDeedel_User extends Authenticatable
{    
    use Billable;
    use EntrustUserTrait;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'alldeedel';
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password', 'websites', 'activated',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the coins for the user.
     */
    public function coins()
    {
        return $this->hasMany('App\AllDeedel_Coin', 'user_id');
    }

    /**
     * Get the websites for the user.
     */
    public function websites()
    {
        return $this->hasMany('App\AllDeedel_Website', 'user_id');
    }

    /**
     * Get the payments for the user.
     */
    public function payments()
    {
        return $this->hasMany('App\AllDeedel_Payment', 'user_id');
    }

    /**
     * Get the wallets for the user.
     */
    public function wallets()
    {
        return $this->hasMany('App\AllDeedel_Wallet', 'user_id');
    }

    /**
     * Get the coin type for the user.
     */
    public function coinType($type) {
        return Coins::where('types', $type)->sum('value');                    
    }   

    public function pages(){
        return $this->hasMany('App\CMSDeedel_Pages', 'author');
    }

    public function blogs(){
        return $this->hasMany('App\CMSDeedel_Blog', 'author');
    }

    public function roles(){
        return $this->belongsToMany('App\Role', 'Role_User', 'user_id', 'role_id');
    }

    public function getWallet() {
        return Auth::user()->wallets->where('status', 'release')->sum('amount');
    }

    public function getPurchased() {
        return Auth::user()->payments->where('transaction', 'purchased')->sum('amount');
    }

    public function getTotalBalance() {
        return Auth::user()->getWallet() - Auth::user()->getPurchased();
    }
}
