<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_PricingCoins extends Model
{
    //
    protected $table = 'pricing_coins';

    public function coinsType(){
    	return $this->belongsTo('App\AllDeedel_CoinsType', 'coins_type_id');
    }
}
