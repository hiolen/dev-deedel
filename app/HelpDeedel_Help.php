<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpDeedel_Help extends Model
{
    protected $connection = 'helpdeedel';
	protected $table = 'help';
	
    public function helpcategories(){
    	return $this->belongsTo(HelpDeedel_HelpCategory::class,'helpCategoryID');
    }
}
