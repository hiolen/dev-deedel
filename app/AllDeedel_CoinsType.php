<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_CoinsType extends Model
{
    protected $table = 'coins_type'; 
    protected $fillable = ['name', 'desc', 'price'];

    public function pricingCoins(){
    	return $this->hasMany('App\AllDeedel_PricingCoins', 'coins_type_id');
    }
    public function pricingActivities(){
    	return $this->hasMany('App\AllDeedel_PricingActivities', 'coins_type_id');
    }
    public function coupon(){
    	return $this->hasMany('App\MarketingDeedel_Coupon', 'value');
    }
}
