<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllDeedel_Coin extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coins';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['value', 'types', 'user_id', 'amount'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'token'
    ];

    /**
     * Get the coin that owns the user.
     */
    public function user()
    {
        return $this->belongsTo('App\AllDeedel_User', 'user_id');
    }
}
