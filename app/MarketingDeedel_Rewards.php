<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingDeedel_Rewards extends Model
{
    //
    protected $connection = "marketingdeedel";
    protected $table = "rewards";
}
