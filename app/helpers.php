<?php
include 'helper_assets.php';
include 'helper_assets_account.php';
include 'helper_assets_admin.php';
include 'helper_assets_deedel.php';
include 'helper_urls.php';
include 'helper_forms.php';
include 'helper_messages.php';

    function coupon_code($coupon_code){
        return $coupon_code;
    }
    function free_coins($total_coins,$discount){
        return $total_coins * ($discount / 100);
    }
    function compute_coins($total_coins,$value){
        return $total_coins * $value;
    }
    function compute_green_coin_value($amount=968.88,$coins=10000,$value_php=40){
        return round(($amount / $coins) * $value_php,2);
    }
    function compute_pink_coin_value($amount=58.88,$coins=50,$value_php=45){
        return round(($amount / $coins) * $value_php,2);
    }

    //DeeDel
    function get_website($website_id,$type){
        $website = App\AllDeedel_Website::find($website_id);
    ?> 
        <div class="thumbnail">
            <h4><a href="<?php echo websites_url(); ?>/<?php echo $website->slug; ?>"><?php echo $website->name; ?></a></h4>
            <p><a href="#">Website image</a></p>
            <p><a href="<?php echo $website->type; ?>://www.<?php echo $website->domain; ?>" target="_blank">www.<?php echo $website->domain; ?></a></p>
        </div>
    <?php
    }
    
    function get_pricing_membership($membership_id){
        $membership = App\AllDeedel_PricingMembership::find($membership_id);
    ?> 
        <div class="pricing-item">
            <form>
                <h2><?php echo $membership->name; ?></h2>
                <input type="hidden" name="membershipType" value="<?php echo $membership->pricing_type; ?>">
                <input type="hidden" name="membershipID" value="<?php echo $membership->id; ?>">
                <div>
                    <?php echo $membership->shortdesc; ?>
                </div>
                <p>From <span class="original-price"><?php echo $membership->price; ?></span>
                    to <?php echo $membership->price-$membership->discount; ?>
                </p>
                <p><a href="<?php echo $membership->slug; ?>">Read More</a></p>
                <p><?php echo $membership->pricing_type; ?></p>
                <p><button type="submit" class="btn btn-primary">Upgrade Now</button></p>
            </form>
        </div>
    <?php
    }

?>