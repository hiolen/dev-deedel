<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahayWebsiteDeedel_Meta extends Model
{
	/**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'bahaywebsitedeedel';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;    
	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'desc', 'types'];

    /**
     * Get the properties for the meta.
     */
    public function properties()
    {
        return $this->hasMany('App\BahayWebsiteDeedel_Property', 'meta_id');
    }

}
